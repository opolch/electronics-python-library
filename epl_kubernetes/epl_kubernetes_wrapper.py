from kubernetes import client
from enum import Enum, auto


class KubernetesInteraction:
    """
    Class providing functionality to interact with Kubernetes.
    """

    class PodDictKeys(Enum):
        """
        Keys to map the PodDict returned by various functions (list_pods)
        """
        pod_name = auto()
        pod_state = auto()
        pod_last_state = auto()
        image = auto()
        host_ip = auto()
        pod_ip = auto()
        start_time = auto()
        container_name = auto()
        node_name = auto()

    class ServiceDictKeys(Enum):
        """
        Keys to map the ServiceDict returned by various functions (list_services)
        """
        ports = "ports"

    class DictEntryState(Enum):
        """
        Enum to define the error state of a pod dict entry.
        """
        NOMINAL = 1
        WARN = 2
        ERROR = 3

    class DictEntry:
        """
        Class to define members for dict entries
        """

        def __init__(self, entry_value, entry_state):
            """
            Initializing the dict entry.
            :param entry_value: Value
            :param entry_state: State
            """
            self.entry_value = entry_value
            self.entry_state = entry_state

    def __init__(self, host, token, logger, prefix=None):
        """
        Inits the class.
        :param host: URL to connect to host:port.
        :param token: Token to be used with authorization.
        :param logger: The epl_logging_log.Log object for logging.
        :param prefix: Prefix for token (eg `Bearer`).
        """
        client_config = client.Configuration()
        client_config.host = host
        client_config.verify_ssl = False
        client_config.api_key = {"authorization": "{} {}".format(prefix, token) if prefix is not None else token}
        self._api_client = client.ApiClient(client_config)
        self._logger = logger

    def list_pods_dict(self, namespaces=list()):
        """
        Lists all the pods for given/all namespaces.
        :param namespaces: List of namespaces to be queried (empty list for all).
        :return:
        """

        def add_pod_to_dict(pod_to_add):
            """
            Adds pod details to dict.
            :param pod_to_add: the pod object.
            :return:
            """
            # Pod name based on container name
            _pod_name = pod_to_add.spec.containers[0].name

            # State
            if pod_to_add.status.container_statuses[0].state.terminated is not None:
                pod_state = self.DictEntry("terminated, {}".format(
                    pod_to_add.status.container_statuses[0].state.terminated.started_at),
                    self.DictEntryState.ERROR.value)
            elif pod_to_add.status.container_statuses[0].state.waiting is not None:
                pod_state = self.DictEntry("waiting, {}".format(
                    pod_to_add.status.container_statuses[0].state.waiting.message),
                    self.DictEntryState.WARN.value)
            elif pod_to_add.status.container_statuses[0].state.running is not None:
                pod_state = self.DictEntry("running, {}".format(
                    pod_to_add.status.container_statuses[0].state.running.started_at),
                    self.DictEntryState.NOMINAL.value)
            else:
                pod_state = self.DictEntry("unknown", self.DictEntryState.ERROR.value)

            # Last-State
            if pod_to_add.status.container_statuses[0].last_state.terminated is not None:
                pod_last_state = self.DictEntry("terminated, started:{}, finished:{}".format(
                    pod_to_add.status.container_statuses[0].last_state.terminated.started_at,
                    pod_to_add.status.container_statuses[0].last_state.terminated.finished_at),
                    self.DictEntryState.NOMINAL.value)
            elif pod_to_add.status.container_statuses[0].last_state.waiting is not None:
                pod_last_state = self.DictEntry("waiting, started:{}, finished:{}".format(
                    pod_to_add.status.container_statuses[0].last_state.waiting.started_at,
                    pod_to_add.status.container_statuses[0].last_state.terminated.finished_at),
                    self.DictEntryState.NOMINAL.value)
            elif pod_to_add.status.container_statuses[0].last_state.running is not None:
                pod_last_state = self.DictEntry("running, started:{}, finished:{}".format(
                    pod_to_add.status.container_statuses[0].last_state.running.started_at,
                    pod_to_add.status.container_statuses[0].last_state.terminated.finished_at),
                    self.DictEntryState.NOMINAL.value)
            else:
                pod_last_state = self.DictEntry("unknown", self.DictEntryState.NOMINAL.value)

            # Write values to dict
            _new_pod = {
                self.PodDictKeys.pod_state.name: pod_state,
                self.PodDictKeys.pod_last_state.name: pod_last_state,
                self.PodDictKeys.pod_name.name:
                    self.DictEntry("{}".format(pod_to_add.metadata.name),
                                   self.DictEntryState.NOMINAL.value),
                self.PodDictKeys.image.name:
                    self.DictEntry("{}".format(pod_to_add.status.container_statuses[0].image),
                                   self.DictEntryState.NOMINAL.value),
                self.PodDictKeys.host_ip.name:
                    self.DictEntry("{}".format(pod_to_add.status.host_ip),
                                   self.DictEntryState.NOMINAL.value),
                self.PodDictKeys.pod_ip.name:
                    self.DictEntry("{}".format(pod_to_add.status.pod_ip),
                                   self.DictEntryState.NOMINAL.value),
                self.PodDictKeys.start_time.name:
                    self.DictEntry("{}".format(pod_to_add.status.start_time),
                                   self.DictEntryState.NOMINAL.value),
                self.PodDictKeys.container_name.name:
                    self.DictEntry("{}".format(pod_to_add.spec.containers[0].name),
                                   self.DictEntryState.NOMINAL.value),
                self.PodDictKeys.node_name.name:
                    self.DictEntry("{}".format(pod_to_add.spec.node_name),
                                   self.DictEntryState.NOMINAL.value)
            }

            # Add pod to pods dict (make sure we have list)
            if _pod_name not in pods_dict:
                pods_dict[_pod_name] = list()
            pods_dict[_pod_name].append(_new_pod)

        api_instance = client.CoreV1Api(self._api_client)
        pods_dict = {}

        # All namespaces?
        if len(namespaces) == 0:
            pods = api_instance.list_pod_for_all_namespaces(watch=False)

            # Iterate over pod items
            for pod in pods.items:
                add_pod_to_dict(pod)
        else:
            for namespace in namespaces:
                pods = api_instance.list_namespaced_pod(namespace=namespace, watch=False)

                # Iterate over pod items
                for pod in pods.items:
                    add_pod_to_dict(pod)

        return pods_dict

    def list_services(self, namespaces=list()):
        """
        Lists all the services for given/all namespaces.
        :param namespaces: List of namespaces to be queried (empty list for all).
        :return:
        """

        def add_service_to_dict(service_to_add):
            """
            Adds service details to dict.
            :param service_to_add: the service object.
            :return:
            """

            # Write values to dict
            services_dict[service_to_add.metadata.name] = {
                self.ServiceDictKeys.ports.name:
                    self.DictEntry(" ; ".join(["int:{}|ext:{}".format(port.name, port.node_port)
                                               for port in service_to_add.spec.ports]),
                                   self.DictEntryState.NOMINAL.value),
            }

        api_instance = client.CoreV1Api(self._api_client)
        services_dict = {}

        # All namespaces?
        if len(namespaces) == 0:
            services = api_instance.list_service_for_all_namespaces(watch=False)

            # Iterate over service items
            for service in services.items:
                add_service_to_dict(service)
        else:
            for namespace in namespaces:
                services = api_instance.list_namespaced_service(namespace=namespace, watch=False)

                # Iterate over pod items
                for service in services.items:
                    add_service_to_dict(service)

        return services_dict
