import katcp
from katcp import Sensor
from epl.epl_katcp.epl_katcp_sensor import KatcpSensor
from epl.epl_katcp.epl_katcp_command import KatcpCommand, CommandState
from epl.epl_katcp import epl_katcp_helper
from epl.epl_string import epl_string_split
from epl.epl_katcp.epl_katcp_helper import CommandLogInform
from threading import Thread
import time
from epl.epl_logging.epl_logging_log import Log
from enum import Enum, auto
from queue import Queue


class KatcpClient(katcp.DeviceClient):
    """
    Class derived from base class katcp.DeviceClient to provide common katcp
    client functionality and adapt it to our needs.
    """

    class DeviceType(Enum):
        """
        Enum for device type parameter
        """
        GENERIC_KATCP = auto()
        PACKETIZER = auto()

    class DeviceStates(Enum):
        """
        Enum to define device states handled in device states queue.
        """
        UNDEFINED = auto()
        UNINITIALIZED = auto()
        SENSOR_LIST_INITIALIZING = auto()
        SENSOR_LIST_INITIALIZED = auto()
        COMMAND_LIST_INITIALIZING = auto()
        COMMAND_LIST_INITIALIZED = auto()
        INITIALIZED = auto()
        ERROR_INIT = auto()

    class MessageHandlers:
        """
        Class to collect default message handles.
        Needed to go with separate class to let devices add/delete fom that
        list based on their needs and not corrupt lists for other devices.
        """

        def __init__(self):
            # Dict with message handle callbacks
            self.MESSAGE_HANDLES = {
                'help': (KatcpClient.handle_help_reply,
                         KatcpClient.handle_help_inform),

                'sensor-list': (KatcpClient.handle_sensor_list_reply,
                                KatcpClient.handle_sensor_list_inform),

                'sensor-status': (KatcpClient.handle_sensor_status_reply,
                                  KatcpClient.handle_sensor_status_inform),

                'log': (KatcpClient.handle_log_reply, KatcpClient.handle_log_inform),

                'sensor-sampling': (KatcpClient.handle_sensor_sampling_reply,
                                    KatcpClient.handle_sensor_sampling_inform),

                'sensor-value': (KatcpClient.handle_sensor_value_reply,
                                 KatcpClient.handle_sensor_value_inform)
            }

    def __init__(self,
                 device_host,
                 device_port,
                 device_name,
                 katcp_root_name='',
                 connect_timeout=2,
                 init_timeout=10,
                 update_sensors_dict=dict(),
                 command_list=list(),
                 auto_reconnect=False,
                 sensor_period_default=2,
                 logger=None,
                 device_type=DeviceType.GENERIC_KATCP,
                 message_handles=None,
                 description_cleanup_handle=None,
                 sensor_hist_depth=256,
                 log_limit="fatal"
                 ):
        """
        Init the object.
        :param device_host: Katcp server's ip address/hostname number.
        :param device_port: Katcp server's port number.
        :param device_name: Unique device name.
        :param katcp_root_name: Root name of the katcp devices eg. `rxs`
        :param connect_timeout: Timeout for connection process.
        :param init_timeout: Timeout for init process.
        :param update_sensors_dict: Dict with sensors (katcp_name:sampling_strategy:params) to be fetched.
            Pass empty dict or nothing for ALL_SENSORS.
        :param command_list: List with commands to make available.
            Pass empty list or nothing for ALL_COMMANDS.
        :param auto_reconnect: Auto reconnect the server after connection loss.
        :param sensor_period_default: Interval for sensor update strategy when
            in update_sensors_all-mode (update_sensors_dict is empty).
        :param logger: epl_logging_log.Log logger.
        :param update_sensors_dict: Sensor dict along with sensor name and
            their sensor sampling strategies + params
        :param message_handles: dict holding message handles to override or extend the existing ones.
        :param description_cleanup_handle: Function to clean up the sensor/command descriptions
        :param sensor_hist_depth: Depth of sensor history.
        :param log_limit: Defining the logging level of the server [info|warning|error|fatal].
        """
        # Init the logger
        if logger is None:
            self._log_writer = Log()
        else:
            self._log_writer = logger

        self.device_name = device_name
        self.device_type = device_type
        self._katcp_root_name = katcp_root_name
        self._device_host = device_host
        self._device_port = device_port
        self._connect_timeout = connect_timeout
        self._init_timeout = init_timeout
        self._refresh_interval = sensor_period_default
        self.update_sensors_dict = update_sensors_dict
        self._description_cleanup_handle = description_cleanup_handle
        self._sensor_hist_depth = sensor_hist_depth
        self._thread_was_started = False
        self._log_limit = log_limit

        # Init functions
        self._init_funcs = [self._init_logging, self.init_sensor_list, self.init_command_list,
                            self.init_sensor_sampling, self.init_sensor_values]

        # Flag indicating the successful sensor-/command-list initializing
        self._state_queue = Queue(1)
        self._state_queue.put(self.DeviceStates.UNDEFINED)

        # The client's sensor map.
        self.SENSOR_MAP = dict()

        # If dict is empty, we fetch all the sensors
        self._update_sensors_all = True
        if len(update_sensors_dict) > 0:
            self._update_sensors_all = False

        # The clients command map. If list was empty, all commands will be activated
        self._commands_all = True
        if len(command_list) > 0:
            self.COMMAND_MAP = dict.fromkeys(command_list)
            self._commands_all = False
        else:
            self.COMMAND_MAP = dict()

        # Init the parent class
        super().__init__(device_host,
                         device_port,
                         auto_reconnect=auto_reconnect,
                         logger=self._log_writer.logger)

        # Create message handles
        # Must come from a class and not global list since some devices
        # will add to this list. When global, all devices have them.
        mh = KatcpClient.MessageHandlers()
        self._MESSAGE_HANDLES = mh.MESSAGE_HANDLES

        # Handles to be added or overwritten?
        if message_handles is not None:
            self._MESSAGE_HANDLES.update(message_handles)

        # Thread for the init_sequence
        self._init_thread = None

    def device_state_set(self, state):
        """
        Sets the device state in the device state queue.
        :return:
        """
        if self._state_queue.full():
            self._state_queue.get()
        self._state_queue.put(state)

    def device_state_get(self, keep):
        """
        Returns the actual device state from the device state queue.
        :return: None for no
        """
        state = None
        if self._state_queue.empty() is not True:
            state = self._state_queue.get()
            if keep:
                self.device_state_set(state)
        return state

    def init_connect(self):
        """
        Tries to connect the client to the katcp server and initializes
        everything.

        :return: TODO: Proper return values
        """

        # First check whether client is already initializing
        if self._init_thread is not None and self._init_thread.is_alive() is True:
            self._log_writer.write_debug("Client {}: currently initializing, be patient...".format(self.device_name))
            return

        # Start the client (only if not running)
        if not self.start():

            self._log_writer.write_info("Client {}: was already running -> re-starting.".format(self.device_name))

            # If thread is for some reason not running, restart
            if self.running() is False:
                try:
                    # Make sure stopped
                    self.stop()
                finally:
                    try:
                        # Start
                        self.start()
                    except:
                        self._log_writer.write_error("Client {}: Restarting of client thread failed.".
                                                     format(self.device_name))

        self._log_writer.write_info("Client {}: Connecting to port {}:{}.".
                                    format(self.device_name,
                                           self._device_host,
                                           self._device_port))

        # Wait for the client to be connected
        self.wait_connected(self._connect_timeout)

        # Check if client was successfully connected
        if self.is_connected() is False:
            self._log_writer.write_warning('Client {}: No successful connection within timeout period.'.
                                           format(self.device_name))
            return False

        return True

    def send_request(self, request_string):
        """
        Creates a katcp request message and adds a callback to send it.
        :param request_string: Message string.
        :return:
        """

        # katcp parser for message parsing
        _katcp_parser = katcp.MessageParser()

        msg = _katcp_parser.parse(request_string.encode('utf-8'))
        self.ioloop.add_callback(self.send_message, msg)

    def _init_thread_setup(self, re_init=False):
        """
        Sets up the init thread which guides the client through the init process.
        :return:
        """
        self._init_thread = Thread(target=self._init_process)
        self._init_thread.start()

    def _init_process(self):
        """
        Actual init process, calling all the init functions and handling their returns.
        :return:
        """

        # Set device state
        self.device_state_set(self.DeviceStates.UNINITIALIZED)

        # Clear sensor MAP
        self.SENSOR_MAP.clear()
        self.COMMAND_MAP.clear()

        # Iterate over init funcs
        for init_func in self._init_funcs:
            if init_func(self._init_timeout) is not True:

                # Force the client to disconnect, to make it reconnect and retry the init
                self.disconnect()
                self._log_writer.write_error('Client {}: Init was not successful, disconnecting to let it re-connect'
                                             ' -> re-init (failed on {}).'.
                                             format(self.device_name, init_func.__name__))
                time.sleep(1)
                return False

        self.device_state_set(self.DeviceStates.INITIALIZED)
        self._log_writer.write_info('Client {}: Init successful.'.format(self.device_name))

    def _init_logging(self, timeout_s=0.0):
        """
        Set the logging level for the katcp server.
        :param timeout_s: Not used, just for consistency to be able to loop through init_funcs.
        :return: True
        """

        # Update sensor list
        self.send_request("?log-limit {}".format(self._log_limit))

        # Successfully initialized
        return True

    def init_sensor_list(self, timeout_s=10.0):
        """
        Routine to init the sensor list.
        :param timeout_s: Max timeout for all the sens to be initialized.
        :return: True-> Success.
        """

        # Set device state
        self.device_state_set(self.DeviceStates.SENSOR_LIST_INITIALIZING)

        # Update sensor list
        self.send_request("?sensor-list")

        # Wait for the sensor-list to be initialized
        wait_initialized = 0.0
        while True:
            if self.device_state_get(keep=True) == self.DeviceStates.SENSOR_LIST_INITIALIZED:
                break
            time.sleep(0.1)
            wait_initialized += 0.1
            if wait_initialized > timeout_s:
                self._log_writer.write_error('Client {}: Sensor-List not initialized.'.
                                             format(self.device_name))
                return False

        # Successfully initialized
        return True

    def init_sensor_sampling(self, timeout_s=10.0, check_timeout=True):
        """
        Sets the sensor-sampling strategy and verifies the replies.
        :param timeout_s: Max timeout for all the sens to be initialized.
        :param check_timeout: Use timeout functionality.
        :return: True->Success.
        """
        # Set sensor-sampling strategy
        for sensor_name, katcp_sensor in self.SENSOR_MAP.items():
            katcp_sensor.set_initialized(False)

            if self._update_sensors_all is True:
                self.send_request("?sensor-sampling {} period {}".format(sensor_name, self._refresh_interval))
            else:
                for update_sensor, values in self.update_sensors_dict.items():
                    if update_sensor in sensor_name:
                        # If all fine, send sampling strategy request
                        req = "?sensor-sampling {} {} {}". \
                            format(sensor_name, katcp_sensor.sampling_strategy.name.replace('_', '-'),
                                   ' '.join(katcp_sensor.sampling_strategy_params))

                        self.send_request(req)
                        break

        # In some modes it makes sense to not check the timeout
        # eg. reconnect
        if check_timeout is True:
            # Wait for sensors to be initialized
            wait_initialized = 0.0
            while True:
                num_success_init = 0
                for sensor_name, katcp_sensor in self.SENSOR_MAP.items():
                    if katcp_sensor.is_initialized() is False:
                        break
                    num_success_init += 1

                if num_success_init == len(self.SENSOR_MAP):
                    return True

                time.sleep(0.1)
                wait_initialized += 0.1
                if wait_initialized > timeout_s:
                    self._log_writer.write_error('Client {}: Sensor-Sampling not initialized.'.
                                                 format(self.device_name))
                    return False

        return True

    def init_command_list(self, timeout_s=10.0):
        """
        Routine to init the command list.
        :param timeout_s: Max timeout for all the sens to be initialized.
        :return: True-> Success.
        """

        # Set device state
        self.device_state_set(self.DeviceStates.COMMAND_LIST_INITIALIZING)

        # Update command list
        self.send_request("?help")

        # Wait for the command-list to be initialized
        wait_initialized = 0.0
        while True:
            # if self._command_list_initialized is True:
            if self.device_state_get(keep=True) == self.DeviceStates.COMMAND_LIST_INITIALIZED:
                break
            time.sleep(0.1)
            wait_initialized += 0.1
            if wait_initialized > timeout_s:
                self._log_writer.write_error('Client {}: Command-List not initialized.'.
                                             format(self.device_name))
                return False

        # Successfully initialized
        return True

    def init_sensor_values(self, timeout_s=0):
        """
        Fetches the current values of the KATCP sensors if the strategy is
        differential or event based.
        :param timeout_s: Not used, just for consistency to be able to loop through init_funcs.
        :return: True
        """
        if self._update_sensors_all is True:
            # If in update_all mode, all sensors are set to periodic.
            pass
        else:
            for sensor_name, katcp_sensor in self.SENSOR_MAP.items():
                try:
                    if (katcp_sensor.sampling_strategy.name in
                            KatcpSensor.SamplingStrategy.differential_rate.name or
                            katcp_sensor.sampling_strategy.name in
                            KatcpSensor.SamplingStrategy.event_rate.name):
                        self.send_request('?sensor-value {} '.format(sensor_name))
                except Exception as ex:
                    self._log_writer.write_warning('init_sensor_values: Sensor {} could not be initialized, ex: {}'.
                                                    format(sensor_name, ex))

        return True

    def handle_sensor_status_reply(self, message):
        """
        Sensor status reply handler.
        :param message: Katcp message object.
        :return:
        """
        return

    def handle_sensor_status_inform(self, message):
        """
        Updates values/adds values (if value-list is activated) in the
        SENSOR_MAP.

        :param message: Katcp message object.
        :return:
        """

        # Convert katcp message object to string
        message_string = str(message)

        # Split message fields
        fields = message_string.split()
        self.SENSOR_MAP[fields[3]].set_value(fields[5],
                                             KatcpSensor.SensorStatus
                                             [fields[4]],
                                             fields[1])
        return

    def handle_sensor_sampling_reply(self, message):
        """
        sensor-sampling reply handler. Used to check if all sampling strategy
        of all sensors have been set.

        :param message: Katcp message object.
        :return:
        """
        try:
            # Extract the katcp command
            sampling_reply = epl_katcp_helper.extract_reply_general(message.name, message.arguments)
            self.SENSOR_MAP[sampling_reply.params[0].decode('utf8')].set_initialized(True)
        except IndexError:
            self._log_writer.write_error('Client {}: Sampling-Strategy for some sensor (sensor-name->see previous logs)'
                                         ' could not be set.'.format(self.device_name))
        return

    def handle_sensor_sampling_inform(self, message):
        """
        sensor-sampling inform handler. Currently not used.
        :param message: Katcp message object.
        :return:
        """
        return

    def handle_sensor_list_reply(self, message):
        """
        After sensor-list was finished, remove un-supported entries from
        SENSOR_MAP.

        :param: Katcp message object.
        :return:
        """
        # A copy is necessary to not corrupt the dict while iterating
        sensors_not_supported = list()
        for key, value in self.SENSOR_MAP.copy().items():  # TODO: Move to dict library
            if value is None:
                del self.SENSOR_MAP[key]
                sensors_not_supported.append(key)

        # Any items to print
        if len(sensors_not_supported) > 0:
            self._log_writer.write_warning('Client {}: Following sensors are not supported by the device: {}.'
                                           .format(self.device_name,
                                                   sensors_not_supported))

        # Indicate successful sensor-list initializing
        self.device_state_set(self.DeviceStates.SENSOR_LIST_INITIALIZED)

    def handle_sensor_list_inform(self, message):
        """
        Updates sensor list.
        :param message: Katcp message object.
        :return:
        """

        # Extract the single sensors (separated by linebreaks)
        s_list = epl_string_split.split_lines(str(message))

        for entry in s_list:
            fields = entry.split()

            # Parse the sensor type
            sens_type = Sensor.parse_type(fields[4])

            # Convert int/float params to int/float values, not strings
            # TODO: Bug in katcp library
            if sens_type in [KatcpSensor.FLOAT, KatcpSensor.INTEGER]:
                params = [float(i) for i in fields[5:]]
            else:
                params = fields[5:]

            # Add all sensors in _sensors_all mode
            if self._update_sensors_all is True:
                self.SENSOR_MAP[fields[1]] = KatcpSensor(
                    sensor_type=sens_type,
                    name=fields[1],
                    description=fields[2].replace("\\_", " "),
                    units=fields[3],
                    params=params,
                    value_list_max_size=self._sensor_hist_depth,
                    sampling_strategy=KatcpSensor.SamplingStrategy.period,
                    sampling_strategy_params=str(self._refresh_interval),
                    sampling_period_default=self._refresh_interval,
                    logger=self._log_writer
                )
            else:
                # Only add sensor if exists in dict
                update_sensor = None

                if fields[1] in self.update_sensors_dict.keys():
                    # First try to find the sensor as exact match
                    update_sensor = self.update_sensors_dict[fields[1]]
                else:
                    # Now as wildcard search
                    for sensor_name, value in self.update_sensors_dict.items():
                        if sensor_name in fields[1]:
                            update_sensor = value

                # If sensor was found, add it
                if update_sensor is not None:
                    self.SENSOR_MAP[fields[1]] = KatcpSensor(
                        sensor_type=sens_type,
                        name=fields[1],
                        description=fields[2].replace("\\_", " "),
                        units=fields[3],
                        params=params,
                        value_list_max_size=256,
                        sampling_strategy=KatcpSensor.SamplingStrategy[update_sensor['strategy']],
                        sampling_strategy_params=update_sensor['params'],
                        sampling_period_default=self._refresh_interval,
                        logger=self._log_writer
                    )

    def handle_sensor_value_reply(self, message):
        """
        Sensor status reply handler.
        :param message: Katcp message object.
        :return:
        """
        return

    def handle_sensor_value_inform(self, message):
        """
        Updates values/adds values (if value-list is activated) in SENSOR_MAP.
        :param message: Katcp message object.
        :return:
        """
        try:
            # Convert katcp message object to string
            message_string = str(message)

            # Split message fields
            fields = message_string.split()

            self.SENSOR_MAP[fields[3]].set_value(fields[5],
                                                 KatcpSensor.SensorStatus
                                                 [fields[4]],
                                                 fields[1])
        except Exception as ex:
            self._log_writer.write_warning("handle_sensor_value_inform: Sensor {}, ex {}".format(fields[3], ex))
        return

    def handle_help_reply(self, message):
        """
        After help was finished, remove un-supported entries from COMMAND_MAP.
        :param message: Katcp message object.
        :return:
        """

        # A copy is necessary to not corrupt the dict while iterating
        commands_not_supported = list()
        for key, value in self.COMMAND_MAP.copy().items():  # TODO: Move to dict library
            if value is None:
                del self.COMMAND_MAP[key]
                commands_not_supported.append(key)

        # Any items to print
        if len(commands_not_supported) > 0:
            self._log_writer.write_warning('Client {}: Following commands are not supported by the device: {}.'
                                           .format(self.device_name, commands_not_supported))

    def handle_help_inform(self, message):
        """
        Updates command list.
        :param message: Katcp message object.
        :return:
        """
        # Clean up the message by externally provided handle
        if self._description_cleanup_handle is not None:
            self._description_cleanup_handle(message)

        # Extract the help command parameters
        help_command = epl_katcp_helper.extract_inform_help(message.arguments)

        # Check if command must be added and add it to commands list
        if (self._commands_all is False and
            help_command.command_name in self.COMMAND_MAP) \
                or self._commands_all is True:
            self.COMMAND_MAP[help_command.command_name] \
                = KatcpCommand(help_command.command_name,
                               help_command.type,
                               values=help_command.values,
                               description=help_command.description)

        # Indicate successful command-list initializing
        self.device_state_set(self.DeviceStates.COMMAND_LIST_INITIALIZED)

    def handle_log_reply(self, message):
        """
        Log message reply handler.
        :param message: Katcp message object.
        :return:
        """

        return

    def handle_log_inform(self, message):
        """
        Log message inform handler.
        :param message: Katcp message object.
        :return:
        """

        try:
            log_mes = epl_katcp_helper.extract_inform_log(str(message))

            # Always write log messages to debug log
            self._log_writer.write_debug('Client {}:{}: {}'
                                         .format(self.device_name, log_mes.log_level, log_mes.log_message))

            # Only write messages with level > info to info log
            if log_mes.log_level.value > CommandLogInform.LogLevel.INFO.value:
                self._log_writer.write_info('Client {} [{}] {}'
                                            .format(self.device_name, log_mes.log_level.name, log_mes.log_message))

        except Exception as e:
            self._log_writer.write_exception('Client {}: Log inform message could not be analyzed: {}, ex: {}'
                                             .format(self.device_name, message, e))
        return

    def handle_reply(self, msg):
        """
        Called when a reply message arrives.
        :param msg: Common katcp message.
        :return:
        """

        try:
            self._log_writer.write_debug('Client {}: Reply message: {}.'
                                         .format(self.device_name, bytes(msg)))

            if msg.name in self._MESSAGE_HANDLES.keys():
                self._MESSAGE_HANDLES[msg.name][0](self, msg)
            else:
                # if reply is not known, see if it was on a set command
                self._handle_reply_command(msg)
        except Exception as e:
            self._log_writer.write_exception('Client {}: Reply command could'
                                             'not be analyzed: {}, ex: {}'.format(self.device_name, msg, e))

    def handle_inform(self, msg):
        """
        Called when an inform message arrives.
        :param msg: Common katcp message.
        :return:
        """
        try:
            self._log_writer.write_debug('Client {}: Inform message: {}.'
                                         .format(self.device_name, bytes(msg)))
            if msg.name in self._MESSAGE_HANDLES.keys():
                self._MESSAGE_HANDLES[msg.name][1](self, msg)
            else:
                # if inform is not known, see if it was on a set command
                self._handle_inform_command(msg)

        except Exception as e:
            self._log_writer.write_exception('Client {}: Inform command could '
                                             'not be analyzed: {}, ex: {}'
                                             .format(self.device_name, bytes(msg), e))

    def _handle_reply_command(self, message):
        """
        Global command reply handler.
        Searches list of commands and handles the reply.
        :param message: Entire katcp reply message.
        :return:
        """
        # Extract reply parameters
        reply = epl_katcp_helper.extract_reply_general(message.name, message.arguments)
        if reply.command_name in self.COMMAND_MAP.keys():
            command = self.COMMAND_MAP[reply.command_name]
            command.command_set(command_state=reply.status,
                                reply_message=reply.params)
            command.was_updated = True

            return True

        return False

    def _handle_inform_command(self, message):
        """
        Global command reply handler.
        Searches list of commands and handles the reply.
        :param message: Entire katcp reply message.
        :return:
        """
        # Extract inform parameters
        inform = epl_katcp_helper.extract_inform_general(message.name, message.arguments)
        if inform.command_name in self.COMMAND_MAP.keys():
            # Only set last_message here, state will be set on reply_handler
            command = self.COMMAND_MAP[inform.command_name]
            command.command_set(inform_message=inform.message)

            return True

        return False

    def notify_connected(self, connected):
        """
        Override event handler on connection status change.
        :param connected: See base class.
        :return:
        """
        if connected is False:
            self._log_writer.write_warning('Client {}: Disconnected.'.
                                           format(self.device_name))

            self.device_state_set(self.DeviceStates.UNINITIALIZED)
        else:
            self._log_writer.write_info('Client {}: Connected.'.
                                        format(self.device_name))

            # Start the init process
            self._init_thread_setup(re_init=True)

    def sensor_map_get(self):
        """
        Returns the client's sensor map.
        :param self:
        :return:
        """
        return self.SENSOR_MAP

    def sensor_map_items_get(self):
        """
        Returns the client's sensor map items.
        :param self:
        :return:
        """
        return self.SENSOR_MAP.items()

    def command_map_get(self):
        """
        Returns the client's sensor map.
        :param self:
        :return:
        """
        return self.COMMAND_MAP

    def command_map_items_get(self):
        """
        Returns the client's sensor map items.
        :param self:
        :return:
        """
        return self.COMMAND_MAP.items()

    def start(self, timeout=None):
        """
        Starts the thread, but only if not running.
        Call Stop() before to restart.
        """
        if not self._thread_was_started:
            try:
                super(KatcpClient, self).start(timeout)
            finally:
                self._thread_was_started = True
                return True
        return False

    def stop(self, timeout=None):
        """
        Stops the client thread.
        """
        if self._thread_was_started:
            try:
                # Stop the client
                super(KatcpClient, self).stop(timeout=timeout)
            finally:
                # Reset flag
                self._thread_was_started = False

                return True
        return False

    # # Dict for message handles callbacks
    # _MESSAGE_HANDLES = {
    #     'help': (handle_help_reply,
    #              handle_help_inform),
    #
    #     'sensor-list': (handle_sensor_list_reply,
    #                     handle_sensor_list_inform),
    #
    #     'sensor-status': (handle_sensor_status_reply,
    #                       handle_sensor_status_inform),
    #
    #     'log': (handle_log_reply, handle_log_inform),
    #
    #     'sensor-sampling': (handle_sensor_sampling_reply,
    #                         handle_sensor_sampling_inform),
    #
    #     'sensor-value': (handle_sensor_value_reply,
    #                      handle_sensor_value_inform)
    # }
