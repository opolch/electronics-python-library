import socket
from epl.epl_logging.epl_logging_log import Log


class SockUDP(socket.socket):
    """
    Class providing UPD-Socket functionality.
    """

    def __init__(self,
                 host,
                 port,
                 timeout=10,
                 send_termination=None,
                 receive_termination=None,
                 logger=Log(),
                 encoding='utf8'):
        """
        Init the socket and local variables.
        Afterwards send/receive functions can be executed.
        :param host: Host for the socket.
        :param port: Port for the socket.
        :param timeout: Receive timeout.
        :param send_termination: Send termination string (None for none).
        :param receive_termination: Receive termination string (None for none).
        :param logger: epl_logging_log.Log
        :param encoding: Message encoding for sen/receive messages (as string).
        """

        # Variables
        self._host = host
        self._port = port
        self._send_termination = send_termination
        self._receive_termination = receive_termination
        self._logger = logger
        self._encoding = encoding

        # Init base class to create the socket
        super(SockUDP, self).__init__(socket.AF_INET, socket.SOCK_DGRAM)

        # Timeout for data reception
        self.settimeout(timeout)

    def send_receive(self,
                     send_data,
                     receive_data_len):
        """
        Sends a message and waits for the answer (handshake-like implementation)
        :param send_data:
        :param receive_data_len:
        :return: Sucess->Receive-Message string; Fail->None
        """
        # First send message
        self.send_data(send_data)

        # Wait for the answer
        return self.receive_data(receive_data_len)

    def send_data(self,
                  data):
        """
        Send data over the socket.
        :param data: String data to be sent.
        :return:
        """
        return self.sendto("{}{}".format(data, self._send_termination if not None else '').encode(self._encoding),
                           (self._host, self._port))

    def receive_data(self,
                     length):
        """
        Receive data over socket.
        :param length: Max length of data to be received.
        :return: Sucess->Message string; Fail->None
        """

        receive_message = b''

        # Use termination to detect end of message
        if self._receive_termination is not None:
            # Get all characters till the termination is detected
            while 1:
                try:
                    data, _ = self.recvfrom(60000)
                    receive_message = receive_message + data
                    # Check for termination
                    if len(receive_message) >= len(self._receive_termination):
                        if bytes(data.decode()[-len(self._receive_termination):], self._encoding) == \
                                bytes(self._receive_termination, self._encoding):
                            break

                except Exception as e:
                    # Nothing received within timeout
                    self._logger.write_error('Error while receiving, receive-message:{}, ex:{}'.format(
                        receive_message, e.args))
                    receive_message = None
                    break
        else:
            try:
                receive_message = self.recv(length)
            except:
                receive_message = None

        if receive_message is not None:
            receive_message = receive_message.decode(self._encoding).strip('\n')
        else:
            self._logger.write_error('No message was received within timeout period')

        return receive_message

