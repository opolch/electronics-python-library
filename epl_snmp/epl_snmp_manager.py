# -*- coding: utf-8 -*-
"""
Created on Tue Jan 26 10:26:19 2021

@author: hardikar
"""

from pysnmp import hlapi
import time
import threading
from epl.epl_logging.epl_logging_log import Log
import collections


class TaskSNMP():
    """
    Class to define SNMP specific project tasks.

    """
    def __init__(self, oid, mirror_function):
        """
        :param oid: OID of the task.
        :param mirror_function: Mirror function of the task.

        :return:

        """
        self.oid = oid
        self.mirror_function = mirror_function


class Device():
    """
    Class to define the SNMP manager.

    """
    def __init__(self,
                 snmp_agent,
                 port,
                 logger,
                 update_task_list=[],
                 auto_update=True,
                 update_interval=1
                 ):
        """
        This function initializes the Device class.

        :param snmp_agent: IP address of the SNMP agent device to connect to.
        :param port: SNMP connection port.
        :param logger: Logger module to implement the logging functionality.
        :param update_task_list: List of updatabble objects of type 'task_snmp'
            (OIDs of the task and its mirror function as extracted from main
            project task_list).
        :param auto_update: Flag to indicate if the auto update of parameters
            is desired.
        :param update_interval: Time interval between successive update in case
            auto update is desired.
            
        :return:

        """

        self.snmp_agent = snmp_agent
        self.port = port
        self.update_task_list = update_task_list
        self.auto_update = auto_update
        self.update_interval = update_interval

        if logger is None:
            self._log_writer = Log()
        else:
            self._log_writer = logger

        self.tx_buffer = collections.deque(maxlen=1000)
        self.SNMP_COMMANDS_MAX_COUNT = 100

        self.update_flag = False
        self.connected = True
        self.start_threads()
        if self.auto_update is False:
            self.init_params()

    def init_params(cls):
        """
        Read the initial values of all the SNMP objects from the agent if the\
        auto update is disabled.

        :return:

        """

        for task in cls.update_task_list:
            cls.tx_buffer.append(task.oid)
            try:
                cls.send_receive_msg()
            except Exception as e:
                cls._log_writer.write_info("init_params: Connection reset\
                                           error, waiting for connection.", e)

    def send_receive_msg(cls):
        """
        This function sends a message and waits for its reply.

        This function awaits the reply from the agent. Once the reply is\
        received, it finds the suitable task from the task list, and  executes\
        its mirror function.

        :return:

        """
        while cls.tx_buffer:
            oid = cls.tx_buffer.popleft()
            try:
                value_received = cls.get(cls.snmp_agent,
                                         [oid],
                                         hlapi.CommunityData('public'))
                if value_received is not None:
                    for task in cls.update_task_list:
                        if task.oid == oid:
                            task.mirror_function(task, value_received)
                            break
                else:
                    pass  # No mirror received.

                return value_received
            except RuntimeError:
                cls.connected = False

    def construct_object_types(cls, list_of_oids):
        """
        Creates a list of objects representing MIB variable from given object\
        Id string.

        :param list_of_oids: list of OIDs of the objects whose values are to\
            be read out.

        :return:

        """
        object_types = []
        for oid in list_of_oids:
            object_types.append(hlapi.ObjectType(hlapi.ObjectIdentity(oid)))
        return object_types

    def get(cls, target, oids, credentials, port=161,
            engine=hlapi.SnmpEngine(), context=hlapi.ContextData()):
        """
        Read the value of the desired parameter described by the OID.

        :param target: hostname or IP address of the SNMP agent.
        :param oids: list of OIDs to be enquired.
        :param credentials: credentials, if necessary.
        :param port: SNMP port. Default value is 161.
        :param engine: SNMP engine for the operation.
        :param context: context data.

        :return: Dictionary containing the OIDs as keys, and their read-out
            data as the respective values.

        """
        handler = hlapi.getCmd(
            engine,
            credentials,
            hlapi.UdpTransportTarget((target, port)),
            context,
            *cls.construct_object_types(oids)
        )
        return cls.fetch(handler, 1)[0]

    def fetch(cls, handler, count):
        """
        Extract the OID and their parameter value from the obtained results of
        SNMP get operations.

        :param handler: returned value of the get operations.
        :param count: number of get operations, i.e, OID values to be fetched.

        :return: Dictionary containing the OIDs as keys, and their read-out
            data as the respective values.

        """
        result = []
        for i in range(count):
            try:
                error_indication, error_status, error_index, var_binds = next(handler)
                if not error_indication and not error_status:
                    items = {}
                    for var_bind in var_binds:
                        items[str(var_bind[0])] = cls.cast(var_bind[1])
                    result.append(items)
                else:
                    raise RuntimeError('Got SNMP error: {0}'.
                                       format(error_indication))
            except StopIteration:
                break
        return result

    def cast(cls, value):
        """
        Convert the value into one of the python data types.

        :param value: Value to be converted into its correct python data type
            from pysnmp data type.

        :return: Value with suitable python data type.
        """
        try:
            return int(value)
        except (ValueError, TypeError):
            try:
                return float(value)
            except (ValueError, TypeError):
                try:
                    return str(value)
                except (ValueError, TypeError):
                    pass
        return value

    def get_bulk(cls, target, oids, credentials, count, start_from=0, port=161,
                 engine=hlapi.SnmpEngine(), context=hlapi.ContextData()):
        """


        :param target: hostname or IP address of the SNMP agent.
        :param oids: list of OIDs to be enquired.
        :param credentials: credentials to authenticate the session, if
            necessary.
        :param count: number of parameters to read.
        :param start_from: OID to start reading from.
        :param port: SNMP port. Default value is 161.
        :param engine: SNMP engine for the operation.
        :param context: context data.

        :return: Processed result of the get operation.

        """

        handler = hlapi.bulkCmd(
            engine,
            credentials,
            hlapi.UdpTransportTarget((target, port)),
            context,
            start_from, count,
            *cls.construct_object_types(oids)
        )
        return cls.fetch(handler, count)

    def get_bulk_auto(cls, target, oids, credentials, count_oid, start_from=0,
                      port=161, engine=hlapi.SnmpEngine(),
                      context=hlapi.ContextData()):
        """


        :param target: hostname or IP address of the SNMP agent.
        :param oids: list of OIDs to be enquired.
        :param credentials: credentials, if necessary.
        :param count_oid: number of parameters to read.
        :param start_from: OID to start reading from.
        :param port: SNMP port. Default value is 161.
        :param engine: SNMP engine for the operation.
        :param context: context data.

        :return: Processed result of the get operation.

        """

        count = cls.get(target, [count_oid], credentials, port, engine,
                        context)[count_oid]
        return cls.get_bulk(target, oids, credentials, count, start_from,
                            port, engine, context)

    def update_params(cls):
        """
        This function takes care of the periodic updating of the parameters.

        This function calls the send function and then receives the response.
        It is followed by the execution of the mirror function of the task.

        :return:

        """
        cls._log_writer.write_info("Starting updation of parameters.")
        while True:
            if cls.update_flag is True:
                for task in cls.update_task_list:
                    cls.tx_buffer.append(task.oid)
                    cls.send_receive_msg()

                time.sleep(cls.update_interval)

    def start_threads(cls):
        """
        Starts all the threads related to the device after reading the flags.

        :return:

        """

        if cls.auto_update is True:
            cls.update_flag = True
            update_thread = threading.Thread(target=cls.update_params)
            update_thread.start()
            cls._log_writer.write_info("Update thread started.")
        else:
            pass
