import ctypes


def di(obj_id):
    """
    Inverse of id() function.
    :param obj_id: ID of object to be returned.
    :return:
    """
    return ctypes.cast(obj_id, ctypes.py_object).value
