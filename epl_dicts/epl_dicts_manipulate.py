def flatten_dict(source_dict: dict, result_dict: dict = None):
    """
    Flattens a dict of dicts of...
    :param source_dict: Dict to be flattened.
    :param result_dict: Optional dict for result.
    :return: Flattened dict.
    """
    if result_dict is None:
        result_dict = {}
    for x, y in source_dict.items():
        if not isinstance(y, dict):
            result_dict[x] = y
        else:
            flatten_dict(y, result_dict)
    return result_dict
