from enum import Enum, auto


class EmailErrors(Enum):
    """
    Enum to define mail types.
    """
    NO_ERROR = auto()
    ADMIN_UNKNOWN_SENDER = auto()
    ADMIN_HEADER_MISSING = auto()
    ADMIN_PAYLOAD_LEN = auto()