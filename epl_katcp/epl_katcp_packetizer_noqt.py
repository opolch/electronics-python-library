from queue import Queue
from epl.epl_katcp.epl_katcp_client import KatcpClient
from epl.epl_threading.epl_threading_thread import TriggerTimeout
import array
import datetime
import copy
import math
from enum import Enum, auto
import sys


def construct_array(data_type, data):
    """
    Constructs an array.array with datatype [data_type] and fills it with the
    given [data] string / bytes object using correct method for python versions
    greater and lower of 3.9.

    """
    res = array.array(data_type)
    if sys.version_info.minor >= 9:
        res.frombytes(data)
    else:
        res.fromstring(data)
    return res


class Packetizer(KatcpClient):
    """
    Class derived from standard epl_katcp_client.KatcpClient class extending it
    with functionality to handle a Packetizer device.
    """

    class MessageScaling(Enum):
        """
        Enum to get the type of snapshot data scaling
        """
        linear = auto()
        log = auto()

    class SnapshotSpecData:
        """
        Helper class to form spectra data.
        """

        class FieldNames(Enum):
            """
            Enum to organize field names for spectra data.
            """
            header = auto()  # For completeness
            adc0 = auto()
            adc1 = auto()
            level0 = auto()
            level1 = auto()

        class Header:
            """
            Header of spectra message.
            """

            def __init__(self, message):
                self.band_width = float(message.arguments[1]) * 1e3
                self.integration_time = float(message.arguments[2])
                self.num_channels = int(message.arguments[3])
                self.band_width_adc = float(message.arguments[4])
                self.spec_counter = int(message.arguments[5])

                # Create epoch from yyyy-mm-ddThh:mm:ss.ff
                time_str, fraq = message.arguments[6].decode('utf-8').split(".")
                time_stamp = datetime.datetime.strptime(time_str, "%Y-%m-%dT%H:%M:%S")
                epoch_str = time_stamp.strftime("%S") + "." + fraq[0:-3]
                self.time_stamp = float(epoch_str)

                # Packer 2nd gen has additional option to set the scaling (linear/log)
                try:
                    self.message_scaling = Packetizer.MessageScaling[message.arguments[7].decode('utf-8')]
                except:
                    self.message_scaling = Packetizer.MessageScaling.log

            def dict_from_class(self):
                """
                Returns all the class members as dict.
                :return:
                """

                # List with keys to be excluded
                _excluded_keys = ['message_scaling']

                # Create return dict from class keys
                return_dict = dict(
                    (key, value) for (key, value) in self.__dict__.items()
                    if key not in _excluded_keys
                )

                # plain string here, no enum
                return_dict['message_scaling'] = self.message_scaling.name.lower()

                return return_dict

        class Level:
            """
            Level data of spectra message.
            """

            def __init__(self, message):
                self.level_data = construct_array('I', message.arguments[1]).tolist()

        class ADC:
            """
            ADC data of spectra message.
            """

            def __init__(self, message, message_scaling):
                self.signal_data = construct_array('f', message.arguments[1])

                # Create data list
                if message_scaling == Packetizer.MessageScaling.linear:
                    self.signal_data = [10 * math.log10(x) if x > 0 else 1 for x in self.signal_data[1:]]
                else:
                    self.signal_data = list(self.signal_data[1:])

        def __init__(self):
            """
            Inits the spectra data object.
            """

            # Init the parameters
            self.header_data = None
            self.level_data = dict()
            self.signal_data = dict()
            self.header_raw = None
            self.level_raw = dict()
            self.signal_raw = dict()

        def set_header(self, message):
            """
            Sets the header part of the spectra data.
            param message: Entire Katcp inform message.
            :return:
            """
            self.header_data = self.Header(message)
            self.header_raw = message.arguments

        def set_level_data(self, channel, message):
            """
            Sets the level part of the spectra data.
            param channel: ADC-Channel
            param message: Entire Katcp inform message.
            :return:
            """
            self.level_data[channel] = self.Level(message)
            self.level_raw[channel] = message.arguments

        def set_signal_data(self, channel, message):
            """
            Sets the signal part of the spectra data.
            param channel: ADC-Channel
            param message: Entire Katcp inform message.
            :return:
            """
            self.signal_data[channel] = self.ADC(message, self.header_data.message_scaling)
            self.signal_raw[channel] = message.arguments

    def __init__(self,
                 device_host,
                 device_port,
                 device_name,
                 katcp_root_name='',
                 connect_timeout=2,
                 init_timeout=10,
                 update_sensors_dict=dict(),
                 command_list=list(),
                 auto_reconnect=False,
                 refresh_interval_s=2,
                 logger=None,
                 message_handles=None,
                 integration_time=1.0,
                 number_data_consumers=1,
                 spectra_update_callback=None,
                 description_cleanup_handle=None,
                 sensor_hist_depth=256,
                 spectra_enable_on_connect=False,
                 spectra_enable_verify=False,
                 spectra_enable_verify_timeout=60):
        """
        Init the object.
        param device_host: Katcp server's ip address/hostname number.
        param device_port: Katcp server's port number.
        param device_name: Unique device name.
        param katcp_root_name: Root name of the katcp devices eg. `rxs`
        param connect_timeout: Timeout for connection process.
        param init_timeout: Timeout for init process.
        param update_sensors_dict: Dict with sensors (katcp_name:sampling_strategy:params) to be fetched.
            Pass empty dict or nothing for ALL_SENSORS.
        param command_list: List with commands to make available. Pass empty list or nothing for ALL_COMMANDS.
        param auto_reconnect: Auto reconnect the server after connection loss.
        param refresh_interval_s: Interval for sensor update strategy.
        param logger: epl_logging_log.Log logger.
        param message_handles: dict holding message handles to override or extend the existing ones.
        param integration_time: Integration time for the spectra snapshots.
        param number_data_consumers: Number of spectra data consumers to hold queues for.
        param spectra_update_callback: Callback for the spectra update
        param description_cleanup_handle: Function to clean up the sensor/command descriptions
        :param sensor_hist_depth: Depth of sensor history.
        :param spectra_enable_on_connect: True->Enable the spectra snapshot on successful connect; False->Not.
        :param spectra_enable_verify: True-> Continuously check for spectra snapshot to be enabled (thread).
        :param spectra_enable_verify_timeout: Timeout for recurring spectra enable checks.
        """

        # Packetizer specific message handles
        _PACK_MESSAGE_HANDLES = {
            # Dict for message handles callbacks
            self._CMD_SENSOR_STATUS:
                (Packetizer._handle_sensor_status_reply_pck, Packetizer._handle_sensor_status_inform_pck),
            '{}{}'.format(katcp_root_name, self._CMD_GET_SNAPSHOT):
                (Packetizer._handler_reply_snapshot_spec_get, Packetizer._handler_inform_snapshot_spec_get),
        }

        # Packetizer specific sensor list
        # Sampling strategy is not used in current implementation (maybe in future)
        _PACK_SENSOR_LIST = {
            '{}{}'.format(katcp_root_name, self._SENS_SNAPSHOT_CNTR): {'strategy': 'event', 'params': ''}
        }

        # Dict for handles packer needs to forward after himself pre-processed the request.
        self._message_handle_overrides = dict()
        if message_handles is not None:
            for handle_name, handle in message_handles.items():
                # If handle is part of packers own handle list, can not forward it to the client
                # but put it handles_override dict
                if handle_name in _PACK_MESSAGE_HANDLES:
                    self._message_handle_overrides[handle_name] = copy.deepcopy(handle)

            # Remove copied handles
            for handle_name in self._message_handle_overrides.keys():
                del (message_handles[handle_name])

            # Now append the remaining handles
            _PACK_MESSAGE_HANDLES.update(message_handles)

        # Init base class
        super().__init__(device_host,
                         device_port,
                         device_name,
                         katcp_root_name=katcp_root_name,
                         connect_timeout=connect_timeout,
                         init_timeout=init_timeout,
                         update_sensors_dict=update_sensors_dict,
                         command_list=command_list,
                         auto_reconnect=auto_reconnect,
                         sensor_period_default=refresh_interval_s,
                         logger=logger,
                         message_handles=_PACK_MESSAGE_HANDLES,
                         description_cleanup_handle=description_cleanup_handle,
                         sensor_hist_depth=sensor_hist_depth,
                         log_limit="off")

        # Set device type
        self.device_type = KatcpClient.DeviceType.PACKETIZER

        # Spectra update callback will be executed on spectra update
        self._spectra_update_callback = spectra_update_callback

        # Add packetizer specific message handles and sensors
        self._init_funcs.append(self.init_pck_connect)

        # Set variables
        self.integration_time = integration_time
        self._spectra_enable_on_connect = spectra_enable_on_connect
        self._spectra_enable_verify = spectra_enable_verify

        # Thread to make sure the spectra snapshot is active
        self._spectra_snapshot_checker = TriggerTimeout(callback=self.snapshot_spec_enable_request,
                                                        timeout=spectra_enable_verify_timeout,
                                                        start_immediate=False)

        # Spectra data object to create a spec data entry
        self._snapshot_spec_data_tmp = Packetizer.SnapshotSpecData()

        # list of thread safe queues for spec data (multiple consumers can access the data)
        # Size is 1 to wait till consumer(s) got the entry
        self.snapshot_spec_data = list()
        for i in range(0, number_data_consumers):
            self.snapshot_spec_data.append(Queue(maxsize=1))

    def init_pck_connect(self, timeout=0):
        """
        Packetizer specific tasks to be issued on (re)connect.
        param timeout: Parameter has no effect. Just for consistency when used in init sequence.
        :return:
        """
        if self._spectra_enable_on_connect is True:
            self.snapshot_spec_enable_request()
        if self._spectra_enable_verify is True:
            self._spectra_snapshot_checker.start_thread()

        return True

    def snapshot_spec_enable_request(self, integration_time=0, enable=True):
        """
        Request to enable the spectra snapshots.
        param integration_time: Integration time in s.
        param enable: True->Enable spec snapshot; False->Disable.
        :return:
        """
        self.integration_time = integration_time if integration_time != 0 else self.integration_time
        if enable is True:
            self.send_request('?{}{} {}'.format(self._katcp_root_name, self._CMD_SNAPSHOT_SPEC_ENABLE,
                                                self.integration_time * 1e6))
        else:
            self.send_request('?{}{}'.format(self._katcp_root_name, self._CMD_SNAPSHOT_SPEC_DISABLE))

    def _handle_sensor_status_reply_pck(self, message):
        """
        Sensor status reply handler.
        param message: Katcp message object.
        :return:
        """
        # Forward to base class
        super().handle_sensor_status_reply(message)
        return

    def _handle_sensor_status_inform_pck(self, message):
        """
        Packetizer sensor_status handler to catch specific sensors.
        param message: Katcp message object.
        :return:
        """
        # Convert katcp message object to string
        message_string = str(message)

        # Split message fields
        sensor_name = message_string.split()[3]

        if self._SENS_SNAPSHOT_CNTR in sensor_name:
            # Update the spectra data
            self._update_spectra()

        if message.name in self._message_handle_overrides:
            self._message_handle_overrides[message.name][1](message)
        else:
            super().handle_sensor_status_inform(message)
        return

    def _update_spectra(self):
        """
        Send request to update spectra.
        :return:
        """
        self.send_request('?{}{}'.format(self._katcp_root_name, self._CMD_GET_SNAPSHOT))

    def _handler_reply_snapshot_spec_enable(self, message):
        """
        Spectra snapshot enable reply handler.
        param message: Katcp message object.
        :return:
        """
        pass

    def _handler_inform_snapshot_spec_enable(self, message):
        """
        Spectra snapshot enable inform handler.
        param message: Katcp message object.
        :return:
        """
        pass

    def _handler_reply_snapshot_spec_get(self, message):
        """
        Spectra snapshot get reply handler.
        param message: Katcp message object.
        :return:
        """
        pass

    def _handler_inform_snapshot_spec_get(self, message):
        """
        Spectra snapshot get inform handler.
        param message: Katcp message object.
        :return:
        """

        try:
            # Get data type
            data_type = message.arguments[0].decode('utf-8')

            if 'header' in data_type:
                # Copy last snapshot to queue
                # Prevent emitting signal on first run (when no data was set)
                if self._snapshot_spec_data_tmp.header_data is not None:
                    for i in range(0, len(self.snapshot_spec_data)):
                        try:
                            # Remove the oldest item in case the queue is full
                            if self.snapshot_spec_data[i].full() is True:
                                self.snapshot_spec_data[i].get(block=False)

                            # Add to queue
                            self.snapshot_spec_data[i].put(copy.copy(self._snapshot_spec_data_tmp), False)
                        except Exception as e:
                            pass
                    # Execute the callback
                    if self._spectra_update_callback is not None:
                        self._spectra_update_callback()

                # Set header
                self._snapshot_spec_data_tmp.set_header(message)

            elif 'level' in data_type:
                self._snapshot_spec_data_tmp.set_level_data(data_type, message)

            elif 'adc' in data_type:
                self._snapshot_spec_data_tmp.set_signal_data(data_type, message)

        except Exception:
            self._logger.exception('Could not convert snapshot spec data inform message')

    def stop(self, timeout=None):
        """
        Overwriting the standard stop() to make sure the local threads/timers are stopped.
        param timeout: !Used only in base class! Seconds to wait for both client thread
        to have *started*, and for stopping.
        :return:
        """
        # Stop the katcp client
        if self.running() is True:
            super(Packetizer, self).stop()

        # Stop the spectra enable checker
        self._spectra_snapshot_checker.stop_thread()

    #
    # Constants
    #

    # Sensors
    _SENS_SNAPSHOT_CNTR = '.packetizer.systemstatus.snapshot.ctr'

    # Commands
    _CMD_GET_SNAPSHOT = '-packetizer-snapshot-get-spec'
    _CMD_SNAPSHOT_SPEC_ENABLE = '-packetizer-snapshot-enable-spec'
    _CMD_SNAPSHOT_SPEC_DISABLE = '-packetizer-snapshot-disable-spec'
    _CMD_SENSOR_STATUS = 'sensor-status'
