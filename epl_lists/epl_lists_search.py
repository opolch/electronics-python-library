def contains(search_list, filter_criteria):
    """
    Checks whether list contains an entry with specific member value set. e.g.:
    if contains(myList, lambda x: x.n == 3)  # True if any element has .n==3
    :param search_list: The list to be searched.
    :param filter_criteria: The filter criteria to be used for the search.
    :return: True->contains!
    """
    for x in search_list:
        if filter_criteria(x):
            return True
    return False


def list_items_filter(search_list, filter_criteria) -> list:
    """
    Filters list items based on a filter criteria:
    list_items_filter(myList, lambda x: x.n == 3)
    :param search_list: The list to be searched.
    :param filter_criteria: The filter criteria to be used for the search.
    :return: []
    """
    return [x for x in search_list if filter_criteria(x)]


def first_element_return(iterable, default=None):
    """
    Returns the first element in list matching the iterable. e.g.:
    first(x for x in myList if x.n == 30)  # the first match, if any
    :param iterable:
    :param default:
    :return:
    """
    for item in iterable:
        return item
    return default


def has_any(iterable, default=None):
    """
    Checks an iterable for presence of an item.
    :param iterable: iterable to be searched.
    :param default: default item to be returned when nothing was found.
    :return: Success->Item found; Otherwise->default.
    """
    for item in iterable:
        return item
    return default


def items_as_list(iterable):
    """
    Checks an iterable for presence of items and returns them in a list.
    :param iterable: iterable to be searched.
    :return: Success->Item(s) found; Otherwise->[].
    """
    try:
        return [x for x in iterable]
    except:
        return []
