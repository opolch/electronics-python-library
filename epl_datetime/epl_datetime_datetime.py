from datetime import datetime, timezone, timedelta


def date_time_1970() -> datetime:
    """
    Returns a datetime object in the format "1970-01-01T00:00:00"
    :return:
    """
    return datetime.strptime("1970-01-01T00:00:00", "%Y-%m-%dT%H:%M:%S")


def dt_now_tz(tz=timezone.utc, offset=timedelta(days=0, hours=0, minutes=0)):
    """
    Returns the datetime.now with respect to the provided timezone.
    :param tz: datetime.timezone.
    :param offset: datetime.timedelta to be added (subtracted when negative).
    :return: datetime
    """

    # Get current time as utc
    dt = datetime.now(tz) + offset
    return dt.replace(tzinfo=tz)


def dt_now_tz_timestamp(tz=timezone.utc, offset=timedelta(days=0, hours=0, minutes=0)) -> float:
    """
    Returns the datetime.now with respect to the provided timezone as timestamp.
    :param tz: datetime.timezone.
    :param offset: datetime.timedelta to be added (subtracted when negative).
    :return: float timestamp
    """

    # Current time as timestamp
    return dt_now_tz(tz=tz, offset=offset).timestamp()


def dt_now_tz_timestamp_ms(tz=timezone.utc, offset=timedelta(days=0, hours=0, minutes=0)) -> int:
    """
    Returns the datetime.now with respect to the provided timezone as timestamp in milliseconds as int.
    :param tz: datetime.timezone.
    :param offset: datetime.timedelta to be added (subtracted when negative).
    :return: int timestamp in millisecond granularity
    """

    # Current time as timestamp
    return int(dt_now_tz(tz=tz, offset=offset).timestamp() * 1000)


def dt_now_tz_timestamp_s(tz=timezone.utc, offset=timedelta(days=0, hours=0, minutes=0)) -> int:
    """
    Returns the datetime.now with respect to the provided timezone as timestamp in seconds as int.
    :param tz: datetime.timezone.
    :param offset: datetime.timedelta to be added (subtracted when negative).
    :return: int timestamp in seconds granularity
    """

    # Current time as timestamp
    return int(dt_now_tz(tz=tz, offset=offset).timestamp())


def dt_from_timestamp_tz(timestamp: str, tz=timezone.utc) -> float:
    """
    Returns the timestamp from str timestamp with respect to the provided timezone.
    :param timestamp: The timestamp as string.
    :param tz: datetime.timezone.
    :return: float timestamp
    """
    return datetime.fromtimestamp(float(timestamp), tz=tz).timestamp()
