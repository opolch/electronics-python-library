from katcp import Sensor
from enum import Enum, auto
from epl.epl_string import epl_string_split
from epl.epl_logging.epl_logging_log import Log
import time


class KatcpSensor(Sensor):
    """
    Class derived from katcp sensor class.
    """

    class SensorStatus(Enum):
        """
        Enum with status values for katcp sensors.
        """
        unknown = 0
        nominal = 1
        warn = 2
        error = 3
        failure = 4
        unreachable = 5
        inactive = 6

    class SamplingStrategy(Enum):
        """
        Class to enumerate the KATCP sampling strategies.
        """
        auto = 'auto'
        none = 'none'
        period = 'period'
        event = 'event'
        differential = 'differential'
        event_rate = 'event-rate'
        differential_rate = 'differential-rate'

        @classmethod
        def from_string(cls, strategy_string):
            try:
               return cls(strategy_string)
            except ValueError:
               raise ValueError(f"Invalid strategy string: {strategy_string}")

    """ Mapping from sampling strategy to number of params """
    SAMPLING_STRATEGY_NUMPARMS = {
        SamplingStrategy.auto: 0,
        SamplingStrategy.none: 0,
        SamplingStrategy.period: 1,
        SamplingStrategy.event: 0,
        SamplingStrategy.differential: 1,
        SamplingStrategy.event_rate: 2,
        SamplingStrategy.differential_rate: 3,
    }

    """Mapping from sensor type strings to python type classes"""
    TYPE_CASTINGS = {
        'integer': int,
        'float': float,
        'boolean': bool,
        'lru': str,
        'discrete': str,
        'string': str,
        'timestamp': str,
        'address': str
    }

    def __init__(self, sensor_type, name, description=None, units='',
                 params=None, default=None, initial_status=None, value_list_max_size=1,
                 sampling_strategy=SamplingStrategy.auto, sampling_strategy_params='',
                 sampling_period_default=5, logger=None):
        """
        Inits the sensor object.
        :param sensor_type: Sensor type constant.
        :param name: The name of the sensor.
        :param description: A short description of the sensor.
        :param units: The units of the sensor value. May be the empty string if there are no applicable units.
        :param params: Additional parameters, dependent on the type of sensor:
          * For :const:`INTEGER` and :const:`FLOAT` the list should
            give the minimum and maximum that define the range
            of the sensor value.
          * For :const:`DISCRETE` the list should contain all
            possible values the sensor may take.
          * For all other types, params should be omitted.
        :param default: An initial value for the sensor. By default this is determined by the sensor type.
        :param initial_status: An initial status for the sensor. If None, defaults to
        Sensor.unknown. `initial_status` must be one of the keys in
        Sensor.STATUSES
        :param value_list_max_size: Maximum number of values to store in value_list.
        :param sampling_strategy: Sensor's sampling strategy.
        :param sampling_strategy_params: Params field for sampling strategy.
        :param sampling_period_default: Default value for fall-back sampling strategy period
            (in case preferred strategy is not supported by sensor).
        :param logger: epl_logging_log.Log logger.
        """

        # Set variables
        self._value_list_max_size = value_list_max_size
        self._initialized = False
        self.value_list = list()
        self._was_updated = False

        if logger is None:
            self._log_writer = Log()
        else:
            self._log_writer = logger

        # Init the parent class
        super(KatcpSensor, self).__init__(sensor_type, name, description, units,
                                          params, default, initial_status)

        # Extract limits
        self.lower_warn = None
        self.upper_warn = None
        self.lower_error = None
        self.upper_error = None

        if self.sensor_type_float_int_check():
            try:
                if len(params) == 4:
                    self.lower_warn, self.upper_warn, self.lower_error, self.upper_error = \
                        [float(x) for x in self.params]
                elif len(params) == 2:
                    self.lower_error, self.upper_error = [float(x) for x in self.params]
            except:
                self._log_writer.write_warning('Can not convert limits for task `{}`, params: {}'.
                                             format(name, self.params))

        # Check whether the sensors not supporting it, have been mistakenly allocated
        # differential-based sampling. If yes, set the periodic sampling
        # for the initialization process.

        if ((self.parse_type(self.type) == self.FLOAT or self.parse_type(self.type) == self.INTEGER
             or self.parse_type(self.type) == self.TIMESTAMP)
                or (sampling_strategy.name != KatcpSensor.SamplingStrategy.differential.name
                    and sampling_strategy.name != KatcpSensor.SamplingStrategy.differential_rate.name)):

            self.sampling_strategy_params = epl_string_split.split_always_list(
                sampling_strategy_params, remove_whitespaces=False)

            # Check number of parameters
            if (len(self.sampling_strategy_params) ==
                    self.SAMPLING_STRATEGY_NUMPARMS[sampling_strategy]):

                # Make sure passing properly formatted limits
                if self.parse_type(self.type) == self.INTEGER:
                    self.sampling_strategy_params = \
                        [str(int(float(x))) if float(x) > 0.5 else '1' for x in self.sampling_strategy_params]

                self.sampling_strategy = sampling_strategy

            else:
                self.sampling_strategy = self.SamplingStrategy.period
                self.sampling_strategy_params = [str(sampling_period_default)]

                self._log_writer.write_debug(
                    "{}: Wrong number of parameters for given sampling strategy. "
                    "Tentatively setting the periodic sampling for it.".format(self.name))
        else:
            # Use event-based sampling as default for this type of sensor
            self.sampling_strategy = self.SamplingStrategy.event
            self.sampling_strategy_params = ""

            self._log_writer.write_debug(
                "Sensor {} of type {} does not support {} strategy (please change its strategy). "
                "Tentatively setting the event-based sampling for it.".
                format(self.name, self.type, sampling_strategy.name))

    def set_value(self, value, status=None, timestamp=None):
        """
        Overriding the set_value function to implement history data functionality.
        :param value: value to be set.
        :param status: SensorStatus, when None is passed, function tries to perform auto-check against limits.
        If that fails (no proper limits, ...) NOMINAL will be set.
        :param timestamp: Time stamp.
        :return:
        """

        set_status = status

        # Take current time if no time provided
        if timestamp is None:
            timestamp = time.time()

        self._log_writer.write_debug('Updating sensor {}, time: {},  msg: {}, type: {}'.
                                     format(self.name, timestamp, value, self._sensor_type))

        # If no status has been passed, try to perform auto-check
        if set_status is None:

            # Currently only with int,float type sensors
            try:
                if self.lower_error == self.upper_error == 0.0:
                    set_status = self.SensorStatus.nominal
                elif self.lower_error <= float(value) <= self.upper_error:
                    set_status = self.SensorStatus.nominal
                else:
                    set_status = self.SensorStatus.error
            except:
                set_status = self.SensorStatus.nominal

        # Call the base function
        super(KatcpSensor, self).set_value(value, status=int(set_status.value), timestamp=timestamp)

        # Add value to values list
        if self.sensor_type_float_int_check():
            self.value_list.insert(0, [float(timestamp), int(set_status.value), float(value)])
        else:
            self.value_list.insert(0, [float(timestamp), int(set_status.value), value])

        # Check list size
        if len(self.value_list) > self._value_list_max_size:
            self.value_list.pop()
        self._was_updated = True

    def value_list_clear(self, keep_one_element=False):
        """
        Clear the value list.
        :return:
        """
        if keep_one_element is True:
            self.value_list = self.value_list[0:1]
        else:
            self.value_list.clear()

        self._was_updated = False

    def sensor_type_float_int_check(self):
        """
        Checks if the sensor type is either float or int.
        :return: True->Type is flot/int.
        """
        return self._sensor_type in [Sensor.FLOAT, Sensor.INTEGER]

    def read(self):
        """
        Read out most current value, timestamp, status from list.
        Overrides the base read function since it is not used here.
        :return:
        """
        # Reset update flag
        self._was_updated = False

        # Return default value in case the list is empty
        if len(self.value_list) == 0:
            if self.type in [int, float]:
                return [0.0, int(self.SensorStatus.error.value), 0.0]
            else:
                return [0.0, int(self.SensorStatus.error.value), 0]
        return self.value_list[0]

    def is_initialized(self):
        """
        Reads back the status of initialized flag.
        :return: bool.
        """
        return self._initialized

    def set_initialized(self, initialized):
        """
        Sets the status of initialized flag.
        :param initialized: bool.
        :return:
        """
        self._initialized = initialized

    def was_updated(self):
        """
        True if sensor was updated since last read().
        :return:
        """
        return self._was_updated
