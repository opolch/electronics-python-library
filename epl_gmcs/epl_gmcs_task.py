from enum import Enum, auto
from epl.epl_logging.epl_logging_log import Log
from epl.epl_gmcs.epl_gmcs_config import *
from epl.epl_gmcs.epl_gmcs_action import ActionInfo, ActionTrigger
from epl.epl_types.epl_types_convert import type_name_convert
from epl.epl_datetime.epl_datetime_datetime import dt_now_tz_timestamp_s


class TaskResult:
    """
    Class defining a generic result object for update callbacks.
    """

    class TaskResultSuccess(Enum):
        """
        Enum defining the success of a task callback execution.
        """
        NO_CALLBACK = auto()
        EXCEPTION = auto()
        SUCCESS = auto()
        NO_NEW_VALUE = auto()

    def __init__(self, success: TaskResultSuccess, ret_val: object, error_state: SensorStatus):
        """
        Inits the object.
        :param success: TaskResultSuccess defining whether the callback execution was successful at all.
        :param ret_val: The return value of the callback.
        :param error_state: Gmcs SensorStatus
        """
        self.success = success
        self.ret_val = ret_val
        self.error_state = error_state


class LastCurrentValue:
    """
    Class to store the last result of a current value together with the corresponding timestamp.
    """

    def __init__(self, max_period_no_new_value_s: int, value=None, timestamp_s=0):
        """
        Init.
        :param max_period_no_new_value_s: Max period in seconds in which a constant value will not be updated.
        :param value: The value.
        :param timestamp_s: The corresponding timestamp (in seconds_granularity).
        """
        self._value = value
        self._timestamp_s = timestamp_s
        self._max_period_no_new_value_s = max_period_no_new_value_s

    @property
    def last_value(self):
        """
        Returns the last current value.
        :return:
        """
        return self._value

    def set_value(self, value, timestamp_s: int):
        """
        Sets value and timestamp.
        :param value: Value (type cast).
        :param timestamp_s: Timestamp in seconds granularity.
        :return:
        """
        self._value = value
        self._timestamp_s = timestamp_s

    def max_period_exceeded(self, check_timestamp_s: int) -> bool:
        """
        Checks for a period exceeding against a given timestamp.
        :param check_timestamp_s: Timestamp (in seconds granularity) to check against.
        :return: True->Exceeding.
        """

        if check_timestamp_s - self._timestamp_s > self._max_period_no_new_value_s:
            return True
        return False

    def check_change_timeout(self, new_value, timestamp_s) -> bool:
        """
        Checks:
         1. Whether a given value changed compared to the stored value.
         2. Whether the max period in seconds for a constant value was exceeded.
        It will also update the last value in case of 1. OR 2.
        :param new_value: Value to compare against the stored one.
        :param timestamp_s: Timestamp (in seconds granularity) to check against.
        :return: True->Value changed or timeout exceeded so must be pushed; False->No action required.
        """

        # Check for value change and period exceeding
        if new_value != self._value or self.max_period_exceeded(check_timestamp_s=timestamp_s):
            self.set_value(value=new_value, timestamp_s=timestamp_s)
            return True

        return False


class Task:

    def __init__(self, logger: Log, task_name: str, value_type: ValueTypes, options_available: OptionsAvailable,
                 limits: GmcsLimits = None, diagram: GmcsDiagram = None, redundancy: Redundancy = None, display_name="",
                 task_type=TaskType.undefined, unit="", callback_value_update: callable = None,
                 callback_set_value: callable = None, metadata_kwargs={}, max_period_no_new_value_s=30):
        """
        Init the object.
        :param logger: epl_logging.Log
        :param task_name: Name of the task.
        :param value_type: ValueTypes.
        :param options_available: OptionsAvailable
        :param limits: GmcsLimits.
        :param diagram: GmcsDiagram.
        :param redundancy: Redundancy.
        :param display_name: Display name.
        :param task_type: Tasks type as per TaskType enum (only with task node_type)
        :param unit: Value's unit.
        :param callback_value_update: The callback to be invoked when updating a read value.
        :param callback_set_value: The callback to be invoked when setting a value.
        :param metadata_kwargs: Dict to be passed as kwargs for metadata.
        :param max_period_no_new_value_s: Max period in seconds in which a constant value will not be updated.
        """

        self._logger = logger

        # Init the node info
        self.node_info = NodeInfo(node_type=NodeType.task, node_name=task_name, metadata=Metadata(
            value_type=value_type.value,
            options_available=options_available,
            limits=limits,
            display_name=display_name,
            diagram=diagram,
            task_type=task_type.value,
            unit=unit,
            redundancy=redundancy,
            **metadata_kwargs
        ), devices=None, tasks=None, node_id="", init_dict=None)

        # Callbacks for value_update and set_value
        self._callback_value_update = callback_value_update
        self._callback_set_value = callback_set_value

        # Field for the current task value
        self._current_value = None

        # Actions
        self._actions = list()
        self._actions_pending = list()
        self._actions_pending_max = 1000

        # Last task value and the period limit
        self._last_current_value = LastCurrentValue(max_period_no_new_value_s=max_period_no_new_value_s)

    @property
    def value_type_as_type(self):
        return type_name_convert(self.node_info.metadata.value_type)

    @property
    def value_type_as_str(self) -> str:
        return self.node_info.metadata.value_type

    @property
    def current_value(self):
        return self._current_value

    @property
    def display_name(self) -> str:
        return self.node_info.metadata.display_name

    @property
    def tree_name(self) -> str:
        return self.node_info.tree_name

    @property
    def redundant_node_id(self) -> str:
        try:
            return self.node_info.metadata.redundancy.redundant_node_id
        except:
            return None

    @property
    def redundancy_std_dev(self) -> float:
        try:
            return self.node_info.metadata.redundancy.redundancy_std_dev
        except:
            return None

    @property
    def redundancy_count(self) -> float:
        try:
            return self.node_info.metadata.redundancy.redundancy_count
        except:
            return None

    @property
    def node_id(self):
        """
        Returns the task's node id.
        :return:
        """
        return self.node_info.node_id

    @property
    def task_name(self):
        """
        Returns the task's task name.
        :return:
        """
        return self.node_info.node_name

    @property
    def has_actions(self):
        """
        Indicator whether a task has any actions.
        :return: True->Yes.
        """
        return len(self._actions) > 0

    def action_add(self, action_info: ActionInfo):
        """
        Adds an action to the actions-list.
        :param action_info: The action_info object to be added to the task.
        :return:
        """

        self._actions.append(action_info)

    def _actions_execute(self) -> [ActionTrigger]:
        """
        Executes actions.
        This function must be called whenever a value-update for the current value occurred.
        :return: List with pending actions as ActionTrigger objects
        """

        # Loop over all actions
        for action in self._actions:

            # Check whether an action was triggered
            action_payload = action.check_for_trigger(current_value=self.current_value,
                                                      last_value=self._last_current_value.last_value)

            # Any triggered?
            if action_payload:
                if len(self._actions_pending) < self._actions_pending_max:
                    self._actions_pending.append((action, action_payload))
                else:
                    self._logger.write_error(f"Max number of pending actions reached `{self._actions_pending_max}`")

        # Return pending actions
        return self.actions_pending

    @property
    def actions_pending(self) -> []:
        """
        Returns pending actions.
        :return:
        """
        # TODO as Queue not list
        return self._actions_pending

    def actions_pending_clear(self):
        """
        Clears the list with pending actions.
        :return:
        """

        return self._actions_pending.clear()

    def current_value_set(self, current_value) -> bool:
        """
        Set the current value in respect to the value_type.
        :param current_value: Value to be set.
        :return: True->Success; False->Type Convert error.
        """
        try:
            if isinstance(current_value, list) or isinstance(current_value, dict):
                self._current_value = current_value
            else:
                self._current_value = self.value_type_as_type(current_value)
            return True
        except Exception as e:
            return False

    def check_limits(self, check_value) -> SensorStatus:
        """
        Checks a value for violation against the task's limits.
        :param check_value: The value to be checked.
        :return: SensorStatus
        """

        # Limit check only supported for float and int type tasks
        if self.node_info.metadata.value_type in [ValueTypes.int.value, ValueTypes.float.value]:

            # Error first
            if self.node_info.metadata.limits.lower_err or self.node_info.metadata.limits.upper_err:
                if not self.node_info.metadata.limits.lower_err <= check_value <= \
                       self.node_info.metadata.limits.upper_err:
                    return SensorStatus.ERROR
            # Now warning
            if self.node_info.metadata.limits.lower_warn or self.node_info.metadata.limits.upper_warn:
                if not self.node_info.metadata.limits.lower_warn <= check_value <= \
                       self.node_info.metadata.limits.upper_warn:
                    return SensorStatus.WARN
        return SensorStatus.NOMINAL

    def execute_callback_get(self):
        """
        Execute the value_update_callback and return the generic TaskResult.
        :return: TaskResult
        """
        # The return value
        result = TaskResult(success=TaskResult.TaskResultSuccess.NO_CALLBACK,
                            ret_val=None, error_state=SensorStatus.UNREACHABLE)

        try:
            # Does the task provide a callback function and has GET option?
            if ((self._callback_value_update and
                 self.node_info.metadata.options_available)
                    in [OptionsAvailable.GET, OptionsAvailable.GET_SET]):

                # Update the current value
                self.current_value_set(self._callback_value_update())

                # Trigger actions
                # TODO
                self._actions_execute()

                # Check whether value changed
                if self._last_current_value.check_change_timeout(new_value=self._current_value,
                                                                 timestamp_s=dt_now_tz_timestamp_s()):
                    # Set result values
                    result.ret_val = self._current_value
                    result.error_state = self.check_limits(result.ret_val)
                    result.success = TaskResult.TaskResultSuccess.SUCCESS
                else:
                    # Indicate nothing changed
                    result.success = TaskResult.TaskResultSuccess.NO_NEW_VALUE

        except Exception as e:
            # Write log and indicate a general error.
            self._logger.write_exception(f"Error during value_update; ex:{e}")
            result.success = TaskResult.TaskResultSuccess.EXCEPTION

        return result

    def execute_callback_set(self, set_value) -> TaskResult:
        """
        Execute the _callback_set_value and return the generic TaskResult.
        :param set_value: Value to be set (will be type cast by task's value-type).
        :return: TaskResult
        """
        # The return value
        result = TaskResult(success=TaskResult.TaskResultSuccess.NO_CALLBACK,
                            ret_val=None, error_state=SensorStatus.UNREACHABLE)

        try:
            # Does the task provide a callback function and has GET option?
            if self._callback_set_value and \
                    self.node_info.metadata.options_available in \
                    [OptionsAvailable.SET, OptionsAvailable.GET_SET]:

                # Cast set value
                set_value_cast = self.value_type_as_type(set_value)

                # Check whether value to be set is within limits
                result.error_state = self.check_limits(check_value=set_value_cast)

                if result.error_state == SensorStatus.NOMINAL:
                    # Execute callback and set new value type cast
                    result.ret_val = self._callback_set_value(set_value_cast)

                # If we come here, execution was successful
                result.success = TaskResult.TaskResultSuccess.SUCCESS
        except Exception as e:
            # Write log and indicate a general error.
            self._logger.write_exception(f"Error during value_update; ex:{e}")
            result.success = TaskResult.TaskResultSuccess.EXCEPTION

        return result
