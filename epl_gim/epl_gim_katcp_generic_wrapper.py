from epl.epl_katcp.epl_katcp_client import KatcpClient
from epl.epl_katcp.epl_katcp_sensor import KatcpSensor, Sensor
from epl.epl_katcp.epl_katcp_server import ServerTask
from epl.epl_katcp.epl_katcp_request import KatcpRequest
from epl.epl_katcp.epl_katcp_gim import KatcpGimInfo
from epl.epl_katcp.epl_katcp_command import CommandState
from gim_exporter.gim import GIM
from epl.epl_string.epl_string_split import split_always_list
from epl.epl_string.epl_string_search import wildcard_search
from epl.epl_string.epl_string_convert import str_to_base64_str
from epl.epl_gim.epl_gim_exporter import GenericGimExporter
from epl.epl_datetime.epl_datetime_datetime import dt_now_tz_timestamp
from epl.epl_types.epl_types_convert import type_name_convert
from katcp import Message
from copy import deepcopy
from epl.epl_katcp.epl_katcp_packetizer_noqt import Packetizer
import asyncio
import time
from enum import Enum, auto
import copy
import json


class GimGenericWrapper:
    """
    Main class for the KatcpServerWrapper
    """

    class WrapperRootUsage(Enum):
        """
        Enum to define the usage of the wrapper_root argument
        """
        NONE = auto()  # Don't use at all
        GIM_CONFIG = auto()  # Use with gim-config
        WRAPPER_PREFIX = auto()  # Use as root-prefix for wrapper sensors/requests
        DISPLAY = auto()  # Use for displaying purposes

    # Requests to be ignored (mostly because they are added by the wrapper-katcp-server again)
    _REQ_IGNORE_MAP = ['arb', 'client-list', 'define', 'dict', 'dispatch', 'get', 'halt', 'help', 'job',
                       'listener-create', 'log-default', 'log-level', 'log-limit', 'log-record', 'notice',
                       'process',
                       'restart', 'search', 'sensor', 'sensor-limit', 'sensor-list', 'sensor-sampling',
                       'sensor-sampling-clear', 'sensor-value', 'set', 'spectrometer', 'system-info',
                       'version',
                       'version-list', 'watchdog']

    # Dict to map client to server sensors
    _WRAPPER_SENSOR_MAP = {}

    # Dict to map generic client to server requests
    _WRAPPER_REQ_MAP = {}

    _tasklist = {}

    _send_data_bulk = {}

    # Tasks to be treated special when handling error states
    _TASKS_SPECIAL_STATE_HANDLING = {}

    def __init__(self, server_host, server_port, server_sensor_update_rate, server_root,
                 wrapper_root, wrapper_root_usage, command_white_list, wrapper_sampling_strategy,
                 wrapper_sampling_strategy_args, prevent_device_name_shadowing, log_level, logger_main, logger_katcp,
                 logger_exporter, gim_url, gim_user, gim_pass, gim_redis_url,
                 gim_parent, gim_policy, gim_sensor_min_update_rate,
                 nbatch, loop, gim_info_definitions=None
                 ):
        """
        Inits the wrapper object.
        :param server_host: Host of server-katcp-server to be wrapped.
        :param server_port: Port of server-katcp-server to be wrapped.
        :param server_sensor_update_rate: Update-rate for server sensors (period-based sampling only).
        :param server_root: Root name of server (used with spectra snapshot).
        :param wrapper_root: Root name to prefix all the wrapper sensors/requests with.
        :param wrapper_root_usage: Defines the usage of the wrapper_root prefix.
        :param command_white_list: List with sensor/request names to be wrapped (empty->add-all, others ignored).
        :param wrapper_sampling_strategy: Sampling strategy to be used for gim-config with the katcp-server.
        :param wrapper_sampling_strategy_args: Arguments to be used with the server sampling strategy.
        :param prevent_device_name_shadowing: True->Ignore sensors/requests that shadow device names (issue with gim)
        :param log_level: log_level
        :param logger_main: Main application logger.
        :param logger_katcp: Katcp module logger.
        :param logger_exporter: gim exporter logger.
        :param gim_url: URL to connect with.
        :param gim_user: The gim user.
        :param gim_pass: GIM user's password.
        :param gim_redis_url : GIM redis stream to connect to
        :param gim_parent: The device's parent device name.
        :param gim_policy: Task generation policies to be invoked when creating tasks.
        :param gim_sensor_min_update_rate: Minimum rate (in s) a sensor will be updated with (in case of no change).
        :param nbatch: Number of updates to collect before performing a batch request.
        :param loop: asyncio.loop
        :param gim_info_definitions: Dict with predefined gim info.
        """

        # General variables
        self._server_host = server_host
        self._server_port = server_port
        self._server_sensor_update_rate = server_sensor_update_rate
        self._server_root = server_root
        self._wrapper_root = wrapper_root
        self._wrapper_root_usage = wrapper_root_usage
        self._white_list = command_white_list if command_white_list is not None else []
        self._log_level = log_level
        self._logger_main = logger_main
        self._logger_katcp = logger_katcp
        self._wrapper_sampling_strategy = wrapper_sampling_strategy
        self._wrapper_sampling_strategy_args = wrapper_sampling_strategy_args
        self._main_thread_stop = False
        self._katcp_server = None
        self._prevent_device_name_shadowing = prevent_device_name_shadowing
        self._nbatch = nbatch
        self._loop = loop
        self._heart_beat_task = None

        # Predefined gim info definitions
        # Wrap the keys (pop original entries)
        self._gim_info_definitions = dict()
        for sensor_name in gim_info_definitions.keys():
            self._gim_info_definitions[
                self._wrap_sens_req_name(sensor_name=sensor_name, purpose=self.WrapperRootUsage.WRAPPER_PREFIX)] = \
                copy.deepcopy(gim_info_definitions[sensor_name])

        # Specific message handles
        # Need to be defined here since the handles need the `self` object
        self._MESSAGE_HANDLES = {
            'sensor-status':
                (self._handle_sensor_status_reply, self._handle_sensor_status_inform),
        }

        # Dict to map special server requests: not simply ones to be forwarded to client
        # Needs to be defined here since the handles need the `self` object
        # {req_name:[KatcpCommand, special_callback]}
        self._WRAPPER_REQ_SPECIAL_MAP = {}

        # Init a katcp client to connect the server
        self._katcp_client = KatcpClient(device_host=self._server_host,
                                         device_port=self._server_port,
                                         device_name='wrapper',
                                         katcp_root_name=self._server_root,
                                         connect_timeout=5,
                                         init_timeout=float(10),
                                         update_sensors_dict=dict(),
                                         command_list=dict(),
                                         auto_reconnect=True,
                                         sensor_period_default=self._server_sensor_update_rate,
                                         logger=self._logger_katcp,
                                         message_handles=self._MESSAGE_HANDLES,
                                         description_cleanup_handle=self._description_clean_up,
                                         log_limit="off")

        self._gim_req_queue = asyncio.Queue()
        self._gim_ack_queue = asyncio.Queue()

        # Create gim exporter
        self._exporter = GenericGimExporter(gim_url=gim_url, gim_user=gim_user, gim_pass=gim_pass,
                                            gim_redis_url=gim_redis_url,parent_device_name=gim_parent,
                                            req_queue=self._gim_req_queue,ack_queue=self._gim_ack_queue,
                                            policies=gim_policy, override_gim=True,
                                            logger=logger_exporter, sensor_min_update_rate_s=gim_sensor_min_update_rate)
        # Create heartbeat task, not starting it now         
        self._logger_main.write_info("Adding heartbeat sensors...")         
        self._loop.create_task(self._exporter.heartbeat(initial_wait_time=10, heartbeats_connected=False))

    async def _handle_requests(self):
        """
        Forward incoming requests to client(katcp)
        Needs to be modified in case of different client
        :param:
        :return:
        """
        while True:
          
          await asyncio.sleep(0.01)

          if not self._gim_req_queue.empty():

            self._logger_main.write_info("Got Requests from exporter...")

            try:
              _task = await self._gim_req_queue.get()

              if _task.task_name in self._WRAPPER_REQ_SPECIAL_MAP:
                command = self._WRAPPER_REQ_SPECIAL_MAP[_task.task_name.replace('.', '-')][0]
                self._WRAPPER_REQ_SPECIAL_MAP[_task.task_name][1](command)
              else:
                # Get related command
                _command = self._WRAPPER_REQ_MAP[_task.task_name.replace('.', '-')]    
                # katcp handling
                _command.command_set(command_state=CommandState.INIT)
                
                # Create request
                _send_message = '?{} {}'.format(_command.command_name,_task.desired_value)

                # Send
                self._logger_main.write_info("Sending request: {}".format(_send_message))
                self._katcp_client.send_request(_send_message)

   
                # Wait for the server's response
                _wait_response = 0
                _inform_message = ''

                while 1:
                  if _command.command_state != CommandState.INIT:
                    break
                  elif _wait_response > 300:
                    _reply_message = 'Timeout, no answer from the server'
                    _command.command_set(CommandState.FAIL, reply_message=_reply_message)
                  _wait_response += 1
                  await asyncio.sleep(0.01)

                # In case of != OK, at least some info for the client
                if _command.command_state != CommandState.OK and _command.reply_message == '':
                   _reply_message = 'Unknown, No Info from the server'
                   _command.command_set(_command.command_state, reply_message=_reply_message)
                   _task.stream_data['pending_request'] =GIM.TASK_FAILED
                   _task.stream_data['last_error_message'] = _reply_message
                else:
                   _reply_message  = str(_command.reply_message.decode())

                   for _msg in _command.inform_message:
                       if isinstance(_msg, list):
                           _inform_message = _inform_message + ' '.join(''.join(_imsg.decode()) for _imsg in _msg)
                       else:
                           _inform_message = _inform_message + _msg  
                   
                   _error_state = _command.command_state.value
                   _error_key   = str(_command.command_state.name)
                   if _error_state is None:
                       _error_state = 2

                   self._logger_main.write_info("Request executed for task:{},response:{}".format(_task.task_name,_reply_message))

                   # Default message for SET if none sent by server
                   _kmsg = _error_key + ':' + _reply_message + ' ' + _inform_message
                   if len(_kmsg) < 3:
                      _kmsg = 'Set Request Completed.'

                   # Create Reply message and push to Ack Queue
                   _task.stream_data['pending_request'] = GIM.TASK_COMPLETE
                   _task.stream_data['last_error_message'] = _kmsg 
                   _task.stream_data['error_state'] = _error_state
                   
                   
                   if _error_state == 1:
                      _task.stream_data['current_value'] = _task.desired_value 
                
                await self._gim_ack_queue.put(_task)
                self._gim_req_queue.task_done()
            except Exception as e:
                # Set request result to inform client
                self._logger_main.write_exception("Error during execution of request for task:{}, error:{}".format(_task.task_name,e))
                _task.stream_data['pending_request'] = GIM.TASK_FAILED 
                _task.stream_data['last_error_message'] = str(e)
                await self._gim_ack_queue.put(_task)
            finally:
                await asyncio.sleep(0.001)

    async def main_loop(self):
        """
        Main packer_wrapper loop.
        It initializes the connections and implements an endless loop pushing data to gim.
        :return:
        """

        # Init the exporter
        await self._exporter.init_connect()
        
        await self._exporter.redis_connect()

        self._logger_main.write_info("Init of SET Queues")
        self._loop.create_task(self._exporter.redis_get_streams())                     
        self._loop.create_task(self._exporter.redis_ack_streams())                     
        self._loop.create_task(self._handle_requests())
        
        # Endless loop to connect client and init server
        while self._main_thread_stop is False:

            try:

                # If uninitialized, initialize
                if self._katcp_client.device_state_get(keep=True) != KatcpClient.DeviceStates.INITIALIZED:

                    # Wait for client to be initialized
                    while self._katcp_client.device_state_get(keep=True) != KatcpClient.DeviceStates.INITIALIZED:

                        # Set heartbeats to disconnected
                        self._exporter.heartbeats_connected = 0

                        # Make sure to stop also during init
                        if self._main_thread_stop:
                            return

                        # Init the client
                        self._logger_main.write_info(
                            'Initializing client {}:{}.'.format(self._server_host, self._server_port))
                        self._katcp_client.init_connect()

                        # If not successful, be patient...
                        await asyncio.sleep(1)

                    # Create tasklist based on katcp client sensor/request list
                    self._logger_main.write_info("Creating task list...")
                    self._create_katcp_tasks()

                    # Sync gim tree
                    self._logger_main.write_info("Syncing gim tree...")
                    await self._exporter.update_from_tasklist(self._tasklist)

                    # Set heartbeats to connected
                    self._exporter.heartbeats_connected = 1

                # Send next bulk of data
                len_bulk = sum([len(x) for x in self._send_data_bulk.values()])

                # Reduce logging
                if not len_bulk % 10:
                    self._logger_main.write_debug(f"Actual batch length: {len_bulk}; "
                                                 f"another {self._nbatch - len_bulk} left...")

                # Bulk len reached?
                if len_bulk > self._nbatch:
                    # Send bulk data
                    self._logger_main.write_debug(f"Processing Batch with {len_bulk} entries")
                    await self._exporter.sensor_bulk_update(self._send_data_bulk)

                    # Clear bulk
                    self._send_data_bulk.clear()

            except Exception as e:
                self._logger_main.write_exception("Some error in execution of main loop; ex: {}".format(e))
            finally:
                # Give resources to other tasks
                await asyncio.sleep(0.01)

    def _wrap_sens_req_name(self, sensor_name=None, request_name=None, purpose=WrapperRootUsage.NONE):
        """
        Returns the wrapper-sensor name for a given server_sensor_name.
        :param sensor_name: Name of server sensor.
        :param request_name: Name of server request.
        :param purpose: WrapperRootUsage enum to define the purpose of the wrapped sensor_name.
        :return: Wrapped name, when purpose in self._wrapper_root_usage. Returns None in case no names are passed.
        """
        # Check whether purpose must be wrapped
        if purpose.value in [e.value for e in self._wrapper_root_usage] and purpose is not self.WrapperRootUsage.NONE:
            # Only if we have a wrapper_root prefix
            if self._wrapper_root != '':
                # Distinguish sensor/request
                if sensor_name is not None:
                    return '{}.{}'.format(self._wrapper_root, sensor_name)
                elif request_name is not None:
                    return '{}-{}'.format(self._wrapper_root, request_name)

        # Else simply return the original name
        return sensor_name if sensor_name is not None else request_name

    def _handle_sensor_status_inform(self, katcp_client, message):
        """
        Specific handler for the sensor-status inform.
        :param message: Entire katcp message
        :return:
        """

        # Convert katcp message object to string
        message_string = str(message)

        # Split message fields
        _, timestamp, _, sensor_name, sensor_status, current_value = message_string.split()

        # Handle sensor status
        sensor_status = KatcpSensor.SensorStatus[sensor_status].value

        # Some sensors need special treatment
        # if sensor_name in self._tasks_special_state:
        #     if self._tasks_special_state[sensor_name] == SpecialStateHandling.no_checks:
        #         sensor_status = KatcpSensor.SensorStatus.NOMINAL.value
        #     elif self._tasks_special_state[sensor_name] == SpecialStateHandling.limit_checks_own:
        #         sensor_status = None

        # Get sensor
        try:
            # Get wrapped name
            sensor_name_wrapped = self._wrap_sens_req_name(sensor_name=sensor_name,
                                                           purpose=self.WrapperRootUsage.WRAPPER_PREFIX)
            # Look up task
            task = self._tasklist[sensor_name_wrapped]

            # Add data
            self._bulk_data_add(sensor_name_wrapped, task.gim_info.value_type, sensor_status, current_value, None)

        except KeyError:
            self._logger_main.write_debug('Sensor {} not in wrapper sensor list'.
                                          format(sensor_name))

    def _handle_sensor_status_reply(self, katcp_client, message):
        """
        Specific handler for the sensor-status inform.
        :param message: Entire katcp message
        :return:
        """
        pass

    def _description_clean_up(self, help_inform_message):
        """
        Clean up the help messages.
        :param help_inform_message: help inform message.
        :return:
        """

        # Decode description
        description_string = help_inform_message.arguments[1].decode()

        # Do clean-up
        # eg. description_string = description_string.replace('||', '|')

        # Encode description again
        help_inform_message.arguments[1] = description_string.encode()

        return help_inform_message

    def _create_katcp_tasks(self):
        """
        Creates KATCP sensors/requests from the katcp_client's task-list and stores them
        in a list that can be passed through the server to be registered there.
        :return: None
        """

        def _device_shadowing_check(shadowing_sensor_name):
            """
            Check whether a given sensor name shadows an existing device name in the task_list.
            Note: No manipulation will be performed on the sensor/device-name.
            :param shadowing_sensor_name: The sensor-name to look for in the task_list.
            :return: True->Ok, sensor can be added; False->Sensor shadows and must be dropped.
            """

            # Prevent device name shadowing
            for shadowing_task_name in self._tasklist.keys():

                # Get lengths
                _shadowing_sensor_name_len = len(shadowing_sensor_name.split('.'))
                _shadowing_task_name_len = len(shadowing_task_name.split('.'))

                # A two-way search must be performed. Since we are looking for device-names, entry
                # with more hierarchy levels must be stripped by last one.
                if _shadowing_sensor_name_len > _shadowing_task_name_len:
                    if shadowing_sensor_name.rsplit('.', 1)[0] == shadowing_task_name:
                        return False
                elif _shadowing_task_name_len > _shadowing_sensor_name_len:
                    if shadowing_task_name.rsplit('.', 1)[0] == shadowing_sensor_name:
                        return False

            # No shadowing found
            return True

        def _white_list_check(white_list_sensor_name):
            """
            Check sensor name against white_list.
            :param white_list_sensor_name: The sensor same to check for.
            :return: True->Found in white_list or white_list empty.
            """
            # Helper for return value
            ret_val = False

            # Empty white_list?
            if len(self._white_list) == 0:
                return True

            # Iterate over white_list and perform the search
            for entry in self._white_list:
                if wildcard_search(white_list_sensor_name, entry) is True:
                    ret_val = True

            return ret_val

        # 1. Add sensor/request pairs based on sensors
        for sensor_name, sensor in self._katcp_client.SENSOR_MAP.items():

            # Check whether sensor is in white list
            if _white_list_check(sensor_name) is False:
                continue

            # Convert sensor names to wrapper names
            sensor.name = self._wrap_sens_req_name(sensor_name=sensor_name,
                                                   purpose=self.WrapperRootUsage.WRAPPER_PREFIX)

            # Prevent device name shadowing ?
            if self._prevent_device_name_shadowing is True:
                if _device_shadowing_check(sensor.name) is False:
                    self._logger_main.write_warning('{} ignored to prevent device name shadowing'.
                                                    format(sensor.name))
                    continue

            # New task to be added
            new_task = ServerTask()

            # Add sensor
            new_task.katcp_sensor = deepcopy(sensor)

            # Check for corresponding request
            try:
                # Try to find the command
                command = self._katcp_client.COMMAND_MAP[sensor_name.replace('.', '-')]

                # Ignore certain requests
                if command.command_name not in self._REQ_IGNORE_MAP:
                    # Create request
                    new_task.katcp_request = KatcpRequest(command.command_type, command.command_name,
                                                          command.description, '', command.values)
                    # Add link to the wrapper_map
                    self._WRAPPER_REQ_MAP[sensor.name.replace('.', '-')] = command
            except Exception as e:
                self._logger_main.write_info("failed...{}".format(e))
                pass
             # Add new task to tasks dict (to be added to server)
            self._tasklist[sensor.name] = new_task

            # Add link to the wrapper_map
            self._WRAPPER_SENSOR_MAP[sensor_name] = new_task.katcp_sensor
            
        # 2. Add standalone requests
        for command_name, command in self._katcp_client.COMMAND_MAP.items():

            # Check whether command is in white list
            if _white_list_check(command_name) is False:
                continue

            # Ignore certain requests
            if command_name in self._REQ_IGNORE_MAP:
                continue

            # Convert command names to wrapper names
            command_name_add = self._wrap_sens_req_name(request_name=command.command_name,
                                                        purpose=self.WrapperRootUsage.WRAPPER_PREFIX)

            # Create the command name for the dict lookup
            command_name_search = command_name_add.replace('-', '.')

            # Check whether command is already in task list
            command_present = False
            for search_task in self._tasklist.keys():
                if command_name_search == search_task.replace('-', '.'):
                    command_present = True
                    break

            if command_present is True:
                continue

            # Prevent device name shadowing?
            if self._prevent_device_name_shadowing is True:
                if _device_shadowing_check(command_name_search) is False:
                    self._logger_main.write_warning('{} ignored to prevent device name shadowing'.
                                                    format(command_name_search))
                    continue

            # New task to be added
            new_task = ServerTask()

            # Create request
            new_task.katcp_request = KatcpRequest(command.command_type, command_name_add,
                                                  command.description, '', command.values)

            # Add new task to tasks dict
            self._tasklist[command_name_search] = new_task

            # Add link to the wrapper_map
            self._WRAPPER_REQ_MAP[command_name_add] = command

        # 3. gim-info
        for task_name_search, task in self._tasklist.items():

            # Check whether task has a predefined gim-config definition
            if task_name_search in self._gim_info_definitions:
                task.gim_info = self._gim_info_definitions[task_name_search]
            else:
               if task.katcp_sensor is not None:
                 if Sensor.parse_type(task.katcp_sensor.type) != Sensor.DISCRETE:
                    value_type = task.katcp_sensor.type
                 else:
                    value_type = 'str'
                 if len(task.katcp_sensor.params) > 0:
                    gim_limits = task.katcp_sensor.params
                 else:
                    gim_limits = None
               else:
                    if len(task.katcp_request.params) > 0:
                        gim_limits = task.katcp_request.params
                    else:
                        gim_limits = None
                    value_type = task.katcp_request.type

               if task.katcp_request is None:
                  mysql_task_type = KatcpGimInfo.WidgetType.GET
                  options_available = KatcpGimInfo.OptionsAvailable.GET
               elif task.katcp_sensor is None:
                  mysql_task_type = KatcpGimInfo.WidgetType.SET
                  options_available = KatcpGimInfo.OptionsAvailable.SET
               else:
                  if Sensor.parse_type(task.katcp_sensor.type) == Sensor.DISCRETE:
                     mysql_task_type = KatcpGimInfo.WidgetType.DROPDOWN
                  elif Sensor.parse_type(task.katcp_sensor.type) == Sensor.BOOLEAN:
                     mysql_task_type = KatcpGimInfo.WidgetType.BOOL
                  else:
                     mysql_task_type = KatcpGimInfo.WidgetType.GET_SET
               options_available = KatcpGimInfo.OptionsAvailable.GET_SET

               task.gim_info = KatcpGimInfo(logger=self._logger_main, value_type=value_type,
                                    mysql_task_type=mysql_task_type,
                                    sampling_strategy=self._wrapper_sampling_strategy
                                    if task.katcp_sensor is not None else None,
                                    sampling_strategy_params=self._wrapper_sampling_strategy_args
                                    if task.katcp_sensor is not None else None,
                                    description=task.katcp_sensor.description if task.katcp_sensor is not None
                                    else task.katcp_request.description,
                                    options_available=options_available,
                                    display_name=split_always_list(task_name_search, '.')[-1],
                                    limits=gim_limits)

               #self._tasklist[task_name_search] = task

        # Special requests
        for req_name, req_details in self._WRAPPER_REQ_SPECIAL_MAP.items():
            # KatcpCommand
            new_command = req_details[0]

            new_task = ServerTask()

            # Create request
            new_task.katcp_request = KatcpRequest(new_command.command_type, new_command.command_name,
                                                  new_command.description, '', new_command.values)

            # Add new task to tasks dict
            self._tasklist[req_name] = new_task
    

    def _bulk_data_add(self, sensor_name, value_type, sensor_status, current_value=None, data=None):
        """
        Add data to the bulk send data.
        :param sensor_name: The wrapped sensor name.
        :param value_type: value type as string.
        :param sensor_status: Sensor status as str/int (refer to KatcpSensor.SensorStatus).
        :param current_value: The current value (if data type task, omit).
        :param data: Data for data type tasks (if no data type task, omit).
        :return:
        """

        # Add data
        if sensor_name not in self._send_data_bulk:
            self._send_data_bulk[sensor_name] = []
        self._send_data_bulk[sensor_name].append(
            {
                "timestamp": "{}".format(dt_now_tz_timestamp()),
                "current_value": type_name_convert(value_type)(current_value) if current_value else None,
                "data": str_to_base64_str(data) if data else None,
                "value_type": value_type,
                "error_state": "{}".format(sensor_status),
            })

    def update_graph_data(self, spec_data):
        """
        Updates graph sensors.
        :param spec_data: SnapshotSpecData object.
        :return:
        """
        try:
            # Update snapshot sensor
            self._bulk_data_add(Packetizer.SnapshotSpecData.FieldNames.adc0.name,
                                "str", "1", None,
                                str(spec_data.signal_data[
                                        Packetizer.SnapshotSpecData.FieldNames.adc0.name].signal_data).
                                replace(' ', ''), )
            self._bulk_data_add(Packetizer.SnapshotSpecData.FieldNames.adc1.name,
                                "str", "1", None,
                                str(spec_data.signal_data[
                                        Packetizer.SnapshotSpecData.FieldNames.adc1.name].signal_data).
                                replace(' ', ''))
            self._bulk_data_add(Packetizer.SnapshotSpecData.FieldNames.level0.name,
                                "str", "1", None,
                                str(spec_data.level_data[
                                        Packetizer.SnapshotSpecData.FieldNames.level0.name].level_data).
                                replace(' ', ''))
            self._bulk_data_add(Packetizer.SnapshotSpecData.FieldNames.level1.name,
                                "str", "1", None,
                                str(spec_data.level_data[
                                        Packetizer.SnapshotSpecData.FieldNames.level1.name].level_data).
                                replace(' ', ''))
            self._bulk_data_add(Packetizer.SnapshotSpecData.FieldNames.header.name,
                                "str", "1", None,
                                json.dumps(spec_data.header_data.dict_from_class()))

        except Exception as e:
            self._logger_main.write_exception('Error updating spectra data, ex:{}'.format(e))

    def shutdown(self):
        """
        Stops all related threads and modules.
        :return:
        """
        # Stop client and server
        self._katcp_client.stop()
        self._main_thread_stop = True
