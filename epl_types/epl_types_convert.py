_TYPES_LUT = {
    'int': int,
    'float': float,
    'double': float,
    'str': str
}


def type_name_convert(type_as_string):
    """
    Converting a type name to actual type.
    :param type_as_string: Type name; e.g. 'float'
    :return:
    """
    return _TYPES_LUT.get(type_as_string, str)
