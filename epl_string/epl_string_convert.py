import base64


def bool_to_int(v):
    """
    Converts string to bool.
    :param v: String holding the bool.
    :return: bool
    """

    if str(v).lower() in ('yes', 'true', '1.0', 'y'):
        return 1
    elif str(v).lower() in ('no', 'false', 'f', 'n', '0', 'none', '0.0'):
        return 0
    else:
        return 0

def str_to_bool(v):
    """
    Converts string to bool.
    :param v: String holding the bool.
    :return: bool
    """
    # If already bool, return
    if isinstance(v, bool):
        return v

    if str(v).lower() in ('yes', 'true', 't', 'y', '1','1.0'):
        return True
    elif str(v).lower() in ('no', 'false', 'f', 'n', '0', 'none', '', '0.0'):
        return False
    else:
        return False


def str_to_base64_str(input_string):
    """
    Converts input_string to a base64 encoded string.
    :param input_string: String to be converted.
    :return:
    """
    return base64.b64encode(input_string.encode()).decode()


def bytes_to_base64_str(input_bytes):
    """
    Converts input_bytes to a base64 encoded string.
    :param input_bytes: Bytes to be converted.
    :return:
    """
    return base64.b64encode(input_bytes).decode()


def tuple_to_string(tup, separator=" "):
    """
    Converts a tuple to a separator-separated string.
    :param tup: Tuple (can contain strings and other data types).
    :param separator: Separator to be used.
    :return: str
    """
    st = separator.join(map(str, tup))
    return st


def list_to_string(lst, separator=" "):
    """
    Converts a list to a separator-separated string.
    :param lst: List (can contain strings and other data types).
    :param separator: Separator to be used.
    :return: str
    """
    st = separator.join(map(str, lst))
    return st


