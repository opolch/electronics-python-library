from epl.epl_dicts.epl_dicts_objects import *
from varname import nameof
from uuid6 import uuid8
import operator


class ActionType(Enum):
    """
    Enum defining an action type.
    """
    # Default
    none = "none"

    # Matching or exceeding of a threshold configuration
    threshold = "threshold"

    # Exceeding of a timeout configuration
    timeout = "timeout"

    # Any change of the sensor should result in an event
    # e.g.:
    # * A temperature sensor changing from 13.31C to 13.32C
    # * A string value changing from ON to OFF
    event_any_change = "event_any_change"

    # A certain number of changes in a given period should result in an event
    # e.g.:
    # * A button pressed three times within a second
    # * A sensor changes its value 5 times within 30s
    event_num_changes = "event_num_changes"

    # A change active for a given period should result in an event
    # e.g.:
    # * A button pressed for 2.5 seconds
    # * A sensor active for a certain period of time
    event_active_period = "event_active_period"


class ActionSendPayload:
    """
    Class defining a send-payload objet to be sent when an action was triggered.
    """

    class PayloadConfig(Enum):
        """
        Enum defining the send-payload configuration for an action.
        """
        # No payload at all
        none = "none"

        # A fixed value; can be some string or fixed number
        fixed_value = "fixed_value"

        # The actual value
        current_value = "current_value"

        # The actual and last value
        current_last_value = "current_last_value"

    def __init__(self, config: PayloadConfig = None, params: list = None):
        """
        Initializes the object.
        :param config: PayloadConfig
        :param params: List with parameters for the PayloadConfig
        """
        self.config = config if config else self.PayloadConfig.none
        self.params = params if params else []

    def init_from_dict(self, init_dict: dict):
        """
        Option to init the object from a dict.
        :param init_dict: dict holding `class_member:value` pairs.
        :return:
        """

        # Init the object from dict
        object_init_from_dict(self, init_dict=init_dict)

    def as_dict(self) -> dict:
        """
        Creates a dict with all class members by recursively iterating.
        It expects objects to implement `as_dict` function.
        :return: dict
        """
        return object_to_dict(self)


class ActionTrigger:
    """
    Class defining a trigger event for an action object.
    """

    def __init__(self):
        pass


class ActionParams:

    def __init__(self, init_dict: [] = None):
        """
        Init the object.
        :param init_dict: Dict to init the object from. Overrides all others.
        """

        self.action_send_payload = ActionSendPayload()

        # Init from dict?
        if init_dict:
            self.init_from_dict(init_dict=init_dict)

    def init_from_dict(self, init_dict: dict):
        """
        Option to init the object from a dict.
        :param init_dict: dict holding `class_member:value` pairs.
        :return:
        """

        # Init the object from dict
        object_init_from_dict(self, init_dict=init_dict)

    def as_dict(self) -> dict:
        """
        Creates a dict with all class members by recursively iterating.
        It expects objects to implement `as_dict` function.
        :return: dict
        """
        return object_to_dict(self)

    def trigger_action(self, current_value, last_value):
        """
        Checks whether this update triggered the configuration.
        :param current_value: Current value (value which has just been set).
        :param last_value: Value before the recent set.
        :return:
        """
        return None


class Threshold(ActionParams):
    """
    Threshold action type.
    """

    class ExceptThresholdType(Exception):
        """
        ...
        """
        pass

    class ThresholdType(Enum):
        """
        Enum defining comparison types for Thresholds.
        """
        none = None
        lt = operator.lt
        le = operator.le
        gt = operator.gt
        ge = operator.ge
        eq = operator.eq
        ne = operator.ne

    """
    Inversion map for finding inversion operations by simple dict lookup.
    """
    _INVERSION_MAP = {
        ThresholdType.lt: ThresholdType.gt,
        ThresholdType.gt: ThresholdType.lt,
        ThresholdType.le: ThresholdType.ge,
        ThresholdType.ge: ThresholdType.le,
        ThresholdType.eq: ThresholdType.ne,
        ThresholdType.ne: ThresholdType.eq,
    }

    def __init__(self, threshold_type: ThresholdType = None, threshold_value=None, inversion:bool=False,
                 init_dict: [] = None):
        """
        Init the object.
        :param threshold_type: ThresholdType enum.
        :param threshold_value: Threshold value to compare against.
        :param inversion: Boolean value defining whether the inverse operation should be performed as well.
        :param init_dict: Dict to init the object from. Overrides all others.
        """

        # Make sure the threshold type is set to an enum value, otherwise the init from dict will fail
        self.th_type = threshold_type if threshold_type else self.ThresholdType.none
        self.th_value = threshold_value if threshold_value else 0.0
        self.th_inversion = inversion
        # TODO hysteresis

        # Init base class what basically inits from dict
        super().__init__(init_dict=init_dict)

        # Make sure the threshold type is valid
        if self.th_type not in Threshold.ThresholdType:
            raise self.ExceptThresholdType(
                f"Threshold type not one of the following valid types `{','.join(x.name for x in self.ThresholdType)}`"
            )

    def trigger_action(self, current_value, last_value):
        """
        Checks whether this update triggered the configuration.
        :param current_value: Current value (value which has just been set).
        :param last_value: Value before the recent set.
        :return: ActionTrigger
        """

        # Perform the operation
        if self.th_type.value(current_value, self.th_value):

            if self.action_send_payload.config == ActionSendPayload.PayloadConfig.fixed_value:
                return self.action_send_payload.params[0]
            # TODO: More to be implemented
            else:
                return None
        # TODO inversion
        elif self.th_inversion:

            if self._INVERSION_MAP[self.th_type].value(current_value, self.th_value):
                if self.action_send_payload.config == ActionSendPayload.PayloadConfig.fixed_value:
                    return self.action_send_payload.params[1]
                else:
                    return None

        return None


class EventAnyChange(ActionParams):
    """
    A certain number of changes in a given period
    e.g.: A button pressed three times within a second
    """

    def __init__(self, init_dict: [] = None):
        """
        Initializes the object.
        :param init_dict: Dict to init the object from. Overrides all others.
        """

        # Init base class what basically inits from dict
        super().__init__(init_dict=init_dict)

    def trigger_action(self, current_value, last_value):
        """
        Checks whether this update triggered the configuration.
        :param current_value: Current value (value which has just been set).
        :param last_value: Value before the recent set.
        :return: ActionTrigger
        """

        # Any change?
        if current_value != last_value:

            # Return a fixed value?
            if self.action_send_payload.config == ActionSendPayload.PayloadConfig.fixed_value:
                return self.action_send_payload.params[0]
            elif self.action_send_payload.config == ActionSendPayload.PayloadConfig.current_value:
                return current_value
            elif self.action_send_payload.config == ActionSendPayload.PayloadConfig.current_last_value:
                return current_value, last_value

        return None


class EventNumChanges(ActionParams):
    """
    A certain number of changes in a given period
    e.g.: A button pressed three times within a second
    """

    def __init__(self, num_changes: int = None, period: float = None, init_dict: [] = None):
        """
        Initializes the object.
        :param num_changes: Number of changes that must occur to fire the event.
        :param period: Max period in which the changes must occur.
        :param init_dict: Dict to init the object from. Overrides all others.
        """
        self.num_changes = num_changes
        self.period = period

        # Init base class what basically inits from dict
        super().__init__(init_dict=init_dict)


class EventActivePeriod(ActionParams):
    """
    A change active for a given period of time.
    e.g.: A button pressed for 2.5 seconds
    """

    def __init__(self, num_changes: int = None, period: float = None, init_dict: [] = None):
        """
        Initializes the object.
        :param period: Max period in which the changes must occur.
        :param init_dict: Dict to init the object from. Overrides all others.
        """
        self.period = period

        # Init base class what basically inits from dict
        super().__init__(init_dict=init_dict)


class Metadata:
    """
    Define an action's metadata.
    """

    def __init__(self, init_dict: [] = None):
        """
        Initialized the object.
        :param init_dict: Dict to init the object from. Overrides all others.
        """

        # Init from dict?
        if init_dict:
            self.init_from_dict(init_dict=init_dict)

    def init_from_dict(self, init_dict: dict):
        """
        Option to init the object from a dict.
        :param init_dict: dict holding `class_member:value` pairs.
        :return:
        """
        object_init_from_dict(self, init_dict)

    def as_dict(self) -> dict:
        """
                Creates a dict with all class members by recursively iterating.
                It expects objects to implement `as_dict` function.
                :return: dict
                """
        return object_to_dict(self)


class ActionInfo:
    """
    Base class to define an action object.
    """

    #
    # Exceptions
    #
    class ExceptNotProperlyInitialized(Exception):
        """
        ...
        """
        pass

    # Dict for mapping action types to corresponding ActionParams derived classes
    action_type_class_mapping = {
        ActionType.threshold.name: Threshold,
        ActionType.event_num_changes.name: EventNumChanges,
        ActionType.event_any_change.name: EventAnyChange,
        ActionType.event_active_period.name: EventActivePeriod,
    }

    def __init__(self, init_dict: [] = None, check_consistency=True):
        """
        Initializes the object.
        Raises ExceptNotProperlyInitialized in case of not properly initialized object.
        :param init_dict: Dict to init the object from. Overrides all others.
        :param check_consistency: Check whether object structure is consistent after initialization.
        """

        self.action_id = ""
        self.description = ""
        self.source_id = list()
        self.destination_id = list()
        self.action_type = ActionType.none
        self.action_params: list[ActionParams] = list()
        self.metadata = Metadata()

        # Init from dict?
        if init_dict:
            self.init_from_dict(init_dict=init_dict)

        # Check object consistency
        if check_consistency:
            self.consistency_check()

    def action_id_fresh_insert(self, overwrite_existing=False):
        """
        Insert a fresh action id.
        :param overwrite_existing: False->Only set if node_id is not set.
        :return:
        """

        # Set id
        if overwrite_existing or not self.action_id:
            # Skip existing when not to be overwritten
            self.action_id = uuid8().hex

    def consistency_check(self):
        """
        Checks whether the object has been properly initialized.
        This is especially important when initializing from dict, where consistency cannot be guaranteed.
        :return:
        """

        def check_not_none(check_att, check_val):
            """
            Performs the actual =!None check.
            :param check_att: The attribute's name.
            :param check_val:  The attribute's value to be checked.
            :return:
            """

            if isinstance(check_val, list):
                # Loop through list elements and perform additional checks
                for list_val in check_val:
                    check_not_none(check_att, list_val)

            if isinstance(check_val, ActionParams):
                # ActionParams objects get checked in detail
                for ap_att, ap_val in vars(check_val).items():
                    check_not_none(ap_att, ap_val)

            # Check != None
            if check_val is None:
                raise self.ExceptNotProperlyInitialized(
                    f"{check_att} not properly initialized, its value is {check_val}")

        # Make sure all attributes have a value != None since
        # None would mean the attribute was not properly initialized
        for att, value in vars(self).items():
            check_not_none(att, value)

    def init_from_dict(self, init_dict: dict):
        """
        Option to init the object from a dict.
        :param init_dict: dict holding `class_member:value` pairs.
        :return:
        """

        try:
            # To properly init the object from dict, we need to know the action type in advance
            self.action_type = ActionType[init_dict[_action_type_name]]

            # Now init the object from dict
            # The list mapping is set up via the object member's names
            object_init_from_dict(self, init_dict=init_dict, list_mapping={
                _action_params_name: self.action_type_class_mapping[self.action_type.value],
                _source_id_name: str,
                _destination_id_name: str,
            })

        except Exception as e:
            raise self.ExceptNotProperlyInitialized(f"Initialization of action object failed", e.args)

    def as_dict(self) -> dict:
        """
        Creates a dict with all class members by recursively iterating.
        It expects objects to implement `as_dict` function.
        :return: dict
        """
        return object_to_dict(self)

    def check_for_trigger(self, current_value, last_value) -> [ActionTrigger]:
        """
        Checks whether this update triggered any action_configuration and returns a list with ActionTrigger objects.
        :param current_value: Current value (value which has just been set).
        :param last_value: Value before the recent set.
        :return: [ActionTrigger]
        """
        triggers = []

        # Loop over actions and check for triggers
        for action_param in self.action_params:

            # Each action param class implements an individual trigger check function
            trigger = action_param.trigger_action(current_value=current_value, last_value=last_value)

            # Any trigger?
            if trigger:
                triggers.append(trigger)

        return triggers


# Variable names to be stored for later use
_dummy_action = ActionInfo()
_action_type_name = nameof(_dummy_action.action_type)
_action_params_name = nameof(_dummy_action.action_params)
_source_id_name = nameof(_dummy_action.source_id)
_destination_id_name = nameof(_dummy_action.destination_id)
