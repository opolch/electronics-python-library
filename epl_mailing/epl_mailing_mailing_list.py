import json
import queue
from enum import Enum, auto
from epl.epl_mailing.epl_mailing_email import EmailErrors
from epl.epl_string.epl_string_convert import str_to_bool
import asyncio
import smtplib
import imaplib
import email
import email.encoders
from queue import Queue
import base64


class MailingList:
    """
    Class to handle mailing lists.
    All infos related to a mailing list are exposed.
    """

    class Mailserver:
        """
        Class defining a mail server.
        """

        def __init__(self, d=None):

            # Define variables, so outer world knows them
            self.imap_address = ""
            self.imap_user = ""
            self.imap_password = ""
            self.smtp_address = ""
            self.smtp_user = ""
            self.smtp_password = ""
            self.sender_address = ""

            # Set values from dict
            if d is not None:
                for key, value in d.items():
                    setattr(self, key, value)

    class Member:
        """
        Class defining a member of a mailing list.
        """

        class Role(Enum):
            """
            Enum to define the member role.
            """
            user = auto()
            admin = auto()

        def __init__(self, dict_vals=None, name="", email="", role=Role.user.name):
            """
            Initializes the object.
            Remark: Setting via dict_vals has priority. Set None (or omit) when single values should be used for init.
            """

            # Define variables, so outer world knows them
            self.name = name
            self.email = email
            self.role = role

            # Set values from dict
            if dict_vals is not None:
                for key, value in dict_vals.items():
                    setattr(self, key, value)

    class MetaData:
        """
        Class holding metadata fields on the mailing list.
        """

        def __init__(self, d=None):

            # Define variables, so outer world knows them
            self.subject_prefix = ""
            self.footer = ""

            # Set values from dict
            if d is not None:
                for key, value in d.items():
                    setattr(self, key, value)

    def __init__(self, d=None):
        """
        Main init function.
        :param d: Dict with key:value pairs holding the attributes matching the class members.
        """
        if d is not None:
            for key, value in d.items():
                if key == "mailserver":
                    self.mailserver = self.Mailserver(value)
                elif key == "members":
                    self.members = [self.Member(x) for x in value]
                elif key == "metadata":
                    self.metadata = self.MetaData(value)
                else:
                    setattr(self, key, value)


class MailErrors:
    """
    Class defining mail errors.
    """

    def __init__(self, error, message):
        """
        Initializing the object.
        :param error: The EmailErrors.
        :param message: Additional error message.
        """
        self.error = error
        self.message = message


class SubjectHeader:
    """
    Class defining a subject header.
    """

    def __init__(self, mailing_list="", sender="", topic="", admin=""):
        self.mailing_list = mailing_list
        self.sender = sender
        self.topic = topic
        self.admin = admin


class Worker:
    """
    Class providing workers to interact with IMAP and SMTP.
    """

    def __init__(self, logger, mail_list, config_file_json, init_config_file=False, queue_size=1000, test_mode=False,
                 payload_max_len_bytes=500000):

        # Variables
        self._logger = logger
        self._receiving_loop_stop = False
        self._sending_loop_stop = False
        self._send_queue = Queue(queue_size)
        self._config_file_json = config_file_json
        self._test_mode = test_mode
        self._payload_max_len_bytes = payload_max_len_bytes

        # Initializing mailing lists
        self._mail_list = mail_list

        # Creation of sessions
        self._logger.write_info('Creating imap/smtp sessions...')
        # self._imap_session = imaplib.IMAP4_SSL(self._mail_list.mailserver.imap_address)
        self._smtp_session = smtplib.SMTP_SSL(self._mail_list.mailserver.smtp_address)
        self._logger.write_info('imap/smtp sessions created')

        # Get/Init config file data
        if init_config_file:
            self._config_data = {
                "latest_email_id": "-1",
                "threads":
                    [
                    ]
            }

            # Update config data
            self._config_data_update()

            self._logger.write_info("Config file initialized")
        else:
            try:
                with open(self._config_file_json, "rt") as config_file:
                    self._config_data = json.load(config_file)
            except FileNotFoundError as e:
                self._logger.write_exception('Config file not found. Application will exit!\n '
                                             'It is expected under `{}`'.format(self._config_file_json))
                raise e

    def stop_loops(self):
        """
        Stop all loops.
        """

        self._receiving_loop_stop = True
        self._sending_loop_stop = True

    def _config_data_update(self, key=None, value=None):
        """
        Sets a config parameter and writes config data to config file.
        """

        # If no key/value provided, only write actual config data
        if key and value:
            # Set value
            self._config_data[key] = value

        # Write config
        with open(self._config_file_json, "w") as config_file:
            json.dump(self._config_data, config_file)

    async def analyze_received_mail(self, received_mail):
        """
        Analyzes a received_mail and puts it to the send queue.
        :param received_mail: Email object.
        :return:
        """

        def payload_size_calculate(payload, current_sum):
            """
            Calculates the overall size of the entire payloads.
            :param payload: Payload.
            :param current_sum: Function is recursive so need to preserve the calculation value.
            :return: Overall length in bytes.
            """
            # Save actual result
            ret = current_sum

            # List means multiple payloads
            if isinstance(payload, list):
                for pay in payload:
                    try:
                        # Check for nested payload
                        ret += payload_size_calculate(pay.get_payload(), ret)
                    except:
                        # Return payload's len
                        return len(pay)
            else:
                try:
                    # Check for nested payload
                    ret += payload_size_calculate(payload.get_payload(), ret)
                except:
                    # Return payload's len
                    return len(payload)

            return ret

        # Collect errors during mail analyzing
        mail_errors = []

        # Create a subject header which is manipulated while analyzing the mail
        subject_header = SubjectHeader(mailing_list=self._mail_list.metadata.subject_prefix)

        in_reply_to_header = ""

        # Headers to be analyzed, also for errors
        headers_analyze = {
            "from": "",
            "subject": "",
            "message-id": ""
        }

        # Get the payload len to prevent spreading large messages
        payload_len = payload_size_calculate(received_mail.get_payload(), 0)
        self._logger.write_info("Payload length: {} bytes".format(payload_len))

        # Check
        if payload_len > self._payload_max_len_bytes:
            # Set error
            mail_errors.append(MailErrors(EmailErrors.ADMIN_PAYLOAD_LEN.name, "{} bytes".format(payload_len)))

            # Remove payload
            received_mail.set_payload(None)

            self._logger.write_warning("Max payload length exceeded: {} bytes ({} bytes allowed)".format(
                payload_len, self._payload_max_len_bytes)
            )

        # Analyze headers
        for header_analyze in headers_analyze.keys():
            try:
                if not received_mail[header_analyze]:
                    raise KeyError
                headers_analyze[header_analyze] = received_mail[header_analyze]
            except KeyError:
                headers_analyze[header_analyze] = "not provided or empty"
                mail_errors.append(MailErrors(EmailErrors.ADMIN_HEADER_MISSING.name, header_analyze))

        # Check whether sender is a valid member of the mailing list
        sending_member = [x for x in self._mail_list.members if x.email.lower() in headers_analyze["from"]]
        if not sending_member:

            # Append mail error
            mail_errors.append(MailErrors(EmailErrors.ADMIN_UNKNOWN_SENDER.name, headers_analyze["from"]))

            # Generate sending member based on from header for admin purposes
            sending_member = MailingList.Member(name=headers_analyze["from"], email=headers_analyze["from"])

            self._logger.write_warning(
                "Mail from unknown sender {} gets forwarded to admins only".format(headers_analyze["from"]))
        else:
            # Set sending member
            sending_member = sending_member[0]

        # Only if no errors, analyze mail in detail
        if not mail_errors:
            # Check whether received mail contains in-reply-to header and is reply to an existing thread
            existing_thread = None
            if received_mail["in-reply-to"]:
                for thread in self._config_data["threads"]:
                    if received_mail["in-reply-to"] in thread["references"]:
                        # Reply to an existing thread
                        existing_thread = thread
                        break

            # Answer to an existing thread
            if existing_thread:
                # Set thread's subject
                subject_header.topic = existing_thread["subject"]

                # reply-to-header
                in_reply_to_header = existing_thread["references"][-1] if existing_thread["references"] else ""

                # Store updated message id
                existing_thread["references"].append(headers_analyze["message-id"])
                self._config_data_update()

                self._logger.write_info("Reply to an existing thread: {}".format(existing_thread))
            else:
                # Set subject
                subject_header.topic = headers_analyze["subject"]

                # Set and update config data
                self._config_data["threads"].append({
                    "references": [headers_analyze["message-id"]],
                    "subject": headers_analyze["subject"],
                    "status": "open"
                })
                self._config_data_update()

                self._logger.write_info("New thread started")

        # Manipulate headers
        #
        # Delete old headers
        global_headers_overwrite = ['reply-to', 'subject', 'from', 'to', 'bcc', 'cc', 'in-reply-to']
        for header_overwrite in global_headers_overwrite:
            del received_mail[header_overwrite]

        # Set new headers
        received_mail['from'] = "{}".format(self._mail_list.mailserver.sender_address)
        received_mail['reply-to'] = self._mail_list.mailserver.sender_address
        # received_mail['in-reply-to'] = headers_analyze["message-id"]
        received_mail['in-reply-to'] = in_reply_to_header

        # Prepare subject header, will be manipulated based on type and errors
        subject_header.sender = sending_member.name

        # Differentiate header values based on mail type
        if mail_errors:

            # Collect admin-role users
            recipients = [x.email for x in self._mail_list.members
                          if x.role == MailingList.Member.Role.admin.name]

            # Add errors to subject
            subject_header.topic = headers_analyze["subject"]
            subject_header.admin = ";".join("{}|{}".format(x.error, x.message) for x in mail_errors)

            # Only admin role members get admin mails
            self._logger.write_info(
                "This ADMIN mail will be sent to admin-role users only; mail: {}".format(subject_header.topic)
            )
        else:
            recipients = [x.email for x in self._mail_list.members]

        # Subject, add admin only for admin mails
        received_mail['subject'] = "[{}][VON:{}][THEMA:{}]".format(
            subject_header.mailing_list, subject_header.sender, subject_header.topic) + (
                                       "[ADM:{}]".format(subject_header.admin) if subject_header.admin else "")

        # BCC: All recipients will be added as bcc
        received_mail['bcc'] = ", ".join(recipients)

        # Add mail to queue
        self._send_queue.put((received_mail, mail_errors, received_mail['subject'], sending_member, recipients))

    async def receiving_loop(self):
        """
        The receiving loop, handling receive of IMAP messages.
        """

        # Loop start
        self._logger.write_info("Starting receive loop...")

        try:

            with imaplib.IMAP4_SSL(self._mail_list.mailserver.imap_address) as _imap_session:

                while not self._receiving_loop_stop:

                    # Make sure logged in with imap
                    if _imap_session.state == "NONAUTH":
                        self._logger.write_info('Logging in with imap')
                        _imap_session.login(self._mail_list.mailserver.imap_user,
                                                 self._mail_list.mailserver.imap_password)

                    # Select inbox
                    _imap_session.select("inbox")

                    # Get all email
                    result, data = _imap_session.search(None, "UNDELETED")

                    # Get the mail ids
                    email_ids = data[0].split()

                    for email_id in email_ids:

                        # New mail?
                        email_id_decoded = email_id.decode()
                        if int(email_id_decoded) > int(self._config_data["latest_email_id"]):
                            # fetch the email body (RFC822) for the given ID
                            result, data = _imap_session.fetch(email_id, "(RFC822)")
                            raw_email = data[0][1]

                            # Parse email
                            new_mail = email.message_from_bytes(raw_email)
                            self._logger.write_info(
                                "Received new mail: {}, {}".format(new_mail["from"], new_mail["subject"]))
                            self._logger.write_debug("\n{}".format(raw_email.decode()))

                            # Analyze the received mail
                            await self.analyze_received_mail(new_mail)

                            # Write ID to file
                            self._logger.write_debug(
                                'Writing most recent email id `{}` to config file'.format(email_id_decoded)
                            )
                            # Set latest id in config data and update config file
                            self._config_data_update("latest_email_id", email_id_decoded)

                    # Any new messages?
                    if not self._send_queue.qsize():
                        self._logger.write_info(
                            "No new mail received, last_mail_id remains: {}".format(self._config_data["latest_email_id"])
                        )

                    # Sleeping
                    self._logger.write_info("Session states: imap:->{}".format(_imap_session.state))
                    self._logger.write_info('Receive loop sleeping ...')

                    await asyncio.sleep(1)

        except Exception as e:
            self._logger.write_exception("Error in receive loop: {}".format(e))

        self._logger.write_warning("Receiving loop stopped...")

    async def sending_loop(self):
        """
        The sending loop, handling send of SMTP messages.
        """

        # List with items failed during send, thus to be re-sent
        resend_items = list()

        self._logger.write_info("Starting send loop...")

        while not self._sending_loop_stop:
            try:
                # Add resend items to queue
                [self._send_queue.put(x) for x in resend_items]
                resend_items.clear()

                # Sending
                with smtplib.SMTP_SSL(self._mail_list.mailserver.smtp_address) as smtp_session:

                    # Login with SMTP
                    smtp_session.login(self._mail_list.mailserver.smtp_user, self._mail_list.mailserver.smtp_password)

                    # Process all new mails
                    for new_mail, new_mail_type, subject_header, sending_member, recipients in iter(
                            self._send_queue.get_nowait, None
                    ):

                        # Send message
                        try:
                            self._logger.write_info(
                                "Sending mail: `{}`".format(subject_header))

                            # In test mode don't send but drop message
                            if self._test_mode:
                                self._logger.write_info(
                                    "In test_mode: Not sending mail, but dropping same: `{}`".format(new_mail))
                            else:
                                # Send mail
                                smtp_session.sendmail(self._mail_list.mailserver.sender_address, recipients,
                                                      new_mail.as_bytes())

                        except Exception as e:
                            self._logger.write_exception(
                                "Sending of mail failed, putting mail back to send "
                                "queue; mail: `{}`; ex:".format(subject_header, e))

                            # Append failed mail to resend list (will be added to queue later)
                            resend_items.append((new_mail, new_mail_type, subject_header, sending_member, recipients))

                            # Break outer loop
                            break

                        self._logger.write_info(
                            "Mail sent: {}".format(subject_header))

                        # Wait some time to not brute force the smtp server
                        await asyncio.sleep(0.1)

            except queue.Empty:
                pass
            except Exception as e:
                self._logger.write_exception("Error in send loop: {}".format(e))
            finally:
                try:
                    smtp_session.quit()
                except:
                    pass
                self._logger.write_info('Send loop sleeping ...')
                await asyncio.sleep(10)

        self._logger.write_warning("Sending loop stopped...")
