import logging
from enum import Enum
import inspect


class Log:
    """
    Class providing logging functionality.
    """

    # TODO:https://stackoverflow.com/questions/900392/getting-the-caller-function-name-inside-another-function-in-python

    class Formatters(Enum):
        """
        Class providing formatter strings for the log message.
        """
        LEVEL_NAME = '%(levelname)-7s'
        MESSAGE = '%(message)s'
        FUNC_NAME = '%(funcName)s'
        ASC_TIME = '%(asctime)s'
        LOGGER_NAME = '%(name)s'
        LOGGER_NAME_LEN = '%(name)'
        LINE_NUMBER = '%(lineno)5d'
        FILE_NAME = '%(filename)s'

    # Some formatters need to be treated specially and can no
    FORMATTERS_TREAT_SPECIAL = [
        Formatters.FUNC_NAME,
        Formatters.FILE_NAME,
        Formatters.LINE_NUMBER
    ]

    class LoggingLevels(Enum):
        """
        Logging levels, derived from logging package.
        """
        DEBUG = 0
        INFO = 1
        WARNING = 2
        ERROR = 3

    def __init__(self, name='',
                 file_name='',
                 formatter_console=[Formatters.ASC_TIME],
                 formatter_file=[Formatters.ASC_TIME],
                 level_console=LoggingLevels.WARNING,
                 level_file=LoggingLevels.WARNING,
                 ):
        """
        Inits the logger object.
        :param name: Name of logger.
        :param file_name: File name to log to (empty for NO_LOG_TO_FILE).
        :param formatter_console: Formatter string for the console log (use Formatters).
        :param formatter_file: Formatter string for the file log (use Formatters).
        :param level_console: Level to be logged on the console.
        :param level_file: Level to be logged for file log.
        """

        super(Log, self).__init__()

        # Create logger
        self._logger = logging.getLogger(name)

        # Log-Level of main logger is always DEBUG
        # Handlers define their levels based on init parameter
        self._logger.setLevel(self.LoggingLevels(min([level_console.value, level_file.value])).name)

        # Don't propagate message to ancestor handlers
        self._logger.propagate = 0

        # Generate the formatters
        self._formatters_console = [x for x in formatter_console if x not in self.FORMATTERS_TREAT_SPECIAL]
        self._formatters_console_special = [x for x in formatter_console if x in self.FORMATTERS_TREAT_SPECIAL]
        self._formatters_file = [x for x in formatter_file if x not in self.FORMATTERS_TREAT_SPECIAL]
        self._formatters_file_special = [x for x in formatter_file if x in self.FORMATTERS_TREAT_SPECIAL]

        # Create/Configure the console handler
        console_handler = logging.StreamHandler()
        console_handler.setLevel(level_console.name)
        console_formatter = logging.Formatter(
            "[{}]".format(self.Formatters.LEVEL_NAME.value) +
            "".join(["[{}]".format(x.value) for x in self._formatters_console]) +
            "{}".format(self.Formatters.MESSAGE.value)
        )
        console_handler.setFormatter(console_formatter)

        # Add the console handler
        self._logger.addHandler(console_handler)

        # While the console_handler is standard, a file_handler is optional
        if file_name != '':
            # Create/Configure the file handler
            file_handler = logging.FileHandler(file_name)
            file_handler.setLevel(level_file.name)
            file_formatter = logging.Formatter(
                "[{}]".format(self.Formatters.LEVEL_NAME.value) +
                " ".join([x.value for x in self._formatters_file]) +
                " {}".format(self.Formatters.MESSAGE.value)
            )
            file_handler.setFormatter(file_formatter)

            # Add the file handler
            self._logger.addHandler(file_handler)

        # Alias function names to be compatible with standard log
        self.exception = self.write_exception
        self.info = self.write_info
        self.error = self.write_error
        self.debug = self.write_debug
        self.warn = self.write_warning
        self.warning = self.write_warning

    def _add_formatters(self):
        formatters = ""
        if self.Formatters.FILE_NAME in self._formatters_console_special:
            formatters += "[F:{}]".format(inspect.stack()[2].filename.split('/')[-1])
        if self.Formatters.FUNC_NAME in self._formatters_console_special:
            formatters += "[FN:{}]".format(inspect.stack()[2].function)
        if self.Formatters.LINE_NUMBER in self._formatters_console_special:
            formatters += "[LN:{}]".format(inspect.stack()[2].lineno)
        return formatters

    def write_info(self, message):
        """
        Write info level message.
        :param message:
        :return:
        """
        if self._logger.isEnabledFor(logging.INFO):
            self._logger.info("{} {}".format(self._add_formatters(), message).strip())

    def write_warning(self, message):
        """
        Write warning level message.
        :param message:
        :return:
        """
        if self._logger.isEnabledFor(logging.WARNING):
            self._logger.warning("{} {}".format(self._add_formatters(), message).strip())

    def write_error(self, message):
        """
        Write error level message.
        :param message:
        :return:
        """
        if self._logger.isEnabledFor(logging.ERROR):
            self._logger.error("{} {}".format(self._add_formatters(), message).strip())

    def write_debug(self, message):
        """
        Write debug level message.
        :param message:
        :return:
        """
        if self._logger.isEnabledFor(logging.DEBUG):
            self._logger.debug("{} {}".format(self._add_formatters(), message).strip())

    def write_exception(self, message):
        """
        Write error level message + exception.
        :param message:
        :return:
        """
        self._logger.exception("{} {}".format(self._add_formatters(), message).strip())

    @property
    def logger(self):
        """
        Read access to the logger object.
        :return:
        """
        return self._logger
