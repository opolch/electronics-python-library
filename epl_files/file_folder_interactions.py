import os

def get_files_in_folder(folder_path, absolute_paths=False):
    """
    Queries all files/folders in a given path.
    :param folder_path:
    :param absolute_paths: True->Return absolute path for all elements.
    :return: list of tuples: [(dir_path, dir_names, file_names)]
    """

    ret = []

    for (dir_path, dir_names, file_names) in os.walk(folder_path):
        if absolute_paths:
            ret.append((
                "{}/{}".format(folder_path, dir_path),
                ["{}/{}".format(folder_path, x) for x in dir_names],
                ["{}/{}".format(folder_path,x) for x in file_names]))
        else:
            ret.append((dir_path, dir_names, file_names))

    return ret