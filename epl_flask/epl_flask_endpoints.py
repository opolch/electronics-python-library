from http import HTTPStatus
from epl.epl_dicts.epl_dicts_objects import object_to_dict, object_init_from_dict


class EndpointReturnPayload:
    """
    Class defining the payload for a return message.
    """

    def __init__(self, return_message="", return_payload={}, init_dict={}):
        """
        Inits the object.
        :param return_message: Message field for the return message.
        :param return_payload: Payload field for the return message.
        :param init_dict: Dict to init the object from. Will overwrite all others.
        """
        self.message = return_message
        self.payload = return_payload

        if init_dict:
            self.init_from_dict(init_dict=init_dict)

    def as_dict(self) -> dict:
        # """
        # Creates a dict with all class members by recursively iterating.
        # It expects objects implement `as_dict` function.
        # :return: dict
        # """
        return object_to_dict(self)

    def init_from_dict(self, init_dict: dict):
        """
        Option to init the object from a dict.
        :param init_dict: dict holding `class_member:value` pairs.
        :return:
        """
        object_init_from_dict(self, init_dict)


class EndpointReturn:
    """
    Class defining a common return object for endpoints.
    """

    def __init__(self, request_code: HTTPStatus, return_message="", return_payload={}):
        """
        Init the object.
        :param request_code: http compliant request code.
        :param return_message: The value for the return message part.
        :param return_payload: The value for the return payload part.
        """
        self._request_code = request_code
        self._payload = EndpointReturnPayload(
            return_message=return_message,
            return_payload=return_payload
        )

    @property
    def request_code(self) -> HTTPStatus:
        return self._request_code

    @request_code.setter
    def request_code(self, set_val):
        self._request_code = set_val

    @property
    def payload(self) -> EndpointReturnPayload:
        return self._payload

    @property
    def return_message(self) -> str:
        return self._payload.message

    @return_message.setter
    def return_message(self, set_val):
        self._payload.message = set_val

    @property
    def return_payload(self) -> dict:
        return self._payload.payload

    @return_payload.setter
    def return_payload(self, set_val):
        self._payload.payload = set_val

    def as_dict(self) -> dict:
        # """
        # Creates a dict with all class members by recursively iterating.
        # It expects objects implement `as_dict` function.
        # :return: dict
        # """
        return object_to_dict(self)

    def init_from_dict(self, init_dict: dict):
        """
        Option to init the object from a dict.
        :param init_dict: dict holding `class_member:value` pairs.
        :return:
        """
        object_init_from_dict(self, init_dict)
