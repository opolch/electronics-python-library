#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Nov 28 09:04:03 2019

@author: hardikar
"""
import struct
import encryption
import binascii
from pathlib import Path
SOH = 0x01
EOT = 0x04
DLE = 0x10
cmd = 0x03


def calculate_stage(csv):
    """
    Calculates channel number from the channel specific value of the command.
    
    :param csv: Channel specific value of the canicom command
    
    :return: Stage number 
    """
    if csv == '00000000':
        stage = 0
    else:
        stage = int(csv.rstrip("0"), 16)
    return stage


def calculate_csv(stage_no):
    """
    Calculates channel specific value from the csv.
    
    :param stage_no: Channel/stage number

    :return: Channel specific value (CSV)
    """

    if stage_no == 0:
        channel_no = 0x00000000
    elif stage_no == 1:
        channel_no = 0x01000000
    elif stage_no == 2:
        channel_no = 0x02000000
    elif stage_no == 3:
        channel_no = 0x03000000
    elif stage_no == 4:
        channel_no = 0x04000000
    elif stage_no == 5:
        channel_no = 0x05000000
    elif stage_no == 6:
        channel_no = 0x06000000
    elif stage_no == 7:
        channel_no = 0x07000000
    elif stage_no == 8:
        channel_no = 0x08000000

    return channel_no


def swap(x):
    """
    This function swaps the bytearray passed to it.
    
    :param x: Data in byte array format.
    
    :return: Swapped input array.
    """
    swapped_data = bytearray(len(x))
    for i in range(len(x)):
        swapped_data[i] = x[len(x)-1-i]

    return swapped_data


def float_to_hex(f):
    """
    Function to calculate the hexadecimal equivalent of the input float value.
    
    :param f: Float value.
    
    return: Hex value of the input parameter.
    """
    return hex(struct.unpack('<I', struct.pack('<f', f))[0])  


def data_extraction(received_data, _type):
    """
    Function to extract the float value from received hex data.
    
    :param received_data: received data in hex format.
    :param _type: Data type of the received data
    
    return: Float value of the input data, rounded off to 2 decimals.
    
    """

    data_raw = received_data
    data_raw = swap(data_raw)
    
    list_values = []
    
    for i in range(0, len(data_raw), 4):
        list_values.append(bytes(data_raw[i:i+4]).hex())
        
    list_values = list_values[::-1]
    for i in range(len(list_values)):
        if _type == float:
            list_values[i] = struct.unpack('f', struct.pack('I', int(list_values[i], 16)))
            list_values[i] = round(list_values[i][0], 2)
        elif _type == int:
            list_values[i] = int(list_values[i], 16)
        else:
            pass
    
    if _type == 'DISCRETE':
        while len(list_values) != 8:
            list_values.append(0)
        for j in range(len(data_raw)):
            list_values[j] = data_raw[j] 

    elif _type == str:
        list_values = list_values
    return list_values
            

def hex_to_binary(hex_val, num_of_bits_for_binary):
    """
    Funtion to convert hex to binary value containing specified number of bits(append zeros on LHS to cpmplete the
     required bits).
    
    :param hex_val: Hexadecimal value. 
    :param num_of_bits_for_binary: Required number of bits in the resultant binary value.
    
    return: Binary value containing the specified number of bits.
    """ 
    
    return bin(int(hex_val, 16))[2:].zfill(num_of_bits_for_binary)


def file_conversion(curr_dir, file_name):
    """
    Converts the given hex file into CAN frames as per the required format.

    :param curr_dir: Current working directory of the project
    :param file_name: Name of the file to be converted.

    Returns:

    """
    
    crc16_microchip = encryption.CRC16Microchip()   
    path = Path(curr_dir)
    
    file_to_open = path / file_name.decode('utf-8')
    with open(file_to_open) as fp:
        "Read lines of the file."
        lines = fp.readlines()
        last_line = lines[-1]  # Store the last line of the file.
        data = []  # Create an array to store the final data with cmd, checksum and DLEs.
        data_before_escape = []  # Create an array to store the cmd+data for checksum calculation.
        command_counter = 1  # Initialize command counter to 1.
        
        for line in lines:
            # Creation of the first data packet
            if len(data) == 0:
                data.append([])
                data_before_escape.append([])
    #            
                data[command_counter-1].append(SOH)
                data[command_counter-1].append(cmd)
                
                data_before_escape[command_counter-1].append(cmd)
                
            line1 = line.strip(':').strip()
            decoded_line = binascii.a2b_hex(line1)  # Convert ascii to binary
            file_bytearray = bytearray(decoded_line)  # Convert binary to bytearray.
            
            line_data = []
            for i in range(len(file_bytearray)):
                if file_bytearray[i] == SOH or file_bytearray[i] == EOT or file_bytearray[i] == DLE:
                    line_data.append(DLE)
                    pass
                line_data.append(file_bytearray[i])
                
                line_data_hex = []
                for i in range(len(line_data)):
                    line_data_hex.append(hex(line_data[i]))
                
            if (len(data[command_counter-1]) + len(line_data)) < 125:
                data[command_counter-1] += (line_data)
                
                for i in range(len(file_bytearray)):
                    data_before_escape[command_counter-1].append(file_bytearray[i])
            else:   # The command data has reached the limits of 128 bytes.
                
                # Calculate the checksum and append it to the packet, and proceed to the next package.
                checksum_byte1, checksum_byte2 = (crc16_microchip.calculate_crc(data_before_escape[command_counter-1]))
                
                checksum_array = [checksum_byte1, checksum_byte2]
                for i in range(len(checksum_array)):
                    if checksum_array[i] == SOH or checksum_array[i] == EOT or checksum_array[i] == DLE:
                        data[command_counter-1].append(DLE)
                    data[command_counter-1].append(checksum_array[i])
                data[command_counter-1].append(EOT)
                
                command_counter += 1
                
                data.append([])
                data_before_escape.append([])
                
                data_before_escape[command_counter-1].append(cmd)
                
                data[command_counter-1].append(SOH)
                data[command_counter-1].append(cmd)
                data[command_counter-1] += (line_data)
                for i in range(len(file_bytearray)):
                    data_before_escape[command_counter-1].append(file_bytearray[i])
            
            if line == last_line:
                checksum_byte1, checksum_byte2 = (crc16_microchip.calculate_crc(data_before_escape[command_counter-1]))
                checksum_array = [checksum_byte1, checksum_byte2]
                for i in range(len(checksum_array)):
                    if checksum_array[i] == SOH or checksum_array[i] == EOT or checksum_array[i] == DLE:
                        data[command_counter-1].append(DLE)
                    data[command_counter-1].append(checksum_array[i])
                    
                data[command_counter-1].append(EOT)
                
        data_final = []
                
        for i in range(len(data)):
            data_final.append([])
            for j in range(0, len(data[i]), 8):
                data_final[i].append(data[i][j:j+8])
    
        return data_final
