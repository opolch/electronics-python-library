import asyncio
from enum import Enum, auto
from epl.epl_logging.epl_logging_log import Log


class StateReturn:
    """
    Class to define state return objects.
    State machine operate with generic return objects to analyze results of states.
    """

    class ReturnSuccess(Enum):
        """
        Defining the success of a state callback response.
        """
        ONGOING = auto()
        DONE_FORWARD = auto()
        DONE_BACKWARDS = auto()
        ERROR = auto()

    def __init__(self,
                 success=ReturnSuccess.ONGOING,
                 message="",
                 ):
        """
        Init the object.
        :param success: Success.
        :param message: String message.
        """
        self.success = success
        self.message = message

    def __str__(self):
        """
        String implementation.
        :return:
        """
        return "success:{};message:{}".format(self.success, self.message)


class GenericState:
    """
    The main state object defining the single states operated by the state machine.
    """

    def __init__(self,
                 state_name: str,
                 callback: callable,
                 timeout_s=0.0,
                 call_period_s=0.0,
                 fw_jump_step=1,
                 bw_jump_step=1
                 ):
        """
        Init the object.
        :param state_name: State's name.
        :param callback: The callback to be executed when state is active.
        :param timeout_s: Timeout in s for the state (will error out on exceeding).
        A `0.0` deactivates the timeout functionality for a task.
        :param call_period_s: Period with which the state should be called.
        If `0.0` is passed, a global value will be used.
        :param fw_jump_step: Jump width for a forward jump.
        :param bw_jump_step: Jump width for a backward jump.
        """
        self.state_name = state_name
        self.callback = callback
        self.timeout_s = timeout_s
        self.call_period_s = call_period_s
        self.forward_jump = fw_jump_step
        self.backward_jump = bw_jump_step


class HealthState:
    """
    Class defining the overall health state of the state machine.
    """

    class HealthStateValue(Enum):
        INIT = auto()
        STOPPED = auto()
        RUNNING = auto()
        ERROR = auto()

    def __init__(self,
                 health_state: HealthStateValue,
                 health_message: str):
        """
        Init the object.
        :param health_state: HealthStateValue
        :param health_message: State corresponding message.
        """
        self.state = health_state
        self.message = health_message

    def __str__(self):
        """
        String implementation.
        :return:
        """
        return "{};{}".format(self.state, self.message)


class GenericStateMachine:
    """
    The state machine itself.
    """

    def __init__(self,
                 states: list,
                 period_s=1.0,
                 logger=None
                 ):
        """
        Init the state machine.
        :param states: List with states to be executed from the state machine.
        :param period_s: The period with which the state machine should operate.
        :param logger: epl_logging.Log object.
        """

        # Variables
        self._logger = logger if logger is not None else Log(
            name="epl_sm", level_console=Log.LoggingLevels.DEBUG,
            formatter_console=[Log.Formatters.ASC_TIME, Log.Formatters.FILE_NAME,
                               Log.Formatters.FUNC_NAME, Log.Formatters.LINE_NUMBER],
        )

        self._logger.write_info("Initializing state machine object...")

        # Setting variables
        self._states = states if states is not None else list()
        self._period = period_s

        # Helper to remember the current state
        self._current_state_idx = -1
        self._timeout_counter = 0.0

        # State machine health state, will be initialized during init of actual state machine
        self._health_state = None

        # Init the state machine
        self._init_state_machine()

        self._logger.write_info("State machine object initialized.")

    def _init_state_machine(self):
        """
        Initializes the state machine to default conditions.
        :return:
        """

        self._logger.write_debug("Initializing state machine, not starting it.")

        # Always start at index 0
        self._current_state_idx_set(0)

        # Set health state to running
        self._health_state_set(HealthState.HealthStateValue.INIT, "init")

    def state_machine_start(self,
                            also_init=False):
        """
        Starts the state machine by setting the health state to running.
        :param also_init: True->Also re-init the state machine. Otherwise, state machine runs from last state.
        :return:
        """

        # Also initialize the state machine with health state change?
        if also_init:
            self._init_state_machine()

        # Set health state
        self._health_state_set(HealthState.HealthStateValue.RUNNING, "started")

    def state_machine_stop(self,
                           also_init=False):
        """
        Stops the state machine by setting the health state to stopped, also timeouts are stopped in this state.
        :param also_init: True->Also re-init the state machine. Otherwise, state machine runs from last state.
        :return:
        """

        if also_init:
            self._init_state_machine()

        self._health_state_set(HealthState.HealthStateValue.STOPPED, "stopped")

    def _current_state_idx_set(self,
                               state_index: int) -> bool:
        """
        Set the current state by index. Checks whether state is available from states list.
        Also resets the timeout counter.
        :param state_index: The state index in states list.
        :return: True->ok; False->State index not available.
        """

        # Reset timeout counter
        self._timeout_counter = 0.0

        # Check whether state index is in range
        if len(self._states) > state_index >= 0:
            self._logger.write_debug("Setting current state index to: {}".format(state_index))
            self._current_state_idx = state_index
            return True
        self._logger.write_debug("Could not set state index to `{}`; out of range since len(states)={}".format(
            state_index, len(self._states)))
        return False

    def _current_state_idx_increase(self, jump_width=1) -> bool:
        """
        Increasing the current state index.
        :param jump_width: Int value for increasing the index.
        :return:
        """
        return self._current_state_idx_set(self._current_state_idx + jump_width)

    def _current_state_idx_decrease(self, jump_width=1) -> bool:
        """
        Decreasing the current state index.
        :param jump_width: Int value for increasing the index.
        :return:
        """
        return self._current_state_idx_set(self._current_state_idx - jump_width)

    def _current_state_get(self) -> GenericState:
        """
        Returns the current active state.
        :return:
        """
        return self._states[self._current_state_idx]

    def _health_state_set(self,
                          state: HealthState.HealthStateValue,
                          message: str):
        """
        Setting the state machine's health state.
        :param state: HealthStateValue
        :param message: State corresponding message.
        :return:
        """
        self._health_state = HealthState(state, message)

    def health_state_get(self) -> HealthState:
        """
        Returns the actual HealthState.
        :return:
        """
        return self._health_state

    async def state_machine_callback(self):
        """
        The state machine thread.
        :return:
        """

        self._logger.write_info("State machine loop started...")

        # Initialize the call period
        call_period = self._period

        # State machine endless loop
        while 1:
            try:
                # State machine only operates in RUNNING state, other states stop it.
                if self._health_state.state == HealthState.HealthStateValue.RUNNING:

                    # Get current state and its call period
                    current_state = self._current_state_get()
                    call_period = current_state.call_period_s if current_state.call_period_s > 0 else self._period
                    self._logger.write_info("Current state: {}".format(current_state.state_name))

                    # Execute current state's callback
                    self._logger.write_debug("Executing state callback: {}".format(current_state.callback))
                    state_return = await current_state.callback()

                    # Analyze response
                    self._logger.write_debug("State callback response: `{}`".format(state_return))

                    # Jump forward
                    if state_return.success == StateReturn.ReturnSuccess.DONE_FORWARD:

                        self._current_state_idx_increase(current_state.forward_jump)

                    # Jump backwards
                    elif state_return.success == StateReturn.ReturnSuccess.DONE_BACKWARDS:

                        self._current_state_idx_decrease(current_state.backward_jump)

                    # Error
                    elif state_return.success == StateReturn.ReturnSuccess.ERROR:

                        # Set health state to indicate the error situation
                        self._health_state_set(HealthState.HealthStateValue.ERROR, state_return.message)

                        self._logger.write_debug("State callback returned an error, success:{}; message:{}".format(
                            state_return.success, state_return.message))

                    # Stay in current state and handle timeouts
                    else:
                        # Handle timeouts
                        # Remember: a `0` deactivates the timeout functionality for a task
                        if 0 < current_state.timeout_s < self._timeout_counter:
                            # Set health state to indicate the error condition
                            self._health_state_set(HealthState.HealthStateValue.ERROR,
                                                   "Timeout for state `{}` exceeded: {}s".format(
                                                       current_state.state_name, current_state.timeout_s))

                            self._logger.write_error("Timeout for state `{}` exceeded: {}s".format(
                                current_state.state_name, current_state.timeout_s))
                        else:
                            # Count up state's timeout counter
                            self._timeout_counter += call_period

                else:
                    # Set call period to default
                    call_period = self._period

                    self._logger.write_debug("State machine in state: {}".format(self._health_state))

            except Exception as e:
                self._logger.write_debug("State machine general exception, ex: {}".format(e.args))
            finally:
                await asyncio.sleep(call_period)
