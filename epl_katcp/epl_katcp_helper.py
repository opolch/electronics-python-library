from epl.epl_string import epl_string_split, epl_string_search
from epl.epl_katcp.epl_katcp_command import CommandType
from enum import Enum, auto


class CommandHelpInform:
    """
    Class to extract parameters from command #help inform message.
    """
    def __init__(self, message_args):
        """
        Extract parameters from inform message.
        :param message_args: The katcp message args.
        """
        self.command_name = message_args[0].decode()
        self.description = message_args[1].decode()
        self.values = list()
        self.type = CommandType.UNKNOWN
        self.minimum = 0
        self.maximum = 0

        try:
            # Find string enclosed by [ ]
            values = epl_string_search.substring_character(self.description, '[', ']')
            if '|' in values:
                self.type = CommandType.DISCRETE
                self.values = epl_string_split.split_always_list(values, '|')
            elif '...' in values:

                # Split min/max by separator ...
                self.values = epl_string_split.split_always_list(values, '...')
                if '.' in self.values[0]:
                    self.type = CommandType.FLOAT
                else:
                    self.type = CommandType.INT

                # Set min/max values
                self.minimum = self.values[0]
                self.maximum = self.values[1]
        except Exception as e:
            self.values = ''


class CommandGenReply:
    """
    Class to extract parameters from a generic reply message.
    """
    def __init__(self, message_name, message_arguments):
        """
        Extract parameters from reply message.
        :param message_name: Message's name.
        :param message_arguments: Message's arguments.
        """
        self.status = message_arguments[0].decode('utf8').upper()
        self.command_name = message_name
        if len(message_arguments) > 1:
            self.params = message_arguments[1:]
        else:
            self.params = list()


class CommandGenInform:
    """
    Class to extract parameters from a generic inform message.
    """
    def __init__(self, message_name, message_arguments):
        """
        Extract parameters from reply message.
        :param message_name: Message's name.
        :param message_arguments: Message's arguments.
        """
        self.command_name = message_name
        self.message = message_arguments


class CommandLogInform:
    """
    Class to extract parameters from a #log inform message.
    """

    class LogLevel(Enum):
        """
        Enum class to define log level.
        """
        INFO = auto()
        WARN = auto()
        ERROR = auto()

    def __init__(self, message_string):
        """
        Extract parameters from a #log inform message.
        :param message_string: The entire katcp message.
        """
        single_params = epl_string_split.split_always_list(message_string, remove_whitespaces=False)
        self.log_level = self.LogLevel[single_params[1].upper()]
        self.log_message = str(single_params[4]).replace('\\_', ' ')


def extract_inform_help(message_args):
    """
    Extracts the parameters of a #help command.
    :param message_args: Entire katcp message.
    :return: Object of type CommandHelpInform.
    """
    try:
        return CommandHelpInform(message_args)
    except Exception as e:
        return None


def extract_reply_general(message_name, message_arguments):
    """
    Extracts the parameters of a generic reply message.
    :param message_name: Message's name.
    :param message_arguments: Message's arguments.
    :return: Object of type CommandGenReply.
    """
    try:
        return CommandGenReply(message_name, message_arguments)
    except Exception as e:
        return None

def extract_inform_general(message_name, message_arguments):
    """
    Extracts the parameters of a generic inform message.
    :param message_name: Message's name.
    :param message_arguments: Message's arguments.
    :return: Object of type CommandGenReply.
    """
    try:
        return CommandGenInform(message_name, message_arguments)
    except Exception as e:
        return None


def extract_inform_log(source_string):
    """
    Extracts the parameters of a generic log message.
    :param source_string: The katcp command.
    :return: Success->Command name as string; Error->None
    """
    try:
        return CommandLogInform(source_string)
    except Exception as e:
        return None


def extract_command_name(source_string):
    """
    Extracts the katcp command name from a katcp command.
    :param source_string: The katcp command.
    :return: Success->Command name as string; Error->None
    """
    try:
        return epl_string_split.split_index(source_string, 0, ' ').replace('#', '').replace('!', '')
    except Exception as e:
        return None


class EplKatcpHelper:
    """
    Class providing katcp helper functions.
    """

    def __init__(self):
        pass
