# -*- coding: utf-8 -*-
"""
Created on Mon Nov  9 10:32:34 2020

@author: hardikar

Description:
    This file provides the functionality of the SCPI device client.
    The purpose is to make it more flexible and generic for its usage in
    various projects concerning the SCPI devices.
"""

import time
import threading
import socket
from epl.epl_logging.epl_logging_log import Log
from epl.epl_socket.socket_udp import SockUDP
from epl.epl_socket.socket_tcp import SockTCP
from epl.epl_string.epl_string_split import split_always_list
import collections
from enum import Enum, auto


class TaskScpi:
    """
    Class to define SCPI specific project tasks.

    """

    def __init__(self, scpi_name, mirror_function, active=True):
        """
        Init the task.
        :param scpi_name: Full task name (including all hierarchies).
        :param mirror_function: Pointer to mirror function (called on value update).
        :param active: Flag indicating whether the task is currently active (requests and updates).
        """
        self.scpi_name = scpi_name
        self.mirror_function = mirror_function
        self.active = active


class Device:
    """
    Class to define the SCPI client.

    """

    class SocketType(Enum):
        UDP = auto()
        TCP = auto()

    class SetIndicator(Enum):
        NONE = auto()
        CMD_PREFIX = auto()

    SetIndicatorMapping = {
        SetIndicator.NONE.name: '',
        SetIndicator.CMD_PREFIX.name: 'cmd',
    }

    def __init__(self,
                 host,
                 port,
                 logger,
                 mirror_timeout=0.1,
                 update_task_list=None,
                 scpi_prefix='',
                 separator=':',
                 query_termination_character='?',
                 msg_termination_character='\n',
                 multiple_commands=False,
                 set_indicator=SetIndicator.NONE,
                 auto_update=True,
                 update_interval=1,
                 socket_type=SocketType.UDP,
                 device_reply_header_present=True,
                 device_reply_on_set=True
                 ):
        """
        This function initializes the Device class.

        :param host: IP address of the server device to connect to.
        :param port: Port of the server.
        :param logger: Logger module to implement the logging functionality.
        :param mirror_timeout: Timeout value for mirror checking in case of
            support for multiple commands by the device.
        :param update_task_list: Dict of updatable objects of value_type 'task_scpi'
            (scpi levels of task and its function as extracted from main
            project task_list)
        :param scpi_prefix: Device-specific prefix for the scpi commands.
        :param separator: SCPI separator which the server device uses.
        :param query_termination_character: Termination character for query
            messages, used by the device.
        :param msg_termination_character: Termination character for messages,
            used by the device.
        :param multiple_commands: Flag to indicate if the device can handle
            multiple commands at a time.
        :param set_indicator: Indicator to be used to mark a set request as SET (enum SetIndicator).
        :param auto_update: Flag to indicate if the auto update of parameters
            is desired.
        :param update_interval: Time interval between successive update in case
            auto update is desired.
        :param socket_type: Type of the socket connection (TCP/UDP) required 
            by the device.
        :param device_reply_header_present: Flag indicating whether the device
            sends any header with the reply.
        :param device_reply_on_set: Flag indicating whether the device replies
            to a set command after its successful execution.
        """

        if update_task_list is None:
            update_task_list = dict()
        self.host = host
        self.port = port
        self.mirror_timeout = mirror_timeout
        self.update_task_list = update_task_list
        self.scpi_prefix = scpi_prefix
        self.separator = separator
        self.query_termination = query_termination_character
        self.msg_termination_character = msg_termination_character
        self.set_indicator = set_indicator
        self.multiple_commands = multiple_commands
        self.auto_update = auto_update
        self.update_interval = update_interval
        self.device_reply_header_present = device_reply_header_present
        self.device_reply_on_set = device_reply_on_set

        self._sock = None
        self._socket_type = socket_type
        if self._socket_type is Device.SocketType.UDP:
            self._sock_init = self._sock_udp_init
            self._sock_send = self._sock_udp_send
            self._sock_receive = self._sock_udp_receive
        else:
            self._sock_init = self._sock_tcp_init
            self._sock_send = self._sock_tcp_send
            self._sock_receive = self._sock_tcp_receive

        if logger is None:
            self._log_writer = Log()
        else:
            self._log_writer = logger

        self.tx_buffer = collections.deque(maxlen=1000)
        self.SCPI_COMMANDS_MAX_COUNT = 1000

        self.state_machine_flag = False
        self.update_flag = False

        # self.s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)  # Create a socket.
        # self.s.settimeout(5)  # Set timeout value for data reception.
        try:
            self._sock_init()
            self.connected = True
        except Exception as e:
            self.connected = False
            self._log_writer.write_error('Initial socket initialization failed {}:{}, {}'.
                                         format(self.host, self.port, e))
            self.wait_on_connect(2)
            time.sleep(1)
        self.start_threads()
        if self.auto_update is False:
            self.init_params()

    def init_params(self):
        """
        Read the initial values of all the sensors from the server if the auto\
        update is disabled.

        :return: None
        """

        for task_name, task in self.update_task_list.items():
            self.scpi_command = self.query_message_create(task_name)
            self.tx_buffer.append(self.scpi_command)
            try:
                self.send_receive_msg()
            except Exception as e:
                self._log_writer.write_error("Exception occurred during the send/receive operation: {}".format(e))

    def wait_on_connect(self, timeout):
        """
        Wait till the connection to the socket is successful.
        :param timeout: Time period in second to wait after failure to connect.
        :return: None
        """
        while not self.connected:
            # attempt to reconnect, otherwise sleep for 2 seconds
            try:
                if self._socket_type is Device.SocketType.TCP:
                    self._sock_init()

                self.connected = True
                self._log_writer.write_info("Re-connection with {}:{} successful.".format(self.host, self.port))
            except socket.error as e:
                self._log_writer.write_error('Socket initialization failed {}:{}, {}'.
                                             format(self.host, self.port, e))
                time.sleep(timeout)  # Wait before re-attempting the connection

    def tasks_activate(self, add_list):
        """
        Activates tasks for updates/requests.
        :param add_list: List(str task name) to be activated.
        :return:
        """
        for task in add_list:
            if task in self.update_task_list:
                self.update_task_list[task].active = True

    def tasks_deactivate(self, remove_list):
        """
        Deactivates tasks for updates/requests.
        :param remove_list: List(str task name) to be deactivated.
        :return:
        """
        for task in remove_list:
            if task in self.update_task_list:
                self.update_task_list[task].active = False

    def update_params(self):
        """
        This function takes care of the periodic updating of the parameters.

        This function calls the send function and then receives the response.
        It is followed by the execution of the mirror function of the task.
        """
        self._log_writer.write_info("Starting updation of parameters.")
        while True:
            for task_name, task in self.update_task_list.items():
                if self.update_flag is True:
                    if task.active is True:
                        scpi_command = self.query_message_create(task_name)
                        self.tx_buffer.append(scpi_command)
                        try:
                            self.active_send = True
                            self.send_receive_buffer()
                        except Exception as e:
                            self.connected = False
                            self._log_writer.write_error('Error while sending/receiving. Connection lost? '
                                                         'Trying to re-connect. {}'.format(e))
                            self.wait_on_connect(2)
                        finally:
                            self.active_send = False

                    time.sleep(self.update_interval)

    def send_receive_buffer(self):
        """
        Sends all pending message from tx buffer.
        :return:
        """
        while self.tx_buffer:
            self.send_receive_msg(self.tx_buffer.popleft())

    def send_receive_msg(self, send_message=None):
        """
        This function sends a message and waits for its reply.
        :return: Received message. None if no message received.
        """
        if send_message is not None:
            self._sock_send(send_message)
            self._log_writer.write_debug('Send-Receive-Message sent {}'.format(send_message))

            msg_received = self.receive_msg().lower()
            self._log_writer.write_debug("Received message {}".format(msg_received))
            if msg_received is not None:
                if self.device_reply_header_present == True:
                    receive_cmd_name = self.receive_message_parse(msg_received)
                else:
                    receive_cmd_name = send_message.strip(self.msg_termination_character).strip(self.query_termination)
                                        
                    receive_cmd_name = self.receive_message_parse(send_message)

                if receive_cmd_name in self.update_task_list:
                    if self.device_reply_header_present == True:
                        self.update_task_list[receive_cmd_name].mirror_function(msg_received)
                    else:
                        self._log_writer.write_info("Calling mirror function {}".format(receive_cmd_name))
                        self.update_task_list[receive_cmd_name].mirror_function(self.update_task_list[receive_cmd_name],
                                                                                msg_received)
                    self._log_writer.write_debug(
                        'Answer received `extracted cmd:{}` `message:{}`'.format(receive_cmd_name, msg_received))

                else:
                    self._log_writer.write_error(
                        'Answer received: `extracted cmd:{}` `message:{}`'.format(receive_cmd_name, msg_received))
            else:
                self._log_writer.write_debug('Corrupt SCPI answer received:  {}'.format(send_message))

            return msg_received
        else:
            return None

    def send_msg(self):
        """
        Iterates over the messages in the transmit buffer and sends them out
        over TCP.
        :return:
        """
        while self.tx_buffer:
            cmd = self.tx_buffer.popleft()
            self.s.send(cmd.encode())

    def receive_msg(self):
        """
        Receives all the characters from the rx buffer till the message is completed.
        :return:
        """
        return self._sock_receive()

    def query_value(self, query_task):
        """
        Function to execute a query on the device.
        :param query_task: Task-Name (including all hierarchy levels)
        :return: True->Success
        """

        if self.auto_update is True:
            return True
        else:
            self.tx_buffer.appendleft(self.query_message_create(query_task))
            return self.send_receive_msg() is not None

    def query_message_create(self, query_task):
        """
        Creates the entire query message (prefixed, terminated) from a task_name.
        :param query_task: Task name including all hierarchies.
        :return: Query message
        """
        # First add prefix
        query_task = self.scpi_prefix_add(query_task)

        # Create query
        return self.scpi_termination_add('{}{}'.format(query_task, self.query_termination))

    def set_value(self, task_name, message):
        """
        Function to execute a set request on the device.
        :param task_name: Task name (including all hierarchies)
        :param message: The payload message.
        :return: True->Success
        """

        try:
            # Pause the update thread to not mess up the reply messages
            self.update_flag = False

            # First create a query message on that parameter to have an immediate update after the set
            scpi_msg = self.query_message_create(task_name)

            # Append left --> immediate
            self.tx_buffer.appendleft(scpi_msg)

            # Wait till send_receive routine is free
            wait_count = 0
            while self.active_send is True:
                wait_count += 1
                time.sleep(0.01)
                if wait_count > 100:
                    return None

            # Create set message
            # Prefix with '!!!' to indicate it should immediately be set
            scpi_msg = '{} !!!{}'.format(task_name, message)

            # Create the set message
            scpi_msg = self.set_message_create(scpi_msg)

            # Analyze the answer
            ret_val = self.send_receive_msg(scpi_msg)

        finally:
            # Start the update thread again
            self.update_flag = True

        # Analyze the answer
        return ret_val

    def set_message_create(self, message):
        """
        Creates the entire set message (prefixed, terminated) from a 'task_name+value'-message.
        :param message: Task name (including all hierarchies) + value.
        :return: Set message
        """
        # First add the prefix
        set_message = self.scpi_prefix_add(message)

        # In case we have a command prefix, insert it
        set_message = self.command_prefix_add(set_message)

        # Add termination
        return self.scpi_termination_add(set_message)

    def receive_message_parse(self, message):
        """
        Parses a receive message and removes any prefixes and terminations.
        :param message: Receive message to be parsed.
        :return: Parsed message.
        """

        # Remove the termination
        message = message.strip(self.msg_termination_character)

        # Remove the scpi prefix
        if self.scpi_prefix != '':

            message = split_always_list(message, separator=' ', remove_whitespaces=False)[0]. \
                replace('{}{}'.format(self.scpi_prefix, self.separator), '').\
                replace(self.query_termination, '')
        else:
            message = split_always_list(message, separator=' ', remove_whitespaces=False)[0]. \
                lstrip(':').replace(self.query_termination, '')
        # Remove the command prefix
        message = self.command_prefix_remove(message)

        return message

    def scpi_prefix_add(self, message):
        """
        Adds the scpi prefix ([prefix][separator]) to a message.
        :param message: Message to add the prefix to.
        :return: Prefixed message.
        """
        return '{}{}{}'.format(self.scpi_prefix,
                               self.separator,
                               message)

    def scpi_termination_add(self, message):
        """
        Adds the scpi termination ([message][termination]) to a message.
        :param message: Message to add the termination to.
        :return: Terminated message.
        """
        return '{}{}'.format(message, self.msg_termination_character)

    def command_prefix_add(self, message):
        """
        Adds the command prefix ([devices][command-prefix][task-name]) to a message.
        :param message: Message to add the prefix to.
        :return: Message with command prefixed task name.
        """
        if self.set_indicator == Device.SetIndicator.CMD_PREFIX:
            message = split_always_list(message, self.separator, remove_whitespaces=False)
            message[-1] = '{}{}'.format(Device.SetIndicatorMapping[self.set_indicator.name], message[-1])
            message = self.separator.join(message)
        return message

    def command_prefix_remove(self, message):
        """
        Removes the command prefix ([devices][command-prefix][task-name]) from a message.
        :param message: Message to remove the prefix from.
        :return: Message without command prefixed task name.
        """
        if self.set_indicator == self.SetIndicator.CMD_PREFIX:
            # Remove CMD command prefix
            message = split_always_list(message, self.separator, remove_whitespaces=False)
            message[-1] = message[-1].replace(
                Device.SetIndicatorMapping[self.set_indicator.name], '')
            message = self.separator.join(message)
        return message

    # def state_machine(self):
    #     """
    #     This function runs continuously and takes care of the send/receive
    #     sequence if the device supports multiple command handling at a time.
    #     :return: None
    #     """
    #     while True:
    #         if self.state_machine_flag is True:
    #             if self.state_machine_flag is True:
    #
    #                 while (self.rx_buffer.qsize() is not
    #                        self.SCPI_COMMANDS_MAX_COUNT):
    #
    #                     if self.rx_buffer.empty() is True:
    #                         if (self.mirror_timeout_flag !=
    #                                 self.mirror_timeout_flag_old):
    #                             self.mirrors_pending_check(None)
    #                         self.mirror_timeout_flag_old = \
    #                             self.mirror_timeout_flag
    #                         self.send_msg()
    #
    #                     else:
    #                         while self.rx_buffer.empty() is False:
    #                             read_msg = self.rx_buffer.get()
    #                             # extract the message name and other parameters
    #                             self.process_message(read_msg)
    #                     time.sleep(0.0001)
    #
    #                 self.rx.buffer.clear()

    def start_threads(self):
        """
        Starts all the threads related to the device after reading the flags.

        :return: None
        """

        if self.auto_update is True:
            self.update_flag = True
            update_thread = threading.Thread(target=self.update_params)
            update_thread.start()
            self._log_writer.write_info("Update thread started.")
        else:
            # This will be non-auto-update specific function that will respond
            # to the katcp requests on fly
            pass

        if self.multiple_commands is True:
            self.state_machine_flag = True
            state_machine_thread = threading.Thread(target=self.state_machine)
            state_machine_thread.start()

    def stop(self):
        """
        Stops all the threads related to the device.

        :return:
        """
        self.update_flag = False
        self.state_machine_flag = False

    def _sock_udp_init(self):
        self._sock = SockUDP(
            self.host,
            self.port,
            10,
            self.msg_termination_character,
            self.msg_termination_character,
            self._log_writer
        )

    def _sock_udp_send(self, send_data):
        self._sock.send_data(send_data)

    def _sock_udp_receive(self):

        # Send message
        _receive_message = self._sock.receive_data(60000)

        return _receive_message

    def _sock_tcp_init(self):
        self._sock = SockTCP(
            self.host,
            self.port,
            5,
            self.msg_termination_character,
            self.msg_termination_character,
            self._log_writer
        )

    def _sock_tcp_send(self, send_data):
        self._sock.send_data(send_data)

    def _sock_tcp_receive(self):
        """
        Receives all the characters from the rx buffer of TCP till the message
        is completed.

        :return:
        """
        data = self._sock.receive_data(1)
        return data
