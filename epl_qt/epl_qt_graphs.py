from PyQt5.QtWidgets import QVBoxLayout, QHBoxLayout, QPushButton, QWidget

from matplotlib.backends.backend_qt5agg import \
    NavigationToolbar2QT as NavigationToolbar, \
    FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure
import math


# TODO commenting once finalized
class MplCanvas(FigureCanvas):

    def __init__(self, parent=None, width=5, height=4, dpi=100):
        self.figure = Figure(figsize=(width, height), dpi=dpi, constrained_layout=True)
        self.axes = self.figure.add_subplot(111)

        super().__init__(self.figure)


class Plotter(QWidget):

    def __init__(self,
                 use_toolbar=False,
                 show_legend=False,
                 plots=list(),
                 label_x='',
                 label_y='',
                 margins_autoscale=0.5,
                 graph_title='',
                 widget_name='',
                 show_close_button=False):

        # Init the base QWidget
        super().__init__()

        # Init variables
        self.setObjectName(widget_name)
        self._show_legend = show_legend

        # Internal layout is QVBox to have canvas and toolbar separated
        self.layout = QVBoxLayout()

        # Canvas widget that displays the `figure`
        self.canvas = MplCanvas()

        # Use navigation toolbar?
        if use_toolbar is True:
            # Toolbar
            self.toolbar = NavigationToolbar(self.canvas, self.canvas)

            toolbar_layout = QHBoxLayout()

            # Add toolbar widget to layout
            toolbar_layout.addWidget(self.toolbar)

            # Add close button?
            if show_close_button is True:
                self.close_button = QPushButton('Remove Graph')
                self.close_button.setObjectName(graph_title)
                toolbar_layout.addWidget(self.close_button)

            self.layout.addLayout(toolbar_layout)

        # Insert an empty line for every plot to initialize the plot_refs.
        self.plot_refs = dict()
        for plot in plots:
           self.add_plot(plot)

        # Set axes labels/title
        self.canvas.axes.set_xlabel(label_x)
        self.canvas.axes.set_ylabel(label_y)
        self.canvas.axes.set_title(graph_title)

        # Set margins
        self.canvas.axes.margins(margins_autoscale)

        # Show legend?
        self._update_legend()

        # Add the canvas widget
        self.layout.addWidget(self.canvas)

        # Set layout
        self.setLayout(self.layout)

    def add_plot(self, plot):
        """
        Adds a plot to the canvas.
        :param plot:
        :return:
        """
        if plot not in self.plot_refs:
            self.plot_refs[plot], = self.canvas.axes.plot([0], [0], label=plot, linestyle='solid',
                                                          linewidth=1, markersize=5, marker='o')
        # Update legend
        self._update_legend()

    def _update_legend(self):
        """
        If legend should be shown, it shows the legend.
        Also updates the current legend.
        :return:
        """
        if self._show_legend is True:
            self.canvas.axes.legend(fontsize='x-small', loc='upper right', mode='expand')

    def plot(self, data_x, data_y, plot_name):
        """
        Plot x/y data
        :param data_x: List with x-data.
        :param data_y: List with y-data.
        :param plot_name: Name of plot to be updated.
        :return:
        """

        # Use plot ref to update the data for that line.
        self.plot_refs[plot_name].set_xdata(data_x)
        self.plot_refs[plot_name].set_ydata(data_y)

        # Auto-scale the graph
        self.canvas.axes.relim()
        self.canvas.axes.autoscale(tight=False)

        # Trigger the canvas to update and redraw.
        # Only redraw when necessary
        self.canvas.draw()

