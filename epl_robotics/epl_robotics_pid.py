from basicpid import BasicPid
from epl.epl_threading.epl_threading_thread import TriggerTimeout
from epl.epl_logging.epl_logging_log import Log


class PidThreadClass(BasicPid):

    def __init__(self,
                 kp, ki, kd,
                 output_invert=False,
                 output_offset=0.0,
                 output_ll=None,
                 output_ul=None,
                 reset_pid_ll=False,
                 reset_pid_ul=False,
                 thread_period_s=1.0,
                 thread_start_immediate=True,
                 logger=None):
        """
        Initializes the module.
        :param kp: PID-KP
        :param ki: PID-KI
        :param kd: PID-KD
        :param output_invert: Invert output on calculation.
        :param output_offset:  Add an offset to calculation (0.0 to deactivate).
        :param output_ll: Lower-Limit for output -> will cut output at this value.
        :param output_ul: Upper-Limit for output -> will cut output at this value.
        :param reset_pid_ll: True->Reset pid when lower-limit was exceeded.
        :param reset_pid_ul: True->Reset pid when upper-limit was exceeded.
        :param thread_period_s: Period for the pid thread to execute with.
        :param thread_start_immediate: True->Start thread immediately.
        :param logger: epl_logging.Log
        """

        # Init base class
        super().__init__()

        # Init the pid thread
        self._pid_thread = TriggerTimeout(callback=self._pid_thread_callback,
                                          timeout=thread_period_s,
                                          args=None,
                                          start_immediate=thread_start_immediate)

        # Set logger, if None create new one
        self._logger = logger if logger is not None else Log(
            name="pid", level_console=Log.LoggingLevels.DEBUG,
            formatter_console=[Log.Formatters.ASC_TIME, Log.Formatters.FILE_NAME,
                               Log.Formatters.FUNC_NAME, Log.Formatters.LINE_NUMBER],
        )

        # The external signals
        self._ref_signal = 0
        self._output_signal = 0
        self._output_offset = output_offset
        self._output_ll = output_ll
        self._output_ul = output_ul

        # Reset flags
        self._reset_pid_ll = reset_pid_ll
        self._reset_pid_ul = reset_pid_ul

        # Flags related to output calculation
        self._output_invert = output_invert

        # The calculated pid result
        self._pid_result = 0

        # Debug
        self._logger.write_info("pid:{},{}".format(self._name, self._vers))

        # Set gains
        self.setGains(kp, ki, kd)

        # check settings
        self._logger.write_debug("Gains:{}".format(self.getGains()))

        # use integrate mode
        self.setIntegrateModeOn()

        # check mode flags
        self._logger.write_debug("inIntegrateMode:{}".format(self.inIntegrateMode()))
        self._logger.write_debug("inIterateMode:{}".format(self.inIterateMode()))

    def thread_start(self):
        """
        Starts the pid thread.
        :return:
        """

        # Start thread
        self._pid_thread.start_thread()

    def thread_stop(self):
        """
        Stops the pid thread.
        :return:
        """

        # Stop thread
        self._pid_thread.stop_thread()

    def ref_signal_set(self, float_value):
        """
        Sets the reference signal to a new float value.
        :param float_value: float value to be set.
        :return:
        """
        self._logger.write_info("Setting ref-signal to:{}".format(float_value))
        self._ref_signal = float(float_value)

    def ref_signal_get(self):
        """
        Returning the actual ref signal.
        :return:
        """
        return self._ref_signal

    def output_signal_update(self, float_value):
        """
        Sets the reference signal to a new float value.
        :param float_value: float value to be set.
        :return:
        """
        self._logger.write_info("Setting output signal to:{}".format(float_value))
        self._output_signal = float(float_value)

    def pid_result_get(self):
        """
        Returns the actual output signal.
        :return: float
        """
        return self._pid_result

    def _pid_thread_callback(self):
        """
        Callback for the pid thread.
        It implements the actual error calculation.
        :return:
        """

        self._logger.write_debug("Entering pid thread callback")

        # Calculate new output value
        pid_result_tmp = self.get(self._ref_signal, self._output_signal)

        self._logger.write_debug("Calculated pid result from ref-signal:{}, output-signal:{} is ".format(
            self._ref_signal, self._output_signal, pid_result_tmp))

        # Invert the output?
        if self._output_invert:
            pid_result_tmp = -pid_result_tmp
            self._logger.write_debug("Inverting pid result to:{}".format(pid_result_tmp))

        # Correct offset?
        pid_result_tmp += self._output_offset
        self._logger.write_debug("Correcting with offset `{}` to:{}".format(self._output_offset, pid_result_tmp))

        # Check limits
        if self._output_ul and pid_result_tmp > self._output_ul:

            # Set to upper limit
            pid_result_tmp = self._output_ul

            self._logger.write_debug("Exceeded upper limit, setting pid result to:{}".format(pid_result_tmp))

            # Reset pid on upper limit?
            if self._reset_pid_ul:
                self._logger.write_debug("Resetting pid because of upper level exceeding")
                self.reset()

        elif self._output_ll and pid_result_tmp < self._output_ll:

            # Set to lower limit
            pid_result_tmp = self._output_ll

            self._logger.write_debug("Exceeded lower limit, setting pid result to:{}".format(pid_result_tmp))

            # Reset pid on lower limit?
            if self._reset_pid_ll:
                self._logger.write_debug("Resetting pid because of lower level exceeding")
                self.reset()

        # Update result
        self._pid_result = pid_result_tmp

        self._logger.write_debug("pid result at end of calculation:{}".format(self._pid_result))