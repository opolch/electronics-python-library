import asyncio
from aiokatcp import Sensor, SensorSampler, Timestamp
from enum import Enum, auto
from epl.epl_string import epl_string_split, epl_string_convert
from epl.epl_logging.epl_logging_log import Log
import time
import sys

class KatcpSensor(Sensor):
    """
    Class derived from katcp sensor class.
    """

    class SensorStatus(Enum):
        """
        Enum with status values for katcp sensors.
        """
        unknown = 0
        nominal = 1
        warn = 2
        error = 3
        failure = 4
        unreachable = 5
        inactive = 6

        @classmethod
        def from_string(cls, status_string):
            try:
               return cls(status_string)
            except ValueError:
               raise ValueError(f"Invalid sensor status: {status_string}")

    class SamplingStrategy(Enum):
        """
        Class to enumerate the KATCP sampling strategies.
        """
        auto = 'auto'
        none = 'none'
        period = 'period'
        event = 'event'
        differential = 'differential'
        event_rate = 'event-rate'
        differential_rate = 'differential-rate'
        
        @classmethod
        def from_string(cls, strategy_string):
            try:
               return cls(strategy_string)
            except ValueError:
               raise ValueError(f"Invalid strategy string: {strategy_string}")

    """ Mapping from sampling strategy to number of params """
    SAMPLING_STRATEGY_NUMPARMS = {
        SamplingStrategy.auto: 0,
        SamplingStrategy.none: 0,
        SamplingStrategy.period: 1,
        SamplingStrategy.event: 0,
        SamplingStrategy.differential: 1,
        SamplingStrategy.event_rate: 2,
        SamplingStrategy.differential_rate: 3,
    }

    """Mapping from sensor type strings to python type classes"""
    TYPE_CASTINGS = {
        'integer': int,
        'float': float,
        'boolean': bool,
        'lru': str,
        'discrete': str,
        'string': str,
        'timestamp': str,
        'address': str
    }

    def __init__(self, sensor_type, name, sensor_type_str=None, description=None, units='',
                 params=None, default=None, step=1.0, initial_status=None, type=None, value_list_max_size=1,
                 sampling_strategy=SamplingStrategy.auto, sampling_strategy_params='',
                 sampling_period_default=5, logger=None):
        """
        Inits the sensor object.
        :param sensor_type: Sensor type constant.
        :param name: The name of the sensor.
        :param description: A short description of the sensor.
        :param units: The units of the sensor value. May be the empty string if there are no applicable units.
        :param params: Additional parameters, dependent on the type of sensor:
          * For :const:`INTEGER` and :const:`FLOAT` the list should
            give the minimum and maximum that define the range
            of the sensor value.
          * For :const:`DISCRETE` the list should contain all
            possible values the sensor may take.
          * For all other types, params should be omitted.
        :param default: An initial value for the sensor. By default this is determined by the sensor type.
        :param initial_status: An initial status for the sensor. If None, defaults to
        Sensor.unknown. `initial_status` must be one of the keys in
        Sensor.STATUSES
        :param value_list_max_size: Maximum number of values to store in value_list.
        :param sampling_strategy: Sensor's sampling strategy.
        :param sampling_strategy_params: Params field for sampling strategy.
        :param sampling_period_default: Default value for fall-back sampling strategy period
            (in case preferred strategy is not supported by sensor).
        :param logger: epl_logging_log.Log logger.
        """

        # Set variables
        self._value_list_max_size = value_list_max_size
        self._initialized = False
        self.value_list = list()
        self.value_edge = False
        self.step = step 
        self._was_updated = False
        self.type = sensor_type_str
        self._sensor_type = sensor_type
        if logger is None:
            self._log_writer = Log()
        else:
            self._log_writer = logger

        # Init the parent class
        try:
            super(KatcpSensor, self).__init__(sensor_type, name, description, units,
                                          default, initial_status)
        except Exception as e:
            print(f"katcp init :{name} {sensor_type} failed:{len(params)},{e}")

        try:
          if len(params) > 1:
            self._params = params  
          elif self.type in ['float', 'int']:
            self._params = [-sys.maxsize, sys.maxsize, -sys.maxsize, sys.maxsize]
          else:
            self._params = None
        except Exception as e:
            print(f"params init failed:{len(params)},{e}")

        # Extract limits
        self.lower_warn = None
        self.upper_warn = None
        self.lower_error = None
        self.upper_error = None

        if self.sensor_type_float_int_check():
            try:
                if len(params) == 4:
                    self.lower_warn, self.upper_warn, self.lower_error, self.upper_error = \
                        [float(x) for x in self._params]
                elif len(params) == 2:
                    self.lower_error, self.upper_error = [float(x) for x in self._params]
                elif len(params) == 0:
                    pass
            except:
                self._log_writer.write_warning('Can not convert limits for task `{}`, params: {}'.
                                             format(name, self._params))

        # Check whether the sensors not supporting it, have been mistakenly allocated
        # differential-based sampling. If yes, set the periodic sampling
        # for the initialization process.

        if ((isinstance(self.type, float) or isinstance(self.type, int)
             or isinstance(self.type, Timestamp))
                or (sampling_strategy.name != KatcpSensor.SamplingStrategy.differential.name
                    and sampling_strategy.name != KatcpSensor.SamplingStrategy.differential_rate.name)):

            try:
                self.sampling_strategy_params = epl_string_split.split_always_list(
                     sampling_strategy_params, remove_whitespaces=False)
            except Exception as e:
                print(f"katcp init :{name} {sensor_type} failed:{len(params)},{e}")
            
            # Check number of parameters
            if (len(self.sampling_strategy_params) ==
                    self.SAMPLING_STRATEGY_NUMPARMS[sampling_strategy]):

                # Make sure passing properly formatted limits
                if isinstance(self.type, int):
                    self.sampling_strategy_params = \
                        [int(float(self.sampling_strategy_params))] #[str(int(float(x))) if float(x) > 0.5 else '1' for x in self.sampling_strategy_params]
                elif isinstance(self.type, float):
                    self.sampling_strategy_params = \
                        [float(self.sampling_strategy_params)]
                self.sampling_strategy = sampling_strategy

            else:
                self.sampling_strategy = self.SamplingStrategy.event
                self.sampling_strategy_params = "" [float(sampling_period_default)]

                self._log_writer.write_info(
                    "{}: Wrong number of parameters for given sampling strategy. "
                    "Tentatively setting the event sampling for it.".format(self.name))
        else:
            # Use event-based sampling as default for this type of sensor
            self.sampling_strategy = self.SamplingStrategy.event
            self.sampling_strategy_params = ""

            self._log_writer.write_info(
                "Sensor {} of type {} does not support {} strategy (please change its strategy). "
                "Tentatively setting the event-based sampling for it.".
                format(self.name, self.type, sampling_strategy.name))

    def get_params(self):

        return self._params

    async def set_sensor_value(self, value, status=None, timestamp=None):
        """
        Overriding the set_value function to implement history data functionality.
        :param value: value to be set.
        :param status: SensorStatus, when None is passed, function tries to perform auto-check against limits.
        If that fails (no proper limits, ...) NOMINAL will be set.
        :param timestamp: Time stamp.
        :return:
        """

        set_status = status

        # Take current time if no time provided
        if timestamp is None:
            timestamp = time.time()

        self._log_writer.write_debug('Updating sensor {}, time: {},  msg: {}, type: {}'.
                                     format(self.name, timestamp, value, self._sensor_type))

        # If no status has been passed, try to perform auto-check
        try:
            if set_status is None:

                # Currently only with int,float type sensors
                try:
                    if self.lower_error == self.upper_error == 0.0:
                        set_status = self.SensorStatus.nominal
                    elif self.lower_error <= float(value) <= self.upper_error:
                        set_status = self.SensorStatus.nominal
                    else:
                        set_status = self.SensorStatus.error
                except:
                    set_status = self.SensorStatus.nominal
            else:
                set_status = self.SensorStatus[set_status]
        except:
            set_status = self.SensorStatus.unknown
        
        # Call the base function, super(KatcpSensor, self)
        try: 
            super(KatcpSensor, self).set_value(value, status=int(set_status.value), timestamp=timestamp)
        except Exception as ex:
            self._log_writer.write_warning('Updating sensor {}, time: {},  msg: {}, status: {}, type: {}, exception: {}'.
                                     format(self.name, timestamp, value, set_status,  self._sensor_type, ex))
        self._log_writer.write_debug('Updated sensor {}, time: {},  status: {}, msg: {}, type: {}'.
                                     format(self.name, timestamp, value, set_status, self._sensor_type))

        # Add value to values list
        if self.type in ['bool', 'boolean']:
            _value = epl_string_convert.str_to_bool(value) 
            self.value_list.insert(0, [float(timestamp), int(set_status.value), _value])
            
        elif self.sensor_type_float_int_check():
            try:
               prev_value = self.read()[2]
            except:
               prev_value = 0
            if self.read_edge(prev_value,float(value)):
                self.value_edge = True
            else:
                self.value_edge = False

            self.value_list.insert(0, [float(timestamp), int(set_status.value), float(value)])
        else:
            self.value_list.insert(0, [float(timestamp), int(set_status.value), value])

        # Check list size
        if len(self.value_list) > self._value_list_max_size:
            self.value_list.pop()
        self._log_writer.write_debug(f"sensor:{self.name}, value_list:{self.value_list}")
        self._was_updated = True

    def value_list_clear(self, keep_one_element=False):
        """
        Clear the value list.
        :return:
        """
        if keep_one_element is True:
            self.value_list = self.value_list[0:1]
        else:
            self.value_list.clear()

        self._was_updated = False

    def sensor_type_int_check(self):
        """
        Checks if the sensor type is either float or int.
        :return: True->Type is flot/int.
        """

        return self.type in ['int', 'integer']

    def sensor_type_float_int_check(self):
        """
        Checks if the sensor type is either float or int.
        :return: True->Type is flot/int.
        """
        #return self._sensor_type in [float, int]
        return self.type in ['float', 'int', 'integer','bool','boolean']

    def edge_reset(self):
        """
        Reset edge detection post read
        :return:
        """
        self.value_edge = False
        return True

    def read_edge(self, prev_value, value):
        """
        Detects if a given sensor has a transition 
        value: float
        step: float
        direction: 1->Up/0->Down
        :return:
        """
        
        _result = False
        try:
            _diff   = abs(float(value) - float(prev_value))

            if _diff >= self.step:
                _result = True 
            else:
                _result = False
        except Exception as e:
            _result = False
            self._log_writer.write_warning(f"sensor:{self.name}, edge detection w/ step:{self.step} failed with:{e}")
        
        return _result

    def read(self):
        """
        Read out most current value, timestamp, status from list.
        Overrides the base read function since it is not used here.
        :return:
        """
        # Reset update flag
        self._was_updated = False

        # Return default value in case the list is empty
        if len(self.value_list) == 0:
            if self.sensor_type_float_int_check():
                return [0.0, int(self.SensorStatus.error.value), 0.0]
            else:
                return [0.0, int(self.SensorStatus.error.value), 0]
        return self.value_list[0]

    def is_initialized(self):
        """
        Reads back the status of initialized flag.
        :return: bool.
        """
        return self._initialized

    def set_initialized(self, initialized):
        """
        Sets the status of initialized flag.
        :param initialized: bool.
        :return:
        """
        self._initialized = initialized

    def was_updated(self):
        """
        True if sensor was updated since last read().
        :return:
        """
        return self._was_updated
