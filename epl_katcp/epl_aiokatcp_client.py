import aiokatcp
import asyncio
import signal
from aiokatcp import Sensor, Client, connection
from epl.epl_katcp.epl_aiokatcp_sensor import KatcpSensor
from epl.epl_katcp.epl_aiokatcp_command import KatcpCommand, CommandState
from epl.epl_katcp import epl_aiokatcp_helper
from epl.epl_string import epl_string_split
from epl.epl_katcp.epl_aiokatcp_helper import CommandLogInform
import time
from epl.epl_logging.epl_logging_log import Log
from enum import Enum, auto
from queue import Queue

class MirrorServer(aiokatcp.DeviceServer):
    VERSION = "mirror-1.0"
    BUILD_STATE = "mirror-1.0.0"

class MirrorWatcher(aiokatcp.SensorWatcher):
    def __init__(self, client: aiokatcp.Client, server: aiokatcp.DeviceServer, SENSOR_MAP, logger=None):
        super().__init__(client)
        self.server = server
        self._interface_stale = False
        self.logger = logger
        self.sensor_map = SENSOR_MAP

    def sensor_added(
        self, name: str, description: str, units: str, type_name: str, *args: bytes
    ) -> None:
        super().sensor_added(name, description, units, type_name, *args)
        self.server.sensors.add(self.sensors[name])
        self._interface_stale = True
        self.logger.write_info("Added sensor %s", name)

    def sensor_removed(self, name: str) -> None:
        del self.server.sensors[name]
        self._interface_stale = True
        super().sensor_removed(name)
        self.logger.write_info("Removed sensor %s", name)

    def sensor_updated(
        self, name: str, value: bytes, status: aiokatcp.sensor.Sensor.Status, timestamp: aiokatcp.Timestamp
    ) -> None:
        self._interface_stale = True
        #super().sensor_updated(name, value, status, timestamp)
        try:

            self.sensor_map[name] = [float(timestamp), int(status.value), value]

        except Exception as e:
            self.logger.write_exception(f"Sensor update failed {e} for : {name}, {timestamp}, {status}, {value}")
        self.logger.write_debug(f"Sensor updated: {name}, {timestamp}, {status}, {value}")

    def batch_stop(self) -> None:
        if self._interface_stale:
            self.server.mass_inform("interface-changed", "sensor-list")
            self._interface_stale = False
            self.logger.write_debug("Sent interface-changed")


class KatcpClient(Client):
    """
    Class derived from base class katcp.DeviceClient to provide common katcp
    client functionality and adapt it to our needs.
    """

    class DeviceType(Enum):
        """
        Enum for device type parameter
        """
        GENERIC_KATCP = auto()
        PACKETIZER = auto()

    class DeviceStates(Enum):
        """
        Enum to define device states handled in device states queue.
        """
        UNDEFINED = auto()
        UNINITIALIZED = auto()
        SENSOR_LIST_INITIALIZING = auto()
        SENSOR_LIST_INITIALIZED = auto()
        COMMAND_LIST_INITIALIZING = auto()
        COMMAND_LIST_INITIALIZED = auto()
        INITIALIZED = auto()
        ERROR_INIT = auto()

    class MessageHandlers:
        """
        Class to collect default message handles.
        Needed to go with separate class to let devices add/delete fom that
        list based on their needs and not corrupt lists for other devices.
        """

        def __init__(self):
            # Dict with message handle callbacks
            self.MESSAGE_HANDLES = {
                'help': (KatcpClient.handle_help_reply,
                         KatcpClient.handle_help_inform),

                'sensor-list': (KatcpClient.handle_sensor_list_reply,
                                KatcpClient.handle_sensor_list_inform),

                'sensor-status': (KatcpClient.handle_sensor_status_reply,
                                  KatcpClient.handle_sensor_status_inform),

                'log': (KatcpClient.handle_log_reply, KatcpClient.handle_log_inform),

                'sensor-sampling': (KatcpClient.handle_sensor_sampling_reply,
                                    KatcpClient.handle_sensor_sampling_inform),

                'sensor-value': (KatcpClient.handle_sensor_value_reply,
                                 KatcpClient.handle_sensor_value_inform)
            }

    def __init__(self,
                 device_host,
                 device_port,
                 device_name,
                 katcp_root_name='',
                 connect_timeout=2,
                 init_timeout=10,
                 update_sensors_dict=dict(),
                 command_list=list(),
                 auto_reconnect=False,
                 sensor_period_default=2,
                 logger=None,
                 device_type=DeviceType.GENERIC_KATCP,
                 message_handles=None,
                 description_cleanup_handle=None,
                 autoconnect=True,
                 limit=connection.DEFAULT_LIMIT,
                 loop=None,
                 sensor_hist_depth=256
                 ):
        """
        Init the object.
        :param device_host: Katcp server's ip address/hostname number.
        :param device_port: Katcp server's port number.
        :param device_name: Unique device name.
        :param katcp_root_name: Root name of the katcp devices eg. `rxs`
        :param connect_timeout: Timeout for connection process.
        :param init_timeout: Timeout for init process.
        :param update_sensors_dict: Dict with sensors (katcp_name:sampling_strategy:params) to be fetched.
            Pass empty dict or nothing for ALL_SENSORS.
        :param command_list: List with commands to make available.
            Pass empty list or nothing for ALL_COMMANDS.
        :param auto_reconnect: Auto reconnect the server after connection loss.
        :param sensor_period_default: Interval for sensor update strategy when
            in update_sensors_all-mode (update_sensors_dict is empty).
        :param logger: epl_logging_log.Log logger.
        :param update_sensors_dict: Sensor dict along with sensor name and
            their sensor sampling strategies + params
        :param message_handles: dict holding message handles to override or extend the existing ones.
        :param description_cleanup_handle: Function to clean up the sensor/command descriptions
        :param sensor_hist_depth: Depth of sensor history.
        """

        #super().__init__(host=device_host, port=device_port)

        # Init the logger
        if logger is None:
            self._log_writer = Log()
        else:
            self._log_writer = logger

        self.device_name = device_name
        self.device_type = device_type
        self._katcp_root_name = katcp_root_name
        self._device_host = device_host
        self._device_port = device_port
        self._local_port = 43316
        self._connect_timeout = connect_timeout
        self._init_timeout = init_timeout
        self._refresh_interval = sensor_period_default
        self.update_sensors_dict = update_sensors_dict
        self._description_cleanup_handle = description_cleanup_handle
        self._sensor_hist_depth = sensor_hist_depth
        self._thread_was_started = False

        # Init functions
        self._init_funcs = [self.init_sensor_list, self.init_command_list,
                            self.init_sensor_sampling, self.init_sensor_values]

        # Flag indicating the successful sensor-/command-list initializing
        self._state_queue = asyncio.Queue(1)
        self._state_queue.put_nowait(self.DeviceStates.UNDEFINED)

        # The client's sensor map.
        self.SENSOR_MAP = dict()
        self.SENSOR_UPDATES = dict()

        # If dict is empty, we fetch all the sensors
        self._update_sensors_all = True
        if len(update_sensors_dict) > 0:
            self._update_sensors_all = False

        # The clients command map. If list was empty, all commands will be activated
        self._commands_all = True
        if len(command_list) > 0:
            self.COMMAND_MAP = dict.fromkeys(command_list)
            self._commands_all = False
        else:
            self.COMMAND_MAP = dict()

        self.client = None

        # Create message handles
        # Must come from a class and not global list since some devices
        # will add to this list. When global, all devices have them.
        mh = KatcpClient.MessageHandlers()
        self._MESSAGE_HANDLES = mh.MESSAGE_HANDLES
        self.message_handles = message_handles

    def is_connected(self):

        return self.client.is_connected

    async def disconnect(self):
        """Disconnect from the KATCP server."""
        if self.is_connected():
            await self.stop()
            self._log_writer.write_info("Disconnected from KATCP server.")
        else:
            self._log_writer.write_info("Client is not connected.")

    async def run_client(self):   
        
        self._log_writer.write_info(f"connecting to katcp server on {self._device_host}:{self._device_port}")
        """Connect to the KATCP server."""
        try:
            asyncio.get_event_loop().add_signal_handler(signal.SIGINT, asyncio.current_task().cancel)
            self.client = await Client.connect(self._device_host, self._device_port)  # Calling the parent's connect method
            self._log_writer.write_info(f"Successfully connected to {self._device_host}:{self._device_port}")

            self.server = MirrorServer("0.0.0.0", self._local_port)
            await self.server.start()

            self.watcher = MirrorWatcher(self.client, self.server,self.SENSOR_UPDATES,self._log_writer)
            self.client.add_sensor_watcher(self.watcher)

            # Handles to be added or overwritten?
            if self.message_handles is not None:
               self._MESSAGE_HANDLES.update(message_handles)

            await self._init_process()
 
            #self._log_writer.write_info(f"Adding inform callback")  
            #self.add_inform_callback("sensor-status", self.handle_inform)

            # Debug
            #reply, inform = await self.client.request("sensor-list")
            #await self.handle_sensor_list_inform(inform)
            #reply, inform = await self.client.request("help")
            #await self.handle_help_inform(inform)
            return True
        except Exception as e:
            print(f"Failed to parse: {e}")
            return False

        # Handles to be added or overwritten?
        if self.message_handles is not None:
            self._MESSAGE_HANDLES.update(message_handles)

    async def device_state_set(self, state):
        """
        Sets the device state in the device state queue.
        :return:
        """
        if self._state_queue.full():
            await self._state_queue.get()
        self._state_queue.put_nowait(state)

    async def device_state_get(self, keep):
        """
        Returns the actual device state from the device state queue.
        :return: None for no
        """
        state = None
        if self._state_queue.empty() is not True:
            state = await self._state_queue.get()
            if keep:
                await self.device_state_set(state)
        return state

    async def send_request(self, request_name, request_string=""):
        """
        Creates a katcp request message and adds a callback to send it.
        :param request_string: Message string.
        :return:
        """

        # katcp parser for message parsing
        #_katcp_parser = aiokatcp.core.Message()
        
        result = ""
        informs = ""
        reply = []
        inform = []

        try:
           #msg = aiokatcp.core.Message.parse(request_string.encode('utf-8'))
           #msg = request_string.encode('utf-8')
           reply, inform = await self.client.request(request_name, request_string)

           for i in reply:
               result = result + " " + i.decode()
           for msg in inform:
               msg_reply = epl_aiokatcp_helper.extract_reply_general(msg.name, msg.arguments)
               _params = ''.join(str(i) for i in msg_reply.params)
               informs = informs + msg.name + " " + msg_reply.status + "," + _params + " "
           
           return result, informs
        except Exception as ex :
            self._log_writer.write_error('Msg {}: Katcp Parse Error. {}'.
                                           format(request_string, ex))
            return False, False

    async def _init_process(self):
        """
        Actual init process, calling all the init functions and handling their returns.
        :return:
        """

        # Set device state
        await self.device_state_set(self.DeviceStates.UNINITIALIZED)

        # Clear sensor MAP
        self.SENSOR_MAP.clear()
        self.COMMAND_MAP.clear()

        # Iterate over init funcs
        for init_func in self._init_funcs:
            _init_result = await init_func(self._init_timeout)
            if _init_result is not True:

                # Force the client to disconnect, to make it reconnect and retry the init
                #await self.client.wait_disconnect()
                self._log_writer.write_error('Client {}: Init was not successful, disconnecting to let it re-connect'
                                             ' -> re-init (failed on {}).'.
                                             format(self.device_name, init_func.__name__))
                return False

        await self.device_state_set(self.DeviceStates.INITIALIZED)
        self._log_writer.write_info('Client {}: Init successful.'.
                                    format(self.device_name))

    async def init_sensor_list(self, timeout_s=10.0):
        """
        Routine to init the sensor list.
        :param timeout_s: Max timeout for all the sens to be initialized.
        :return: True-> Success.
        """

        # Set device state
        await self.device_state_set(self.DeviceStates.SENSOR_LIST_INITIALIZING)

        # Update sensor list
        reply, inform = await self.client.request("sensor-list")
        await self.handle_sensor_list_inform(inform)

        # Wait for the sensor-list to be initialized
        wait_initialized = 0.0
        while True:
            device_state = await self.device_state_get(keep=True)
            if device_state == self.DeviceStates.SENSOR_LIST_INITIALIZED:
                break
            await asyncio.sleep(0.1)
            wait_initialized += 0.1
            if wait_initialized > timeout_s:
                self._log_writer.write_error('Client {}: Sensor-List not initialized.'.
                                             format(self.device_name))
                return False

        # Successfully initialized
        return True

    async def init_sensor_sampling(self, timeout_s=10.0, check_timeout=True):
        """
        Sets the sensor-sampling strategy and verifies the replies.
        :param timeout_s: Max timeout for all the sens to be initialized.
        :param check_timeout: Use timeout functionality.
        :return: True->Success.
        """
        # Set sensor-sampling strategy
        for sensor_name, katcp_sensor in self.SENSOR_MAP.items():
            katcp_sensor.set_initialized(False)
            _strategy_name = katcp_sensor.sampling_strategy.name.replace('_', '-')
            _strategy_params = katcp_sensor.sampling_strategy_params
            self._log_writer.write_debug(f'Sensor sampling {sensor_name}:{_strategy_name}:{_strategy_params} Sensor-Sampling init...')
            try:
                if self._update_sensors_all is True:
                    await self.client.request(sensor_name, self._refresh_interval)
                else:
                    for update_sensor, values in self.update_sensors_dict.items():
                        if update_sensor in sensor_name:
                            # If all fine, send sampling strategy request
                            _req = "sensor-sampling"
                            #_msg = "{} {} {}". \
                            #    format(sensor_name, katcp_sensor.sampling_strategy.name.replace('_', '-'),
                            #           ' '.join(katcp_sensor.sampling_strategy_params))                            

                            if len(_strategy_params) > 0:
                               reply, inform = await self.client.request(_req, sensor_name, _strategy_name, _strategy_params[0])
                               self._log_writer.write_debug(f'Sensor sampling {sensor_name}: Sensor-Sampling init done. reply:{reply},{inform}')
                               katcp_sensor.set_initialized(True)
                               break
                            else:
                               reply, inform = await self.client.request(_req, sensor_name, _strategy_name)
                               self._log_writer.write_debug(f'Sensor sampling {sensor_name}: Sensor-Sampling init done. reply:{reply},{inform}')
                               katcp_sensor.set_initialized(True)
                               break

            except Exception as ex:
                    self._log_writer.write_exception('init_sensor_sampling: Sensor sampling {} could not be initialized, error: {}, {}, {}'.
                                                    format(sensor_name, ex, _strategy_name, katcp_sensor.sampling_strategy_params))

        # In some modes it makes sense to not check the timeout
        # eg. reconnect
        if check_timeout is True:
            # Wait for sensors to be initialized
            wait_initialized = 0.0
            while True:
                num_success_init = 0
                for sensor_name, katcp_sensor in self.SENSOR_MAP.items():
                    if katcp_sensor.is_initialized() is False:
                        break
                    num_success_init += 1

                if num_success_init == len(self.SENSOR_MAP):
                    self._log_writer.write_info('Client {}: Sensor-Sampling initialized.'.
                                                 format(self.device_name))
                    return True

                await asyncio.sleep(0.1)
                wait_initialized += 0.1
                if wait_initialized > timeout_s:
                    self._log_writer.write_error('Client {}: Sensor-Sampling not initialized.'.
                                                 format(self.device_name))
                    return False
        
        self._log_writer.write_info('Skipped timeout check; Client {}: Sensor-Sampling initialized.'.
                                                 format(self.device_name))
        return True

    async def init_command_list(self, timeout_s=10.0):
        """
        Routine to init the command list.
        :param timeout_s: Max timeout for all the sens to be initialized.
        :return: True-> Success.
        """

        # Set device state
        await self.device_state_set(self.DeviceStates.COMMAND_LIST_INITIALIZING)

        # Update command list
        reply, inform = await self.client.request("help")
        await self.handle_help_inform(inform)

        # Wait for the command-list to be initialized
        wait_initialized = 0.0
        while True:
            # if self._command_list_initialized is True:
            _dstate = await self.device_state_get(keep=True)
            if  _dstate == self.DeviceStates.COMMAND_LIST_INITIALIZED:
                break
            await asyncio.sleep(0.1)
            wait_initialized += 0.1
            if wait_initialized > timeout_s:
                self._log_writer.write_error('Client {}: Command-List not initialized.'.
                                             format(self.device_name))
                return False

        # Successfully initialized
        return True

    async def init_sensor_values(self, timeout_s=0):
        """
        Fetches the current values of the KATCP sensors if the strategy is
        differential or event based.
        :param timeout_s: Not used, just for consistency to be able to loop through init_funcs.
        :return: True
        """
        if self._update_sensors_all is True:
            # If in update_all mode, all sensors are set to periodic.
            pass
        else:
            for sensor_name, katcp_sensor in self.SENSOR_MAP.items():
                try:
                    if ((katcp_sensor.sampling_strategy.name in
                            KatcpSensor.SamplingStrategy.differential_rate.name) or
                            (katcp_sensor.sampling_strategy.name in
                            KatcpSensor.SamplingStrategy.event_rate.name)):
                        reply, inform = await self.client.request('sensor-value', sensor_name)
                        self._log_writer.write_debug('init_sensor_values: Sensor {} reply, inform: {} {}'.
                                                    format(sensor_name, reply, inform))
                        await self.handle_sensor_value_inform(inform)
                except TypeError as ex:
                    self._log_writer.write_warning('init_sensor_values: Sensor {} could not be initialized, error: {}'.
                                                    format(sensor_name, ex))


        return True

    def handle_sensor_status_reply(self, message):
        """
        Sensor status reply handler.
        :param message: Katcp message object.
        :return:
        """
        return

    def handle_sensor_status_inform(self, message):
        """
        Updates values/adds values (if value-list is activated) in the
        SENSOR_MAP.

        :param message: Katcp message object.
        :return:
        """

        # Convert katcp message object to string
        message_string = str(message)

        # Split message fields
        fields = message_string.split()
        self.SENSOR_MAP[fields[3]].set_value(fields[5],
                                             KatcpSensor.SensorStatus
                                             [fields[4]],
                                             fields[1])
        return

    def handle_sensor_sampling_reply(self, message):
        """
        sensor-sampling reply handler. Used to check if all sampling strategy
        of all sensors have been set.

        :param message: Katcp message object.
        :return:
        """
        try:
            # Extract the katcp command
            sampling_reply = epl_aiokatcp_helper.extract_reply_general(message.name, message.arguments)
            self.SENSOR_MAP[sampling_reply.params[0].decode('utf8')].set_initialized(True)
        except IndexError:
            self._log_writer.write_error('Client {}: Sampling-Strategy for some sensor (sensor-name->see previous logs)'
                                         ' could not be set.'.format(self.device_name))
        return

    def handle_sensor_sampling_inform(self, message):
        """
        sensor-sampling inform handler. Currently not used.
        :param message: Katcp message object.
        :return:
        """
        return

    async def handle_sensor_list_reply(self, message):
        """
        After sensor-list was finished, remove un-supported entries from
        SENSOR_MAP.

        :param: Katcp message object.
        :return:
        """
        # A copy is necessary to not corrupt the dict while iterating
        sensors_not_supported = list()
        for key, value in self.SENSOR_MAP.copy().items():  # TODO: Move to dict library
            if value is None:
                del self.SENSOR_MAP[key]
                sensors_not_supported.append(key)

        # Any items to print
        if len(sensors_not_supported) > 0:
            self._log_writer.write_warning('Client {}: Following sensors are not supported by the device: {}.'
                                           .format(self.device_name,
                                                   sensors_not_supported))

        # Indicate successful sensor-list initializing
        await self.device_state_set(self.DeviceStates.SENSOR_LIST_INITIALIZED)

    async def handle_sensor_list_reply(self, message):
        """
        After sensor-list was finished, remove un-supported entries from
        SENSOR_MAP.

        :param: Katcp message object.
        :return:
        """
        # A copy is necessary to not corrupt the dict while iterating
        sensors_not_supported = list()
        for key, value in self.SENSOR_MAP.copy().items():  # TODO: Move to dict library
            if value is None:
                del self.SENSOR_MAP[key]
                sensors_not_supported.append(key)

        # Any items to print
        if len(sensors_not_supported) > 0:
            self._log_writer.write_warning('Client {}: Following sensors are not supported by the device: {}.'
                                           .format(self.device_name,
                                                   sensors_not_supported))

        # Indicate successful sensor-list initializing
        await self.device_state_set(self.DeviceStates.SENSOR_LIST_INITIALIZED)

    async def handle_sensor_list_inform(self, message):
        """
        Updates sensor list.
        :param message: Katcp message object.
        :return:
        """

        # Extract the single sensors (separated by linebreaks)
        s_list = epl_string_split.split_lines(str(message))

        for entry in message:
            fields = entry.arguments
            self._log_writer.write_debug(f"Message fields:{fields}")
            _ksensor_name = fields[0].decode()
            _ksensor_root = _ksensor_name.split(".")[0]
            _ksensor_add = True
            # filter based on device name
            if self.device_name != _ksensor_root:
                _ksensor_add = False
            # Parse the sensor type
            sens_type = fields[3].decode()
            sens_type_str = str(sens_type)
            # [b'rxs.atc.activate', b'', b'', b'float', b'0.0', b'100000.0']
            # Convert int/float params to int/float values, not strings

            if isinstance(sens_type, (float)):
                params = [float(i) for i in fields[4:]]
            elif sens_type_str in ['bool','boolean']:
                params = [bool(i) for i in fields[4:]]
                sens_type_str = 'boolean'
                if len(params) < 2:
                    params = [b'0.0', b'1.0', b'0.0', b'1.0']
            elif isinstance(sens_type, (int)):
                params = [int(i) for i in fields[4:]]
            else:
                params = fields[4:]

            self._log_writer.write_debug(f"sens type fields:{fields[0].decode()}:{sens_type}{params}")
            # Add all sensors in _sensors_all mode
            if _ksensor_add is True and self._update_sensors_all is True:
                self.SENSOR_MAP[fields[0].decode()] = KatcpSensor(
                    sensor_type=KatcpSensor.TYPE_CASTINGS[sens_type],
                    name=fields[0].decode(),
                    description=fields[1].decode().replace("\\_", " "),
                    sensor_type_str=sens_type_str,
                    units=fields[2].decode(),
                    params=params,
                    value_list_max_size=self._sensor_hist_depth,
                    sampling_strategy=KatcpSensor.SamplingStrategy.from_string("period"),
                    sampling_strategy_params=str(self._refresh_interval),
                    sampling_period_default=self._refresh_interval,
                    logger=self._log_writer
                    )
            elif _ksensor_add is True:

                # Only add sensor if exists in dict
                update_sensor = None

                if fields[0].decode() in self.update_sensors_dict.keys():
                    # First try to find the sensor as exact match
                    update_sensor = self.update_sensors_dict[fields[0].decode()]
                else:
                    # Now as wildcard search
                    for sensor_name, value in self.update_sensors_dict.items():
                        if sensor_name in fields[0].decode():
                            update_sensor = value

                # If sensor was found, add it
                if update_sensor is not None:
                    strategy_string = update_sensor['strategy'].split(".")
                    upd_strategy = KatcpSensor.SamplingStrategy.from_string(strategy_string[2])
                    upd_sensor = fields[2].decode()
                    try:
                         self.SENSOR_MAP[fields[0].decode()] = KatcpSensor(
                          sensor_type=KatcpSensor.TYPE_CASTINGS[sens_type],
                          name=fields[0].decode(),
                          description=fields[1].decode(),
                          sensor_type_str=sens_type_str,
                          units=fields[2].decode(),
                          params=params,
                          value_list_max_size=256,
                          sampling_strategy=upd_strategy,
                          sampling_strategy_params="", #[update_sensor['params']],
                          sampling_period_default=self._refresh_interval,
                          logger=self._log_writer
                          )
                    except Exception as e:
                       self._log_writer.write_error(f"sens type fields:{fields[0].decode()}:{sens_type}{params} {upd_strategy} {update_sensor} error:{e}")
            else:
                pass
        self._log_writer.write_info("sensor list init done")
        await self.device_state_set(self.DeviceStates.SENSOR_LIST_INITIALIZED)

    def handle_sensor_value_reply(self, message):
        """
        Sensor status reply handler.
        :param message: Katcp message object.
        :return:
        """
        return

    async def handle_sensor_value_inform(self, message):
        """
        Updates values/adds values (if value-list is activated) in SENSOR_MAP.
        :param message: Katcp message object.
        :return:
        """
        try:

            for entry in message:
                fields = entry.arguments
                # Parse the sensor info
                _sensor_name  = fields[2].decode()
                _sensor_value = fields[4].decode()
                _timestamp    = float(fields[0].decode())
                _status       = fields[3].decode()

                _katcp_sensor = self.SENSOR_MAP[_sensor_name]

                _sensor_value = self.SENSOR_UPDATES[_sensor_name][2].decode()

                if _katcp_sensor.type in ['bool','boolean']:
                   _sensor_value = bool(_sensor_value)
                elif _katcp_sensor.sensor_type_float_int_check():
                   if _katcp_sensor.sensor_type_int_check():
                      _sensor_value = int(_sensor_value)
                   else:   
                      _sensor_value = float(_sensor_value)

                await _katcp_sensor.set_sensor_value(_sensor_value,
                                                              _status,
                                                              _timestamp)
                self._log_writer.write_debug(f"handle_sensor_value_inform: Sensor {_sensor_name}, value: {_sensor_value}, time: {_timestamp}, status:{_status}")
            await asyncio.sleep(0.01)
        except TypeError as ex:
            self._log_writer.write_warning(f"handle_sensor_value_inform: Type Error: Sensor {_sensor_name}, value: {_sensor_value}, time: {_timestamp}, status:{_status}, error: {ex}")
        except Exception as ex:
            self._log_writer.write_warning(f"handle_sensor_value_inform: Sensor {_sensor_name}, value: {_sensor_value}, time: {_timestamp}, status:{_status}, error: {ex}")
        return True

    def handle_help_reply(self, message):
        """
        After help was finished, remove un-supported entries from COMMAND_MAP.
        :param message: Katcp message object.
        :return:
        """

        # A copy is necessary to not corrupt the dict while iterating
        commands_not_supported = list()
        for key, value in self.COMMAND_MAP.copy().items():  # TODO: Move to dict library
            if value is None:
                del self.COMMAND_MAP[key]
                commands_not_supported.append(key)

        # Any items to print
        if len(commands_not_supported) > 0:
            self._log_writer.write_warning('Client {}: Following commands are not supported by the device: {}.'
                                           .format(self.device_name, commands_not_supported))

    async def handle_help_inform(self, messages):
        """
        Updates command list.
        :param message: Katcp message object.
        :return:
        """
        # Clean up the message by externally provided handle
        #if self._description_cleanup_handle is not None:
        #    self._description_cleanup_handle(message)

        # Extract the help command parameters
        for message in messages:
            help_command = epl_aiokatcp_helper.extract_inform_help(message.arguments)
            _kcommand_name = help_command.command_name
            _kcommand_root = _kcommand_name.split("-")[0]
            _kcommand_add = True
            # filter based on device name
            if self.device_name != _kcommand_root:
                _kcommand_add = False

            # Check if command must be added and add it to commands list
            if _kcommand_add is True and ((self._commands_all is False and
                help_command.command_name in self.COMMAND_MAP) \
                  or self._commands_all is True):
                self.COMMAND_MAP[help_command.command_name] \
                    = KatcpCommand(help_command.command_name,
                               help_command.type,
                               values=help_command.values,
                               description=help_command.description)

        # Indicate successful command-list initializing
        await self.device_state_set(self.DeviceStates.COMMAND_LIST_INITIALIZED)
        self._log_writer.write_info("command list init done")

    def handle_log_reply(self, message):
        """
        Log message reply handler.
        :param message: Katcp message object.
        :return:
        """

        return

    def handle_log_inform(self, message):
        """
        Log message inform handler.
        :param message: Katcp message object.
        :return:
        """
        return
        try:
            #_loop = asyncio.get_event_loop()
            #_loop.stop()
            #for inform in message.arguments:
            #message = b' '.join(message.arguments).decode('ascii')
            #print(message)
            log_mes = epl_aiokatcp_helper.extract_inform_log(message.arguments)

            # Always write log messages to debug log
            self._log_writer.write_debug('Client {}:{}: {}'
                                         .format(self.device_name, log_mes.log_level, log_mes.log_message))

            # Only write messages with level > info to info log
            if log_mes.log_level.value > CommandLogInform.LogLevel.INFO.value:
                self._log_writer.write_info('Client {} [{}] {}'
                                            .format(self.device_name, log_mes.log_level.name, log_mes.log_message))

        except Exception as e:
            self._log_writer.write_exception('Client {}: Log inform message could not be analyzed: {}, ex: {}'
                                             .format(self.device_name, message, e))
        return

    def handle_reply(self, msg):
        """
        Called when a reply message arrives.
        :param msg: Common katcp message.
        :return:
        """

        try:
            self._log_writer.write_info('Client {}: Reply message: {}.'
                                         .format(self.device_name, bytes(msg)))

            if msg.name in self._MESSAGE_HANDLES.keys():
                self._MESSAGE_HANDLES[msg.name][0](self, msg)
            else:
                # if reply is not known, see if it was on a set command
                self._handle_reply_command(msg)
        except Exception as e:
            self._log_writer.write_exception('Client {}: Reply command could'
                                             'not be analyzed: {}, ex: {}'.format(self.device_name, msg, e))

    def inform_sensor_status(self, timestamp: aiokatcp.Timestamp,
                             num_sensors: int, *args) -> None:
        self._log_writer.write_info(f'Sensor updates: {args}')

    async def heartbeat(self, timeout=2):
        """
        heartbeat fn for katcp client
        """
        while True:
              self._log_writer.write_info(f'Katcp Client connected: {self.is_connected()}')
              for _sensor_name in self.SENSOR_UPDATES:

                try:
                   _katcp_sensor = self.SENSOR_MAP[_sensor_name]

                   _sensor_value = self.SENSOR_UPDATES[_sensor_name][2].decode()

                   if _katcp_sensor.type in ['bool','boolean']:
                         _sensor_value = bool(_sensor_value)
                   elif _katcp_sensor.sensor_type_float_int_check():
                      if _katcp_sensor.sensor_type_int_check():
                         _sensor_value = int(_sensor_value)
                      else:
                         _sensor_value = float(_sensor_value) 

                   await _katcp_sensor.set_sensor_value(_sensor_value,
                                                     self.SENSOR_UPDATES[_sensor_name][1],
                                                     self.SENSOR_UPDATES[_sensor_name][0])

                   self._log_writer.write_debug(f"handle_sensor_update: Sensor {_sensor_name}, value: {_sensor_value}, time: {self.SENSOR_UPDATES[_sensor_name][0]}, status:{self.SENSOR_UPDATES[_sensor_name][1]} Updated.")
          
                except Exception as e:
                   self._log_writer.write_warning(f"sensor update failed:{e} for {_sensor_name}")
                finally:
                   await asyncio.sleep(0.01)

              await asyncio.sleep(timeout)

    def unhandled_inform(self, msg):
        """
        Called when an inform message arrives.
        :param msg: Common katcp message.
        :return:
        """
        try:
            self._log_writer.write_info('Client {}: Inform message: {}.'
                                         .format(self.device_name, msg))

        except Exception as e:
            self._log_writer.write_exception('Client {}: Inform command could '
                                             'not be analyzed: {}, ex: {}'
                                             .format(self.device_name, bytes(msg), e))

    def handle_inform(self, msg):
        """
        Called when an inform message arrives.
        :param msg: Common katcp message.
        :return:
        """
        try:
            self._log_writer.write_info('Client {}: Inform message: {}.'
                                         .format(self.device_name, msg))
            if msg.name in self._MESSAGE_HANDLES.keys():
                self._MESSAGE_HANDLES[msg.name][1](self, msg)
            else:
                # if inform is not known, see if it was on a set command
                self._handle_inform_command(msg)

        except Exception as e:
            self._log_writer.write_exception('Client {}: Inform command could '
                                             'not be analyzed: {}, ex: {}'
                                             .format(self.device_name, bytes(msg), e))

    def _handle_reply_command(self, message):
        """
        Global command reply handler.
        Searches list of commands and handles the reply.
        :param message: Entire katcp reply message.
        :return:
        """
        # Extract reply parameters
        reply = epl_aiokatcp_helper.extract_reply_general(message.name, message.arguments)
        if reply.command_name in self.COMMAND_MAP.keys():
            command = self.COMMAND_MAP[reply.command_name]
            command.command_set(command_state=reply.status,
                                reply_message=reply.params)
            command.was_updated = True

            return True

        return False

    def _handle_inform_command(self, message):
        """
        Global command reply handler.
        Searches list of commands and handles the reply.
        :param message: Entire katcp reply message.
        :return:
        """
        # Extract inform parameters
        inform = epl_aiokatcp_helper.extract_inform_general(message.name, message.arguments)
        if inform.command_name in self.COMMAND_MAP.keys():
            # Only set last_message here, state will be set on reply_handler
            command = self.COMMAND_MAP[inform.command_name]
            command.command_set(inform_message=inform.message)

            return True

        return False

    def sensor_map_get(self):
        """
        Returns the client's sensor map.
        :param self:
        :return:
        """
        return self.SENSOR_MAP

    def sensor_map_items_get(self):
        """
        Returns the client's sensor map items.
        :param self:
        :return:
        """
        return self.SENSOR_MAP.items()

    def command_map_get(self):
        """
        Returns the client's sensor map.
        :param self:
        :return:
        """
        return self.COMMAND_MAP

    def command_map_items_get(self):
        """
        Returns the client's sensor map items.
        :param self:
        :return:
        """
        return self.COMMAND_MAP.items()

    def start(self, timeout=None):
        """
        Starts the thread, but only if not running.
        Call Stop() before to restart.
        """
        if not self._thread_was_started:
            try:
                super(KatcpClient, self).start(timeout)
            finally:
                self._thread_was_started = True
                return True
        return False


    async def stop(self, timeout=None):
        """
        Stops the client thread.
        """
        try:
                # Stop the client
                self.client.close()
                #await self.client.wait_closed()
        finally:
                return True
        return False

    async def client_stop(self, timeout=None):
        """
        Stops the client thread.
        """
        try:
                # Stop the client
                await self.server.stop(True)
                self.client.close()
                #await self.client.wait_closed()
        except Exception as e:
               return False
        finally:
                return True
        return False
