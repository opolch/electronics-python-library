from enum import Enum


def object_to_dict(obj=object, ignore_list=[str], ignore_empty_lists=True, ignore_empty_dicts=True) -> dict:
    """
    Creates a (nested) dict with all class members by recursively iterating.
    It expects sub_classes to implement `as_dict` function in the same way.
    :param obj: The object to be converted to dict.
    :param ignore_list: List with attributes to be ignored.
    :param ignore_empty_lists: True->Ignores empty lists.
    :param ignore_empty_dicts: True->Ignores empty dicts.
    :return: dict
    """
    return_dict = {}

    # Iterate over dict
    for key, val in obj.__dict__.items():
        try:
            # Key to be ignored?
            if key in ignore_list:
                continue

            if val is None:
                continue

            if isinstance(val, list):
                # List

                # Ignore empty lists
                if ignore_empty_lists and not val:
                    continue

                # Create list entry
                return_dict[key] = []

                # Loop through list
                for list_entry in val:
                    try:
                        # Try invoking the as_dict method
                        return_dict[key].append(list_entry.as_dict())
                    except:
                        # Simply add the entry
                        return_dict[key].append(list_entry)

            elif isinstance(val, dict):
                # Dict

                # Ignore empty dicts
                if ignore_empty_dicts and not val:
                    continue

                # Create dict entry
                return_dict[key] = {}

                # Iterate over dict
                for sub_key, sub_val in val.items():
                    try:
                        # Try invoking the as_dict method
                        return_dict[key][sub_key] = sub_val.as_dict()
                    except:
                        # Simply add the entry
                        return_dict[key][sub_key] = sub_val

            elif isinstance(val, Enum):
                # Enum

                # For enums we add the name only
                return_dict[key] = val.name
            else:
                # Everything else

                # Try call objects as_dict
                return_dict[key] = val.as_dict()
        except AttributeError:
            # If a general object does not implement as_dict, we take it as it is.
            return_dict[key] = val

    return return_dict


def object_init_from_dict(obj, init_dict: dict, add_unknown_attributes=False, list_mapping:dict=None):
    """
    Option to init the object from a dict.
    :param obj: Object to be initialized.
    :param init_dict: dict holding `class_member:value` pairs.
    :param add_unknown_attributes: True->Adds unknown attributes to the object.
    :param list_mapping: Dict with list type mappings in the form: {"att1_name":type, "att2_name":type}
    :return:
    """

    # Mutable
    if list_mapping is None:
        list_mapping = {}

    # Set values from dict
    if init_dict:
        for key, value in init_dict.items():
            try:
                # Get corresponding attribute
                att = getattr(obj, key)

                # Check how to handle attribute
                if not isinstance(att, type(None)):

                    # Enum type
                    if isinstance(att, Enum):
                        setattr(obj, key, type(att)[value])
                    else:
                        try:

                            # Attribute is dict?
                            if isinstance(att, dict):

                                # Init all the members
                                for att_key, att_value in value.items():
                                    # We expect objects that have init_from_dict here
                                    att[att_key].init_from_dict(att_value)
                            elif isinstance(att, list):
                                # Lists are difficult to handle since they can hold different types of objects.
                                # The here implemented approach assumes objects within a list are from the same type
                                # and this type is passed via the `list_mapping` dict
                                list_type = list_mapping[key]
                                for single_value in value:
                                    att.append(list_type(init_dict=single_value))
                            else:
                                # Check whether this is an object with own init_from_dict
                                att.init_from_dict(value)
                        except:
                            # Otherwise simply set value
                            setattr(obj, key, type(att)(value))
                else:
                    setattr(obj, key, value)

            except AttributeError:
                # Ignore unknown attributes
                if add_unknown_attributes:
                    setattr(obj, key, value)
                pass
