from basicpid import BasicPid
import asyncio
from epl.epl_logging.epl_logging_log import Log


class PidAsyncClass(BasicPid):
    class LastPidResult:
        """
        Class to store a pid result including all the calculation parameter.
        """

        def __init__(self,
                     pid_result: float,
                     e_prev: float,
                     e_prev_prev: float):
            """
            Init the object.
            :param pid_result: The calculated result.
            :param e_prev: e_prev
            :param e_prev_prev: e_prev_prev
            """
            self.pid_result = pid_result
            self.e_prev = e_prev
            self.e_prev_prev = e_prev_prev

    def __init__(self,
                 kp, ki, kd,
                 output_invert=False,
                 output_offset=0.0,
                 output_ll=None,
                 output_ul=None,
                 reset_pid_ll=False,
                 reset_pid_ul=False,
                 thread_period_s=1.0,
                 logger=None):
        """
        Initializes the module.
        :param kp: PID-KP
        :param ki: PID-KI
        :param kd: PID-KD
        :param output_invert: Invert output on calculation.
        :param output_offset:  Add an offset to calculation (0.0 to deactivate).
        :param output_ll: Lower-Limit for output -> will cut output at this value.
        :param output_ul: Upper-Limit for output -> will cut output at this value.
        :param reset_pid_ll: True->Stops calculation when lower-limit was exceeded.
        :param reset_pid_ul: True->Stops calculation when upper-limit was exceeded.
        :param thread_period_s: Period for the pid thread to execute with.
        :param logger: epl_logging.Log
        """

        # Init base class
        super().__init__()

        # Set logger, if None create new one
        self._logger = logger if logger is not None else Log(
            name="pid", level_console=Log.LoggingLevels.DEBUG,
            formatter_console=[Log.Formatters.ASC_TIME, Log.Formatters.FILE_NAME,
                               Log.Formatters.FUNC_NAME, Log.Formatters.LINE_NUMBER],
        )

        # The external signals
        self._ref_signal = 0
        self._output_signal = 0
        self._output_offset = output_offset
        self._output_ll = output_ll
        self._output_ul = output_ul

        # Period
        self._thread_period = thread_period_s

        # Reset flags
        self._reset_pid_ll = reset_pid_ll
        self._reset_pid_ul = reset_pid_ul

        # Flags related to output calculation
        self._output_invert = output_invert
        self._pid_result_last: PidAsyncClass.LastPidResult = None

        # The calculated pid result
        self._pid_result = 0

        # Debug
        self._logger.write_info("pid:{},{}".format(self._name, self._vers))

        # Set gains
        self.setGains(kp, ki, kd)

        # check settings
        self._logger.write_debug("Gains:{}".format(self.getGains()))

        # use integrate mode
        self.setIntegrateModeOn()

        # check mode flags
        self._logger.write_debug("inIntegrateMode:{}".format(self.inIntegrateMode()))
        self._logger.write_debug("inIterateMode:{}".format(self.inIterateMode()))

    def ref_signal_set(self, float_value):
        """
        Sets the reference signal to a new float value.
        :param float_value: float value to be set.
        :return:
        """
        self._logger.write_info("Setting ref-signal to:{}".format(float_value))
        self._ref_signal = float(float_value)

    def ref_signal_get(self):
        """
        Returning the actual ref signal.
        :return:
        """
        return self._ref_signal

    def output_signal_update(self, float_value):
        """
        Sets the reference signal to a new float value.
        :param float_value: float value to be set.
        :return:
        """
        self._logger.write_info("Setting output signal to:{}".format(float_value))
        self._output_signal = float(float_value)

    def pid_result_get(self):
        """
        Returns the actual output signal.
        :return: float
        """
        return self._pid_result

    def _calculate_pid(self) -> float:
        """
        Calculates a new pi result based on most recent ref and output signal.
        Also stores last pid result parameters for restoring them in case of limit exceeding.
        :return: The pid result.
        """
        # Init on first run
        if not self._pid_result_last:
            self._pid_result_last = PidAsyncClass.LastPidResult(0.0, 0.0, 0.0)

        # Save results from last calculation
        self._pid_result_last.pid_result = self._pid_out
        self._pid_result_last.e_prev_prev = self._e_prev_prev
        self._pid_result_last.e_prev = self._e_prev

        # Calculate
        return self.get(self._ref_signal, self._output_signal)

    def _restore_pid_calculation_parameters(self) -> bool:
        """
        Restores pid calculation parameters to last ones.
        :return: False->No last pid result available.
        """
        if self.LastPidResult:
            self._pid_out = self._pid_result_last.pid_result
            self._e_prev_prev = self._pid_result_last.e_prev_prev
            self._e_prev = self._pid_result_last.e_prev
            return True
        return False

    async def pid_thread_callback(self):
        """
        Callback for the pid thread.
        It implements the actual error calculation.
        :return:
        """
        while True:

            self._logger.write_debug("Entering pid thread callback")

            # Calculate new pid result
            pid_result_tmp = self._calculate_pid()

            self._logger.write_debug("Calculated pid result from ref-signal:{}, output-signal:{} is {}".format(
                self._ref_signal, self._output_signal, pid_result_tmp))

            # Invert the result?
            if self._output_invert:
                pid_result_tmp = -pid_result_tmp
                self._logger.write_debug("Inverting pid result to:{}".format(pid_result_tmp))

            # Correct offset?
            pid_result_tmp += self._output_offset
            self._logger.write_debug("Correcting with offset `{}` to:{}".format(self._output_offset, pid_result_tmp))

            # Check limits
            if self._output_ul and pid_result_tmp > self._output_ul:

                # Set to upper limit
                pid_result_tmp = self._output_ul

                self._logger.write_debug("Exceeded upper limit, setting pid result to:{}".format(pid_result_tmp))

                # Reset pid on upper limit?
                if self._reset_pid_ul:
                    self._restore_pid_calculation_parameters()
                    self._logger.write_debug("Restoring last pid because of upper level exceeding")

            elif self._output_ll and pid_result_tmp < self._output_ll:

                # Set to lower limit
                pid_result_tmp = self._output_ll

                self._logger.write_debug("Exceeded lower limit, setting pid result to:{}".format(pid_result_tmp))

                # Reset pid on lower limit?
                if self._reset_pid_ll:
                    self._restore_pid_calculation_parameters()
                    self._logger.write_debug("Restoring last pid because of lower level exceeding")

            # Update result
            self._pid_result = pid_result_tmp

            self._logger.write_debug("pid result at end of calculation:{}".format(self._pid_result))

            # Wait some time
            await asyncio.sleep(self._thread_period)
