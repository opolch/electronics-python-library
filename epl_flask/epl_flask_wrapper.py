from epl.epl_logging.epl_logging_log import Log
from flask import Flask, request
from threading import Thread
from http import HTTPStatus


class EndpointAction(object):

    def __init__(self, logger: Log, handlers: dict[[str], callable]):
        """
        Registers an endpoint.
        :param logger: epl_logging.Log
        :param handlers: dict with handlers e.g. {"GET":callable, "POST":callable}
        """
        self._logger = logger
        self.handlers = handlers

    def __call__(self, **kwargs):
        """
        Callback for endpoints. Forwards the request to the handler.
        :param args:
        :return:
        """
        try:
            try:
                payload = request.get_json()
            except:
                payload = None
            self._logger.write_debug(f"reqeust received:{request}\npayload:{payload}")
            ret = self.handlers[request.method](**kwargs)
            return ret.payload.as_dict(), ret.request_code.value
        except IndexError:
            return "", HTTPStatus.INTERNAL_SERVER_ERROR


class FlaskWrapper:
    # The Flask app needs to be defined statically.
    app: Flask = None

    def __init__(self, logger: Log, name: str, host: str, port: int, debug=False,
                 response_headers: dict[str, str] = {}, ssl_context: () = ""):
        """
        Init the object.
        :param logger: epl_logging.Log
        :param name: Name for the Flask instance.
        :param host: Host for the web server.
        :param port: Port for the web server.
        :param debug: True->debug on.
        :param response_headers: Dict with headers to be added with the responses.
        :param ssl_context: ssl_context parameter for the Flask server.
        """

        self._logger = logger

        # Host and port
        self._host = host
        self._port = port

        self._logger.write_info("Initializing flask app...")

        # The Flask app
        self.app = Flask(import_name=name)
        self.app.debug = debug
        self._ssl_context = ssl_context

        # Thread
        self._thread: Thread = None

        # Headers to be added with a response
        self.response_headers = response_headers

        @self.app.after_request
        def add_response_headers(response):
            """
            Callback which is invoked after a request has been served.
            It is used to manipulate headers.
            TODO: I put it here since it needs the decorator. There might be cleaner solutions.
            :param response: The Flask response object.
            :return: The manipulated Flask response object.
            """
            for header_key, header_value in self.response_headers.items():
                response.headers.set(header_key, header_value)

            return response

    def _run(self):
        """
        Callback for the thread start.
        :return:
        """
        if self._ssl_context:
            self.app.run(host=self._host, port=self._port, ssl_context=self._ssl_context)
        else:
            self.app.run(host=self._host, port=self._port)

    def thread_start(self):
        """
        Starts the Flask app.
        :return:
        """

        # Start Flask as thread
        self._thread = Thread(target=self._run)
        self._thread.start()

    def add_endpoint(self, endpoint: str, endpoint_name: str = None, handlers: dict[[str], callable] = None):
        """
        Adds an endpoint handler.
        :param endpoint: The endpoint url.
        :param endpoint_name: The endpoint name. If no endpoint name was given, it is derived from the endpoint.
        :param handlers: dict with handlers {"GET":callable, "POST":callable}
        :return:
        """

        self._logger.write_debug(f"Adding endpoint: {endpoint}:{[x for x in handlers.keys()]}")

        # If no endpoint name was given, derive it from the endpoint
        if not endpoint_name:
            endpoint_name = endpoint.strip("/").replace("/", "_")

        # Add the endpoint
        self.app.add_url_rule(endpoint, endpoint_name, EndpointAction(logger=self._logger, handlers=handlers),
                              methods=[x for x in handlers.keys()])
