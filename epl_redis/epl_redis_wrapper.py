import redis
from typing import Union
from redis import typing
from redis import ResponseError
from redis.commands.search.query import Query
from epl.epl_logging.epl_logging_log import Log
import time
from enum import Enum, auto


class RedisDataType(Enum):
    """
    Defines the data type for a Redis value.
    """
    DEFAULT = auto()
    JSON = auto()


class RedisInteraction(redis.Redis):

    def __init__(self, logger: Log, host: str, port: int):
        """
        Inits the object.
        :param logger: epl_logging.Log
        :param host: Redis server's hostname.
        :param port: Redis server's port.
        """

        # Logging
        self._logger = logger

        # Redis host/port
        self._redis_host = host
        self._redis_port = port

        # Init base class
        super().__init__(host=self._redis_host, port=self._redis_port)

    def select_db(self, index: int):
        """
        Selects the database at given index.
        :param index: The integer index of the db.
        :return:
        """
        self._logger.write_debug(f"Selecting Redis DB `{index}`")
        return self.select(index=index)

    def set(self, key: str, value: str, expire_seconds=None, **kwargs):
        """
        Executes a Redis SET operation.
        :param key: Key for entry to be set.
        :param value: Value for entry to be set.
        :param expire_seconds: Expiring time in seconds. None->No expiration.
        :param kwargs: Additional params as per base class.
        :return:
        """
        self._logger.write_debug(f"Executing SET request: `name={key}, value={value}, ex={expire_seconds}`")
        return super().set(name=key, value=value, ex=expire_seconds)

    def mset(self, mapping: {}):
        """
        Executes a Redis MSET operation.
        :param mapping: Dict with key:value pairs to be set.
        :return:
        """
        self._logger.write_debug(f"Executing MSET request: `mapping:{mapping}`")
        return super().mset(mapping=mapping)

    def get_ft(self, index: str, query="*", return_fields=[], limit=1000):
        """
        Executes a Redis FT.SEARCH operation.
        :param index: Index to search in.
        :param query: Search query to execute.
        :param return_fields: List with return fields.
        :param limit: Limit number of results.
        :return:
        """
        self._logger.write_debug(f"Executing FT.SEARCH request: `index={index}, query={query}, limit={limit}, "
                                 f"return_fields={return_fields}`")

        # Create query
        query = Query(query).paging(0, limit)

        # Add return fields if passed
        for ret_field in return_fields:
            query = query.return_field(ret_field)

        # Execute request
        res = self.ft(index_name=index).search(query=query)

        self._logger.write_debug(f"Result: `total={res.total}, docs={[x['id'] for x in res.docs]}`")
        return res

    def expire(self, name: str, expiration_time: typing.ExpiryT, **kwargs):
        """
        Sets and expiration time with a key.
        :param name: key.
        :param expiration_time: Time as time delta or int.
        :param kwargs: Additional params as per base class.
        :return:
        """
        return super().expire(name=name, time=expiration_time)

    def json_set(self, key: str, value: dict, expire_seconds=None):
        """
        Executes a Redis JSON.SET operation.
        :param key: Key for entry to be set.
        :param value: Value for entry to be set.
        :param expire_seconds: Expiring time in seconds. None->No expiration.
        :return:
        """
        self._logger.write_debug(f"Executing JSON.SET request: `key={key}, value={value}`")
        ret = super().json().set(name=key, path="$", obj=value)
        if expire_seconds:
            ret &= super().expire(name=key, time=expire_seconds)
        return ret

    def json_mset(self, data: [(str, str, object)]):
        """
        Executes a Redis JSON.MSET operation.
        :param data: List of triplets (key, path value).
        :return:
        """
        self._logger.write_debug(f"Executing JSON.MSET request: `data={data}`")
        return super().json().mset(data)

    def delete(self, keys: list):
        """
        Executes a Redis DEL operation.
        :param keys: List with keys to be deleted.
        :return:
        """
        self._logger.write_debug(f"Executing DEL request: `keys={keys}`")
        res = super().delete(*keys)
        self._logger.write_debug(f"Result: `{res}`")
        return res

    def scan_full(self, match: str, data_type: RedisDataType, max_execution_time=2.0) -> dict:
        """
        Performs a Redis SCAN, followed by a GET operation (in case SCAN returned any results.
        It will loop through the entire database and error out on a max execution timeout.
        :param match: The key to search for (supports reg ex).
        :param data_type: RedisDataType
        :param max_execution_time: Maximum time in sec to wait for the scan to be finished.
        :return: Dict containing decoded key:value pair(s)
        """

        start_time = time.time()
        cursor = 0
        result_dict = {}

        self._logger.write_debug(f"Executing SCAN request: `match={match}, data_type={data_type.name}, "
                                 f"max_execution_time={max_execution_time}`")

        while True:

            # Check for exceeding of max_execution_time
            if time.time() - start_time > max_execution_time:
                self._logger.write_error(f"Max execution time `{max_execution_time}s` for scan operation exceeded.")
                break

            # Perform next SCAN operation.
            cursor, results = super().scan(cursor=cursor, match=match)

            # Any results?
            if results:

                # SCAN only returns keys. To get the corresponding data, a get call is necessary.
                for single_result in results:

                    # Decode key
                    result_key = single_result.decode()

                    # Differentiate on data type
                    if data_type == RedisDataType.JSON:
                        result_dict[result_key] = super().json().get(
                            name=single_result)
                    else:
                        result_dict[result_key] = super().get(
                            name=single_result).decode()

                    self._logger.write_debug(f"Found entry `key={result_key}, value=\n{result_dict[result_key]}`")

            # When cursor is 0, entire database was scanned and we break the loop
            if cursor == 0:
                self._logger.write_debug(f"SCAN operation finished. Returning {len(result_dict)} results.")
                break

        return result_dict

    def ts_add(self,
               key: str,
               timestamp: Union[int, str],
               value: Union[int, float],
               retention_msecs: int = None,
               labels: [[str], str] = None,
               duplicate_policy: str = None,
               **kwargs):
        """
        Executes a TS.ADD request adding/creating a single entry(s) to time series.
        :param key: time-series key
        :param timestamp: Timestamp of the sample. * can be used for automatic timestamp (using the system clock).
        :param value: Numeric data value of the sample
        :param retention_msecs: Maximum retention period, compared to maximal existing timestamp (in milliseconds).
                        If None or 0 is passed then  the series is not trimmed at all.
        :param labels: Set of label-value pairs that represent metadata labels of the key.
        :param duplicate_policy:
            Policy for handling multiple samples with identical timestamps.
            Can be one of:
            - 'block': an error will occur for any out of order sample.
            - 'first': ignore the new value.
            - 'last': override with latest value.
            - 'min': only override if the value is lower than the existing value.
            - 'max': only override if the value is higher than the existing value.
        :return: Array with timestamps of insertions.
        """
        self._logger.write_debug(f"Executing TS.ADD request: `key={key}, ts={timestamp}, val={value}`")
        res = super().ts().add(key=key, timestamp=timestamp, value=value, retention_msecs=retention_msecs,
                               labels=labels, duplicate_policy=duplicate_policy, **kwargs)
        self._logger.write_debug(f"Result: `{res}`")
        return res

    def ts_madd(self, ktv_tuples: [(str, Union[int, str], Union[int, float])]):
        """
        Executes a TS.MADD request adding a single/list of entry(s) to time series.
        :param ktv_tuples: List of tuples [(key, timestamp, value)]
        :return: Array with timestamps of insertions.
        """
        self._logger.write_debug(f"Executing TS.MADD request: `ktv_tuples={ktv_tuples}`")
        res = super().ts().madd(ktv_tuples=ktv_tuples)
        self._logger.write_debug(f"Result: `{res}`")
        return res

    def ts_mrange(self,
                  from_time: Union[int, str],
                  to_time: Union[int, str],
                  label_filters: [str],
                  filter_by_min_value: int = None,
                  filter_by_max_value: int = None,
                  count: int = None,
                  with_labels: bool = False,
                  **kwargs):
        """
        Executes a TS.MRANGE request.
        :param from_time: Start timestamp for the range query.
        `-` can be used to express the minimum possible timestamp (0).
        :param to_time: End timestamp for range query, `+` can be used to express the maximum possible timestamp.
        :param label_filters: filter to match the time-series labels.
        :param filter_by_min_value: Filter result by minimum value (must mention also filter_by_max_value).
        :param filter_by_max_value: Filter result by maximum value (must mention also filter_by_min_value).
        :param count: Limits the number of returned samples.
        :param with_labels: Include in the reply all label-value pairs representing metadata labels of the time series.
        :param kwargs: All available args from base function.
        :return:
        """

        self._logger.write_debug(f"Executing TS.MRANGE request: `"
                                 f"from_time={from_time}; "
                                 f"to_time={to_time}; "
                                 f"label_filters={label_filters}; "
                                 f"filter_by_min_value={filter_by_min_value}; "
                                 f"filter_by_max_value={filter_by_max_value}; "
                                 f"count={count}; "
                                 f"with_labels={with_labels}; "
                                 f"kwargs={kwargs}; "
                                 f"`")

        res = super().ts().mrange(
            from_time=from_time,
            to_time=to_time,
            filters=label_filters,
            count=count,
            with_labels=with_labels,
            filter_by_min_value=filter_by_min_value,
            filter_by_max_value=filter_by_max_value,
            **kwargs)

        self._logger.write_debug(f"Result: `{res}`")
        return res

