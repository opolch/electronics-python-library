from walrus import Database
from enum import Enum
import json
from epl.epl_datetime.epl_datetime_datetime import *
from epl.epl_logging.epl_logging_log import Log


class StreamMessage:
    """
    Class defining a standard stream message.
    """

    class StreamMessageFields(Enum):
        """
        Enum to define keys for stream message elements.
        """
        MESSAGE = "message"

    def __init__(self, message_id: bytes, payload: [bytes]):
        """
        Inits the object. Arguments are expected to have bytes type and will be decoded.
        :param message_id: The message id.
        :param payload: The entire payload.
        """
        # Decode the message id
        self.message_id = message_id.decode()

        # Message time
        self.message_time = dt_from_timestamp_tz(timestamp=self.message_id[0:10])

        # Decode payload dict elements
        payload_dict = {k.decode(): v.decode() for k, v in payload.items()}

        # Extract the message part
        self.message = payload_dict.get(self.StreamMessageFields.MESSAGE.value, "")


class RedisStream(Database):
    """
    Class wrapping the walrus.Database class and providing Redis stream functionality.
    """

    def __init__(self, logger: Log, redis_host: str, redis_port: int, redis_db: int):
        """
        Inits the object.
        :param logger: epl_logging.Log
        :param redis_host: Hostname/ip of the RedisDB.
        :param redis_port: Port of the RedisDB.
        :param redis_db: The database to be used on Redis.
        """

        # Logging
        self._logger = logger

        # Init base class
        super().__init__(host=redis_host, port=redis_port, db=redis_db)

    def stream_add(self, stream_name: str) -> bool:
        """
        Adds a stream to the database.
        :param stream_name: The name for the stream.
        :return: True->Stream added; False->Stream already active.
        """

        # Create stream
        self._logger.write_info(f"Adding redis stream `{stream_name}`")
        return self.Stream(stream_name)

    def stream_read_message(self, stream: Database.Stream, last_message_id: "") -> [StreamMessage]:
        """
        Reads messages from a stream.
        :param stream: Name of the stream to read from.
        :param last_message_id: ID of the last message received.
        :return: [StreamMessage]
        """

        # Check for new message(s)
        self._logger.write_debug(f"Reading message(s) from stream:`{stream}`")
        messages = stream.read(last_id=last_message_id)

        # Any messages?
        if messages:
            self._logger.write_debug(f"Read message(s) from stream:\n`{messages}`")
            return [StreamMessage(message_id=x[0], payload=x[1]) for x in messages]
        else:
            self._logger.write_debug(f"No new message on stream.")
            return []

    def stream_write_message(self, stream: Database.Stream, message: {}):
        """
        Writes a message to a given b2d stream.
        :param stream: The stream to write to.
        :param message: Dict representing the message contents.
        :return: The message id.
        """

        message_id = None

        try:
            # Send message as json blob
            message_id = stream.add({"message": json.dumps(message)}).decode()
            self._logger.write_debug(
                "Adding message to stream:`{}`\nmessage_id:{}\n{}".format(stream, message_id, message)
            )
        except Exception as e:
            self._logger.write_exception(f"Error during stream message writing; ex:{e.args}")

        return message_id
