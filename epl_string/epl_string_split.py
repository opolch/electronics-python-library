def split_index(source_string, index=0, separator=' '):
    """
    Splits a string by a given separator and returns the element at pos=index.
    :param source_string: String to be split.
    :param index: Index to return.
    :param separator: String to use as splitter (default ' ')
    :return: Substring at pos=index (None for error).
    """
    try:
        return source_string.split(separator)[index]
    except:
        return None


def split_always_list(source_string, separator=' ', remove_whitespaces=True):
    """
    Splits a string by a given separator.
    In case the separator is not found, a list containing only the source_string will be returned.
    In case an empty string was passed, an empty list will be returned.
    :param source_string: String to be split.
    :param separator: String to use as splitter (default ' ')
    :param remove_whitespaces: Remove whitespaces before splitting.
    :return: List with separated strings (if separator was not found, list with only the source string).
    """

    if len(source_string) == 0:
        return list()

    if remove_whitespaces is True:
        source_string = source_string.replace(' ', '')

    try:
        ret_list = source_string.split(separator)
    except:
        ret_list = [source_string]
    finally:
        return ret_list


def split_lines(source_string):
    """
    Splits a string by newline character.
    :param source_string: String to be split.
    :return:  List with line-strings (None for error).
    """
    try:
        return source_string.splitlines()
    except:
        return None


def split_nth_occurrence(source_string, separator, n=(-1)):
    """
    Splits a string by the nth occurrence of separator and returns a 2-element list with the substrings.
    :param source_string: The source string.
    :param separator: The separator to split by.
    :param n: The occurrence number of separator to split at (default `-1` -> last one).
    :return: [str_before_nth_sep, str_after_nth_sep]
    """

    ret = source_string.split(separator, n)
    return [separator.join(ret[0:-1]), ret[-1]]


class EplStringSplit:
    """
    Class providing abstract string split functionality.
    """
    def __init__(self):
        pass
