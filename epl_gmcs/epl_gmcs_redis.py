from enum import Enum
import json
from epl.epl_logging.epl_logging_log import Log
from epl.epl_dicts.epl_dicts_objects import object_to_dict, object_init_from_dict
from epl.epl_redis.epl_redis_streams import RedisStream
from epl.epl_threading.epl_threading_thread import TriggerTimeout
from epl.epl_datetime.epl_datetime_datetime import *
from epl.epl_gmcs.epl_gmcs_config import NodeInfo
from epl.epl_gmcs.epl_gmcs_action import ActionInfo
import time
from varname import nameof
import queue

# This dummy is only used to get the instance variable names via varname.nameof as Redis keys.
_node_info_dummy = NodeInfo()
_action_info_dummy = ActionInfo()


class ExceptStreamLookupFailed(BaseException):
    """
    Exception when a stream lookup fails.
    """
    pass


class ExceptTimestampFormat(BaseException):
    """
    Exception when a message-id can not be converted into a timestamp.
    """
    pass


class StreamType(Enum):
    """
    Class defining stream prefixes for common streams.
    """

    b2d = "b2d"  # broker-to-device
    d2b = "d2b"  # device-to-broker
    multicast = "multicast"  # devices-to-devices, also broker is here


class DeviceType(Enum):
    """
    Enum defining device type when working with GMCS Redis streams.
    """
    broker = "broker"
    device = "device"


class StreamMessageRequestCodes(Enum):
    """
    Enum defining stream message request types.
    """
    undefined = "undefined"
    ping = "ping"
    set = "set"
    ack = "ack"
    alive = "alive"


class GmcsNodeKeys(Enum):
    """
    Enum to define keys for GMCS NodeInfo objects in Redis.
    """
    node_name = nameof(_node_info_dummy.node_name)
    node_id = nameof(_node_info_dummy.node_id)
    tree_id = nameof(_node_info_dummy.tree_id)
    node_type = nameof(_node_info_dummy.node_type)
    node_task_type = nameof(_node_info_dummy.metadata.task_type)


class GmcsActionInfoKeys(Enum):
    """
    Enum to define keys for GMCS ActionInfo objects in Redis.
    """
    action_id = nameof(_action_info_dummy.action_id)
    action_type = nameof(_action_info_dummy.action_type)
    source_id = nameof(_action_info_dummy.source_id)
    destination_id = nameof(_action_info_dummy.destination_id)
    action_params = nameof(_action_info_dummy.action_params)
    metadata = nameof(_action_info_dummy.metadata)


class GmcsRedisKeyPrefixes(Enum):
    """
    Defines prefixes for Redis indexes.
    """
    node_prefix = "gmcs_node:"
    connected_devices = "con_dev:"
    actions = "gmcs_action:"


def gmcs_redis_key_create(prefix: GmcsRedisKeyPrefixes, id: str):
    """
    Creates the connected_devices key by prefixing it.
    :param prefix: The prefix to be used.
    :param id: The id to be prefixed.
    :return: Prefixed device_key.
    """
    return f"{prefix.value}{id}"


class StreamRequestMessage:
    """
    Parent class defining basic structure for stream message requests.
    Individual request classes should inherit this one.
    """

    class Payload:
        """
        Defining the payload part of a StreamRequestMessage.
        """

        def __init__(self, init_dict: {} = None):
            """
            Init the object.
            :param init_dict: Dict to init the object from. Overwrites others!
            """
            if init_dict:
                self.init_from_dict(init_dict)

        def as_dict(self):
            """
            Returns all members of the object as dict.
            :return:
            """
            return object_to_dict(self)

        def init_from_dict(self, init_dict: dict):
            """
            Option to init the object from a dict.
            :param init_dict: dict holding `class_member:value` pairs.
            :return:
            """
            object_init_from_dict(self, init_dict)

    def __init__(self, request_code: StreamMessageRequestCodes = StreamMessageRequestCodes.undefined,
                 sender_id="", message_id: str = "", payload: Payload = None, init_dict: {} = None):
        """
        Inits the object.
        :param request_code: StreamMessageRequestCodes.
        :param sender_id: ID of sending device.
        :param message_id: The messages' message_id on the stream. Empty string for new messages.
        :param payload: Payload object for the message. Payload object must implement as_dict function.
        :param init_dict: Dict to init the object from. Overwrites others!
        """
        self.sender_id = sender_id
        self.message_id = message_id
        self.request_code = request_code
        self.payload = payload

        if init_dict:
            self.init_from_dict(init_dict=init_dict)

    def init_from_dict(self, init_dict: dict):
        """
        Option to init the object from a dict.
        :param init_dict: dict holding `class_member:value` pairs.
        :return:
        """
        object_init_from_dict(self, init_dict)

    def as_dict(self) -> dict:
        """
        Creates a dict with all class members by recursively iterating.
        It expects objects to implement `as_dict` function.
        :return: dict
        """

        return object_to_dict(self)

    def get_special_request_message(self):
        """
        Tries to convert self to a special StreamRequestMessage based on the request code.
        :return: Success->Any special StreamRequestMessage object; Failure->None
        """
        try:
            if self.request_code == StreamMessageRequestCodes.set:
                return StreamRequestMessageSet(message_id=self.message_id, set_value=self.payload)
            elif self.request_code == StreamMessageRequestCodes.ack:
                return StreamRequestMessageAck(message_id=self.message_id, init_dict=self.payload)
            elif self.request_code == StreamMessageRequestCodes.alive:
                return StreamRequestMessageAlive(sender_id=self.sender_id, alive_value=self.payload)
            elif self.request_code == StreamMessageRequestCodes.ping:
                return None
        except:
            None

    @property
    def message_time(self):
        """
        Generates the message time based on the message-id
        :return:
        """
        try:
            return dt_from_timestamp_tz(timestamp=self.message_id[0:10])
        except Exception as e:
            raise ExceptTimestampFormat(e.args)


class StreamRequestMessageSet(StreamRequestMessage):
    """
    Child class to define a special request message for SET operations.
    """

    class Payload(StreamRequestMessage.Payload):
        """
        Defining the payload part of a SET message.
        """

        def __init__(self, node_id: str = "", set_value: str = "", init_dict: {} = None):
            """
            Init the object.
            :param node_id: ID for the node to receive the set request.
            :param set_value: The value to be set for the node.
            :param init_dict: Dict to init the object from. Overwrites others!
            """
            self.node_id = node_id
            self.set_value = set_value

            # Init base class what basically inits from dict
            super().__init__(init_dict=init_dict)

    def __init__(self, sender_id: str, message_id: str, set_value: {}):
        """
        Inits the object.
        :param sender_id: ID of sending device.
        :param message_id: The messages' message_id on the stream.
        :param set_value: Dict containing a set value.
        """

        # Create SET payload objects to be sent with the request message
        payload = self.Payload(init_dict=set_value)

        # Init the base class, what creates the message object
        super().__init__(sender_id=sender_id,
                         message_id=message_id,
                         request_code=StreamMessageRequestCodes.set,
                         payload=payload)


class StreamRequestMessageAlive(StreamRequestMessage):
    """
    Child class to define a special request message for ALIVE operations.
    """

    class Payload(StreamRequestMessage.Payload):
        """
        Defining the payload part of a SET message.
        """

        # TODO: Payload and documentation is not properly implemented
        def __init__(self, node_id: str = "", tree_name: str = "", init_dict: {} = None):
            """
            Init the object.
            :param node_id: ID for the node to receive the set request.
            :param tree_name: Tree name for the node.
            :param init_dict: Dict to init the object from. Overwrites others!
            """
            self.node_id = node_id
            self.tree_name = tree_name

            # Init base class what basically inits from dict
            super().__init__(init_dict=init_dict)

    def __init__(self, sender_id, alive_value: {}):
        """
        Inits the object.
        :param sender_id: The sender_id for the message.
        :param alive_value: Dict containing a set value.
        """

        # Create SET payload objects to be sent with the request message
        payload = self.Payload(init_dict=alive_value)

        # Init the base class, what creates the message object
        super().__init__(sender_id=sender_id,
                         request_code=StreamMessageRequestCodes.alive,
                         payload=payload)


class AckState(Enum):
    """
    Enum to define statues for ack messages.
    """
    ok = "ok"
    fail = "fail"
    no_ack_received = "no_ack_received"
    ack_check_disabled = "ack_check_disabled"


class StreamRequestMessageAck(StreamRequestMessage):
    """
    Class defining an ack message.
    """

    class Payload(StreamRequestMessage.Payload):
        """
        Defining the payload part of a SET message.
        """

        def __init__(self,
                     send_message_id: str = "",
                     ack_state: AckState = AckState.ok,
                     payload: dict = None,
                     init_dict: dict = None):
            """
            Init the object.
            :param send_message_id: The id of the send message, this ack acknowledges.
            :param ack_state: The status of the ack.
            :param payload: The payload sent with the ack.
            :param init_dict: Inits the object from dict. Overrides others.
            """
            self.send_message_id = send_message_id
            self.ack_state = ack_state
            self.payload = payload

            # Init base class what basically inits from dict
            super().__init__(init_dict=init_dict)

    def __init__(self,
                 sender_id:str = "",
                 message_id: str = "",
                 send_message_id: str = "",
                 ack_state: AckState = AckState.ok,
                 message: dict = None,
                 init_dict: dict = None):
        """
        Init the object.
        :param sender_id: ID of sending device.
        :param message_id: The messages' message_id on the stream.
        :param send_message_id: The id of the send message, this ack is acknowledging.
        :param ack_state: The status of the ack.
        :param message: The message sent with the ack.
        :param init_dict: Inits the object from dict. Overrides others.
        """
        # Create the ACK payload
        message = self.Payload(send_message_id=send_message_id, ack_state=ack_state, payload=message,
                               init_dict=init_dict)

        # Init the base class, what creates the message object
        super().__init__(sender_id=sender_id,
                         message_id=message_id,
                         request_code=StreamMessageRequestCodes.ack,
                         payload=message)


class StreamListEntry:
    """
    Class defining an entry for the internal gmcs stream list containing b2d and d2b streams.
    """

    class SingleStream:
        """
        Class defining a single stream (b2d or d2b)
        """

        def __init__(self, stream: RedisStream.Stream,
                     init_message_id_time=None):
            """
            Inits the object.
            :param stream: The stream object.
            :param init_message_id_time: Date.
            """
            self.stream = stream
            self.last_message_id = self._message_id_from_datetime(init_message_id_time) \
                if init_message_id_time else None
            self.last_check_time = None
            self.last_alive_message = 0

        def _message_id_from_datetime(self, date_time: datetime):
            """
            Calculates a message-id based on a given datetime.
            :param date_time:
            :return:
            """
            return f"{int(date_time.timestamp() * 1000)}-0"

        @property
        def last_message_time(self):
            """
            Generates the message time based on the message-id
            :return:
            """
            try:
                return dt_from_timestamp_tz(timestamp=self.last_message_id[0:10])
            except Exception as e:
                raise ExceptTimestampFormat(e.args)

    def __init__(self, b2d_stream: RedisStream.Stream, d2b_stream: RedisStream.Stream):
        """
        Inits the object.
        :param b2d_stream: broker2device stream object.
        :param d2b_stream: device2broker stream object.
        """
        self._b2d_stream_entry = self.SingleStream(stream=b2d_stream, init_message_id_time=dt_now_tz())
        self._d2b_stream_entry = self.SingleStream(stream=d2b_stream, init_message_id_time=dt_now_tz())

    def get_stream_entry(self, stream_typ: StreamType) -> SingleStream:
        """
        Returns the stream entry matching a StreamType.
        :param stream_typ: StreamType.
        :return:
        """
        if stream_typ == StreamType.b2d:
            return self._b2d_stream_entry
        elif stream_typ == StreamType.d2b:
            return self._d2b_stream_entry

    @property
    def b2d_stream(self) -> SingleStream:
        return self._b2d_stream_entry

    @property
    def d2b_stream(self) -> SingleStream:
        return self._d2b_stream_entry


class GmcsRedisStreams:
    """
    Class providing a GMCS specific implementation of Redis streams.
    """

    def __init__(self, logger: Log, device_type: DeviceType, redis_host="localhost", redis_port=6379, redis_db=0,
                 stream_housekeeping_thread_activate=False, timeout_ack=2.0, stream_max_messages=1000,
                 stream_max_period_inactive=60, alive_message_period_s=0, sender_id=""):
        """
        Inits the object.
        :param logger: epl_logging.Log
        :param device_type: Device Type as per DeviceType.
        :param redis_host: Hostname/ip of the RedisDB.
        :param redis_port: Port of the RedisDB.
        :param redis_db: The database to be used on Redis.
        :param stream_housekeeping_thread_activate: True->Activate the housekeeping thread.
            Taking care on:
            - Limiting number of messages on stream
            - Deleting inactive streams
            If the thread is not activated, make sure to regularly trigger the housekeeping callback from outside.
        :param timeout_ack: Time in seconds to wait for an ack message.
        :param stream_max_messages: Max number of messages per stream.
        :param stream_max_period_inactive: Max period in seconds before a stream becomes inactive (thus gets deleted).
        :param alive_message_period_s: Period in which a client send alive messages (`0/default` to deactivate)
        :param sender_id: Device-ID for general messages (e.g., alive messages)
        """

        # Logging
        self._logger = logger

        # Stream related variables
        self._stream_max_messages = stream_max_messages
        self._stream_max_period_inactive = stream_max_period_inactive
        self._stream_timeout_ack = timeout_ack
        self._alive_message_period_s = alive_message_period_s
        self._sender_id = sender_id

        # Device-Type
        self.device_type = device_type

        # Only if stream housekeeping thread should be activated, do so
        if stream_housekeeping_thread_activate:
            self._stream_housekeeping_thread = TriggerTimeout(callback=self.stream_housekeeping_thread_callback,
                                                              timeout=30.0, start_immediate=True)
            self._stream_housekeeping_thread.start()

        # Set up the epl redis stream library
        self._redis_stream = RedisStream(logger=logger, redis_host=redis_host, redis_port=redis_port, redis_db=redis_db)

        self._streams: [[str], StreamListEntry] = {}
        self._stream_messages: [[str], ] = {}

        # Queue to buffer requests
        self._queue_request = queue.Queue(1000)

    @property
    def queue_request(self) -> queue.Queue:
        return self._queue_request

    @property
    def timeout_ack(self) -> float:
        return self._stream_timeout_ack

    def stream_housekeeping_thread_callback(self) -> dict:
        """
        Callback for the stream housekeeping thread, performing following actions:
        - Limiting number of messages per stream.
        - Deleting inactive streams (if feature activated).
        - Collecting a dict with active streams (activity in past stream_max_period_inactive).
        :return: Dict with streams that had activity in last check period {stream_name:seconds_since_last_act}.
        """

        def handle_to_be_deleted_streams(stream_to_be_deleted: str):
            """
            Adds a stream to the to_be_deleted list of streams, respecting whether this feature is active at all.
            :param stream_to_be_deleted:
            :return:
            """
            if self._stream_max_period_inactive:
                to_be_deleted_streams.append(stream_to_be_deleted)

        try:
            # List to collect streams to be deleted
            to_be_deleted_streams = []
            active_streams = {}

            # Loop over streams
            self._logger.write_debug(f"Limiting number of messages per stream to {self._stream_max_messages}.")
            for stream_name, stream_entry in self._streams.items():

                # Get streams based on device type
                reqeust_stream = stream_entry.d2b_stream \
                    if self.device_type == DeviceType.broker else stream_entry.b2d_stream
                reply_stream = stream_entry.b2d_stream \
                    if self.device_type == DeviceType.broker else stream_entry.d2b_stream

                if reqeust_stream.last_check_time:
                    # Get latest message on d2b stream, here the device is pushing.
                    # We use this stream for the activity check.
                    # Don't perform this check on first run!

                    # Read out messages
                    new_messages = self.stream_read_message(single_stream_entry=reqeust_stream)

                    # Any message?
                    if new_messages:

                        # Loop over messages and
                        for message in new_messages:

                            # Differentiate based on request type
                            if isinstance(message, StreamRequestMessageAlive):

                                # Alive messages get special treatment, they will be acknowledged
                                self._logger.write_info(f"Alive message received, going to acknowledge it.")

                                # Write acknowledge message via reply stream
                                self.stream_write_message(message=StreamRequestMessageAck(
                                    send_message_id=message.message_id,
                                    ack_state=AckState.ok
                                ).as_dict(), single_stream_entry=reply_stream)

                            else:
                                # All other messages go in the request queue
                                self._logger.write_info(f"Adding request message to request queue:"
                                                        f"\n{message.as_dict()}")
                                self._queue_request.put(message)

                    # Calculate time since last message
                    seconds_since_last_mes = dt_now_tz_timestamp() - reqeust_stream.last_message_time
                    self._logger.write_debug(
                        f"Time since last message for stream `{stream_name}` = {seconds_since_last_mes:.2f}s.")

                    # No message on stream for last period of time -> delete stream
                    if seconds_since_last_mes > self._stream_max_period_inactive:
                        self._logger.write_debug(f"Deleting stream `{stream_name}` due to inactivity.")
                        handle_to_be_deleted_streams(stream_name)
                    else:
                        self._logger.write_debug(f"Activity on stream {stream_name}.")
                        active_streams[stream_name] = seconds_since_last_mes

                # Set last stream check time
                reqeust_stream.last_check_time = dt_now_tz()

                # Special broker/device tasks
                if self.device_type == DeviceType.broker:

                    # Trim both the streams to a max message number
                    reqeust_stream.stream.trim(count=self._stream_max_messages)
                    reply_stream.stream.trim(count=self._stream_max_messages)

                else:
                    # Send alive message
                    if (self._alive_message_period_s and
                            dt_now_tz_timestamp_s() - reply_stream.last_alive_message > self._alive_message_period_s):
                        # Save time of last sent alive message
                        reply_stream.last_alive_message = dt_now_tz_timestamp_s()

                        # Send alive message
                        self._logger.write_debug(f"Sending alive message for stream `{stream_name}`.")
                        alive = StreamRequestMessageAlive(sender_id=self._sender_id, alive_value={"alive": "true"})
                        self.stream_write_message(message=alive.as_dict(), single_stream_entry=reply_stream)

            # Delete inactive streams
            if self._stream_max_period_inactive:
                for s in to_be_deleted_streams:
                    self.stream_pair_delete(s)

            return active_streams

        except Exception as e:
            self._logger.write_exception(
                "Exception in stream_manager_thread_callback, ex:{}".format(e))

        return {}

    def stream_pair_delete(self, stream_name: str) -> bool:
        """
        Deletes a stream pair for a given stream name.
        :param stream_name: Name of stream to be deleted.
        :return: True->Success.
        """

        if stream_name not in self._streams:
            self._logger.write_warning(f"Stream pair for `{stream_name}` not available thus not to be deleted.")
            return False

        self._logger.write_info(f"Deleting stream pair for `{stream_name}`.")
        del (self._streams[stream_name])
        return True

    def stream_pair_add(self, stream_name: str, delete_existing=True) -> bool:
        """
        Adds a stream pair (broker2device + device2broker) to the database.
        Also adds it to the stream dict, so it can easily be accessed later.
        :param stream_name: The name for the stream.
        :param delete_existing: Deletes an existing stream and creates a new one.
        :return: True->Stream added; False->Stream already active.
        """

        self._logger.write_info(f"Adding gmcs redis stream pair for stream `{stream_name}`.")

        # Stream already available
        if stream_name in self._streams and not delete_existing:
            self._logger.write_warning(f"Stream pair for `{stream_name}` already exists on redis.")
            return False

        # Create stream
        b2d_stream = self._redis_stream.stream_add(f"{StreamType.b2d.value}{stream_name}")
        d2b_stream = self._redis_stream.stream_add(f"{StreamType.d2b.value}{stream_name}")

        # Add stream to stream dict, so we can later easily access it
        self._streams[stream_name] = StreamListEntry(b2d_stream=b2d_stream, d2b_stream=d2b_stream)

    def _stream_entry_lookup(self, stream_name: str, stream_type: StreamType) -> StreamListEntry.SingleStream:
        """
        Looks up a stream from the streams' dict.
        :param stream_name: Name of the stream.
        :param stream_type: StreamType to write to.
        :return: StreamListEntry.SingleStream
        """
        try:
            # Lookup the stream
            return self._streams[stream_name].get_stream_entry(stream_type)
        except KeyError as e:
            raise ExceptStreamLookupFailed(e.args)

    def stream_write_message(self, message: {}, single_stream_entry: StreamListEntry.SingleStream = None,
                             stream_name: str = None, stream_type: StreamType = None):
        """
        Writes a message to a given stream.
        The stream can either be passed via name and type or via the StreamListEntry.SingleStream directly.
        The StreamListEntry.SingleStream has priority.
        :param single_stream_entry: StreamListEntry.SingleStream
        :param stream_name: Name of the stream.
        :param message: Dict representing the message contents.
        :param stream_type: StreamType to write to.
        :return: The message id.
        """

        try:
            # Stream variable has priority
            send_stream = single_stream_entry if single_stream_entry \
                else self._stream_entry_lookup(stream_name=stream_name, stream_type=stream_type)

            # Send message
            return self._redis_stream.stream_write_message(stream=send_stream.stream, message=message)

        except ExceptStreamLookupFailed:
            return None

    def stream_read_message(self, single_stream_entry: StreamListEntry.SingleStream = None, stream_name: str = None,
                            stream_type: StreamType = None) -> [StreamRequestMessage]:
        """
        Reads a message from a stream.
        The stream can either be passed via name and type or via the StreamListEntry.SingleStream directly.
        The StreamListEntry.SingleStream has priority.
        :param single_stream_entry: RedisStream.Stream
        :param stream_name: Name of the stream.
        :param stream_type: StreamType to read from.
        :return:
        """

        req_messages = []

        try:
            # Stream variable has priority
            receive_stream = single_stream_entry if single_stream_entry \
                else self._stream_entry_lookup(stream_name=stream_name, stream_type=stream_type)

            # Read message from stream
            messages = self._redis_stream.stream_read_message(stream=receive_stream.stream,
                                                              last_message_id=receive_stream.last_message_id)

            if messages:
                # Store id of latest message
                receive_stream.last_message_id = messages[-1].message_id

                # Handle individual messages
                for message in messages:
                    try:
                        message_dict = json.loads(message.message)
                        message_dict[nameof(message.message_id)] = message.message_id
                        req_messages.append(
                            StreamRequestMessage(init_dict=message_dict).get_special_request_message()
                        )
                    except Exception as e:
                        self._logger.write_exception(f"Ignoring stream message since it does not comply the syntax:\n"
                                                     f"{vars(message)}")

        except ExceptStreamLookupFailed as e:
            # Stream not found
            self._logger.write_exception(f"ExceptStreamLookupFailed:{e.args}")

        except Exception as e:
            # If a stream does not exist, this will fail
            self._logger.write_exception(f"General error during stream read:{e.args}")

        finally:
            return req_messages

    def stream_delete_all_messages(self, stream_name: str):
        """
        Deletes all messages from device related streams.
        This is useful to init our read last message id and not fetch old messages.
        :param stream_name:
        :return:
        """
        stream = self._stream_entry_lookup(stream_name=stream_name, stream_type=StreamType.b2d)
        stream.stream.trim(count=0, approximate=False)
        stream = self._stream_entry_lookup(stream_name=stream_name, stream_type=StreamType.d2b)
        stream.stream.trim(count=0, approximate=False)

    def stream_read_all_ignore(self, stream_name: str):
        """
        Reads out all messages from device related streams and ignores them.
        This is useful to init our read last message id and not fetch old messages.
        :param stream_name:
        :return:
        """
        self.stream_read_message(stream_name=stream_name, stream_type=StreamType.b2d)
        self.stream_read_message(stream_name=stream_name, stream_type=StreamType.d2b)

    def stream_acknowledge_check(self, stream_name: str, message_id,
                                 stream_type: StreamType) -> StreamRequestMessageAck:
        """
        Checks for a message being acknowledged by the client.
        :param stream_name: The stream name the message was sent to.
        :param message_id: The message id of the message sent.
        :param stream_type: StreamType to check ack from.
        :return: Success/ack-received->AcknowledgeMessage; no-ack->None.
        """

        timeout = 0.0

        while timeout < self._stream_timeout_ack:

            # Get ack massages
            stream_messages = self.stream_read_message(stream_name=stream_name, stream_type=stream_type)

            if stream_messages is None:
                # None indicates some general error during read
                return StreamRequestMessageAck(ack_state=AckState.fail, message={
                    "message": "There is currently no active stream to the requested device."})

            # Loop over messages and check for the ack
            for stream_message in stream_messages:

                try:
                    # Check for type AckMessage
                    if isinstance(stream_message, StreamRequestMessageAck):

                        # Check whether we found the ack
                        if stream_message.payload.send_message_id == message_id:
                            self._logger.write_debug(
                                "Received ack for message-id:`{}`\nack:{}".format(
                                    message_id, stream_message.as_dict())
                            )

                            return stream_message
                except:
                    pass

            # Sleep
            time.sleep(0.01)
            timeout += 0.01

        # No ack received within timeout period
        self._logger.write_error(
            "No ack for message-id `{}` received within timeout period of {}s".format(
                message_id, self._stream_timeout_ack)
        )

        return StreamRequestMessageAck(ack_state=AckState.no_ack_received,
                                       message={
                                           "message": f"No ack for message-id `{message_id}` received within timeout "
                                                      f"period of {self._stream_timeout_ack}s."})
