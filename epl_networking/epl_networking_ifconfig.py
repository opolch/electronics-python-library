from enum import Enum, auto


class IfconfigParser:
    """
    Class providing functionality for parsing ifconfig outputs.
    """

    ifconfig_example_output_1 = \
"""enp0s31f6: flags=4099<UP,BROADCAST,MULTICAST>  mtu 1500
        ether 00:2b:67:98:a6:5f  txqueuelen 1000  (Ethernet)
        RX packets 0  bytes 0 (0.0 B)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 0  bytes 0 (0.0 B)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0
        device interrupt 16  memory 0xe3380000-e33a0000  

lo: flags=73<UP,LOOPBACK,RUNNING>  mtu 65536
        inet 127.0.0.1  netmask 255.0.0.0
        inet6 ::1  prefixlen 128  scopeid 0x10<host>
        loop  txqueuelen 1000  (Local Loopback)
        RX packets 2335  bytes 435485 (435.4 KB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packelo: flags=73<UP,ts 2335  bytes 435485 (435.4 KB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

wlp0s20f3: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
        inet 192.168.1.6  netmask 255.255.255.0  broadcast 192.168.1.255
        inet6 fe80::10cb:684c:f9e0:26ba  prefixlen 64  scopeid 0x20<link>
        ether c8:58:c0:2a:5b:ca  txqueuelen 1000  (Ethernet)
        RX packets 99897  bytes 125833503 (125.8 MB)
        RX errors 0  dropped 85  overruns 0  frame 0
        TX packets 41392  bytes 4859750 (4.8 MB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0"""

    ifconfig_example_output_2 = \
"""enp0s3    Link encap:Ethernet  HWaddr 08:00:27:98:26:f6  
        inet addr:134.104.71.27  Bcast:134.104.79.255  Mask:255.255.240.0
        inet6 addr: fe80::e585:726d:3142:bd88/64 Scope:Link
        UP BROADCAST RUNNING MULTICAST  MTU:1500  Metric:1
        RX packets:873006984 errors:0 dropped:2150 overruns:0 frame:0
        TX packets:881020199 errors:0 dropped:0 overruns:0 carrier:0
        collisions:0 txqueuelen:1000 
        RX bytes:164956020765 (164.9 GB)  TX bytes:266726614243 (266.7 GB)

lo        Link encap:Local Loopback  
        inet addr:127.0.0.1  Mask:255.0.0.0
        inet6 addr: ::1/128 Scope:Host
        UP LOOPBACK RUNNING  MTU:65536  Metric:1
        RX packets:33040 errors:0 dropped:0 overruns:0 frame:0
        TX packets:33040 errors:0 dropped:0 overruns:0 carrier:0
        collisions:0 txqueuelen:1000 
        RX bytes:3172782 (3.1 MB)  TX bytes:3172782 (3.1 MB)"""

    ifconfig_example_output_3 = \
"""eth0: flags=-28605<UP,BROADCAST,RUNNING,MULTICAST,DYNAMIC>  mtu 1500
        inet 134.104.71.36  netmask 255.255.240.0  broadcast 134.104.79.255
        inet6 fe80::aa20:fbff:fe32:ca42  prefixlen 64  scopeid 0x20<link>
        ether a8:20:fb:32:ca:42  txqueuelen 1000  (Ethernet)
        RX packets 36402832  bytes 3683896955 (3.4 GiB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 6519757  bytes 1425695143 (1.3 GiB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0
        device interrupt 54  

lo: flags=73<UP,LOOPBACK,RUNNING>  mtu 65536
        inet 127.0.0.1  netmask 255.0.0.0
        inet6 ::1  prefixlen 128  scopeid 0x10<host>
        loop  txqueuelen 1000  (Local Loopback)
        RX packets 4336  bytes 399180 (389.8 KiB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 4336  bytes 399180 (389.8 KiB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

usb0: flags=4099<UP,BROADCAST,MULTICAST>  mtu 1500
        inet 192.168.7.2  netmask 255.255.255.0  broadcast 192.168.7.255
        ether 60:b6:e1:05:1b:5d  txqueuelen 1000  (Ethernet)
        RX packets 0  bytes 0 (0.0 B)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 0  bytes 0 (0.0 B)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

usb1: flags=4099<UP,BROADCAST,MULTICAST>  mtu 1500
        inet 192.168.6.2  netmask 255.255.255.0  broadcast 192.168.6.255
        ether 60:b6:e1:05:1b:61  txqueuelen 1000  (Ethernet)
        RX packets 0  bytes 0 (0.0 B)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 0  bytes 0 (0.0 B)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0"""

    class IfcfgType(Enum):
        """
        Enum to define the ifconfig type the output belongs to.
        """
        LINUX = auto()
        LINUX_OLD = auto()
        MAC = auto()
        WINDOWS = auto()

    class IfcfgFieldsLinux(Enum):
        """
        Keys to map the ifconfig output fields.
        """
        HW_ADDR = "hwaddr"
        MTU = "mtu"
        INET = "inet"
        INET6 = "inet6"
        MASK = "mask"
        PREFIX_LEN = "prefixlen"
        SCOPE = "scope"
        TX_QUEUE_LEN = "txqueuelen"
        RX_PACKETS = "rx packets"
        RX_BYTES = "rx bytes"
        RX_ERRORS = "rx errors"
        RX_DROPPED = "rx dropped"
        RX_OVERRUNS = "rx overruns"
        RX_FRAME = "rx frame"
        TX_PACKETS = "tx packets"
        TX_BYTES = "tx bytes"
        TX_ERRORS = "tx errors"
        TX_DROPPED = "tx dropped"
        TX_OVERRUNS = "tx overruns"

    # Key/sub-key structure to parse the linux ifconfig result by
    _IFCFG_KEYS_LINUX_OLD = [
        IfcfgFieldsLinux.HW_ADDR.value,
        IfcfgFieldsLinux.MTU.value,
        IfcfgFieldsLinux.INET.value,
        IfcfgFieldsLinux.INET6.value,
        IfcfgFieldsLinux.MASK.value,
        IfcfgFieldsLinux.PREFIX_LEN.value,
        IfcfgFieldsLinux.SCOPE.value,
        IfcfgFieldsLinux.TX_QUEUE_LEN.value,
        {
            IfcfgFieldsLinux.RX_PACKETS.value.split(" ")[0]: [
                IfcfgFieldsLinux.RX_PACKETS.value.split(" ")[1],
                IfcfgFieldsLinux.RX_ERRORS.value.split(" ")[1],
                IfcfgFieldsLinux.RX_DROPPED.value.split(" ")[1],
                IfcfgFieldsLinux.RX_OVERRUNS.value.split(" ")[1],
                IfcfgFieldsLinux.RX_FRAME.value.split(" ")[1],
            ]
        },
        {
            IfcfgFieldsLinux.TX_PACKETS.value.split(" ")[0]: [
                IfcfgFieldsLinux.TX_PACKETS.value.split(" ")[1],
                IfcfgFieldsLinux.TX_ERRORS.value.split(" ")[1],
                IfcfgFieldsLinux.TX_DROPPED.value.split(" ")[1],
                IfcfgFieldsLinux.TX_OVERRUNS.value.split(" ")[1],
            ]
        },
        IfcfgFieldsLinux.RX_BYTES.value,
        IfcfgFieldsLinux.TX_BYTES.value,
    ]

    # Key/sub-key structure to parse the linux ifconfig result by
    _IFCFG_KEYS_LINUX = [
        IfcfgFieldsLinux.HW_ADDR.value,
        IfcfgFieldsLinux.MTU.value,
        IfcfgFieldsLinux.INET.value,
        IfcfgFieldsLinux.INET6.value,
        IfcfgFieldsLinux.MASK.value,
        IfcfgFieldsLinux.PREFIX_LEN.value,
        IfcfgFieldsLinux.SCOPE.value,
        IfcfgFieldsLinux.TX_QUEUE_LEN.value,
        {
            IfcfgFieldsLinux.RX_PACKETS.value.split(" ")[0]: [
                IfcfgFieldsLinux.RX_PACKETS.value.split(" ")[1],
                IfcfgFieldsLinux.RX_BYTES.value.split(" ")[1],
            ]
        },
        {
            IfcfgFieldsLinux.RX_ERRORS.value.split(" ")[0]: [
                IfcfgFieldsLinux.RX_ERRORS.value.split(" ")[1],
                IfcfgFieldsLinux.RX_DROPPED.value.split(" ")[1],
                IfcfgFieldsLinux.RX_OVERRUNS.value.split(" ")[1],
                IfcfgFieldsLinux.RX_FRAME.value.split(" ")[1],
            ]
        },
        {
            IfcfgFieldsLinux.TX_PACKETS.value.split(" ")[0]: [
                IfcfgFieldsLinux.TX_PACKETS.value.split(" ")[1],
                IfcfgFieldsLinux.TX_BYTES.value.split(" ")[1],
            ]
        },
        {
            IfcfgFieldsLinux.TX_ERRORS.value.split(" ")[0]: [
                IfcfgFieldsLinux.TX_ERRORS.value.split(" ")[1],
                IfcfgFieldsLinux.TX_DROPPED.value.split(" ")[1],
                IfcfgFieldsLinux.TX_OVERRUNS.value.split(" ")[1],
            ]
        },
    ]

    def __init__(self, ifconfig_output, ifconfig_type=IfcfgType.LINUX):
        """
        Parses the ifconfig output. Results is stored in devices dict.
        :param ifconfig_output: Output as how it comes from the ifconfig command.
        :param ifconfig_type:
        """
        self.devices = {}

        # Currently only Linux supported
        if ifconfig_type == self.IfcfgType.LINUX_OLD:
            ifcfg_keys = self._IFCFG_KEYS_LINUX_OLD
        else:
            ifcfg_keys = self._IFCFG_KEYS_LINUX

        # To make parsing different versions of ifconfig outputs possible, we need to prepare the data
        ifconfig_output = ifconfig_output.lower()
        ifconfig_output = ifconfig_output.replace("inet addr", self.IfcfgFieldsLinux.INET.value)
        ifconfig_output = ifconfig_output.replace("inet6 addr", self.IfcfgFieldsLinux.INET6.value)
        ifconfig_output = ifconfig_output.replace("ether ", "{} ".format(self.IfcfgFieldsLinux.HW_ADDR.value))

        # Interfaces are separated by double \n
        for iface_output in ifconfig_output.split("\n\n"):

            # First get the interface
            # Usually `:` is used to separate the iface, but ` ` was also found
            # In addition, sub-interfaces are indexed in the form `iface:1` where we need to replace the `:` by `_`
            iface = iface_output.split(" ")[0].replace(":", "_").strip("_")

            # Add an entry per interface
            self.devices[iface] = {}
            res_dict_iface = self.devices[iface]

            # Join lines (makes searching easier) and replace double " "
            iface_output = " ".join(iface_output.split("\n")).replace("  ", " ")

            # Search for keys from ifconfig keys lut
            for key in ifcfg_keys:

                # For some fields the key has to be manipulated
                resulting_key = key

                try:
                    # Some keys (e.g. rx/tx) have kind of a key-subkey setup, this is implemented as "dict:[list]"
                    if isinstance(key, dict):

                        # dict only holds one element -> the parent key
                        parent_key = list(key)[0]

                        # Loop over sub keys
                        for list_entry in key[parent_key]:
                            # Generate resulting key (parent+sub)
                            resulting_key = "{} {}".format(parent_key, list_entry)

                            # Get value for key (need to add `:`)
                            res_dict_iface[resulting_key] = iface_output.split(
                                "{}".format(resulting_key))[1].strip(": ").split(" ")[0]

                            # Delete (key + value) from iface, to make the algorithm work
                            # Older version separate key and value by `:`
                            replacer = "{} {}".format(resulting_key, res_dict_iface[resulting_key])
                            iface_output = iface_output.replace(replacer, parent_key)
                            replacer = "{}:{}".format(resulting_key, res_dict_iface[resulting_key])
                            iface_output = iface_output.replace(replacer, parent_key)
                    else:
                        # Single-level keys can be grabbed directly
                        res_dict_iface[resulting_key] = (
                            iface_output.split("{}".format(key))[1].strip(": ").split(" ")[0])
                except IndexError:
                    # Depending on the interface's state, not all keys are present
                    # res_dict_iface[resulting_key] = None
                    pass
                except Exception as e:
                    # In case something else fails, we also put None
                    # res_dict_iface[resulting_key] = None
                    pass
