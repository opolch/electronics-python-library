from threading import Thread
import time


class TriggerTimeout(Thread):
    """
    Class providing timeout trigger functionality.
    Based on a timeout value, a recurring trigger callback will be executed.
    """

    def __init__(self, callback, timeout, args=None, start_immediate=False):
        """
        Init the thread object.
        :param callback: Function to be called every timeout seconds.
        :param args: Args to be passed when callback is called.
        :param timeout: Timeout between calls of callback.
        :param start_immediate: True->Start thread immediately; False->Start later manually.
        """

        # Init base class
        super().__init__(target=self._thread_loop)

        # Variables
        self._timeout = int(timeout * 1000)
        self._stop_thread = False
        self._callback_user = callback
        self._callback_user_args = args

        # Start thread immediate
        if start_immediate is True:
            self.start()

    def _thread_loop(self):
        """
        The main thread loop.
        :return:
        """
        while self._stop_thread is False:

            # Make the loop break immediate
            for sleep_time in range(0, self._timeout):
                # Wait...
                time.sleep(0.001)

                # Break?
                if self._stop_thread is True:
                    return

            # Call callback (with or without args)
            if self._callback_user_args is not None:
                self._callback_user(self._callback_user_args)
            else:
                self._callback_user()

    def start_thread(self):
        """
        Starts the thread but only if not running.
        :return: True->Started; False->Was running.
        """
        # Reset stopper
        self._stop_thread = False

        # Check whether thread is running before starting
        if self.is_alive() is False:
            # Start it
            self.start()
            return True

        return False

    def stop_thread(self):
        """
        Sets the thread stopper.
        :return:
        """
        self._stop_thread = True
