import asyncio
import sys
import uuid
from tango._tango import GreenMode
from tango._tango import AttrWriteType , DevState, Attr, DevFloat, DevString, DevBoolean, UserDefaultAttrProp 
from epl.epl_logging.epl_logging_log import Log
        
class TangoAttr:
    """
    Class to init tango attributes to be 
    process by translator clients
    """

    def __init__(self,
                 attr_name,
                 attr_value=None,
                 attr_req_type="GET",
                 client_info=[],
                 client_reply=[]):
        """
        Init the module
        :param attr_name: uid of the attribute
        :param attr_value: attribute value
        :param attr_req_type: attribute request to client:GET/SET
        :client_info: information from translator client
        :client_reply: reply from translator client on request
        """
        self.id            = uuid.uuid4().hex
        self.attr_name     = attr_name
        self.attr_value    = attr_value
        self.attr_req_type = attr_req_type
        self.client_info   = client_info
        self.client_reply  = client_reply
        self.dev_string    = DevString
        self.dev_float     = DevFloat
        self.dev_bool      = DevBoolean
 
    def create_tango_attr_name(self, sensor_name):
        """
        Obtain the attribute name from the sensor name.

        :param sensor_name: sensor name of the sensor or request to create the tango attribute name from.
        :return:
        """
        a_list = list(sensor_name)  # Convert string to list for processing
        indices = [i for i, e in enumerate(a_list) if (e == '.' or e == '-')]
        for i in indices:
            a_list[i+1] = a_list[i+1].upper()
            
        for i in a_list:
            if i == '.' or i == '-':
                a_list.remove(i)
        attribute_name = ''.join(a_list)
        
        return attribute_name

    def create_attr_prop(self, attribute_name, min_value, max_value):
        """
        Create a Tango attribute based on sensor type

        :param attribute_name: sensor_type name of the sensor or request to create the tango attribute name from.
        :param min_value: tango attribute properties.
        :param max_value: tango attribute type.
        ::return: tango attribute prop
        """

        prop =  UserDefaultAttrProp()
        prop.set_label(attribute_name)
        
        if min_value is not None:
           prop.set_min_value(min_value)
           prop.set_max_value(max_value)
        
        return prop

    def create_attributes(self, attribute_name, req_min, req_max, sensor_type, attr_type=0):
        """
        Create a Tango attribute based on sensor type

        :param sensor_name: sensor_type name of the sensor or request to create the tango attribute name from.
        :param prop: tango attribute properties.
        :param attr_type: tango attribute type.
        ::return: tango attribute
        """
        
        attr_min =  None
        attr_max =  None

        try:
 
            if sensor_type != DevString:
                if req_min == req_max:
                    attr_min = str(-sys.maxsize)
                    attr_max = str(sys.maxsize)

                elif req_min > req_max:
                    attr_min = str(req_max)
                    attr_max = str(req_min)

                else:
                    attr_min = str(req_min)
                    attr_max = str(req_max)

            prop = self.create_attr_prop(attribute_name, attr_min, attr_max)

            if attr_type == 1:
               attr = Attr(attribute_name, sensor_type, AttrWriteType.READ_WRITE)
               attr.set_default_properties(prop)
            elif attr_type == 2:           
               attr = Attr(attribute_name, sensor_type, AttrWriteType.WRITE)
               attr.set_default_properties(prop)
            else:
               attr = Attr(attribute_name, sensor_type, AttrWriteType.READ)
               attr.set_default_properties(prop)

            return attr
        except Exception as e:
            print(f"attr create failed with {e}")
