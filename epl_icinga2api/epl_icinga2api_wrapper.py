import copy
from enum import Enum, auto
from epl.epl_icinga2api.epl_icinga2api_client import Client as IcingaClient
from epl.epl_logging.epl_logging_log import Log
import asyncio
from datetime import datetime


class UpdateMode(Enum):
    """
    Enum to define the icinga update mode when updating hosts/services on icinga.
    """
    keep = 0
    add_unknown = 1
    add_and_update = 2
    full_sync = 3
    fresh_insert = 4


class ObjectType(Enum):
    """
    Enum to define an object type
    """
    Host = auto()
    Service = auto()


class ExitStatusHost(Enum):
    """
    Enum to define a Host's exit status
    """
    OK = 0
    CRITICAL = 1


class ExitStatusService(Enum):
    """
    Enum to define a Service's exit status
    """
    OK = 0
    WARNING = 1
    CRITICAL = 2
    UNKNOWN = 3


class IcingaUpdCreateElement:
    """
    Class for forming an icinga update/create update_create_element.
    """

    class Action(Enum):
        """
        Enum to define an action.
        """
        UPDATE = auto()
        CREATE = auto()
        DELETE = auto()

    def __init__(self, action, object_type, name, attr=None, templates=None):
        """
        Init the update_create_element.
        :param action: See Action class.
        :param object_type: See ObjectType class.
        :param attr: Element's attributes dict.
        :param templates: Templates (host-only)
        """
        self.action = action
        self.object_type = object_type
        self.name = name
        self.attr = attr
        self.templates = templates


class IcingaCheckResultElement:
    """
    Class for forming check_result request.
    """

    def __init__(self, object_type, name, exit_status, plugin_output, check_source, execution_start):
        self.object_type = object_type
        self.name = name
        self.exit_status = exit_status
        self.plugin_output = plugin_output
        self.check_source = check_source
        self.execution_start = execution_start


class Icinga2Api(IcingaClient):
    """
    epl Icinga API extending epl_icinga2api.client class.
    """

    def __init__(self,
                 url=None,
                 username=None,
                 password=None,
                 timeout=None,
                 certificate=None,
                 key=None,
                 ca_certificate=None,
                 config_file=None,
                 logger=Log(name='icinga_logger'),
                 httpx_max_connections=100,
                 cons_request_delay=0.0):
        """
        Init the client.
        :param url: icinga server url.
        :param username: icinga username.
        :param password: icinga password.
        :param timeout: timeout.
        :param certificate: certificate for ssl connection.
        :param key: key for pup-key auth.
        :param ca_certificate:
        :param config_file:
        :param logger: epl_logging_log object.
        :param httpx_max_connections: Maximum connections for http actions.
        :param cons_request_delay: Delay time between consecutive requests.
        """

        super(Icinga2Api, self).__init__(url,
                                         username,
                                         password,
                                         timeout,
                                         certificate,
                                         key,
                                         ca_certificate,
                                         config_file,
                                         logger=logger,
                                         httpx_max_connections=httpx_max_connections)
        # Set the logger
        self._logger = logger

        # General variables
        self._cons_request_delay = cons_request_delay

    async def shutdown(self):
        """
        Close session.
        """
        if self.session is not None:
            await self.session.aclose()

    async def sync_config(self, hosts_config, host_object_attr, service_object_attr, update_mode):
        """
        Function to sync the config on the icinga server with the one from api.
        :param hosts_config: Host conf dict containing hostname:list(host_services) entries.
        :param host_object_attr: The host's attr definition as dict.
            _host_object_attr = {"address": 'no', "check_command": 'dummy', 'enable_active_checks': True,
                             'check_interval': 120.0,
                             'vars':
                                 {
                                     'dummy_state': '2',
                                     'dummy_text': 'No recent updates from Icinga-sidecar, sidecar down?',
                                     "os": "Linux"
                                 }
                             }
        :param service_object_attr: The _icinga_service's attr definition as dict.
            _service_object_attr = {"check_command": 'dummy', 'enable_active_checks': True,
                                'check_interval': 120.0,
                                'vars':
                                    {
                                        'dummy_state': '0',
                                        'dummy_text': 'OK!',
                                    }
                                }
        :param update_mode: UpdateMode
        :return:
        """

        def update_create_attr(object_attr_dict, display_name):
            """
            Create the icinga compliant create/update attr dict.
            The object-list dict differs from the update/create dict.
            :param object_attr_dict: The object-list attr dict.
            :return: icinga compliant create/update attr dict
            """
            _icinga_dict = {}
            for _key, _value in object_attr_dict.items():
                if _key == 'vars':
                    for _var_key, _var_value in _value.items():
                        _icinga_dict['vars.{}'.format(_var_key)] = _var_value
                else:
                    _icinga_dict[_key] = _value

            # Add the display name
            _icinga_dict['display_name'] = display_name

            return _icinga_dict

        # List for elements to be updated/created/deleted
        # Needed for async processing
        icinga_elements = list()

        # Get the actual object list
        self._logger.write_info('Getting icinga objects')

        # Create the joins string first, need to add host.name manually
        host_joins = ['host.{}'.format(att) for att in host_object_attr.keys()]
        host_joins.append('host.name')

        # Get the actual object lists
        _icinga_service_list = await self.objects.list(ObjectType.Service.name,
                                                       joins=host_joins,
                                                       attrs=[att for att in service_object_attr.keys()])
        _icinga_host_list = await self.objects.list(ObjectType.Host.name,
                                                    attrs=[att for att in host_object_attr.keys()])

        # In full_sync mode, delete unknown hosts first
        if update_mode == UpdateMode.full_sync:
            for _icinga_host in _icinga_host_list:

                # Get the hosts name
                _icinga_gim_host_name = _icinga_host['name']

                # A known host?
                if _icinga_gim_host_name in hosts_config.keys():

                    # gim host
                    _gim_host = hosts_config[_icinga_gim_host_name]

                    # Then check for services to be deleted
                    for _icinga_service in _icinga_service_list:

                        # Get the _icinga_service's name
                        _icinga_service_name = _icinga_service['name']
                        _icinga_service_found = False

                        # Does this _icinga_service belong to current gim_host
                        if _icinga_gim_host_name in _icinga_service_name:

                            # Iterate over tasks and look for service name
                            for _gim_task in _gim_host['tasks']:
                                # Found the corresponding task?
                                if _gim_task['id'] in _icinga_service_name:
                                    _icinga_service_found = True
                            if _icinga_service_found is False:
                                # Delete the service
                                icinga_elements.append(
                                    IcingaUpdCreateElement(action=IcingaUpdCreateElement.Action.DELETE,
                                                           object_type=ObjectType.Service,
                                                           name=_icinga_service_name))
                                self._logger.write_debug('Object {} will be deleted'.format(_icinga_service_name))
                else:
                    # Delete the entire host
                    icinga_elements.append(IcingaUpdCreateElement(action=IcingaUpdCreateElement.Action.DELETE,
                                                                  object_type=ObjectType.Host,
                                                                  name=_icinga_gim_host_name))

                    self._logger.write_debug('Object {} will be deleted'.format(_icinga_gim_host_name))

        self._logger.write_info('Found {} icinga objects'.format(len(_icinga_service_list)))

        # Iterate over hosts and perform the update
        for host_id, host_details in hosts_config.items():

            # Get display name
            host_name_display = host_details['display_name']

            # Object flags for creation/update
            object_available = False
            object_update = False

            # Add the individual gim node-id to be used for host/_icinga_service->node mapping
            host_object_attr['vars']['gim_node_id'] = host_id

            # In update and add mode we need to check for presence of objects first
            if update_mode.value >= UpdateMode.add_unknown.value:
                # Check if host already in db
                for ici_object in _icinga_host_list:
                    # Find the host
                    if host_id == ici_object['name']:
                        # Found object
                        object_available = True

                        # For the update mode, compare the configs
                        # Remove name since not part of config, have to use copy to not manipulate original dict
                        ici_object_copy = copy.deepcopy(ici_object)
                        del ici_object_copy['name']

                        # Add display-name (not part)
                        host_object_attr['display_name'] = host_name_display

                        # Compare
                        if ici_object_copy['attrs'] != host_object_attr:
                            object_update = True
                        break

            if not object_available:
                # Add new nodes
                if update_mode.value >= UpdateMode.add_unknown.value:
                    # Create new
                    icinga_elements.append(
                        IcingaUpdCreateElement(action=IcingaUpdCreateElement.Action.CREATE,
                                               object_type=ObjectType.Host,
                                               name=host_id,
                                               attr=update_create_attr(host_object_attr, host_name_display)))

                    self._logger.write_info('{} {}({}) will be created on icinga'.
                                            format(ObjectType.Host.name, host_name_display, host_id))
            elif object_update is True and update_mode.value >= UpdateMode.add_and_update.value:
                # Update existing
                icinga_elements.append(
                    IcingaUpdCreateElement(action=IcingaUpdCreateElement.Action.UPDATE,
                                           object_type=ObjectType.Host,
                                           name=host_id,
                                           attr=update_create_attr(host_object_attr, host_name_display)))

                self._logger.write_info('{} {}({}) will be updated on icinga'.
                                        format(ObjectType.Host.name, host_name_display, host_id))

            # Now check the host's services
            for _service in host_details['tasks']:

                # Reset object flags
                object_available = False
                object_update = False

                # Create the _icinga_service name
                service_name_display = _service['data']['node_name']  # TODO
                service_name_full = '{}!{}'.format(host_id, _service['id'])

                # Add the individual gim node-id to be used for host/_icinga_service->node mapping
                service_object_attr['vars']['gim_node_id'] = _service['id']
                service_object_attr['display_name'] = service_name_display

                # In update and add mode we need to check for presence of objects first
                if update_mode.value >= UpdateMode.add_unknown.value:
                    # Check if _icinga_service already in db
                    for ici_object in _icinga_service_list:
                        if service_name_full == ici_object['name']:
                            object_available = True

                            # Compare the configs
                            if ici_object['attrs'] != service_object_attr:
                                object_update = True
                            break

                # Always add new objects
                if not object_available:
                    # Add new nodes
                    if update_mode.value >= UpdateMode.add_unknown.value:
                        icinga_elements.append(
                            IcingaUpdCreateElement(action=IcingaUpdCreateElement.Action.CREATE,
                                                   object_type=ObjectType.Service,
                                                   name=service_name_full,
                                                   attr=update_create_attr(service_object_attr, service_name_display)))

                        self._logger.write_debug('{} {} will be added on icinga'.
                                                 format(ObjectType.Service.name, service_name_full))

                elif object_update is True and update_mode.value >= UpdateMode.add_and_update.value:
                    icinga_elements.append(
                        IcingaUpdCreateElement(action=IcingaUpdCreateElement.Action.UPDATE,
                                               object_type=ObjectType.Service,
                                               name=service_name_full,
                                               attr=update_create_attr(service_object_attr, service_name_display)))
                    self._logger.write_debug('{} {} will be updated on icinga'.
                                             format(ObjectType.Service.name, service_name_full))

        self._logger.write_info('{} objects to be updated/created/deleted on icinga'.format(len(icinga_elements)))

        # Send update/create request async parallelized
        # First add/delete hosts
        await asyncio.gather(
            *[self.update_create_do(element, sleep_time=i * self._cons_request_delay)
              for i, element in enumerate(icinga_elements) if element.object_type == ObjectType.Host]
        )
        # Proceed with services
        await asyncio.gather(
            *[self.update_create_do(element, sleep_time=i * self._cons_request_delay)
              for i, element in enumerate(icinga_elements) if element.object_type == ObjectType.Service]
        )

    async def update_create_do(self, update_create_element, sleep_time):
        """
        Helper function to gain performance with async calls.
        Function can be called asyncio.gather to send requests parallelized.
        :param update_create_element: Element from type IcingaUpdCreateElement.
        :param sleep_time: Time to wait between calls (asyncio.sleep).
        :return:
        """

        # Introduce a wait to not fire consecutive requests in parallel (workaround)
        await asyncio.sleep(sleep_time)

        self._logger.write_debug("Sending `update_create_do` with wait-time:{}".format(sleep_time))

        try:
            # Distinguish the action and execute
            if update_create_element.action == IcingaUpdCreateElement.Action.CREATE:
                await self.objects.create(
                    update_create_element.object_type.name,
                    name=update_create_element.name,
                    attrs=update_create_element.attr,
                    templates=update_create_element.templates
                )
            elif update_create_element.action == IcingaUpdCreateElement.Action.UPDATE:
                await self.objects.update(
                    update_create_element.object_type.name,
                    name=update_create_element.name,
                    attrs=update_create_element.attr
                )
            elif update_create_element.action == IcingaUpdCreateElement.Action.DELETE:
                await self.objects.delete(
                    update_create_element.object_type.name,
                    name=update_create_element.name,
                    cascade=True)
        except Exception as e:
            self._logger.write_error("update_create_do, ex:{}".format(e))

    async def check_host_service_errors(self, hosts_config):
        """
        Reads history for all the services and checks for violations.
        In case of any, it executes a check request to inform icinga.
        :param hosts_config: Host conf dict containing hostname:list(host_services, host_history) entries.
        :return:
        """

        try:
            # Used to collect requests and send async
            check_results = list()

            # Create list with services currently not reporting OK
            services_failing = [service['name'].split('!', 1)[1]
                                for service in
                                await self.objects.list('Service', attrs=['state'],
                                                        filters='service.state>0 && service.downtime_depth==0.0')]

            # Iterate over hosts and perform the update
            for host_id, host_details in hosts_config.items():

                # Extract details
                host_name = host_details['name']
                host_services = host_details['services']
                host_history = host_details['history']

                # Connection check is done by:
                # 1) Check for any history
                # 2) Check for node_name.sidecar.connected != 0
                host_num_history = 0
                host_connected = False

                # Iterate over services and perform the range checks
                for service in host_services:

                    # Check if value_type is available
                    if 'value_type' in service['data']['metadata']:
                        _value_type = service['data']['metadata']['value_type']
                    else:
                        _value_type = ''
                    _node_name = service['data']['node_name']
                    _node_id = service['id']

                    try:
                        # Get lower/upper limits (only for logging)
                        upper_limit = service['data']['metadata'].get('upper_limit', '`no-limit-provided`')
                        lower_limit = service['data']['metadata'].get('lower_limit', '`no-limit-provided`')

                        try:
                            if float(upper_limit) == float(lower_limit) == 0.0:
                                self._logger.write_debug('Range check disabled by ll/ul=0.0, node `{}`'.
                                                         format(_node_name))
                                continue
                        except:
                            self._logger.write_debug('Limits can not be converted to float ll/ul={}/{}, node `{}`.'.
                                                     format(lower_limit, upper_limit, _node_name))

                        self._logger.write_debug('Node `{}` will be included in range check'.
                                                 format(_node_name))

                        # Reset variables
                        actual_val = -123456789.987654321
                        service_num_history = 0
                        service_in_error = False

                        # Try finding related history by comparing the node id
                        for service_histories in host_history:
                            if service_histories is not None:
                                if len(service_histories) > 0:
                                    if service['id'] == service_histories[0]['nodeId']:
                                        for service_single_history in service_histories:

                                            # Indicate at least one history data (used for connection check)
                                            host_num_history += 1

                                            # Found related hist, do check now
                                            service_num_history += 1

                                            # Get actual value
                                            hist_time = service_single_history['time']
                                            actual_val = service_single_history[_value_type]
                                            error_state = int(service_single_history['error_state'])

                                            # First check for `connected` task
                                            # Since we might have virtual root devices as root (they don't have
                                            # the sidecar.connected task), we need to check for any.
                                            # We only report error here when all connected!=1 (not on a single)
                                            # if '{}.sidecar.connected'.format(host_name) in _node_name:
                                            if 'sidecar.connected' in _node_name:
                                                if actual_val == 1:
                                                    host_connected = True

                                            if error_state != 1:
                                                # Indicate service in error
                                                service_in_error = True

                                                # Process the check result to report the error
                                                check_results.append(
                                                    IcingaCheckResultElement(
                                                        object_type=ObjectType.Service,
                                                        name='{}!{}'.format(host_id, _node_id),
                                                        exit_status=ExitStatusService.CRITICAL,
                                                        plugin_output='Service in error {}<=val<={}, val={}, '
                                                                      'err-state={}'.
                                                        format(lower_limit, upper_limit, actual_val, error_state),
                                                        check_source='icinga-sidecar',
                                                        execution_start=datetime.now().timestamp()
                                                    ))
                                                self._logger.write_info(
                                                    'Task {}: in error {}<=val<={}, val={}, time={}'.
                                                    format(_node_name, lower_limit, upper_limit,
                                                           actual_val, hist_time))
                                                # Can break the loop here, single report is sufficient
                                                break
                                        # If we found the service's history, can break the loop
                                        if service_num_history > 0:
                                            break

                        # Any history available?
                        if service_num_history == 0:
                            self._logger.write_info(
                                'Empty history for {}.{}'.format(host_name, _node_name))
                        elif _node_id in services_failing and service_in_error is False:
                            # If the node is currently in failing state and was not updated in this run,
                            # we set it back to OK state. Otherwise, an error update would have been occurred.
                            check_results.append(
                                IcingaCheckResultElement(
                                    object_type=ObjectType.Service,
                                    name='{}!{}'.format(host_id, _node_id),
                                    exit_status=ExitStatusService.OK,
                                    plugin_output='OK: {}<=val<={}, val={}'.
                                    format(lower_limit, upper_limit, actual_val),
                                    check_source='icinga-sidecar',
                                    execution_start=datetime.now().timestamp()
                                ))
                            self._logger.write_debug('Task {}: Was in ERROR/WARNING. Set now back to OK'.
                                                     format(_node_name))

                    except Exception as e:
                        self._logger.write_error('Could not analyse task {}; ex: {}'.format(_node_name, e))

                # Check if host had a single update and update connection state
                if host_connected is False:
                    _exit_status = ExitStatusHost.CRITICAL
                    _plugin_output = 'Host disconnected (sidecar.connected != 1)'
                    self._logger.write_warning('Host {}: {}'.format(host_name, _plugin_output))
                elif host_num_history > 0:
                    _exit_status = ExitStatusHost.OK
                    _plugin_output = 'Host UP'
                else:
                    _exit_status = ExitStatusHost.CRITICAL
                    _plugin_output = 'No recent updates on any task. Host DOWN?'
                    self._logger.write_warning('Host {}: {}'.format(host_name, _plugin_output))

                check_results.append(
                    IcingaCheckResultElement(
                        object_type=ObjectType.Host,
                        name=host_id,
                        exit_status=_exit_status,
                        plugin_output=_plugin_output,
                        check_source='icinga-sidecar',
                        execution_start=datetime.now().timestamp()
                    ))
                self._logger.write_debug('Host {}: {}'.format(host_name, _plugin_output))

            # Execute the check result calls
            self._logger.write_info('Executing icinga check-results for {} items'.format(len(check_results)))
            await asyncio.gather(*[self.update_check_result_do(element, i * self._cons_request_delay)
                                   for i, element in enumerate(check_results)])

        except Exception as e:
            self._logger.write_error("check_host_service_errors, ex:{}".format(e))

    async def check_constant_value_error(self, hosts_config):
        """
        Checks for constant sensor values.
        In case of any, it executes a check request to inform icinga.
        :param hosts_config: Host conf dict containing hostname:list(host_services, host_history) entries.
        :return:
        """

        try:
            # Used to collect requests and send async
            check_results = list()

            # Iterate over hosts and perform the update
            for host_id, host_details in hosts_config.items():

                # Extract details
                host_services = host_details['services']
                host_history = host_details['history']

                # Reset variables
                actual_val = -123456789.987654321

                # Iterate over services and perform the checks
                for service in host_services:

                    _value_type = service['data']['metadata'].get('value_type', '')
                    _node_name = service['data']['node_name']
                    _node_id = service['id']

                    try:
                        self._logger.write_debug('Performing constant-value-checks for task {}.'.
                                                 format(_node_name))
                        plugin_output = None
                        service_num_history = 0

                        # Try finding related history by comparing the node id
                        for service_histories in host_history:
                            if service_histories is not None:
                                if len(service_histories) > 0:
                                    if service['id'] == service_histories[0]['nodeId']:

                                        # list to store all the single history values
                                        values_list = list()

                                        for service_single_history in service_histories:
                                            # Indicate service has histories
                                            service_num_history += 1

                                            # Get actual value
                                            actual_val = service_single_history[_value_type]

                                            # Append val to list
                                            values_list.append(actual_val)

                                        # A set with len=1 holds constant values
                                        if len(set(values_list)) == 1:
                                            plugin_output = 'Constant-Value error, val={}'.format(actual_val)
                                            break

                        # Finally check for empty history
                        if service_num_history == 0:
                            plugin_output = 'Empty history'

                        # Process the check result to report the error
                        if plugin_output is not None:
                            check_results.append(
                                IcingaCheckResultElement(
                                    object_type=ObjectType.Service,
                                    name='{}!{}'.format(host_id, _node_id),
                                    exit_status=ExitStatusService.CRITICAL,
                                    plugin_output=plugin_output,
                                    check_source='icinga-sidecar',
                                    execution_start=datetime.now().timestamp()
                                ))
                            self._logger.write_error(
                                'Task `{}` shows empty history or constant values within the check period, '
                                'plugin-output: {}'.format(_node_name, plugin_output)
                            )

                    except Exception as e:
                        self._logger.write_error('Could not analyse task {}; ex: {}'.format(_node_name, e))

            # Execute the check result calls
            self._logger.write_info('Executing icinga constant-error check-results for {} items'.
                                    format(len(check_results)))
            await asyncio.gather(*[self.update_check_result_do(element, sleep_time=i * self._cons_request_delay)
                                   for i, element in enumerate(check_results)])

        except Exception as e:
            self._logger.write_error("check_constant_value_error, ex:{}".format(e))

    async def update_check_result_do(self, check_result_element, sleep_time):
        """
        Helper function to gain performance with async calls.
        Function can be called asyncio.gather to send requests parallelized.
        :param check_result_element: Element from type IcingaCheckResultElement.
        :param sleep_time: Time to wait between calls (asyncio.sleep).
        :return:
        """
        try:
            # Introduce a wait to not fire consecutive requests in parallel (workaround)
            await asyncio.sleep(sleep_time)

            self._logger.write_debug("Sending `update_check_result_do` with wait-time:{}".format(sleep_time))

            await self.actions.process_check_result(
                object_type=check_result_element.object_type.name,
                name=check_result_element.name,
                exit_status=check_result_element.exit_status.value,
                plugin_output=check_result_element.plugin_output,
                check_source=check_result_element.check_source,
                execution_start=check_result_element.execution_start
            )
        except Exception as e:
            self._logger.write_error("update_check_result_do, ex:{}".format(e))

    async def clear_all_objects(self, to_be_deleted=None):
        """
        Deletes certain/all the objects from icinga by performing
        host-delete with `cascade` option (deleting related services).
        :param to_be_deleted: List with hosts to be deleted. 'None'->ALL.
        :return:
        """
        try:
            if to_be_deleted is None:
                # Delete all
                _hosts = await self.objects.list(ObjectType.Host.name)
                await asyncio.gather(*[self.objects.delete(ObjectType.Host.name, host['name'], cascade=True)
                                       for host in _hosts])
            else:
                # Delete certain
                await asyncio.gather(*[self.objects.delete('Host', host, cascade=True)
                                       for host in to_be_deleted])
        except Exception as e:
            self._logger.write_error("clear_all_objects, ex:{}".format(e))

    async def objects_get_names(self, object_type, ignore_downtimed=False):
        """
        Returns all elements of Type Hosts
        :param object_type: ObjectType enum.
        :param ignore_downtimed: True->Ignore elements in down-time.
        :return: list(elements)
        """
        objects = await self.objects.list(object_type.name, attrs=['name'],
                                          filters='host.downtime_depth==0.0' if ignore_downtimed else '')

        # return list with names only
        return [name['name'] for name in objects]

    async def objects_get_display_names(self, object_type, ignore_downtimed=False):
        """
        Returns the display names of all elements of Type Hosts
        :param object_type: ObjectType enum.
        :param ignore_downtimed: True->Ignore elements in down-time.
        :return: list(elements)
        """
        objects = await self.objects.list(object_type.name, attrs=['display_name'],
                                          filters='host.downtime_depth==0.0' if ignore_downtimed else '')

        # return list with names only
        return [display_name['attrs']['display_name'] for display_name in objects]
