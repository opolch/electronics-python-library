from gim_exporter.gim import Node, GIM
import gim_exporter.gim
from epl.epl_string.epl_string_split import split_always_list
import copy
from epl.epl_katcp.epl_katcp_gim import KatcpGimInfo
from datetime import datetime
import time
import httpx
import asyncio
from redis import asyncio as aioredis

KATCP_DELIMITER = "."
KATCP_REQ_DELIMITER = "-"
IGUI_DELIMITER = "::"
VALID_STATES = [1, 2, 3, 4, 5, 6]


class GenericGimExporter:
    """
    Class to provide a generic interface to the gim_exporter module.
    """

    class RedisStreamsTask:
        """
        Class to init task objects used by SET redis streams
        """

        def __init__(self,
                     stream_key,
                     stream_data,
                     task_name,
                     desired_value):
            """
            Init the module
            :param stream_key: uid of the incoming SET request
            :param stream_data: payload of the request
            :task_name: request name for the client
            :desired_value: value to be SET
            """

            self.stream_key = stream_key
            self.stream_data = stream_data
            self.task_name = task_name
            self.desired_value = desired_value

    def __init__(self,
                 gim_url,
                 gim_user,
                 gim_pass,
                 parent_device_name,
                 gim_redis_url=None,
                 gim_redis_pass=None,
                 req_queue=asyncio.Queue(),
                 ack_queue=asyncio.Queue(),
                 policies=None,
                 valid_istates=None,
                 override_gim=True,
                 logger=None,
                 sensor_min_update_rate_s=1800):
        """
        Init the module.
        :param gim_url: URL to connect with.
        :param gim_user: The gim user.
        :param gim_pass: GIM user's password.
        :param gim_redis_url : GIM redis stream to connect to
        :param parent_device_name: The devices parent name
        :param req_queue: Sensor Request asyncio queue from GIM
        :param ack_queue: Sensor Request Ack asyncio queue to GIM
        :param parent_device_name: The device's parent device name.
        :param valid_istates: Sensor states to be supported (VALID_STATES if None)
        :param override_gim: True->Override existing tree/node configuration.
        :param logger: The epl_logging_log object to be used with logging.
        :param sensor_min_update_rate_s:
        """
        self._logger = logger
        gim_exporter.gim.log = self._logger  # Set gim logging
        self._gim = GIM(gim_url, gim_user, gim_pass)
        self._gim_redis_url = gim_redis_url
        self._gim_redis_pass = gim_redis_pass
        self._req_queue = req_queue
        self._ack_queue = ack_queue
        self._request_stream = None
        self._task_map = {}
        self._parent_device_name = parent_device_name
        self._valid_istates = VALID_STATES if valid_istates is None else valid_istates
        self._root_node = None
        self._override_gim = override_gim
        self._policies = policies
        self._max_update_period_s = sensor_min_update_rate_s
        self.heartbeats_connected = 0
        self.heartbeats_pause = False

    async def redis_connect(self):
        """
        Connect to gim redis dev instance
        """

        self._logger.write_info("Connecting to redis...")
        self._redis = await aioredis.from_url(self._gim_redis_url, password=self._gim_redis_pass)

    async def init_connect(self):
        """
        Initially connect with gim. To be called once the exporter object was created and initialized.
        """

        # GIM authentication

        # GIM authentication
        self._logger.write_info("Authenticating with gim-gui")
        await self._gim.authorize()

        # One way sync with the tree
        self._logger.write_info("Syncing gim tree for root node {}".format(self._parent_device_name))
        self._root_node = await self._gim.build_node_tree(parent=self._parent_device_name)

    async def shutdown(self):
        """
        Shutdown callback. Call on application exit.
        Will close connections and free resources.
        """
        self._logger.write_info("Shutting down exporter")
        await self._gim.shutdown()

    async def _autogen_task(self, sensor, params, config=None):
        """
        Autogeneration of a task from a sensor name.
        Currently, this only supports GET type
        tasks and does not do a corresponding request
        look up.
        :param sensor: Sensor name.
        :param params: Additional parameters defining the task.
        :param config:
        :return:
        """
        if config is None:
            opt_set = "get_set" if params["request"] else "get"
            config = {
                "options": opt_set,
                "metadata": {"mysql_task_type": opt_set.upper()}
            }

        if "units" in params and params["units"] != "discrete":
            config["metadata"]["unit"] = params["units"]
        if "description" in params:
            config["metadata"]["description"] = params["description"]
        if "upper_limit" in params:
            config["metadata"]["upper_limit"] = params["upper_limit"]
        if "lower_limit" in params:
            config["metadata"]["lower_limit"] = params["lower_limit"]
        if "value_type" in params:
            config["metadata"]["value_type"] = params["value_type"]

        self._logger.write_info(f"Autogenerating task {sensor} with config {config}")
        node = self._root_node.add_node(
            sensor, Node.TASKS,
            config, KATCP_DELIMITER)
        self._logger.write_debug(f"Added node {node}")

        # Here we can force an update to the sensor map
        self._root_node.sensor_lookup(sensor, refresh=True)

        # Sync the node to read back the id
        await node._sync_node(self._gim, update=self._override_gim)

        return node

    async def register_sensor(self, sensor, extra_params=None):
        """
        Registers a sensor with gim.
        :param sensor: Sensor name.
        :param extra_params:
        :return:
        """

        node = None

        self._logger.write_debug("Registering sensor {} with params {}".format(sensor, extra_params))

        try:
            node = self._root_node.sensor_lookup(sensor, refresh=True)
            self._logger.write_debug("Sensor already known")
        except KeyError:
            if "ignore_unknown" in self._policies:
                self._logger.write_debug("ignore_unknown policy invoked, ignoring sensor")
            elif "autogen_task" in self._policies:
                self._logger.write_debug("autogen_task policy invoked, generating sensor")
                node = await self._autogen_task(sensor, extra_params)

        return node

    async def sensor_bulk_update(self, sensor_data):
        """
        Perform a bulk update on sensors.
        :param sensor_data: {sensor_name: [timestamps],[data]}
        :return:
        """

        bulk_data = {}

        for name, data in sensor_data.items():
            try:
                node = self._root_node.sensor_lookup(name, refresh=False)

                # Only update in case of any new data
                for single_data in data:
                    # Some special task types (e.g. blob) don't use current value field
                    if single_data["current_value"] is not None:
                        if node.last_sensor_reading is None or \
                                single_data["current_value"] != node.last_sensor_reading["current_value"] or \
                                (float(single_data["timestamp"]) - float(node.last_sensor_reading["timestamp"])) > \
                                self._max_update_period_s:
                            bulk_data[node.node_id] = data
                            node.last_sensor_reading = data[-1]
                            break
                    else:
                        bulk_data[node.node_id] = data

            except KeyError:
                self._logger.write_warning(
                    "Received update for untracked sensor, adding same: {}".format(
                        name))
                # Registering new sensor
                node = await self.register_sensor(name, {
                    "value_type": data[0]["value_type"],
                    "request": ""
                })

                # Now that the sensor was registered, add data
                bulk_data[node.node_id] = data

        if len(bulk_data) > 0:
            # +++ Calculate bulk time
            start_bulk_update = datetime.now()

            # Execute the bulk request
            await self._gim.update_nodes(bulk_data)

            # --- Calculate bulk time
            dur_hist_call = datetime.now() - start_bulk_update

            self._logger.write_info("Duration for bulk update of {} entries: {}".
                                    format(len(bulk_data), dur_hist_call))
        else:
            self._logger.write_info("Empty data for the bulk, not executing request")

    async def sensor_update(self, sensor, value, timestamp, status):
        """
        Updates a sensor on gim. If the sensor does not exist, it will return False.
        :param sensor: The sensor name.
        :param value: The single value to be updated.
        :param timestamp: Timestamp in unix format.
        :param status: VALID_STATES
        :return: True->Update successful
        """
        self._logger.write_debug("Received update for sensor {} with reading {}, {}, {}".
                                 format(sensor, value, timestamp, status))
        if status not in self._valid_istates:
            self._logger.write_debug(
                "Handler ignoring reading with invalid istatus ({})".format(
                    status))
            return False
        try:
            node = self._root_node.sensor_lookup(sensor, refresh=False)
        except KeyError:
            self._logger.write_warning("Received update for untracked sensor: {}".format(sensor))
            return False
        else:
            self._logger.write_debug("Found associated node: {}".format(node))
            # Send update to IGUI
            try:
                await self._gim.update_node(
                    node.task_name(),
                    {"current_value": value,
                     "timestamp": timestamp,
                     "error_state": status
                     })
            except Exception as error:
                self._logger.write_exception("Exception while updating node: {}".format(
                    str(error)))
                return False
        return True

    def init_redis_streams(self, tasklist):
        """
        Inits the correct redis queue names to interact with GIM API
        :params tasklist: current tasklist of the device
        """
        # Iterate over task list and create the devices tree
        task_name = next(iter(tasklist))

        tree_name = task_name.split(".", 1)

        self._request_stream = "SET:" + str(tree_name[0])

        self._logger.write_info("SET Stream name {}".format(self._request_stream))

    async def update_from_tasklist(self, task_list):
        """
        Update the gim configuration from a task-list.
        :param task_list: {task_name:gim_config}
        :return:
        """

        # init redis if url is provided
        if self._gim_redis_url is not None:
           self.init_redis_streams(task_list)

        gim_config = self.gim_config_create(task_list)
        self._root_node.update_from_config(gim_config)
        await self._root_node.sync_with_gim(
            self._gim, update=self._override_gim)

    def gim_config_create(self, task_list):
        """
        Sets up the gim config dict.
        :param task_list: {task_name:gim_config} TODO:
        :return:
        """

        self._logger.write_info("Running gim config..")
        # Dict to build the tree in
        gim_config_dict = dict()

        # Default sampling strategy and policies on root level
        gim_config_dict['default_sampling_strategy'] = {'type': 'event-rate', 'args': [5, 1800]}
        gim_config_dict['policies'] = ['ignore_unknown']

        # Iterate over task list and create the devices tree
        for task_name, task in task_list.items():

            # TODO: Workaround for task lists only holding gim-config
            try:
                gim_info = task.gim_info
            except AttributeError:
                gim_info = task

            # Always start at root level
            cur_device = gim_config_dict

            # List with task names to build the tree
            task_name_list = list()

            # Add a virtual root name
            # if self._virtual_gim_roots is not None:
            #     task_name_list.append(self._virtual_gim_roots)

            # Split task name to get the different device levels
            task_name_list.extend(split_always_list(task_name, '.'))

            # Iterate over task name list to build the device tree
            for i in range(0, len(task_name_list) - 1):

                # The following 3 steps apply for all levels:
                # 1. Add devices entry
                # 2. Add the device
                # 3. Add global parameters

                # Device metadata
                device_metadata = dict()

                # Devices entry
                if 'devices' not in cur_device:
                    cur_device['devices'] = dict()

                # Check whether next device is already present (from another task)
                if task_name_list[i] not in cur_device['devices']:
                    cur_device['devices'][task_name_list[i]] = dict()

                # Store pointer to the next device
                cur_device = cur_device['devices'][task_name_list[i]]

                # +++ META
                # Set top-level device as icinga-host
                if i == 0:
                    device_metadata['icinga_host'] = '1'

                # Devices have to have GET type
                device_metadata['mysql_task_type'] = 'GET'

                # Set the device meta (need to do copy)
                cur_device['metadata'] = copy.deepcopy(device_metadata)
                # --- META

                # Global params on device level
                cur_device['sampling_strategy'] = {'type': 'event-rate', 'args': [5, 1800]}
                cur_device['policies'] = ['autogen_task']

            # Make sure level has tasks key (always needed to attach tasks)
            if 'tasks' not in cur_device:
                cur_device['tasks'] = dict()
            cur_device = cur_device['tasks']

            if gim_info is not None:

                # Some widget types need special treatment
                drop_down_options = dict()
                if gim_info.mysql_task_type is KatcpGimInfo.WidgetType.DROPDOWN:
                    if gim_info.limits is not None:
                        for limit in gim_info.limits:
                            drop_down_options[limit] = str(limit)

                # Add task
                cur_device[task_name_list[-1]] = {  # Use task name as key
                    'sensor_name': '',
                    'request_name': '',
                    'options': gim_info.options_available.name.lower() \
                        if gim_info.options_available is not None else '',
                    'metadata':
                        {
                            'display_name': gim_info.display_name,
                            'lower_limit': gim_info.limits[0]
                            if gim_info.limits is not None else '',
                            'upper_limit': gim_info.limits[-1]
                            if gim_info.limits is not None else '',
                            'value_type': gim_info.value_type,
                            'sampling_strategy':
                                {'type': gim_info.sampling_strategy.value,
                                 'args': gim_info.sampling_strategy_params} \
                                    if gim_info.sampling_strategy is not None \
                                    else {'type': 'event-rate', 'args': [5, 1800]},
                            'mysql_task_type': gim_info.mysql_task_type.name \
                                if gim_info.mysql_task_type is not None else '',
                            'description': gim_info.description \
                                if gim_info.description is not None else '',
                            'options': drop_down_options,
                            'option01': gim_info.option01 if gim_info.option01 is not None else '',
                            'option02': gim_info.option02 if gim_info.option02 is not None else '',
                            'option03': gim_info.option03 if gim_info.option03 is not None else '',
                            'option04': gim_info.option04 if gim_info.option04 is not None else '',
                            'unit': gim_info.unit if gim_info.unit is not None else '',
                            'format': gim_info.format if gim_info.format is not None else '',
                            'icinga_check_constant_errors': gim_info.icinga_check_constant_errors \
                                if gim_info.icinga_check_constant_errors is not None else '',
                        }
                }
        return gim_config_dict

    async def redis_ack_streams(self):
        """
        Sends ack back to GIM on given 
        sensor requests
        :param 
        """
        while True:
            await asyncio.sleep(0.01)
            if not self._ack_queue.empty():
                try:
                    # Get Task from Ack Queue
                    _task = await self._ack_queue.get()
                    #await asyncio.sleep(0.01)
                    _key = _task.stream_key
                    _data = _task.stream_data

                    _ack_txn_id = _data['txnId']
                    _ack_stream = "ACK:" + str(_ack_txn_id)

                    # Generate Ack Payload and Send
                    _id = str(round(time.time() * 1000)) + "-*"
                    _ack_stream_data = dict()
                    for key, value in _data.items():
                        _ack_stream_data[key.encode()] = str(value).encode()

                    _ack_resp = await self._redis.xadd(_ack_stream.encode(), _ack_stream_data, id=_id)
                    self._logger.write_info("Ack pushed with data:{}, resp:{}".format(_ack_stream_data, _ack_resp))

                    self._ack_queue.task_done()
                except Exception as e:
                    # Set request result to inform client
                    self._logger.write_exception("Unknown error:{}".format(e))
                finally:
                    await asyncio.sleep(0.0001)
        #else:
        #        await asyncio.sleep(0.00001)

    async def redis_get_streams(self):
        """
        Catches new request from gim api and puts
        them in respective request queue
        :param tasklist: Device tasklist
        """
        _ikey = 0

        while True:
            # _rdack = await self.redis_ack_streams()

            if self._request_stream is not None: # wait for tasklist to be updated with gim remote
               #await asyncio.sleep(0.01)
               self._logger.write_debug("clearing older than 10min requests...")
               _cutoff_time = str(round((time.time() * 1000) - 6000000)).encode()
               _del_resp = await self._redis.xtrim(self._request_stream.encode(), None, True, _cutoff_time, None)
               self._logger.write_debug("cleared {} requests.".format(_del_resp))

               self._logger.write_debug("waiting for set requests...")

               strm_data = await self._redis.xread(streams={self._request_stream: _ikey}, count=None, block=100)
               #await asyncio.sleep(0.1)
               if len(strm_data) > 0:
                 self._logger.write_info("got requests, {}".format(strm_data))
                 for id, data in strm_data[0][1]:
                    _ikey = id
                    tree_name = data[b'nodeName'].decode()
                    # Split to remove the first root parent u
                    # used by API
                    tree_name = tree_name.split(" | ", 1)
                    # REST API separates nodes via `::`
                    task_name = tree_name[1].replace(" | ", ".")
                    task_name = str(task_name.lower())

                    # redis stream id in unix ms
                    _time = time.time()
                    data[b'timestamp'] = str(_time).encode()
                    _id = str(round(_time * 1000)) + "-*"

                    self._logger.write_info("Trying set request on task:{}".format(task_name))

                    # Generate ACK Stream name from txnId from requeust stream
                    _txn_id = data[b'txnId'].decode()
                    _ack_stream = "ACK:" + _txn_id

                    _redis_task = None

                    try:

                        _desired_value = data[b'desired_value'].decode()

                        _redis_data = dict()

                        # Put request in queue

                        # Create Ack Response
                        data[b'pending_request'] = str(GIM.TASK_RUNNING).encode()
                        data[b'last_error_message'] = b'Set Requested Accepted;'

                        for key, value in data.items():
                            _redis_data[key.decode()] = value.decode()

                        _redis_task = GenericGimExporter.RedisStreamsTask(stream_key=id.decode(),
                                                                          stream_data=_redis_data,
                                                                          task_name=task_name,
                                                                          desired_value=_desired_value)
                        
                        if task_name is not None:
                            self._req_queue.put_nowait(_redis_task)
                            self._logger.write_debug("task pushed!")

                        # Push Ack
                        _ack_resp = await self._redis.xadd(_ack_stream.encode(), data, id=_id)
                        #await asyncio.sleep(0.01)
                        self._logger.write_info(
                            "Ack pushed on stream:{} with data:{}, resp:{}".format(_ack_stream, data, _ack_resp))
                        # Delete Original Request from Stream
                        _rdel_resp = await self._redis.xdel(self._request_stream.encode(), _ikey)
                        #await asyncio.sleep(0.01)
                        self._logger.write_info(
                              "response on request stream:{} delete:{},{}".format(self._request_stream.encode(), _ikey, _rdel_resp))
 
                    except KeyError:
                        self._logger.write_exception("Requested task not found: {}".format(task_name))
                        # Create Error Ack Response
                        data[b'pending_request'] = str(GIM.TASK_FAILED).encode()
                        data[b'last_error_message'] = b'Requested task not found'
                        # Delete given request
                        await self._redis.xdel(self._request_stream.encode(), _ikey)

                        # Push Ack
                        await self._redis.xadd(_ack_stream.encode(), data, id=_id)

                    except Exception as e:
                        # Set request result to inform client
                        self._logger.write_exception("Unknown error:{}".format(e))
                        # Create Error Ack Response
                        data[b'pending_request'] = str(GIM.TASK_FAILED).encode()
                        # data["error_state"]     = 3
                        data[b'last_error_message'] = str(e).encode()
                        # Delete given request
                        await self._redis.xdel(self._request_stream.encode(), _ikey)

                        # Push Ack
                        await self._redis.xadd(_ack_stream.encode(), data, id=_id)
                    finally:
                        await asyncio.sleep(0.001)
            else:
                await asyncio.sleep(1)

    async def heartbeat(self, initial_wait_time=30, heartbeats_connected=1):
        """
        Callback to implement the heartbeat functionality.
        Should be executed as loop: loop.create_task(exporter.heartbeat())
        :param initial_wait_time: Initial time to wait before starting the loop.
        :param heartbeats_connected: Start task with heartbeats connected to True.
        """

        def task_selector(_task):
            if _task.parent is None:
                return False
            else:
                return all([
                    _task.node_name == "connected",
                    _task.parent.node_name == "sidecar"
                ])

        # Set push flag
        self.heartbeats_connected = heartbeats_connected

        # Dict for heartbeat updates
        updates = {}

        # Wait before starting the heartbeats, need to sync with gim first
        await asyncio.sleep(initial_wait_time)
        
        while not self.heartbeats_pause:
            
            timestamp = "{:.6f}".format(time.time())
            for task in self._root_node.list_tasks(filter_func=task_selector):
                self._logger.write_info("Heartbeat on task {}".format(task))

                if task is not None:
                    if task.node_id is not None:
                        updates[task.node_id] = [
                            {
                                "current_value": self.heartbeats_connected,
                                "timestamp": timestamp,
                                "error_state": "1",
                                "value_type": "int"
                            }
                        ]
                    else:
                        self._logger.write_warning("node-id is none for node, {}".format(task))
                else:
                    self._logger.write_warning("None-node found with heartbeat-tasks")

        
            while self.heartbeats_pause:
                await asyncio.sleep(1)
            else:
               try:
                  await self._gim.update_nodes(updates)
               except httpx.ConnectError as error:
                  self._logger.write_error("No connection to gim, heartbeat failed")
               except Exception as error:
                  self._logger.write_error("Error on heartbeat update: {}".format(str(error)))
               finally:
                  await asyncio.sleep(30)
        else:
            await asyncio.sleep(1)
