import copy
from enum import Enum
from uuid6 import uuid8
from epl.epl_lists import epl_lists_search
from epl.epl_dicts.epl_dicts_objects import object_to_dict, object_init_from_dict
from epl.epl_ctypes.epl_ctypes_helper import di
from varname import nameof


class NodeConstants(Enum):
    """
    Various constants to be used with the REST.
    """
    TREE_HIERARCHY_SEPARATOR = "::"


class SensorStatus(Enum):
    """
    Enum with status values for sensors.
    """
    UNKNOWN = 0
    NOMINAL = 1
    WARN = 2
    ERROR = 3
    FAILURE = 4
    UNREACHABLE = 5
    INACTIVE = 6


class ValueTypes(Enum):
    """
    Enum defining gmcs value types.
    """
    none = None
    int = 'int'
    float = 'float'
    str = 'str'


"""
LUT for conversion of value types.
"""
_VALUE_TYPE_LUT = {
    None: ValueTypes.none.value,
    int: ValueTypes.int.value,
    float: ValueTypes.float.value,
    str: ValueTypes.str.value,
    list: ValueTypes.str.value,
    bool: ValueTypes.int.value,
    'discrete': ValueTypes.str.value,
    'None': ValueTypes.none.value,
    'int': ValueTypes.int.value,
    'integer': ValueTypes.int.value,
    'float': ValueTypes.float.value,
    'str': ValueTypes.str.value,
    'real': ValueTypes.float.value,
    'bool': ValueTypes.int.value,
    'DevVoid': ValueTypes.str.value,
    'DevBoolean': ValueTypes.str.value,
    'DevShort': ValueTypes.int.value,
    'DevLong': ValueTypes.int.value,
    'DevFloat': ValueTypes.float.value,
    'DevDouble': ValueTypes.float.value,
    'DevUShort': ValueTypes.int.value,
    'DevULong': ValueTypes.int.value,
    'DevString': ValueTypes.str.value,
    'DevVarCharArray': ValueTypes.str.value,
    'DevVarShortArray': ValueTypes.str.value,
    'DevVarLongArray': ValueTypes.str.value,
    'DevVarFloatArray': ValueTypes.str.value,
    'DevVarDoubleArray': ValueTypes.str.value,
    'DevVarUShortArray': ValueTypes.str.value,
    'DevVarULongArray': ValueTypes.str.value,
    'DevVarStringArray': ValueTypes.str.value,
    'DevVarLongStringArray': ValueTypes.str.value,
    'DevVarDoubleStringArray': ValueTypes.str.value,
    'DevState': ValueTypes.str.value,
    'ConstDevString': ValueTypes.str.value,
    'DevVarBooleanArray': ValueTypes.str.value,
    'DevUChar': ValueTypes.int.value,
    'DevLong64': ValueTypes.int.value,
    'DevULong64': ValueTypes.int.value,
    'DevVarLong64Array': ValueTypes.str.value,
    'DevVarULong64Array': ValueTypes.str.value,
    'DevInt': ValueTypes.int.value,
    'DevEncoded': ValueTypes.str.value,
    'DevEnum': ValueTypes.str.value,
    'DevPipeBlob': ValueTypes.str.value
}


class OptionsAvailable(Enum):
    """
    Enum to set available task options.
    """
    NONE = 0
    GET = 1
    SET = 2
    GET_SET = 3


class NodeType(Enum):
    """
    Class defining node types.
    """
    not_defined = "not_defined"
    root_device = "root_device"
    device = "device"
    task = "task"


class TaskType(Enum):
    """
    Class defining common task types.
    """
    connected = "connected"
    randint = "randint"
    undefined = "undefined"


class GmcsLimits:
    """
    Class to define gmcs limits
    """

    def __init__(self, lower_warn=None, upper_warn=None, lower_err=None, upper_err=None):
        """
        Init
        :param lower_warn: Lower warning limit.
        :param upper_warn: Upper warning limit.
        :param lower_err: Lower error limit.
        :param upper_err: Upper error limit.
        """
        self.lower_warn = lower_warn
        self.upper_warn = upper_warn
        self.lower_err = lower_err
        self.upper_err = upper_err

    def init_from_dict(self, init_dict: dict):
        """
        Option to init the object from a dict.
        :param init_dict: dict holding `class_member:value` pairs.
        :return:
        """
        object_init_from_dict(self, init_dict)

    def as_dict(self) -> dict:
        """
        Creates a dict with all class members by recursively iterating.
        It expects objects to implement `as_dict` function.
        :return: dict
        """
        return object_to_dict(self)


class Redundancy:
    """
    Class to define redundancy configuration.
    """

    class RedundancyType(Enum):
        """
        Redundancy types.
        """
        NONE = "none"
        MASTER = "master"
        SLAVE = "slave"

    def __init__(self, redundancy_type=RedundancyType.SLAVE, redundant_node_id: str = None,
                 redundancy_std_dev: float = None, redundancy_count: int = None):
        """
        Init
        :param redundancy_type: The redundancy type as per RedundancyType enum.
        :param redundant_node_id: The node id of the redundant node.
        :param redundancy_std_dev: Standard deviation for redundancy error checks.
        :param redundancy_count: Count of expected redundant partners.
        """
        self.redundancy_type = redundancy_type
        self.redundant_node_id = redundant_node_id
        self.redundancy_std_dev = redundancy_std_dev
        self.redundancy_count = redundancy_count

    def init_from_dict(self, init_dict: dict):
        """
        Option to init the object from a dict.
        :param init_dict: dict holding `class_member:value` pairs.
        :return:
        """
        object_init_from_dict(self, init_dict)

    def as_dict(self) -> dict:
        """
        Creates a dict with all class members by recursively iterating.
        It expects objects to implement `as_dict` function.
        :return: dict
        """
        return object_to_dict(self)


class GmcsDiagram:
    """
    Class to define gmcs diagram attributes (e.g for graphs).
    """

    def __init__(self, y_axis_min=None, y_axis_max=None):
        """
        Init
        :param y_axis_min: Min for the y-axis.
        :param y_axis_max: Max for the y-axis.
        """
        self.y_axis_min = y_axis_min
        self.y_axis_max = y_axis_max

    def init_from_dict(self, init_dict: dict):
        """
        Option to init the object from a dict.
        :param init_dict: dict holding `class_member:value` pairs.
        :return:
        """
        object_init_from_dict(self, init_dict)

    def as_dict(self) -> dict:
        """
        Creates a dict with all class members by recursively iterating.
        It expects objects to implement `as_dict` function.
        :return: dict
        """
        return object_to_dict(self)


class Metadata:

    def __init__(self,
                 value_type: str = "",
                 description="",
                 options_available=OptionsAvailable.NONE,
                 display_name="",
                 limits: GmcsLimits = None,
                 diagram: GmcsDiagram = None,
                 unit: str = None,
                 format_display: str = None,
                 task_type: str = None,
                 redundancy: Redundancy = None,
                 init_dict=None,
                 **kwargs
                 ):
        """
        Init the object.
        :param value_type: Type of value (for sensor as well as request)
        :param description: Nodes description text.
        :param display_name: Name to be used when displaying node.
        :param options_available: See OptionsAvailable
        :param limits: GmcsLimits (depending on value_type).
        :param diagram: GmcsDiagram defining settings on displaying.
        :param unit: Value's unit.
        :param format_display: Display format for value on gmcs-gui.
        :param task_type: Type of task as per TaskType.
        :param redundancy: Redundancy configuration as per Redundancy.
        :param init_dict: Init object from dict.
        :param kwargs: Additional attributes.
                       Only unknown are supported (those which are not to be set via function parameters)
        """

        self.limits = GmcsLimits() if not limits else limits
        self.diagram = GmcsDiagram() if not diagram else diagram
        self.redundancy = Redundancy(redundancy_type=Redundancy.RedundancyType.NONE) if not redundancy else redundancy
        self.description = description
        self.display_name = display_name
        self.options_available = options_available
        self.unit = unit
        self.format = format_display
        self.task_type = task_type

        # Get value type from LUT
        self.value_type = _VALUE_TYPE_LUT.get(value_type, ValueTypes.str.value)

        # Add additional attributes from kwargs
        # We only support attributes which are no known members
        for key, value in kwargs.items():
            if not hasattr(self, key):
                setattr(self, key, value)

        # Init object from dict
        if init_dict:
            self.init_from_dict(init_dict)

    def init_from_dict(self, init_dict: dict):
        """
        Option to init the object from a dict.
        :param init_dict: dict holding `class_member:value` pairs.
        :return:
        """
        object_init_from_dict(self, init_dict, add_unknown_attributes=True)

    def as_dict(self) -> dict:
        """
        Creates a dict with all class members by recursively iterating.
        It expects objects to implement `as_dict` function.
        :return: dict
        """
        return object_to_dict(self)


class NodeInfo:
    """
    Base class to define a Node.
    """

    #
    # Exception
    #
    class ExceptTreeNotConsistent(Exception):
        """
        Exception on a tree failing the consistency check.
        """
        pass

    def __init__(self,
                 node_type: NodeType = NodeType.not_defined,
                 node_name: str = "",
                 metadata: Metadata = None,
                 devices=None,
                 tasks=None,
                 node_id="",
                 tree_id="",
                 tree_dict: [[str], object] = None,
                 init_dict=None,
                 clear_node_ids=False):
        """
        Inits the object.
        :param node_type: Type of node as per NodeType enum. If node has sub nodes, they get auto typed.
        :param node_name: The node's name.
        :param node_id: The device's uuid.
        :param metadata: Node's metadata as per Metadata class.
        :param tasks: List of NodeInfo task objects.
        :param devices: List of NodeInfo device objects.
        :param tree_id: The node's tree id.
        :param tree_dict: Tree dict for parent node lookup.
        :param init_dict: Init object from dict.
        :param clear_node_ids: True->Clear any existing node ids.
        """

        # Devices can have sub-devices and tasks
        self.devices: list[NodeInfo] = devices if devices else []
        self.tasks: list[NodeInfo] = tasks if tasks else []
        self.node_name = node_name
        self.node_id = node_id
        self.node_type = node_type
        self.tree_id = tree_id

        # Metadata
        self.metadata = metadata if metadata else Metadata()

        # Following variables used for internal tree-building and lookup.
        # They are not populated to the outside world
        self.tree_lookup_dict = tree_dict if tree_dict else {}
        self.tree_lookup_node_id = uuid8().hex
        self.tree_lookup_parent_node_id = ""

        # Init object from dict?
        if init_dict:
            self.init_from_dict(init_dict=init_dict, root_node_type=node_type)

        # Clear all existing node ids
        if clear_node_ids:
            self.node_ids_clear(recursively=True)

        # Sync the node tree
        if node_type != NodeType.not_defined:
            self._sync_node_tree(node_type=node_type)

    @property
    def display_name(self):
        """
        Returns the display name from metadata.
        :return:
        """
        return self.metadata.display_name

    @display_name.setter
    def display_name(self, set_name):
        """
        Sets the display name in metadata.
        :param set_name:
        :return:
        """
        self.metadata.display_name = set_name

    @property
    def tree_name(self) -> str:
        """
        Returns the entire tree name as string.
        :return:
        """

        # Get parent
        parent_node = self.parent_node_get()

        if parent_node:
            # Add parent name
            return NodeConstants.TREE_HIERARCHY_SEPARATOR.value.join([parent_node.tree_name, self.node_name])
        else:
            # Only return my name
            return self.node_name

    @property
    def node_ids_list(self) ->[str]:
        """
        Return all the node ids (root + children, devices + tasks) in a list.
        :return: []
        """

        ids = [self.node_id]

        # Add node ids
        for node in self.nodes_get(recursively=True):
            ids.append(node.node_id)

        return ids

    def parent_node_get(self):
        """
        Reads out the parent node from the tree dict.
        :return: NodeInfo of parent.
        """

        if not self.tree_lookup_parent_node_id:
            return None
        try:
            return di(self.tree_lookup_dict[self.tree_lookup_parent_node_id])
        except KeyError:
            return None

    def _nodes_filter(self, filter_criteria) -> list:
        """
        Filters nodes based on a filter criteria and returns them as a flat relational list.
        _nodes_filter(filter_criteria=lambda x: x.node_id == 4711)
        :param filter_criteria: Filter criteria in form `lambda x: x.y == z`
        :return: []
        """

        return epl_lists_search.list_items_filter(
            search_list=self.flat_relational_list_get(),
            filter_criteria=filter_criteria
        )

    def nodes_get(self, recursively=False) -> list:
        """
        Returns a list containing all the sub nodes (devices and tasks).
        :param recursively: True->Also all the sub-sub... nodes.
        :return:
        """

        return_nodes = []

        # Add my nodes
        nodes = self.devices if self.devices else [] + self.tasks if self.tasks else []
        return_nodes += nodes

        # Also return sub nodes?
        if recursively:
            for node in nodes:
                return_nodes += node.nodes_get(recursively=True)

        return return_nodes

    def init_from_dict(self, init_dict: dict, root_node_type: NodeType):
        """
        Option to init the object from a dict.
        :param init_dict: dict holding `class_member:value` pairs.
        :param root_node_type: Node Type of the root node of this tree. Node types for sub nodes will be auto set.
        :return:
        """

        # The approach of setting an attribute is:
        # - First check whether it implements the `init_from_dict` method.
        # - If not, an exception is thrown and the attribute will be set commonly.

        # Set values from dict
        for key, value in init_dict.items():
            try:
                # Ignore
                if not value:
                    continue

                # Get corresponding attribute
                att = getattr(self, key)

                try:
                    if isinstance(att, Enum):
                        # Enum type

                        setattr(self, key, type(att)[value])

                    elif isinstance(att, list):
                        # List type
                        #
                        # We currently only support NodeInfo type here
                        #

                        if value:
                            # Create a new list and write it to the attribute
                            setattr(self, key, [NodeInfo(tree_dict=self, init_dict=x) for x in value])

                    elif isinstance(att, dict):
                        # dict type
                        #
                        # We currently only support NodeInfo type here
                        #

                        if value:
                            # Create a new dict and write it to the attribute
                            setattr(self, key, {x: NodeInfo(tree_dict=self, init_dict=y) for x, y in value.items()})
                    else:
                        # Otherwise cast and set value
                        setattr(self, key, type(att)(init_dict=value))

                except Exception:
                    # Otherwise cast (only if att can be used for casting) and set value
                    setattr(self, key, type(att)(value) if att else value)

            except AttributeError:
                # Ignore unknown attributes
                pass

        # Generate our internal node tree id
        self.tree_lookup_node_id = uuid8().hex

        # Finally, sync the node tree
        # This is to make sure the node tree is consistent
        if root_node_type != NodeType.not_defined:
            self._sync_node_tree(node_type=root_node_type)

    def _sync_node_tree(self, node_type: NodeType):
        """
        Syncs the node tree recursively:
          - Sets node types
          - Populates the tree dict
          - Sets parent node ids
        This function must be called when either a value of a parent node changes, which must be reflected in sub
        nodes or the tree structure was altered.
        :param node_type: Node type of the root node.
        :return:
        """

        # Set node types recursively
        self.node_type_set(node_type=node_type, recursively=True)

        # Populate tree dict. It is used for internal lookups exclusively and will not be populated on Redis
        # Only the root node defines the actual tree dict; sub nodes have ctypes pointer addresses to this dict
        # Therefor the dict instance check is necessary here
        if isinstance(self.tree_lookup_dict, dict):

            # Add all my sub nodes
            self.tree_lookup_dict = {x.tree_lookup_node_id: id(x) for x in self.nodes_get(recursively=True)}

            # Add my own entry
            self.tree_lookup_dict[self.tree_lookup_node_id] = id(self)

            # Populate tree_dict and tree id, this can be done recursively since all nodes get the same values
            for node in self.nodes_get(recursively=True):
                # The `link` to the tree dict will be set as memory address via ctypes
                # This is to prevent infinite recursion
                node.tree_lookup_dict = self.tree_lookup_dict
                node.tree_id = self.node_id

            # Populate parent ids, root node obviously has no parent
            self.tree_lookup_parent_node_id_set(parent_node_id="")

    def tree_lookup_parent_node_id_set(self, parent_node_id: str):
        """
        Populates parent node ids recursively walking down the tree.
        :param parent_node_id: The node id of the parent.
        :return:
        """

        # Set my parent node id
        self.tree_lookup_parent_node_id = parent_node_id

        # Set parents for sub nodes
        for node in self.nodes_get(recursively=False):
            node.tree_lookup_parent_node_id_set(parent_node_id=self.tree_lookup_node_id)

    def node_type_set(self, node_type: NodeType, recursively=False):
        """
        Sets the node's node type and has the option to auto set the node type of sub nodes based on the tree structure.
        :param node_type: Node type of the root node.
        :param recursively: True->Auto set the node type of sub nodes based on the tree structure.
        :return:
        """

        # Set my own node type
        self.node_type = node_type

        if recursively:
            # Loop over devices and tasks
            if self.devices:
                for device in self.devices:
                    device.node_type_set(node_type=NodeType.device, recursively=True)

            if self.tasks:
                for task in self.tasks:
                    task.node_type_set(node_type=NodeType.task, recursively=True)

    def as_dict(self) -> dict:
        """
        Creates a dict with all class members by recursively iterating.
        It expects objects to implement `as_dict` function.
        :return: dict
        """

        return object_to_dict(self, ignore_list=[nameof(self.tree_lookup_dict), nameof(self.tree_lookup_node_id),
                                                 nameof(self.tree_lookup_parent_node_id)])

    def node_ids_clear(self, recursively=True):
        """
        Clear all ids of the node (and sub nodes when activated).
        :param recursively: Descend into tree.
        :return:
        """

        # Clear ids
        self.node_id = self.tree_id = ""

        # Also clear redundancy node-id for MASTER type redundancy
        if self.metadata.redundancy:
            if self.metadata.redundancy.RedundancyType == Redundancy.RedundancyType.MASTER:
                self.metadata.redundancy.redundant_node_id = ""

        if recursively:
            # Clear ids for all devices recursively
            if self.devices:
                for device in self.devices:
                    device.node_ids_clear(recursively=recursively)

            # Clear ids for all tasks recursively
            if self.tasks:
                for task in self.tasks:
                    task.node_ids_clear(recursively=recursively)

    @property
    def node_no_children(self):
        """
        Returns a node copy containing no child nodes (devices and tasks)
        :return: NodeInfo
        """
        # Create a copy
        copy_node = copy.copy(self)

        # Remove child nodes
        copy_node.devices = copy_node.tasks = []

        return copy_node

    def nodes_redundancy_get(self) -> list:
        """
        Returns a list with nodes providing redundancy metadata.
        :return:
        """
        return self._nodes_filter(
            filter_criteria=lambda x: x.metadata.redundancy and
                x.metadata.redundancy.redundancy_type != Redundancy.RedundancyType.NONE
        )

    def node_ids_fresh_insert(self, overwrite_existing=False, recursively=True, root_node_id=""):
        """
        Insert node ids for node itself and also recursively.
        :param overwrite_existing: False->Only set if node_id is not set.
        :param recursively: Descend into tree.
        :param root_node_id: The node id of the root node to be added as tree id with all children.
        :return:
        """

        # Set own node id
        if overwrite_existing or not self.node_id:
            # Skip existing when not to be overwritten
            self.node_id = uuid8().hex

            # Set redundancy node-id in case of MASTER type redundancy
            if self.metadata.redundancy:
                if self.metadata.redundancy.redundancy_type == Redundancy.RedundancyType.MASTER:
                    self.metadata.redundancy.redundant_node_id = self.node_id

        # Set tree id
        if overwrite_existing or not self.tree_id:
            if self.node_type == NodeType.root_device:
                self.tree_id = self.node_id
            else:
                self.tree_id = root_node_id

        if recursively:
            # Insert ids for all devices recursively
            if self.devices:
                for device in self.devices:
                    device.node_ids_fresh_insert(overwrite_existing=overwrite_existing, recursively=recursively,
                                                 root_node_id=self.tree_id)

            # Insert ids for all tasks recursively
            if self.tasks:
                for task in self.tasks:
                    task.node_ids_fresh_insert(overwrite_existing=overwrite_existing, recursively=recursively,
                                               root_node_id=self.tree_id)

        # We have to sync the node tree after updating node ids
        self._sync_node_tree(node_type=self.node_type)

    def node_ids_copy(self, copy_node_info):
        """
        Copies IDs from another NodeInfo. The match is performed by either node-id or node-name.
        NOTE: It clears existing node ids if they were not found on the copy node.
        :param copy_node_info: NodeInfo object to copy IDs from.
        :return:
        """

        # Map the copy tree for coding convenience
        copy_node_info: NodeInfo = copy_node_info

        # Set my own ID
        self.node_id = copy_node_info.node_id
        self.tree_id = copy_node_info.tree_id

        # Copy redundancy node id
        if self.metadata.redundancy and copy_node_info.metadata.redundancy:
            if self.metadata.redundancy.redundancy_type == Redundancy.RedundancyType.MASTER:
                self.metadata.redundancy.redundant_node_id = copy_node_info.metadata.redundancy.redundant_node_id

        # Loop over devices and set their IDs
        if self.devices:
            # Ignore None objects
            for device in self.devices:
                # Check whether a device with node_id or node_name can be found on the copy node
                device_node = epl_lists_search.first_element_return(
                    x for x in copy_node_info.devices
                    if x.node_id == device.node_id or x.node_name == device.node_name
                )
                # Node found?
                if device_node:
                    # Yes, then copy node_ids recursively
                    device.node_ids_copy(device_node)
                else:
                    # No, then make sure to delete also node ids recursively
                    device.node_ids_clear(recursively=True)

        # Loop over tasks and set their IDs
        if self.tasks:
            # Ignore None objects
            for task in self.tasks:
                # Check whether a task with node_id or node_name can be found on the copy node
                task_node = epl_lists_search.first_element_return(
                    x for x in copy_node_info.tasks
                    if x.node_id == task.node_id or x.node_name == task.node_name)
                # Node found?
                if task_node:
                    # Yes, then copy node_ids recursively
                    task.node_ids_copy(task_node)
                else:
                    # No, then make sure to delete also node ids recursively
                    task.node_ids_clear(recursively=True)

        # We have to sync the node tree after updating node ids
        self._sync_node_tree(node_type=self.node_type)

    def node_lookup_by_id(self, node_id: str) -> object:
        """
        Performs a tree lookup of a node by node id.
        :param node_id: The id to look for.
        :return: NodeInfo->found; None->not found.
        """

        # Am I the one looked up?
        if self.node_id == node_id:
            return self

        # Look up in child nodes
        return epl_lists_search.first_element_return(
            [x for x in self.nodes_get() if x.node_lookup_by_id(node_id=node_id)])

    def node_lookup_by_tree_name(self, tree_name: str) -> object:
        """
        Performs a tree lookup of a node by node name.
        :param tree_name: String with hierarchical node names separated by the hierarchy separator.
        :return: NodeInfo->found; None->not found.
        """

        # Am I the one looked up?
        if self.tree_name == tree_name:
            return self

        for node in self.nodes_get():
            return node.node_lookup_by_tree_name(tree_name=tree_name)

    def flat_relational_tree_dict_get(self, flat_dict: dict = None) -> dict:
        """
        Creates a flat dict with relational node objects as dict from a nested NodeInfo object.
        Relations are realized via list of node_ids for tasks and device members.
        :return:
        """
        if not flat_dict:
            flat_dict = {}

        # Create copy node to add tasks/devices from their ids.
        copy_node = copy.deepcopy(self)
        if self.devices:
            copy_node.devices = [x.node_id for x in self.devices if x is not None]
        if self.tasks:
            copy_node.tasks = [x.node_id for x in self.tasks if x is not None]

        # Add copy node to dict
        flat_dict[self.node_id] = copy_node.as_dict()

        # Loop through all sub-tasks and -devices
        if self.nodes_get():
            for node in self.nodes_get():
                flat_dict = node.flat_relational_tree_dict_get(flat_dict=flat_dict)

        return flat_dict

    def flat_relational_tree_get(self, flat_dict: dict = None) -> dict:
        """
        Creates a flat dict with relational node objects from a nested NodeInfo object.
        Relations are realized via lists of node_ids for tasks and devices members.
        :return:
        """
        if not flat_dict:
            flat_dict = {}

        # Create copy node to add tasks/devices from their ids.
        copy_node = copy.deepcopy(self)
        if self.devices:
            copy_node.devices = [x.node_id for x in self.devices]
        if self.tasks:
            copy_node.tasks = [x.node_id for x in self.tasks if self.tasks is not None]

        # Add copy node to dict
        flat_dict[self.node_id] = copy_node

        # Loop through all sub-tasks and -devices
        if self.nodes_get():
            for node in self.nodes_get():
                flat_dict = node.flat_relational_tree_get(flat_dict=flat_dict)

        return flat_dict

    def flat_relational_list_get(self, flat_list: list = None) -> list:
        """
        Creates a list with relational node objects from a nested NodeInfo object.
        Relations are realized via lists of node_ids for tasks and devices members.
        :return:
        """
        if not flat_list:
            flat_list = []

        # Create copy node to add tasks/devices from their ids
        copy_node = copy.deepcopy(self)
        if self.devices:
            copy_node.devices = [x.node_id for x in self.devices]
        if self.tasks:
            copy_node.tasks = [x.node_id for x in self.tasks if self.tasks is not None]

        # Add copy node to dict
        flat_list.append(copy_node)

        # Loop through all subtasks and -devices
        if self.nodes_get():
            for node in self.nodes_get():
                flat_list = node.flat_relational_list_get(flat_list=flat_list)

        return flat_list

    def init_from_flat_relational_tree(self, flat_tree: list):
        """
        Initializes the object from a list containing flat relational node info objects.
        :param flat_tree: List containing all the node info objects in a flat tree structure.
        :return:
        """

        def search_node_in_flat_tree(node_id: str):
            """
            Searches a node in a list of nodes by its node_id.
            :param node_id: The node id to search for.
            :return:
            """
            return epl_lists_search.first_element_return(
                [x for x in flat_tree if x.node_id == node_id])

        # Loop over tasks/devices and look up corresponding NodeInfo objects from flat tree
        # Devices
        if self.devices:
            self.devices = [search_node_in_flat_tree(str(dev)) for dev in self.devices]
            for device in self.devices:
                device.init_from_flat_relational_tree(flat_tree=flat_tree)

        # Tasks
        if self.tasks:
            self.tasks = [search_node_in_flat_tree(str(task)) for task in self.tasks]
            for task in self.tasks:
                task.init_from_flat_relational_tree(flat_tree=flat_tree)

        # We have to sync the node tree after altering the same
        self._sync_node_tree(node_type=self.node_type)

    def node_alter(self, alter_node: object, check_tree_consistency=False) -> bool:
        """
        Alters the content of a node.
        :param alter_node: The NodeInfo object containing the info the node should be altered to.
        :param check_tree_consistency: True->Check tree consistency at the end.
        :return: True->Successful and if consistency should be checked means consistent.
        """

        # Casting
        alter_node: NodeInfo = alter_node

        # Am I the one to be altered
        if alter_node.node_id == self.node_id:
            self.init_from_dict(init_dict=alter_node.as_dict(), root_node_type=alter_node.node_type)
        else:
            # Loop through sub nodes
            for node in self.nodes_get():
                node.node_alter(alter_node=alter_node)

        # We have to sync the node tree after altering the same
        self._sync_node_tree(node_type=self.node_type)

        if check_tree_consistency:
            # Check for tree consistency
            return self.tree_consistency_check()

    def tree_consistency_check(self, you_are_root=False) -> bool:
        """
        Checks a tree for consistency:
        - no double node names.
        - root node with proper node type.
        :param you_are_root: True->Check whether only the root device has node_type=root_device
            This is useful when checking an entire node tree object (root node with sub nodes).
            On a sub/child node this check makes no sense and will fail.
        :return: True->Consistent; False->Inconsistency detected.
        """

        # Check whether we have a proper node type
        if you_are_root:
            if self.node_type != NodeType.root_device:
                raise self.ExceptTreeNotConsistent(
                    f"Root node does not have proper node type `{NodeType.root_device.value}`",
                    self.as_dict()
                )

        # Loop over all nodes (tasks and devices) and perform consistency checks
        for node in self.nodes_get():
            # Perform checks
            if len(epl_lists_search.items_as_list(
                    [x for x in self.nodes_get() if (
                            (x.node_name == node.node_name) or  # Double node names
                            (x.node_id == node.node_id)  # Double node ids
                    )])) > 1:
                raise self.ExceptTreeNotConsistent("Tree contains double node names/node ids", self.as_dict())

            # Check for node_type on tasks
            for task in node.tasks:
                if task.node_type != NodeType.task:
                    raise self.ExceptTreeNotConsistent(
                        f"Task node does not have proper node type `{NodeType.task.value}`",
                        task.as_dict()
                    )

            # Descend to sub devices
            for device in node.devices:
                # Check for node_type on devices
                if device.node_type != NodeType.device:
                    raise self.ExceptTreeNotConsistent(
                        f"Device node does not have proper node type `{NodeType.device.value}`",
                        device.as_dict()
                    )
                # Check sub node's consistency
                device.tree_consistency_check()

        return True


class SetRequest:
    """
    Class to define a set request.
    """

    #
    # Exceptions
    #
    class ExceptSetNotInitialized(Exception):
        """
        Exception on SET request not properly initialized via init dict.
        """
        pass

    def __init__(self, node_id: str = None, set_value: object = None, init_dict: dict = None):
        """
        Inits the object.
        :param node_id: The gmcs node_id.
        :param set_value: The value to be set.
        :param init_dict: A dict to initialize the object from (overwrites others).
        """

        self.node_id = node_id
        self.set_value = set_value

        if init_dict:
            self.init_from_dict(init_dict)

    def init_from_dict(self, init_dict: dict):
        """
        Inits the instance's variables from a dict.
        :param init_dict:
        :return:
        """

        for key, value in init_dict.items():
            try:
                # Get corresponding attribute, throws an exception in case the attribute is not available
                getattr(self, key)

                # Set value
                setattr(self, key, value)

            except AttributeError:
                # Ignore unknown attributes
                pass

        # Raise exception when not properly initialized
        if not self.check_initialized():
            raise self.ExceptSetNotInitialized()

    def as_dict(self) -> dict:
        """
        Creates a dict with all class members by recursively iterating.
        It expects objects to implement `as_dict` function.
        :return: dict
        """

        return object_to_dict(self)

    def check_initialized(self) -> bool:
        """
        Checks whether all the instance's variables are initialized.
        :return: True->All initialized (not None).
        """
        for x, y in vars(self).items():
            if not y:
                return False
        return True
