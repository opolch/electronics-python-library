import pyvisa
from enum import Enum
from epl.epl_logging.epl_logging_log import Log
from pyvisa import VisaIOError, InvalidSession

class Ls218:

    class ExceptQuery(BaseException):
        """
        Exception during query execution.
        """
        pass

    class MessageSpecialChars(Enum):
        """
        Class defining special message characters like delimiters, ...
        """
        QueryChar = "?"

    class MessageCommands(Enum):
        """
        Class defining commands supported by the LS218 device.
        """
        KelvinReading="KRDG"

    def __init__(self, ip_address:str, port:int, logger:Log):
        """
        Inits the object.
        :param ip_address: Device IP.
        :param port: Device port.
        """
        # Variables
        self._ip_address = ip_address
        self._port = port
        self._logger = logger

        # Init the VISA connection
        self._resource_manager = pyvisa.ResourceManager()
        self._instrument = self._resource_manager.open_resource(f"TCPIP0::{self._ip_address}::{self._port}::SOCKET")
        self._reopen_and_init_connection()

    def _reopen_and_init_connection(self):
        """
        Function to be called to open (also re-open) the connection to the remote device and make initial settings.
        :return:
        """
        self._instrument.close()
        self._instrument.open()
        self._instrument.write_termination = "\r\n"
        self._instrument.read_termination = "\r\n"

    def create_query_message(self, command:MessageCommands):
        """
        Creates a standard query message for a given command.
        :param command: MessageCommands
        :return: str
        """
        return f"{command.value}{self.MessageSpecialChars.QueryChar.value}"

    def _execute_query(self, query_message):
        """
        Executing a query and handling upcoming errors.
        :param query_message: The query message to be sent.
        :return:
        """
        try:
            return self._instrument.query(query_message)
        except (VisaIOError, InvalidSession, BrokenPipeError) as ex:
            self._logger.write_exception(f"Visa error `{type(ex).__name__}`, trying to reopen resource now; ex:{ex}")

            # Open connection again
            self._reopen_and_init_connection()
            self._logger.write_info(f"Re-opening of resource successful.")

            raise Ls218.ExceptQuery(ex.args)

    def query_kelvin_temps(self):
        """
        Queries Kelvin temperatures and returns them as list.
        :return: list of floats
        """
        temp_val = self._execute_query(self.create_query_message(self.MessageCommands.KelvinReading))
        return [float(x) for x in temp_val.split(",")]
