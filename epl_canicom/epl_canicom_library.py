#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Apr 29 12:44:52 2019

@author: hardikar
"""
import can
import threading
import time
from concurrent.futures import ThreadPoolExecutor

from epl.epl_logging.epl_logging_log import Log
from epl.epl_canicom import epl_canicom_header, epl_canicom_parm_tasks
from epl.epl_canicom import epl_canicom_data_extraction as dm
from epl.epl_canicom import epl_canicom_can_hardware

import psutil
import logging
import os

global updateThreadFlag
bootloader_event = threading.Event()

config_data_ready = threading.Event()


class Canicom():

    def __init__(self, can_interface_name, my_device, extended_can_flag, task_list, mirror_timer_timeout_value,
                 board_mapping_dict, canicom_sender_id, mirror_received, error_frame_check,
                 pending_mirror_buffer_length_check, generic_set_function_response, logger):
        """
        The constructor for the Canicom class.

        :param can_interface_name: Name of the hardware CAN interface of the device.
        :param my_device: An object of CanHardwareSetup specifying the hardware-specific details of the CAN-bus.
        :param extended_can_flag: A flag stating if the CAN protocol is extended or not.
        :param task_list: The list of tasks to process.
        :param mirror_timer_timeout_value: The time interval in seconds at which the pendingMirrorsBuffer should
            be checked.
        :param board_mapping_dict: Mapping dictionary of the board.
        :param canicom_sender_id: Id of the tx device.
        :param mirror_received: Signal object which notifies when the mirror is received.
        :param error_frame_check: Flag indicating whether the received frames should be checked for the error frame.
        :param pending_mirror_buffer_length_check: Flag indicating whether the pending mirror buffer length should be
            checked before sending out the new message.
        :param generic_set_function_response: Flag indicating whether the mirror function of the set request is generic.
        :param logger: logger module.
        :return:
        """

        super(Canicom, self).__init__()

        self.can_interface_name = can_interface_name
        self.my_device = my_device
        self.extended_can_flag = extended_can_flag
        self.task_list = task_list
        self.mirror_timer_timeout_value = mirror_timer_timeout_value
        self.board_mapping_dict = board_mapping_dict
        self.canicom_sender_id = canicom_sender_id
        self.mirror_received = mirror_received
        self.error_frame_check = error_frame_check
        self.pending_mirror_buffer_length_check = pending_mirror_buffer_length_check
        self.generic_set_function_response = generic_set_function_response

        if logger is None:
            self._log_writer = Log()
        else:
            self._log_writer = logger
        # Calculating the group Id from the device Id
        self.group_id = int((bin(int(hex(self.my_device), 16))[2:].zfill(12))
                            [0:5], 2)

        # Filter for normal (unicast) incoming messages
        filter1 = my_device << 5
        mask1 = 0xfff << 5

        # Filter for incoming multicast messages
        filter2 = self.group_id << 12
        mask2 = 0xfff << 5

        self.can_hw = epl_canicom_can_hardware.CanHardwareSetup(can_interface_name,
                                                                [[filter1, mask1], [filter2, mask2]])


        self.board_dict = {}
        self.bus = self.can_hw.bus
        self.mirror_timer = threading.Timer(mirror_timer_timeout_value, self.mirror_timeout_thread)

        self.rx_pending_buffer = []
        self.pending_mirror_buffer = []
        self.state_machine_flag = True
        self.board_update_flag = True
        self.channel_update_flag = True

        self.listener = can.BufferedReader()
        try:
            self.listener.buffer.maxsize = 1000
        except:
            pass
        self.notifier = can.Notifier(self.bus, [self.listener], 5)

        if self.extended_can_flag is True:
            self.num_of_bits_for_binary = 29
        else:
            self.num_of_bits_for_binary = 11

        self.service_mode_flag = False
        self.threadpool = ThreadPoolExecutor(5)
        self.bootloader_status_flaglist = []
        self.bootloader_mode_flaglist = []
        self.bootloader_abort_flaglist = []
        self.messages_completed = []
        self.total_messages = []

        for i in range(10):

            self.bootloader_status_flaglist.append('Not_started')
            self.bootloader_mode_flaglist.append(False)
            self.bootloader_abort_flaglist.append(False)
            self.messages_completed.append(0)
            self.total_messages.append(0)

    def process_message(cls, message):
        """
        This function processes the received message.

        This function converts the received message on the can bus into CANICOM specific format and separates various
        fields of the arbitration ID and header.

        :param message: Received message over CAN bus.

        :return:
        """

        "Convert int type arbitration id to hex"

        if cls.error_frame_check == True:
            if message.is_error_frame is True:
                cls.handle_error_frame()
            else:
                cls.extract_canicom_command(message)

        else:
            cls.extract_canicom_command(message)

    def extract_canicom_command(cls, message):
        """
        Extracts the canicom command from the received canicom message.

        :return:
        """
        msg_id = hex(message.arbitration_id)
        'Convert hex to binary value of 29 bits (append zeros on LHS to cpmplete 29 bits)'
        msg_id = dm.hex_to_binary(msg_id, cls.num_of_bits_for_binary)

        grp_id = int(msg_id[0:5], 2)
        msg_id = str(msg_id)

        "Split the header packet in its constituent fields"
        header = message.data
        message_header = epl_canicom_header.MessageHeader(int((hex(header[0])), 16),
                                                          '0x{0:0{1}X}'.format(header[2], 2),
                                                          hex(header[3]),
                                                          bytes(header[4:8]).hex()
                                                          )

        header = bytes(header).hex()

        "Split the canId in its constituent fields"
        can_id = epl_canicom_header.CanId(hex(int(msg_id[0:12], 2)),
                                          hex(int(msg_id[12:24], 2)),
                                          hex(int(msg_id[24:28], 2)),
                                          msg_id[28],
                                          )
        can_id.group_id = grp_id
        if can_id.pack_type == '1':
            data_in_packet = message.data
        else:
            data_in_packet = bytearray()

        command = epl_canicom_header.Command(can_id, message_header, 0, 0, data_in_packet)
        cls.rx_command_handle(command)

    def handle_error_frame(cls):
        """
        Clears all buffers and restarts the CAN communication by resending the introduce command.

        :return:
        """

        cls._log_writer.write_info("Error frame detected, Restarting...")
        cls.stop_update()
        cls.rx_pending_buffer.clear()
        epl_canicom_parm_tasks.tx_buffer.clear()
        cls.pending_mirror_buffer.clear()

        time.sleep(1)

        while cls.listener.buffer.empty() is False:
            cls.listener.buffer.get()
        cls._log_writer.write_info("listener buffer now empty.")

        for task in cls.task_list:
            if task.command_code.value == epl_canicom_header.TasksRequestCode.INTRODUCE.value:
                task.send_func()
                break

    def rx_command_handle(cls, cmd):
        """
        This function distinguishes between header-only packet, header with data packet and data packet.

        :param cmd: CANICOM command.

        :return:
        """
        if cmd.can_id.pack_type == '0':
            if cmd.header_val.count_packages == 0:
                #  It's a header-only message, directly analyze
                cls.rx_command_analyze(cmd)
            else:
                #  It's an incomplete header, put it in pending rx buf
                for rx_pending_command in cls.rx_pending_buffer:
                    if (rx_pending_command.can_id.sender_id == cmd.can_id.sender_id):
                        cls.rx_pending_buffer.remove(rx_pending_command)
                cls.rx_pending_buffer.append(cmd)

        elif cmd.can_id.pack_type == '1':
            #  It's a data packet, iterate through the list of pending rx commands list"
            cls.rx_pending_buffer_check(cmd)

        else:
            pass

    def rx_pending_buffer_check(cls, cmd):
        """
        This function completes the command before passing for further
        analysis.

        This function runs the command through the rxPending buffer and searches for the previously received incomplete
        part of the new command and completes it by copying the data of the new command to the data field of the old
        command. This completed command is further passed for analysis.

        :param cmd: canicom command.

        :return:
        """
        for i in range(len(cls.rx_pending_buffer)):
            if (cmd.can_id.sender_id == cls.rx_pending_buffer[i].can_id.sender_id and
                    cmd.can_id.recipient_id == cls.rx_pending_buffer[i].can_id.recipient_id):

                cls.rx_pending_buffer[i].data += cmd.data
                cls.rx_pending_buffer[i].already_sent_received_counter += 1

                if (cls.rx_pending_buffer[i].already_sent_received_counter >=
                        cls.rx_pending_buffer[i].header_val.count_packages):

                    #  Command completed. Now analyzing.
                    cls.rx_command_analyze(cls.rx_pending_buffer[i])
                    cls.rx_pending_buffer.remove(cls.rx_pending_buffer[i])
                    break
            else:
                pass

    def rx_command_analyze(cls, command):
        """
        This function is for the analysis of the received command.

        This function analyzes the command based on its request type and passes it to the parmTaskAnalyze function.
        In case of a mirror command, it checks for any error before passing it to the parmTaskAnalyze().

        :param command: CANICOM command.

        :return:
        """

        if command.can_id.req_type == epl_canicom_header.ReqType.REQTYPE_MIRROR_SYNC.value:

            cls.mirrors_pending_check(command)
            if command.header_val.error_code == epl_canicom_header.MessageError.NOERROR.value:
                cls.parm_task_analyze(command)
            else:
                cls.handle_error(command)

        elif command.can_id.req_type == epl_canicom_header.ReqType.REQTYPE_ERROR.value:
            cls._log_writer.write_debug("Error frame")

        elif command.can_id.req_type == epl_canicom_header.ReqType.REQTYPE_SET.value:
            cls.parm_task_analyze()

        elif command.can_id.req_type == epl_canicom_header.ReqType.REQTYPE_GET.value:
            cls.parm_task_analyze()

        elif command.can_id.req_type == epl_canicom_header.ReqType.REQTYPE_MIRROR_ASYNC.value:
            cls.parm_task_analyze(command)

        cls.command = command

    def handle_error(cls, command):
        """
        This function checks for the error type, in case of any error.

        :param command: CANICOM command.

        :return:
        """

        if command.header_val.error_code == epl_canicom_header.MessageError.CMDNOTKNOWN.value:
            cls._log_writer.write_debug("Error: Unknown command {}".format(command.header_val.command))

        elif command.header_val.error_code == epl_canicom_header.MessageError.OPTNOTAVAIL.value:
            cls._log_writer.write_debug("Error: Option not available")

        elif command.header_val.error_code == epl_canicom_header.MessageError.VALUE_TOO_HIGH.value:
            cls._log_writer.write_debug("Error: Value too high on rx side")
            cls.parm_task_analyze(command)

        elif command.header_val.error_code == epl_canicom_header.MessageError.VALUE_TOO_LOW.value:
            cls._log_writer.write_debug("Error: Value too low")

        elif command.header_val.error_code == epl_canicom_header.MessageError.MIRROR_TIMEOUT.value:
            cls._log_writer.write_debug("Error: Mirror timeout")

        elif command.header_val.error_code == epl_canicom_header.MessageError.RECEIVE_TIMEOUT.value:
            cls._log_writer.write_debug("Error: Receive timeout")

        elif command.header_val.error_code == epl_canicom_header.MessageError.PTYPE_UNKNOWN.value:
            cls._log_writer.write_debug("Error: P type unknown")

        else:
            cls._log_writer.write_debug("Error: Unknown error code {} {}".format(command.header_val.command,
                                                                                 command.can_id.sender_id))

    def mirrors_pending_check(cls, cmd):
        """
        This function Keeps track of the commands that are expecting mirror messages. When mirrors are received,
        it deletes the corresponding command from the buffer. If no mirrors are received till the timeout occurs, it
        raises an error of timeout.

        :param cmd: canicom command.

        :return:
        """

        counter = 0
        index = 0
        for pending_mirror in cls.pending_mirror_buffer:
            if pending_mirror is not None:
                if cmd is not None:
                    if hex(pending_mirror.can_id.recipient_id) == hex(cls.my_device):

                        #  Pending mirror of broadcast command
                        cls.pending_mirror_buffer.remove(pending_mirror)
                        break
                    elif cmd.can_id.sender_id == hex(pending_mirror.can_id.recipient_id):
                        if cmd.header_val.command == hex(pending_mirror.header_val.command):
                            if (cmd.header_val.command_spec_value == '{0:0{1}X}'.format(pending_mirror.header_val.
                                                                                        command_spec_value, 8)):
                                if cmd.can_id.pack_type == '0' and pending_mirror.data is not None:
                                    cmd.data_set += pending_mirror.data
                                cls.pending_mirror_buffer.remove(pending_mirror)
                                break
                            else:
                                counter += 1
                    else:
                        pass
                else:
                    # process command
                    index += 1
                    pending_mirror.pending_counter += 1
                    if pending_mirror.pending_counter >= epl_canicom_header.CANICOM_COMMAND_MIRROR_MAX_PENDING:
                        # timeout occurred, deleting mirror
                        cls.pending_mirror_buffer.remove(pending_mirror)

    def rx_pending_check(cls):
        """
        This function runs through the list of incomplete received commands and deletes them after a particular interval
        of time.

        :return:
        """

        for rx_pending_cmd in cls.rx_pending_buffer:
            rx_pending_cmd.pending_counter += 1
            if rx_pending_cmd.pending_counter >= epl_canicom_header.CANICOM_COMMAND_RX_MAX_PENDING:
                # timeout occured, deleting rx command
                cls.rx_pending_buffer.remove(rx_pending_cmd)

    def parm_task_analyze(cls, cmd):
        """
        This function carries out the function requested by the command.

        It Runs through the list of tasks to find the appropriate task representing the command and performs an
        appropriate function requested by the command.

        :param cmd: canicom command.

        :return:
        """

        if cmd.can_id.req_type == epl_canicom_header.ReqType.REQTYPE_MIRROR_SYNC.value:
            # RPi in master mode, loop through task list
            if cmd.header_val.command == hex(epl_canicom_header.TasksRequestCode.BOOTLOADER.value):
                cls.bootloader_mirror(cmd)

            else:
                for i in range(len(cls.task_list)):

                    if hex(cls.task_list[i].command_code.value) == cmd.header_val.command:
                        if cmd.header_val.command == hex(epl_canicom_header.TasksRequestCode.INTRODUCE.value):
                            if cls.task_list[i].mirror_func is not None:
                                cls.task_list[i].mirror_func(cmd)
                            else:
                                cls.device_config_mirror(cmd, i)
                        else:
                            if len(cmd.data) == 0:  # It's the mirror of the SET function.
                                if cls.generic_set_function_response == True:
                                    cls.generic_set_mirror(cmd, i)
                                else:
                                    cls.task_list[i].mirror_func(cmd)
                            else:
                                cls.task_list[i].generic_get_mirror(cmd, i)  # It's the mirror of the GET function.

        else:  # RPi in slave mode.
            for i in range(len(cls.slave_task_list)):
                if hex(cls.slave_task_list.command_code.value) == cmd.header_val.command:
                    if cmd.can_id.req_type == epl_canicom_header.ReqType.REQTYPE_SET.value:
                        if cls.slave_task_list[i].options_available == epl_canicom_header.TasksOptions.SET:
                            cls.slave_task_list[i].set_func()
                        else:
                            cls._log_writer.write_debug("No set options")

                    elif cmd.can_id.req_type == epl_canicom_header.ReqType.REQTYPE_GET.value:
                        if cls.slave_task_list[i].options_available == epl_canicom_header.TasksOptions.GET:
                            cls.slave_task_list[i].get_func()
                        else:
                            cls._log_writer.write_debug("No get options")

    def send_get_message(cls, messages_to_be_transmitted):
        """
        Sends the update commands from the transmit buffer over the CAN bus.

        :param messages_to_be_transmitted: Number of messages to be tried sending at a time.
        :return:
        """

        message_count = 0
        while epl_canicom_parm_tasks.tx_buffer:
            if cls.pending_mirror_buffer_length_check == True:
                if len(cls.pending_mirror_buffer) == 0:
                    cls.send_message(epl_canicom_parm_tasks.tx_buffer)
                    message_count += 1
                    if message_count == messages_to_be_transmitted:
                        break
                else:
                    break
            else:
                cls.send_message(epl_canicom_parm_tasks.tx_buffer)
                message_count += 1
                if message_count == messages_to_be_transmitted:
                    break

    def send_message(cls, transmit_buffer):
        """
        This function creates a message in a format compatible with the python can library.

        :param transmit_buffer: Buffer to send a message from.

        :return:
        """

        "Split the command into hexadecimal arbitration id and bytearray data"
        cmd = transmit_buffer.popleft()
        tx = cmd.can_id.sender_id
        rx = cmd.can_id.recipient_id
        req = cmd.can_id.req_type
        pckt = cmd.can_id.pack_type
        arbitration_id = tx << 17 | rx << 5 | req << 1 | pckt

        # Create an integer bytearray to put the values in the format that's compatible with the python-can
        # library. It will be sent over can.
        data1 = bytearray(8)

        # Using 'format' function to keep the leading zeros while conversion.
        cnt_pckgs = '0x{0:0{1}X}'.format(cmd.header_val.count_packages, 4)

        data1[0] = int(cnt_pckgs[2:4], 16)
        data1[1] = int(cnt_pckgs[4:6], 16)

        err = '0x{0:0{1}X}'.format(cmd.header_val.error_code, 2)
        data1[2] = int(err[2:4], 16)

        cm = '0x{0:0{1}X}'.format(cmd.header_val.command, 2)
        data1[3] = int(cm[2:4], 16)

        # Convert it in string so as to be able to split the fields
        cmd_spec = '{0:0{1}X}'.format(cmd.header_val.command_spec_value, 8)
        data1[4] = int(cmd_spec[0:2], 16)
        data1[5] = int(cmd_spec[2:4], 16)
        data1[6] = int(cmd_spec[4:6], 16)
        data1[7] = int(cmd_spec[6:8], 16)

        data = data1
        msg = can.Message(arbitration_id=arbitration_id,
                          data=data,
                          extended_id=True)

        if cmd.can_id.pack_type == 0:
            cls.pending_mirror_buffer.append(cmd)
            cmd.already_sent_received_counter += 1
            cls.bus.send(msg)

        # sending data
        if cmd.data is not None:
            pckt = 0x1
            arbitration_id = tx << 17 | rx << 5 | req << 1 | pckt

            for i in range(len(cmd.data)):
                msg = can.Message(arbitration_id=arbitration_id,
                                  data=cmd.data[i],
                                  extended_id=True)
                cls.bus.send(msg)

    def send_set_message(cls, messages_to_be_transmitted):
        """
        Send the commands from the transmit buffer of set commands to set the values of the parameters.

        :param messages_to_be_transmitted: Number of commands to be sent at a time.
        :return:
        """
        "Split the command into hexadecimal arbitration id and bytearray data"
        msg_count = 0
        while epl_canicom_parm_tasks.tx_set_buffer:

            cmd = epl_canicom_parm_tasks.tx_set_buffer.popleft()
            tx = cmd.can_id.sender_id
            rx = cmd.can_id.recipient_id
            req = cmd.can_id.req_type
            pckt = cmd.can_id.pack_type
            arbitration_id = tx << 17 | rx << 5 | req << 1 | pckt

            # Create an integer bytearray to put the values in the format that is compatible
            # with the python-can library. This will be sent over the can.
            data1 = bytearray(8)

            # Using 'format' function to keep the leading zeros while conversion.
            cnt_pckgs = '0x{0:0{1}X}'.format(cmd.header_val.count_packages, 4)
            data1[0] = int(cnt_pckgs[2:4], 16)
            data1[1] = int(cnt_pckgs[4:6], 16)

            err = '0x{0:0{1}X}'.format(cmd.header_val.error_code, 2)
            data1[2] = int(err[2:4], 16)

            cm = '0x{0:0{1}X}'.format(cmd.header_val.command, 2)
            data1[3] = int(cm[2:4], 16)

            # Convert it in string so as to be able to split the fields
            cmdspec = '{0:0{1}X}'.format(cmd.header_val.command_spec_value, 8)
            data1[4] = int(cmdspec[0:2], 16)
            data1[5] = int(cmdspec[2:4], 16)
            data1[6] = int(cmdspec[4:6], 16)
            data1[7] = int(cmdspec[6:8], 16)


            data = data1
            msg = can.Message(arbitration_id=arbitration_id,
                              data=data,
                              extended_id=True)

            if cmd.can_id.pack_type == 0:
                cls.pending_mirror_buffer.append(cmd)
                cmd.already_sent_received_counter += 1
                cls.bus.send(msg)

            # sending data
            if cmd.data != None:
                pckt = 0x1
                arbitration_id = tx << 17 | rx << 5 | req << 1 | pckt

                for i in range(len(cmd.data)):
                    msg = can.Message(arbitration_id=arbitration_id,
                                      data=cmd.data[i],
                                      extended_id=True)
                    cls.bus.send(msg)
            msg_count += 1
            if msg_count == messages_to_be_transmitted:
                break

    def state_machine(cls):
        """
        This function takes care of the sending and receiving of messages. This function runs continuously, and switches
        between sending messages, and processing the received messages sequentially.

        :return:
        """
        while True:
            if cls.state_machine_flag is True:

                while(cls.listener.buffer.qsize() is not epl_canicom_header.CANICOM_COMMANDS_MAX_COUNT):
                    if cls.listener.buffer.empty() is True:

                        if epl_canicom_header.mirror_timeout_flag != epl_canicom_header.mirror_timeout_flag_old:
                            cls.mirrors_pending_check(None)
                            cls.rx_pending_check()
                        epl_canicom_header.mirror_timeout_flag_old = epl_canicom_header.mirror_timeout_flag
                        cls.send_get_message(10)

                    else:
                        while cls.listener.buffer.empty() is False:
                            read_msg = cls.listener.buffer.get()
                            cls.process_message(read_msg)
                    time.sleep(0.0001)

                while cls.listener.buffer.empty() is False:
                    cls.listener.buffer.get()

    def mirror_timeout_thread(cls):
        """
        This function performs the timeout functionality of mirror timer.

        :return:
        """
        cls.mirror_timer.cancel()
        epl_canicom_header.mirror_timeout_flag = not(epl_canicom_header.mirror_timeout_flag)
        if epl_canicom_header.timer_enable_flag is True:
            threading.Timer(cls.mirror_timer_timeout_value, cls.mirror_timeout_thread).start()

        else:
            threading.Timer(cls.mirror_timer_timeout_value, cls.mirror_timeout_thread).cancel()

    def start_communication(cls):
        """
        This function starts the CAN communication. It puts the state machine function in a thread and starts it
        when called from another module.

        :return:
        """
        can_thread = threading.Thread(target=cls.state_machine)
        can_thread.start()

        "Start the mirror timer."
        cls.mirror_timer.start()

    def broadcast_update(cls, broadcast_device_address, interval):
        """
        This function sends the broadcast command to update the parameters of the canicom task.

        :param broadcast_device_address: Broadcast address of the network.
        :param interval: Interval between the successive updates.
        :return:
        """

        while True:
            if cls.board_update_flag is True:
                if len(epl_canicom_parm_tasks.tx_buffer) == 0:
                    for i in range(len(cls.task_list)):
                        if (cls.task_list[i].options_available == epl_canicom_header.TasksOptions.GET or
                                cls.task_list[i].options_available == epl_canicom_header.TasksOptions.GET_SET):
                            for board in cls.boards_list:
                                get_command = cls.task_list[i].send_func(0, board, 0, i)
                                epl_canicom_parm_tasks.add_in_send_buffer(get_command)
                                time.sleep(interval)

    def start_update(cls, broadcast_device_address, interval):
        """
        Starts the update of the canicom task values.

        :param broadcast_device_address: Broadcast device address specific to the network.
        :param interval: Interval between the successive updates.
        :return:
        """

        cls.broadcast_address = broadcast_device_address
        cls.update_interval = interval
        board_spec_update_thread = threading.Thread(target=cls.broadcast_update,
                                                    args=(broadcast_device_address, interval))
        board_spec_update_thread.start()

    def stop_update(cls):
        """
        Stops the update of the canicom task values.

        :return:
        """
        cls.board_update_flag = False
        cls.channel_update_flag = False

    def stop_can_communication(cls):
        """
        Stops the CAN communication by stopping the state machine.

        :return:
        """
        cls.state_machine_flag = False
        cls.mirror_timer.cancel()

    def stop_canicom(cls):
        """
        Stops the canicom communication. This, in the end, stops the update thread and the canicom state machine.

        :return:
        """
        cls.stop_update()
        cls.stop_can_communication()
        try:
            p = psutil.Process(os.getpid())
            for handler in p.get_open_files() + p.connections():
                os.close(handler.fd)
        except Exception as e:
            logging.error(e)

    def device_config_mirror(cls, *args):
        """
        Mirror function for the device config command.

        :param args:
        :return:
        """
        cmd = args[0]
        cls._log_writer.write_debug('received config data {}'.format(cmd.can_id.sender_id))
        device_id = cmd.can_id.sender_id

        if device_id in cls.board_mapping_dict.values():
            cls._log_writer.write_info("Device ID in the list is: {}".format(device_id, cls.listener.buffer.qsize()))
            for key, value in cls.board_mapping_dict.items():
                if value == device_id:
                    board_key = key
                    break
        else:
            cls._log_writer.write_debug("Device ID not in the list: {}".format(device_id))

        board_index = list(cls.board_mapping_dict.keys()).index(board_key)

        cls.board_dict[board_index+1] = device_id
        time.sleep(1)

        if cls.listener.buffer.empty() is True:
            global config_data_ready
            config_data_ready.set()

            "Pass the dictionary storing actual present boards to the tasks. This dictionary is created in the mirror" \
                "function of the device config."
            for i in range(len(cls.task_list)):
                cls.task_list[i].boards = cls.board_dict

            "Extract the values of the dictionay, which represent board addresses present in the network."
            cls.boards_list = list(cls.board_dict.values())
            cls.board_update_flag = True
            cls.channel_update_flag = True
            cls._log_writer.write_debug("From config mirror {}".format(cls.boards_list))
        else:
            cls._log_writer.write_debug("listener buffer not empty".format(cls.listener.buffer.qsize()))

    def range_check_mirror(cls, cmd):
        """
        Function to check if the received value in the mirrored response message falls within the range.

        :param cmd: Received command
        :return:
        """
        if cmd.header_val.error_code == epl_canicom_header.MessageError.NOERROR.value:
            cls.mirror_received.param = 'Operation succesful'
        else:
            if cmd.header_val.error_code == epl_canicom_header.MessageError.VALUE_TOO_HIGH.value:
                cls.mirror_received.param = 'Value too high on the receiver.'

            elif cmd.header_val.error_code == epl_canicom_header.MessageError.VALUE_TOO_LOW.value:
                cls.mirror_received.param = 'Value too low on the receiver.'

            else:
                cls.mirror_received.param = 'Unknown error code.'

    def generic_set_mirror(cls, *args):
        """
        Generic mirror function for the set command.

        :param args:
        :return:
        """
        cmd = args[0]
        task_no = args[1]

        if len(cmd.data) == 0:  # It's a mirror of the set request.
            cls.range_check_mirror(cmd)
            cls.mirror_received.set()
            # if cls.generic_set_operation == False:
            #     if cls.task_list[task_no].mirror_func is not None:
            #         cls.task_list[task_no].mirror_func(cmd)
        else:
            for i in range(len(cls.task_list)):
                if (hex(cls.task_list[i].command_code.value) == cmd.header_val.command) and (i != task_no):
                    task_no = i
                    break
            cls.generic_get_mirror(cmd, task_no)

    def bootloader_send(cls, board_no, device_id, csv, number_of_packets, data):
        """
        Executes the function to send the bootloader-specific commands to the device.

        :param board_no: Number of the board, to which the bootloader command is to be sent.
        :param device_id: Device Id of the board.
        :param csv: Command specific value of the bootloader task.
        :param number_of_packets: Number of packets being sent with the command
        :param data: Data being sent with the command.
        :return:
        """
        if cls.bootloader_abort_flaglist[board_no] is not True:

            can_id = epl_canicom_header.CanId(cls.canicom_sender_id, device_id, 0x1, 0x0)
            can_header = epl_canicom_header.MessageHeader(number_of_packets, 0x00, 0x03, csv)
            bootloader_command = epl_canicom_header.Command(can_id, can_header, 0, 0, data)
            epl_canicom_parm_tasks.add_in_send_buffer(bootloader_command)

        else:
            cls._log_writer.write_debug("Bootloading aborted".format(cls.bootloader_abort_flaglist))

    def bootloader_send_data(cls, board_no, device_id, csv, file_name):

        cls.bootloader_status_flaglist[board_no] = 'Processing data'
        data = dm.file_conversion(cls.curr_dir, file_name)
        i = 0
        cls.total_messages[board_no] = len(data)
        cls.bootloader_status_flaglist[board_no] = 'Sending data'
        while i < len(data):
            if cls.bootloader_abort_flaglist[board_no] is False:
                number_of_packets = len(data[i]) << 8

                command_data = data[i]

                cls.bootloader_send(board_no, device_id, 0x00000000, number_of_packets, command_data)
                bootloader_event.wait(1)
                if bootloader_event.is_set() is True:
                    bootloader_event.clear()
                    i += 1
                    cls.messages_completed[board_no] += 1
                else:
                    pass
            else:
                cls._log_writer.write_debug("Bootloading process aborted in data mode {}".
                                            format(cls.bootloader_abort_flaglist))
                break

    def bootloader_mirror(cls, *args):
        """
        Mirror of the bootloader process.

        :param args:
        :return:
        """
        cmd = args[0]
        if cmd.header_val.error_code == epl_canicom_header.MessageError.NOERROR.value:
            bootloader_event.set()
            cls.mirror_received.param = 'Operation succesful'
            cls.mirror_received.set()

        else:
            cls._log_writer.write_debug("Error in data {}".format(cmd.header_val.command_spec_value))

    def bootloader_activate(cls, board_no, *args):
        """
        Executes the activate request of the bootloader.

        :param board_no: Number of the board, for which the bootloader process is currently active.
        :param args: args passed with the command.
        :return: Status of the bootloader startup process (ok/aborted)
        """

        cls.bootloader_status_flaglist[board_no] = 'Not started yet'
        cls.messages_completed[board_no] = 0
        cls.total_messages[board_no] = 0

        device_id = int(args[1], 16)
        file_name = args[3][1]

        cls._log_writer.write_debug("Sending the start bootloader command")
        cls.bootloader_send(board_no, device_id, 0x01000000, 0, None)
        cls.bootloader_status_flaglist[board_no] = 'Starting'
        bootloader_event.wait()
        cls._log_writer.write_debug("Clearing the bootloader signal")
        bootloader_event.clear()

        time.sleep(2)  # Wait for the device to be rebooted.

        cls._log_writer.write_debug("Sending the erase command")
        cls.bootloader_send(board_no, device_id, 0x02000000, 0, None)
        cls.bootloader_status_flaglist[board_no] = 'Erasing'
        bootloader_event.wait()
        bootloader_event.clear()

        cls.bootloader_status_flaglist[board_no] = 'Sending data'
        cls.bootloader_send_data(board_no, device_id, 0x00000000, file_name)
        bootloader_event.clear()

        if cls.messages_completed[board_no] == cls.total_messages[board_no]:

            cls._log_writer.write_debug("Jumping to application")
            cls.bootloader_status_flaglist[board_no] = 'Finished'
            cls.bootloader_send(board_no, device_id, 0x03000000, 0, None)
            bootloader_event.wait()
            bootloader_event.clear()

            time.sleep(2)
            cls._log_writer.write_debug("Starting application")

            return "OK"

        else:
            return 'Process aborted.'

    def bootloader_send_status(cls, board_no):
        """
        "This function returns the status of the bootloader process completed in percentage.

        :param board_no: Number of the board, for which the bootloader process is currently active.
        :return: Status of the bootloader process completion in percentage.
        """
        cls._log_writer.write_debug("Reading status")
        if cls.total_messages[board_no] == 0:
            status = '0%'
        else:
            status = [str(cls.messages_completed[board_no]),
                      str(cls.total_messages[board_no]),
                      str(round(cls.messages_completed[board_no] / cls.total_messages[board_no] * 100)) + '%']

        return status

    def bootloader(cls, *args):
        """
        Takes care of the bootloader request task.

        :param args:
        :return:
        """
        device_id_hex = args[1]
        for board in cls.board_dict:
            if cls.board_dict[board] == device_id_hex:
                board_no = (board-1)

        if cls.service_mode_flag is True:

            if args[3] is not None:
                req = args[3][0].decode('utf-8')

                if req == 'activate':
                    cls.stop_update()
                    time.sleep(1)
                    if len(args[3]) == 2:
                        file_name = args[3][1].decode('utf-8')
                        cls.curr_dir = os.getcwd()+'/Bootloader'
                        if file_name in os.listdir(cls.curr_dir):
                            if cls.bootloader_mode_flaglist[board_no] is False:

                                cls.bootloader_abort_flaglist[board_no] = False
                                cls.bootloader_mode_flaglist[board_no] = True
                                cls.threadpool.submit(cls.bootloader_activate, board_no, *args)
                                cls.bootloader_status_flaglist[board_no] = 'Activated'
                                return 'OK'
                            else:
                                return 'Already running bootloader function.'
                        else:
                            return 'Error', 'File does not exist.'
                    else:
                        return 'Please provide the name of the bootloader file.'

                elif req == 'status':
                    status = cls.bootloader_send_status(board_no)
                    if cls.bootloader_status_flaglist[board_no] == 'Sending data':
                        return cls.bootloader_status_flaglist[board_no], status
                    else:
                        return cls.bootloader_status_flaglist[board_no]

                elif req == 'end':
                    # Reeset all the settings and flags
                    cls.bootloader_status_flaglist[board_no] = 'Not_started'
                    cls.messages_completed[board_no] = 0
                    cls.total_messages[board_no] = 0
                    cls.bootloader_mode_flaglist[board_no] = False
                    cls.board_update_flag = True

                    return 'OK', 'Exited from the bootloader mode.'

                elif req == 'abort':
                    cls.board_update_flag = True
                    cls.bootloader_status_flaglist[board_no] = 'Aborted'
                    cls.bootloader_abort_flaglist[board_no] = True
                    cls.bootloader_mode_flaglist[board_no] = False
                    return 'OK', 'Aborted the bootloading process.'

                else:
                    return 'Invalid bootloader action. Please select between: activate/status/end/abort.'
            else:
                return 'Please provide bootloader action.'

        elif cls.service_mode_flag is False:
            return 'Please first enter into the service mode.'
