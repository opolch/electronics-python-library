from epl.epl_gmcs.epl_gmcs_config import GmcsInfo, GmcsInfoHandler
import json

info_dict = {
    "t1": GmcsInfo(options_available=GmcsInfo.OptionsAvailable.GET_SET, limits=GmcsInfo.GmcsLimits(lower_warn=10.0)),
    "devl1.devl2.devl31.t1": GmcsInfo(),
    # "devl1.devl2.devl32.t1": NodeInfo()
}

gmcs_handler = GmcsInfoHandler(info_dict, device_root="dev_root")

dump = json.dumps(gmcs_handler.gmcs_config_dict)
print(dump)
load = json.loads(dump)
print(load)
load["tasks"]["t1"]["metadata"]["limits"]["lower_err"] = 99.0


def flatten_dict(source_dict: dict, result_dict: dict = None):
    if not result_dict:
        result_dict = {}
    for x, y in source_dict.items():
        if not isinstance(y, dict):
            result_dict[x] = y
        else:
            flatten_dict(y, result_dict)
    return result_dict


test = flatten_dict(load["tasks"]["t1"])
new_info = GmcsInfo(init_dict=flatten_dict(load["tasks"]["t1"]))

i = 0
