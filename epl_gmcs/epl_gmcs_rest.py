from enum import Enum


class RequestMethod(Enum):
    """
    HTTP request types supported by GMCS.
    """
    GET = "GET"
    PUT = "PUT"
    POST = "POST"
    DELETE = "DELETE"


class _EndpointTemplate:
    """
    Template class for defining endpoints.
    """

    @classmethod
    def __init__(cls, path: str):
        cls.path = path


class Endpoints:
    """
    Class defining Endpoints fpr GMCS.
    """
    class _EpDevices(_EndpointTemplate):
        """
        Endpoint /devices
        """
        get = RequestMethod.GET.value
        post = RequestMethod.POST.value

        @classmethod
        def __init__(cls):
            super().__init__(path="/devices")

    class _EpDevicesLookup(_EndpointTemplate):
        """
        Endpoint /devices/<lookup>
        """
        get = RequestMethod.GET.value
        put = RequestMethod.PUT.value

        @classmethod
        def __init__(cls):
            super().__init__(path="/devices/<lookup>")

    class _EpDevicesLookupSet(_EndpointTemplate):
        """
        Endpoint /devices/<lookup>/set
        """
        put = RequestMethod.PUT.value

        @classmethod
        def __init__(cls):
            super().__init__(path="/devices/<lookup>/set")

    class _EpStatusDevicesConnected(_EndpointTemplate):
        """
        Endpoint /status/devices/connected
        """
        get = RequestMethod.GET.value

        @classmethod
        def __init__(cls):
            super().__init__(path="/status/devices/connected")

    class _EpStatusDevicesConnectedLookup(_EndpointTemplate):
        """
        Endpoint /status/devices/connected/<lookup>
        """
        get = RequestMethod.GET.value
        put = RequestMethod.PUT.value

        @classmethod
        def __init__(cls):
            super().__init__(path="/status/devices/connected/<lookup>")

    class _EpConfigActions(_EndpointTemplate):
        """
        Endpoint /config/actions
        """
        get = RequestMethod.GET.value
        post = RequestMethod.POST.value

        @classmethod
        def __init__(cls):
            super().__init__(path="/config/actions")

    class _EpConfigActionsLookup(_EndpointTemplate):
        """
        Endpoint /config/actions/<lookup>
        """
        get = RequestMethod.GET.value
        put = RequestMethod.PUT.value

        @classmethod
        def __init__(cls):
            super().__init__(path="/config/actions/<lookup>")

    class _EpInfoGmcsConfig(_EndpointTemplate):
        """
        Endpoint /info/gmcs_config
        """
        get = RequestMethod.GET.value

        @classmethod
        def __init__(cls):
            super().__init__(path="/info/gmcs_config")

    class _EpMgmtDevicesDelete(_EndpointTemplate):
        """
        Endpoint /mgmt/devices/delete
        """
        put = RequestMethod.PUT.value

        @classmethod
        def __init__(cls):
            super().__init__(path="/mgmt/devices/<lookup>/delete")

    class _EpMgmtDevicesRename(_EndpointTemplate):
        """
        Endpoint /mgmt/devices/rename
        """
        put = RequestMethod.PUT.value

        @classmethod
        def __init__(cls):
            super().__init__(path="/mgmt/devices/<lookup>/rename")

    # Definition of endpoints
    ep_devices = _EpDevices()
    ep_devices_lookup = _EpDevicesLookup()
    ep_devices_lookup_set = _EpDevicesLookupSet()
    ep_status_devices_connected = _EpStatusDevicesConnected()
    ep_status_devices_connected_lookup = _EpStatusDevicesConnectedLookup()
    ep_info_gmcs_config = _EpInfoGmcsConfig()
    ep_mgmt_devices_delete = _EpMgmtDevicesDelete()
    ep_mgmt_devices_rename = _EpMgmtDevicesRename()
    ep_config_actions = _EpConfigActions()
    ep_config_actions_lookup = _EpConfigActionsLookup()
