import asyncio
from enum import Enum, auto
from epl.epl_logging.epl_logging_log import Log
from queue import Queue, Full


class StateReturn:
    """
    Class to define state return objects.
    State machine operate with generic return objects to analyze results of states.
    """

    class ReturnSuccess(Enum):
        """
        Defining the success of a state callback response.
        """
        ONGOING = auto()
        DONE_RIGHT = auto()
        DONE_LEFT = auto()
        ERROR = auto()

    def __init__(self,
                 success=ReturnSuccess.ONGOING,
                 message="",
                 ):
        """
        Init the object.
        :param success: Success.
        :param message: String message.
        """
        self.success = success
        self.message = message

    def __str__(self):
        """
        String implementation.
        :return:
        """
        return "success:{};message:{}".format(self.success, self.message)


class GenericState:
    """
    The main state object defining the single states operated by the state machine.
    """

    def __init__(self,
                 state_name: str,
                 jump_right_state: str,
                 jump_left_state: str,
                 jump_error_state: str,
                 callback: callable,
                 timeout_s=0.0,
                 call_period_s=0.0,
                 ):
        """
        Init the object.
        :param state_name: State's name.
        :param jump_right_state: Name of the right next state.
        :param jump_left_state: Name of the left next state.
        :param jump_error_state: Name of the state to jump to upon error.
        :param callback: The callback to be executed when state is active.
        :param timeout_s: Timeout in s for the state (will error out on exceeding).
        A `0.0` deactivates the timeout functionality for a task.
        :param call_period_s: Period with which the state should be called.
        If `0.0` is passed, a global value will be used.
        """
        self.state_name = state_name
        self.callback = callback
        self.timeout_s = timeout_s
        self.call_period_s = call_period_s
        self.jump_right_state_name = jump_right_state
        self.jump_left_state_name = jump_left_state
        self.jump_error_state_name = jump_error_state


class HealthState:
    """
    Class defining the overall health state of the state machine.
    """

    class HealthStateValue(Enum):
        INIT = auto()
        STOPPED = auto()
        RUNNING = auto()
        ERROR = auto()

    def __init__(self,
                 health_state: HealthStateValue,
                 health_message: str):
        """
        Init the object.
        :param health_state: HealthStateValue
        :param health_message: State corresponding message.
        """
        self.state = health_state
        self.message = health_message

    def __str__(self):
        """
        String implementation.
        :return:
        """
        return "{};{}".format(self.state, self.message)


class GenericStateMachine:
    """
    The state machine itself.
    """

    def __init__(self,
                 states: dict[[str], GenericState],
                 init_state_name: str,
                 period_s=1.0,
                 logger=None
                 ):
        """
        Init the state machine.
        :param states: Dict with states to be executed from the state machine.
        :param init_state_name: Name of the initial state the state machine should start at.
        :param period_s: The period with which the state machine should operate.
        :param logger: epl_logging.Log object.
        """

        # Variables
        self._logger = logger if logger is not None else Log(
            name="epl_sm", level_console=Log.LoggingLevels.DEBUG,
            formatter_console=[Log.Formatters.ASC_TIME, Log.Formatters.FILE_NAME,
                               Log.Formatters.FUNC_NAME, Log.Formatters.LINE_NUMBER],
        )

        self._logger.write_info("Initializing state machine object...")

        # Setting variables
        self._states: dict[[str], GenericState] = states if states is not None else dict()
        self._period_s = period_s

        # Queue to store the history of state changes
        self._state_history_queue = Queue(1000)

        # Helper to remember the current state
        self._current_state: GenericState = None

        # The name of the initial state
        self._init_state_name = init_state_name

        # Timeout counter for state timeout functionality
        self._timeout_counter = 0.0

        # State machine health state, will be initialized during init of actual state machine
        self._health_state: HealthState = None

        # Init the state machine
        self._init_state_machine()

        self._logger.write_info("State machine object initialized.")

    def _init_state_machine(self):
        """
        Initializes the state machine to default conditions.
        :return:
        """

        self._logger.write_debug("Initializing state machine, not starting it.")

        # Make state machine at init state
        self._current_state_set(self._init_state_name)

        # Set health state to init
        self._health_state_set(HealthState.HealthStateValue.INIT, "init")

    def state_machine_start(self, also_init=False):
        """
        Starts the state machine by setting the health state to running.
        :param also_init: True->Also re-init the state machine. Otherwise, state machine runs from last state.
        :return:
        """

        # Also initialize the state machine with health state change?
        if also_init:
            self._init_state_machine()

        # Set health state
        self._health_state_set(HealthState.HealthStateValue.RUNNING, "started")

    def state_machine_stop(self,
                           also_init=False):
        """
        Stops the state machine by setting the health state to stopped, also timeouts are stopped in this state.
        :param also_init: True->Also re-init the state machine. Otherwise, state machine runs from last state.
        :return:
        """

        if also_init:
            self._init_state_machine()

        self._health_state_set(HealthState.HealthStateValue.STOPPED, "stopped")

    def _current_state_set(self, state_name: str) -> bool:
        """
        Set the current state by index. Checks whether state is available from states list.
        Also resets the timeout counter.
        :param state_name: The state name in states list.
        :return: True->ok; False->State index not available.
        """

        # Reset timeout counter
        self._timeout_counter = 0.0

        # Check whether state index is in range
        if state_name in self._states:
            self._logger.write_debug("Setting current state to: {}".format(state_name))

            # Set new state
            self._current_state = self._states[state_name]

            try:
                # Store state in history queue
                self._state_history_queue.put(state_name, block=False)
            except Full:
                self._logger.write_debug(
                    f"state history queue is full len=`{self._state_history_queue.qsize()}`")

            return True

        self._logger.write_error("Could not set current state to `{}`; state not in states dict".format(state_name))
        return False

    def _current_state_jump_right(self) -> bool:
        """
        Activates the right neighbor of the current state.
        :return:
        """
        return self._current_state_set(self._current_state.jump_right_state_name)

    def _current_state_jump_left(self) -> bool:
        """
        Activates the left neighbor of the current state.
        :return:
        """
        return self._current_state_set(self._current_state.jump_left_state_name)

    def _current_state_jump_error(self) -> bool:
        """
        Activates the error-state related to the current state.
        :return:
        """
        return self._current_state_set(self._current_state.jump_error_state_name)

    def current_state_get(self) -> GenericState:
        """
        Returns the current active state.
        :return:
        """
        return self._current_state

    def state_history_get(self, clear_list=True) -> list:
        """
        Returns the state history as list.
        :param clear_list: True->Clear list after reading.
        :return: list
        """

        # Convert queue to list
        state_list = list(self._state_history_queue.queue)

        # Clear the queue?
        if clear_list:
            with self._state_history_queue.mutex:
                self._state_history_queue.queue.clear()

        return state_list

    def _health_state_set(self,
                          state: HealthState.HealthStateValue,
                          message: str):
        """
        Setting the state machine's health state.
        :param state: HealthStateValue
        :param message: State corresponding message.
        :return:
        """
        self._health_state = HealthState(state, message)

    def health_state_get(self) -> HealthState:
        """
        Returns the actual HealthState.
        :return:
        """
        return self._health_state

    async def state_machine_callback(self):
        """
        The state machine thread.
        :return:
        """

        self._logger.write_info("State machine loop started...")

        # Initialize the call period
        call_period = self._period_s

        # State machine endless loop
        while 1:
            try:
                # State machine only operates in RUNNING and ERROR states, other states stop it.
                if self._health_state.state in [HealthState.HealthStateValue.RUNNING,
                                                HealthState.HealthStateValue.ERROR]:

                    self._logger.write_info("Current state: {}".format(self._current_state.state_name))

                    # Execute current state's callback
                    self._logger.write_debug("Executing state callback: {}".format(self._current_state.callback))
                    state_return = await self._current_state.callback()

                    # Analyze response
                    self._logger.write_debug("State callback response: `{}`".format(state_return))

                    # Jump right
                    if state_return.success == StateReturn.ReturnSuccess.DONE_RIGHT:

                        self._current_state_jump_right()

                    # Jump left
                    elif state_return.success == StateReturn.ReturnSuccess.DONE_LEFT:

                        self._current_state_jump_left()

                    # Error
                    elif state_return.success == StateReturn.ReturnSuccess.ERROR:

                        self._logger.write_error("Callback returned an error message: `{}`".format(state_return))
                        self._current_state_jump_error()

                        # Set health state to indicate the error situation
                        self._health_state_set(HealthState.HealthStateValue.ERROR, state_return.message)

                        self._logger.write_debug("State callback returned an error, success:{}; message:{}".format(
                            state_return.success, state_return.message))

                    # Stay in current state and handle timeouts
                    else:
                        # Handle timeouts
                        # Remember: a `0` deactivates the timeout functionality for a task
                        if 0 < self._current_state.timeout_s < self._timeout_counter:
                            # Set health state to indicate the error condition
                            self._health_state_set(HealthState.HealthStateValue.ERROR,
                                                   "Timeout for state `{}` exceeded: {}s".format(
                                                       self._current_state.state_name, self._current_state.timeout_s))

                            self._logger.write_error("Timeout for state `{}` exceeded: {}s".format(
                                self._current_state.state_name, self._current_state.timeout_s))
                        else:
                            # Count up state's timeout counter
                            self._timeout_counter += call_period

                    # Set call period based on the current state
                    call_period = self._current_state.call_period_s \
                        if self._current_state.call_period_s > 0 else self._period_s

                else:
                    # Set call period to default
                    call_period = self._period_s

                    self._logger.write_debug("State machine in state: {}".format(self._health_state))

            except Exception as e:
                self._logger.write_exception("State machine general exception, ex: {}".format(e.args))
            finally:
                await asyncio.sleep(call_period)
