from epl.epl_gmcs.epl_gmcs_config import ValueTypes, SensorStatus, NodeInfo, Redundancy
from influxdb_client import Point
from varname import nameof
from enum import Enum

# This dummy is only used to get the instance variable names via varname.nameof as Redis keys.
_node_info_dummy = NodeInfo()


class GmcsInfluxKeys(Enum):
    """
    Enum to define keys for GMCS NodeInfo objects in Redis.
    """
    node_id = nameof(_node_info_dummy.node_id)
    tree_name = nameof(_node_info_dummy.tree_name)
    redundancy_type = nameof(_node_info_dummy.metadata.redundancy.redundancy_type)
    redundant_node_id = nameof(_node_info_dummy.metadata.redundancy.redundant_node_id)
    redundancy_std_dev = nameof(_node_info_dummy.metadata.redundancy.redundancy_std_dev)
    redundancy_count = nameof(_node_info_dummy.metadata.redundancy.redundancy_count)


def create_data_point(node_id: str, node_tree_name: str, value_type: str, error_state: str, current_value,
                      redundant_node_id: str = None, measurement: str = "gmcs") -> Point:
    """
    Returns a InfluxDB data point object with standard GMCS structure.
    :param node_id: The node id.
    :param node_tree_name: Node's tree name starting with the root.
    :param value_type: value type as string.
    :param error_state: SensorStatus as string.
    :param current_value: Current value with proper type.
    :param redundant_node_id: Redundant node_id for redundant tasks.
    :param measurement: The measurement: Usually `gmcs`.
    :return: influxdb_client.Point
    """
    try:
        # Make sure value type and error state are in range
        field_value_type = ValueTypes[value_type].value
        tag_error_state = SensorStatus[error_state]

        # Create data point
        data_point = (Point(measurement).field("node_id", node_id).tag("value_type", field_value_type).
                      tag("node_tree_name", node_tree_name).field("error_state", tag_error_state.value).
                      field(field=field_value_type, value=current_value))

        # Redundancy information
        if redundant_node_id:
            data_point.tag("redundant_node_id", redundant_node_id)

        return data_point

    except NameError:

        return None


def create_redundancy_point(node_id: str, node_tree_name: str, redundant_node_id: str,
                            redundancy_type: Redundancy.RedundancyType, redundancy_count: int = None,
                            redundancy_std_dev: float = None, measurement: str = "gmcs_redundancy") -> Point:
    """
    Returns an InfluxDB redundancy point object with standard GMCS structure.
    :param node_id: The node id.
    :param node_tree_name: Node's tree name starting with the root.
    :param redundant_node_id: Redundant node_id for redundant tasks.
    :param redundancy_std_dev: std_dev for error checks on redundant tasks.
    :param measurement: The measurement: Usually `gmcs`.
    :param redundancy_count: Count of expected redundant partners.
    :param redundancy_type: Redundancy type as per RedundancyType
    :return: influxdb_client.Point
    """
    # Create redundancy point
    redu_point = (Point(measurement).tag(GmcsInfluxKeys.node_id.value, node_id).
                  tag(GmcsInfluxKeys.tree_name.value, node_tree_name).
                  tag(GmcsInfluxKeys.redundancy_type.value, redundancy_type.value).
                  tag(GmcsInfluxKeys.redundant_node_id.value, redundant_node_id))

    # Some info only for master type nodes
    if redundancy_std_dev and redundancy_type == Redundancy.RedundancyType.MASTER:
        redu_point.field(GmcsInfluxKeys.redundancy_std_dev.value, redundancy_std_dev)
    if redundancy_count and redundancy_type == Redundancy.RedundancyType.MASTER:
        redu_point.field(GmcsInfluxKeys.redundancy_count.value, redundancy_count)

    return redu_point
