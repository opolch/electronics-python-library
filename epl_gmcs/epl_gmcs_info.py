from epl.epl_dicts.epl_dicts_objects import *


class GmcsInfrastructureInfo:
    """
    Class to define a gmcs info object, providing configuration info on gmcs.
    """

    def __init__(self, influxdb_host="", influxdb_port="", influxdb_measurement="", influxdb_organization="",
                 influxdb_token="", redis_db_host="", redis_db_port="", init_dict: [] = None):
        """
        Initialized the object.
        :param influxdb_host: The InfluxDB host value.
        :param influxdb_port: The InfluxDB port value.
        :param influxdb_measurement: The InfluxDB measurement.
        :param influxdb_organization: The InfluxDB organization.
        :param influxdb_token: The InfluxDB token.
        :param redis_db_host: The RedisDB host.
        :param redis_db_port: The RedisDB port.
        :param init_dict: Dict to init the object from. Overrides all others.
        """
        # InfluxDB
        self.influxdb_host = influxdb_host
        self.influxdb_port = influxdb_port
        self.influxdb_measurement = influxdb_measurement
        self.influxdb_organization = influxdb_organization
        self.influxdb_token = influxdb_token

        # Redis
        self.redis_db_host = redis_db_host
        self.redis_db_port = redis_db_port

        if init_dict:
            self.init_from_dict(init_dict=init_dict)

    def init_from_dict(self, init_dict: dict):
        """
        Option to init the object from a dict.
        :param init_dict: dict holding `class_member:value` pairs.
        :return:
        """
        object_init_from_dict(self, init_dict)

    def as_dict(self) -> dict:
        # """
        # Creates a dict with all class members by recursively iterating.
        # It expects objects implement `as_dict` function.
        # :return: dict
        # """
        return object_to_dict(self)
