from time import sleep
import pyvisa
from pyvisa import InvalidSession, VisaIOError
from enum import Enum
from retry import retry
from epl.epl_logging.epl_logging_log import Log
from epl.epl_datetime.epl_datetime_datetime import dt_now_tz_timestamp_ms

class FSW:

    class ExceptQuery(BaseException):
        """
        Exception during query execution.
        """
        pass

    class ExceptCommand(BaseException):
        """
        Exception during command execution.
        """
        pass

    class ExceptTimeout(BaseException):
        """
        Timeout exception during some operation.
        """
        pass

    class MessageSpecialChars(Enum):
        """
        Class defining special message characters like delimiters, ...
        """
        QueryChar = "?"

    class MessageCommands(Enum):
        """
        Class defining commands supported by the FSW device.
        """
        Q_Identity = "*IDN"
        Q_StateOperCond = ":STAT:OPER:COND"
        Q_TraceX = "TRAC:X"
        Q_TraceY = "TRAC"
        C_Format = "FORMAT"
        C_MeasureSingle = ":INIT:CONT"
        C_MeasureCont = ":INIT:CONT"
        C_MeasureStart = ":INIT:IMM"
        C_SysDispUdpOn = ":SYST:DISP:UPD"

    class MeasurementMode(Enum):
        """
        Class to define the measurement mode (used with corresponding method)
        """
        Continuous = 0
        Single = 1

    class StatusOperation(Enum):
        """
        Class to define return values for the `status operation` query.
        """
        Finish = 0
        Ongoing = 24

    class MeasureDataFormat(Enum):
        """
        Class to define available formats to be selected for measurement data.
        """
        ASCII = "ASCII"

    def __init__(self, ip_address:str, port:int, logger:Log):
        """
        Inits the object.
        :param ip_address: Device IP.
        :param port: Device port.
        """
        # Variables
        self._ip_address = ip_address
        self._port = port
        self._logger = logger

        # Init the VISA connection
        self._resource_manager = pyvisa.ResourceManager()
        self._instrument = self._resource_manager.open_resource(f"TCPIP0::{self._ip_address}::{self._port}::SOCKET")
        self._reopen_and_init_connection()

    def _reopen_and_init_connection(self):
        """
        Function to be called to open (also re-open) the connection to the remote device and make initial settings.
        :return:
        """
        self._instrument.close()
        self._instrument.open()
        self._instrument.write_termination = "\n"
        self._instrument.read_termination = "\n"

    def _create_query_message(self, command:MessageCommands, params=""):
        """
        Creates a standard query message for a given command.
        :param command: MessageCommands
        :param params: Additional parameters to be added after the query delimiter.
        :return: str
        """
        return f"{command.value}{self.MessageSpecialChars.QueryChar.value} {params}"

    def _create_command_message(self, command:MessageCommands, params=""):
        """
        Creates a standard command message for a given command.
        :param command: MessageCommands
        :param params: Additional parameters to be added after the command.
        :return: str
        """
        return f"{command.value} {params}"

    @retry(ExceptQuery, tries=5, delay=1, jitter=1)
    def _execute_query(self, query_message:str):
        """
        Executing a query and handling upcoming errors.
        :param query_message: The query message to be sent.
        :return: Query result
        """
        try:
            self._logger.write_debug(f"Executing query {query_message}")
            ret = self._instrument.query(query_message)
            self._logger.write_debug(f"Query result:`{ret}`")
            return ret
        except (VisaIOError, InvalidSession, BrokenPipeError, ConnectionRefusedError) as ex:
            self._logger.write_exception(f"Visa error `{type(ex).__name__}`, trying to reopen resource now; ex:{ex}")

            # Open connection again
            self._reopen_and_init_connection()
            self._logger.write_info(f"Re-opening of resource successful.")

            raise FSW.ExceptQuery(ex.args)

    @retry(ExceptCommand, tries=5, delay=1, jitter=1)
    def _execute_command(self, command_message:str):
        """
        Executing a command and handling upcoming errors.
        :param command_message: The command message to be sent.
        :return: Query result
        """
        try:
            self._logger.write_debug(f"Executing query {command_message}")
            ret = self._instrument.write(command_message)
            self._logger.write_debug(f"Query result:`{ret}`")
            return ret
        except (VisaIOError, InvalidSession, BrokenPipeError, ConnectionRefusedError) as ex:
            self._logger.write_exception(f"Visa error `{type(ex).__name__}`, trying to reopen resource now; ex:{ex}")

            # Open connection again
            self._reopen_and_init_connection()
            self._logger.write_info(f"Re-opening of resource successful.")

            raise FSW.ExceptQuery(ex.args)

    def query_identity(self):
        """
        Query the identification string.
        :return: Identification string
        """
        return self._execute_query(self._create_query_message(self.MessageCommands.Q_Identity))

    def query_spectra(self, trace=1):
        """
        Queries spectra for a given trace. Will return as dict{datax:[strings],datay:[strings]}
        :return: dict
        """

        spec = {}

        # Query x/y data and put in dict
        x = self._execute_query(self._create_query_message(self.MessageCommands.Q_TraceX, params=f'TRACE{trace}'))
        spec["xdata"] = [f for f in x.split(",")]
        y = self._execute_query(self._create_query_message(self.MessageCommands.Q_TraceY, params=f'TRACE{trace}'))
        spec["ydata"] = [f for f in y.split(",")]

        return spec

    def query_status_operation(self):
        """
        Query the operation status.
        :return: Operation status value
        """
        return int(self._execute_query(self._create_query_message(self.MessageCommands.Q_StateOperCond))) & 2047

    def cmd_system_disp_upd(self, on=True):
        """
        Command to activate/deactivate the display updating.
        :return:
        """
        if on:
            return self._execute_command(self._create_command_message(self.MessageCommands.C_SysDispUdpOn, "ON"))
        else:
            return self._execute_command(self._create_command_message(self.MessageCommands.C_SysDispUdpOn, "OFF"))

    def cmd_measure_mode(self, mode:MeasurementMode):
        """
        Command to set the measurement mode.
        :param mode: MeasurementMode enum
        :return:
        """
        if mode == self.MeasurementMode.Continuous:
            return self._execute_command(self._create_command_message(self.MessageCommands.C_MeasureCont, "ON"))
        else:
            return self._execute_command(self._create_command_message(self.MessageCommands.C_MeasureCont, "OFF"))

    def cmd_start_measurement(self):
        """
        Command to start a measurement `:INIT:IMM`
        :return:
        """
        return self._execute_command(self._create_command_message(self.MessageCommands.C_MeasureStart))

    def cmd_data_format(self, data_format=MeasureDataFormat):
        """
        Command to set the format for the trace data.
        :param data_format: MeasureDataFormat
        :return:
        """
        return self._execute_command(self._create_command_message(self.MessageCommands.C_Format, data_format.value))

    def measurement_setup(self, display_update:bool, measurement_mode:MeasurementMode):
        """
        Setup of measurement.
        :param display_update: Update the display during measurement?
        :param measurement_mode: MeasurementMode
        :return:
        """

        # Display should update
        self.cmd_system_disp_upd(display_update)

        # Set measurement mode to single
        self.cmd_measure_mode(mode=measurement_mode)


    def measure(self, timeout_ms=5000):
        """
        Abstract method to execute a measurement routine.
        :SYST:DISP:UPD ON
        :INIT:CONT OFF
        :INIT:IMM
        :STAT:OPER:COND?
        FORMAT ASCII
        TRAC:X? TRACE1
        TRAC? TRACE1
        :INIT:CONT ON
        :param: timeout_ms: Max timeout in ms for the action to be completed.
        :return:
        """
        # Start measurement
        self.cmd_start_measurement()

        # Save start time
        start_time = dt_now_tz_timestamp_ms()

        # Wait for the measurement to be finished by querying the status register
        while self.query_status_operation() != self.StatusOperation.Finish.value:

            # Check for timeout
            if (dt_now_tz_timestamp_ms() - start_time) > timeout_ms:
                raise self.ExceptTimeout(f"Measure operation timed out (>{timeout_ms}ms)")

            sleep(0.1)

        # Read out spectra
        return self.query_spectra()
