import tango
import asyncio
from tango.asyncio import DeviceProxy, AttributeProxy
import os
from epl.epl_string.epl_string_split import split_nth_occurrence


class TangoInteraction:

    def __init__(self, tango_host, logger, devices_filter=None, queue_max_size=10000):
        """
        Initializes the object.
        :param tango_host: str, "TANGO_HOST:PORT"
        :param logger: The epl_logging.Log object.
        :param devices_filter: list(str) with devices to be handled (None for all).
        :param queue_max_size: Max size for the data queue.
        """

        # Set TANGO_HOST environment variable
        os.environ["TANGO_HOST"] = tango_host

        self._logger = logger
        self._db = tango.Database()
        self.queue = asyncio.Queue(maxsize=queue_max_size)

        self._logger.write_info("Getting devices list")
        devices_available = self._db.get_device_exported("*")

        self._logger.write_debug("Available devices: {}".format(devices_available))
        if devices_filter:
            self._logger.write_debug("Filtering available devices based on filter: {}".format(devices_filter))
            self._devices_filter = [x for x in devices_available if x.lower() in devices_filter]
        else:
            self._devices_filter = [x for x in devices_available]

        # Will be filled later during the device init-process
        self._devices = {}

        self._logger.write_info("Handling the following devices: {}".format(self._devices_filter))

    async def init_devices(self):
        """
        Global init function performing following actions:
        - Querying device attributes
        - Initializing attribute events
        :return:
        """

        await self._get_attributes()
        await self._init_attribute_events()

    async def get_device(self):
        return self._devices

    async def get_attribute_list(self):
        """
        Global init function performing following actions:
        - Querying device attributes
        - Initializing attribute events
        :return:
        """

        await self._get_attributes()
        return self._devices

    async def _get_attributes(self):
        """
        Querying device attributes and collecting in local devices dict.
        :return:
        """

        self._logger.write_info("Querying device attributes...")

        # Get device proxys for all devices to query their attributes
        devices = await asyncio.gather(* [DeviceProxy(device, timeout=10) for device in self._devices_filter])
        self._logger.write_debug("Queried following DeviceProxies: {}".format(devices))

        # Query attribute list
        for device in devices:
            self._devices[device.dev_name()] = [device, device.get_attribute_list()]
            self._logger.write_debug("Queried attributes for device `{}`: {}".format(
                device.dev_name(), self._devices[device.dev_name()]))

    async def _init_attribute_events(self):
        """
        Initializes attribute events.
        In the current super simple implementation, alle attributes are sampled by strategy CHANGE.
        TODO: This must be developed further in coming releases.
        :return:
        """

        self._logger.write_info("Initializing attribute events...")

        for device, details in self._devices.items():
            for attribute in details[1]:
                self._logger.write_debug("Subscribing to events for attribute: {}:{}".format(device, attribute))
                details[0].subscribe_event(
                    attribute,
                    tango.EventType.ARCHIVE_EVENT,
                    self._event_callback,
                    stateless=True,
                    # stateless: When this flag is set to false, an exception will be thrown when
                    # the event subscription encounters a problem.
                    # With the stateless flag set to true, the event subscription will always succeed,
                    # even if the corresponding device server is not running.
                    # The keep alive thread will try every 10 seconds to subscribe for the specified event.
                    # At every subscription retry, a callback is executed which contains the corresponding exception
                    green_mode=tango.GreenMode.Asyncio
                )

    async def _event_callback(self, event):
        """
        The general callback for attribute events. It extracts timestamp/data and fills a queue.
        :param event: The event-data object holding all event related info.
        :return:
        """
        self._logger.write_debug("Event received: {}".format(event))

        # Extract single fields
        # Attribute name (entire tree) located after the 3th `/`
        # eg. 'tango://10.96.64.100:10000/mid_dsh_0000/spf/spfvac/requeststatus'
        attribute_name_tree = split_nth_occurrence(event.attr_name, '/', 3)[1]
        device_name, attribute_name = split_nth_occurrence(attribute_name_tree, '/')  # split at last `/`
        attribute_value_type = str(event.attr_value.type)
        attribute_value = event.attr_value.value
        attribute_timestamp = "{}.{}".format(event.attr_value.time.tv_sec, event.attr_value.time.tv_usec)
        await self.queue.put((device_name, attribute_name, attribute_value_type, attribute_timestamp, attribute_value))
