# -*- coding: utf-8 -*-
"""
Created on Wed Feb 19 17:56:19 2020

@author:
    Devika Hardikar

Description:
    This file provides the extended functionality of the KATCP device server.
    The purpose is to make it more flexible and generic for its usage in
    various projects.
"""
import copy

import katcp
from epl.epl_string.epl_string_split import split_always_list
from epl.epl_dicts.epl_dicts_search import sub_string_in_keys
from epl.epl_logging.epl_logging_log import Log
import json
from epl.epl_katcp.epl_katcp_gim import KatcpGimInfo
from epl.epl_katcp.epl_katcp_sensor import KatcpSensor


def docstring_parameter(*args):
    def dec(obj):
        obj.__doc__ = obj.__doc__.format(*args)
        return obj

    return dec


class ServerTask:
    """
    Class to define a standard task for the katcp server:
    sensor, request, gim_config
    """

    def __init__(self, sensor=None, request=None, gim_info=None):
        """
        Inits the task object.
        :param sensor: KatcpSensor
        :param request: KatcpRequest
        :param gim_info: KatcpGimInfo
        """
        self.katcp_sensor = sensor
        self.katcp_request = request
        self.gim_info = gim_info


class KatcpServer(katcp.DeviceServer):
    """
    Class derived from base class katcp.DeviceServer to provide common katcp
    server functionality and adapt it to our needs.
    """

    def __init__(self,
                 host,
                 port,
                 tasks,
                 callback_function,
                 logger,
                 virtual_gim_roots=None,
                 sampling_strategy_default=KatcpSensor.SamplingStrategy.event_rate,
                 sampling_strategy_default_args=[30, 1800]):

        """
        Initializes the parameters from the received ones.
        :param host: Host name for the KATCP connection
        :param port: Port number for the KATCP connection
        :param tasks: dict of ServerTask objects (key is always the sensor (dot-separated) name.
        !Even though there is no sensor available!
        :param callback_function: Function to extract the katcp request details
        :param logger: Logger for the log functionality
        :param virtual_gim_roots: Root name prefix to prefix all gim_config sensors/requests with
        :param sampling_strategy_default: Default sampling strategy to be used for devices/tasks
        :param sampling_strategy_default_args: Args for default sampling strategy
        """
        # Init the logger
        if logger is None:
            self._log_writer = Log()
        else:
            self._log_writer = logger

        # Interface
        self._host = host
        self._port = port

        # Sampling strategy
        self._sampling_strategy_default = sampling_strategy_default
        self._sampling_strategy_default_args = sampling_strategy_default_args

        # The task dict
        self.tasks = tasks

        # Root name
        self._virtual_gim_roots = virtual_gim_roots

        # Call back function for the requests
        self.callback_function = callback_function

        # We collected all necessary info, we can initialize the base class
        super().__init__(host, port, logger=self._log_writer.logger)

        # Register sensors and commands
        self.register_sensors()
        self.register_requests()

        # Set up the gim config dict
        self.gim_config_dict = dict()
        self.gim_config_create()

    def sensor_find(self, sensor_name, substring_lookup=False):
        """
        Finds a sensor in the task dict by its sensor name.
        :param sensor_name: Sensor name to look for.
        :param substring_lookup: True->Also return sensor where sensor_name is substring; False->Full comparison.
        :return: Found->KatcpSensor; Not found->None.
        """
        try:
            if substring_lookup:
                return sub_string_in_keys(self.tasks, sensor_name).katcp_sensor
            else:
                return self.tasks[sensor_name].katcp_sensor
        except:
            return None

    def sensor_update(self, sensor_name, value, status=None, substring_lookup=False):
        """
        Updates a sensor from the task list.
        :param sensor_name: Sensor name.
        :param value: Value to be set.
        :param status: SensorStatus to be set.
        :param substring_lookup: True->Also return sensor where sensor_name is substring; False->Full comparison.
        :return:
        """
        try:
            sensor = self.sensor_find(sensor_name, substring_lookup=substring_lookup)

            sensor.set_value(value, status)
            return True
        except Exception as e:
            self._log_writer.write_exception("Error during update of sensor:{}, val:{}, status:{}, ex:{}".
                                             format(sensor_name, value, status, e))
            return False

    def gim_config_create(self):
        """
        Sets up the gim config dict.
        """

        # Dict to build the tree in
        self.gim_config_dict = dict()

        # Default sampling strategy and policies on root level
        self.gim_config_dict['default_sampling_strategy'] = {
            'type': self._sampling_strategy_default.name,
            'args': self._sampling_strategy_default_args
        }
        self.gim_config_dict['policies'] = ['ignore_unknown']

        # Iterate over task list and create the devices tree
        for task_name, task in self.tasks.items():

            # Always start at root level
            cur_device = self.gim_config_dict

            # List with task names to build the tree
            task_name_list = list()

            # Add a virtual root name
            if self._virtual_gim_roots is not None:
                task_name_list.append(self._virtual_gim_roots)

            # Split task name get the different device levels
            task_name_list.extend(split_always_list(task_name, '.'))

            # Iterate over task name list to build the device tree
            for i in range(0, len(task_name_list) - 1):

                # The following 3 steps apply for all levels:
                # 1. Add devices entry
                # 2. Add the device
                # 3. Add global parameters

                # Device metadata
                device_metadata = dict()

                # Devices entry
                if 'devices' not in cur_device:
                    cur_device['devices'] = dict()

                # Check whether next device is already present (from another task)
                if task_name_list[i] not in cur_device['devices']:
                    cur_device['devices'][task_name_list[i]] = dict()

                # Store pointer to the next device
                cur_device = cur_device['devices'][task_name_list[i]]

                # +++ META
                # Set top-level device as icinga-host
                if i == 0:
                    device_metadata['icinga_host'] = '1'

                # Devices have to have GET type
                device_metadata['mysql_task_type'] = 'GET'

                # Set the device meta (need to do copy)
                cur_device['metadata'] = copy.deepcopy(device_metadata)
                # --- META

                # Global params on device level
                cur_device['sampling_strategy'] = {
                    'type': self._sampling_strategy_default.name,
                    'args': self._sampling_strategy_default_args
                }
                cur_device['policies'] = ['autogen_task']

            # Make sure level has tasks key (always needed to attach tasks)
            if 'tasks' not in cur_device:
                cur_device['tasks'] = dict()
            cur_device = cur_device['tasks']

            if task.gim_info is not None:

                # Some widget types need special treatment
                drop_down_options = dict()
                if task.gim_info.mysql_task_type is KatcpGimInfo.WidgetType.DROPDOWN:
                    if task.gim_info.limits is not None:
                        for limit in task.gim_info.limits:
                            drop_down_options[limit] = str(limit)

                # Add task
                cur_device[task_name_list[-1]] = {  # Use task name as key
                    'sensor_name': '',
                    'request_name': '',
                    'options': task.gim_info.options_available.name.lower() \
                        if task.gim_info.options_available is not None else '',
                    'metadata':
                        {
                            'display_name': task.gim_info.display_name,
                            'lower_limit': task.gim_info.limits[0]
                            if task.gim_info.limits is not None else '',
                            'upper_limit': task.gim_info.limits[-1]
                            if task.gim_info.limits is not None else '',
                            'value_type': task.gim_info.value_type,
                            'sampling_strategy':
                                {'type': task.gim_info.sampling_strategy.value,
                                 'args': task.gim_info.sampling_strategy_params} \
                                    if task.gim_info.sampling_strategy is not None \
                                    else {'type': self._sampling_strategy_default.name,
                                          'args': self._sampling_strategy_default_args},
                            'mysql_task_type': task.gim_info.mysql_task_type.name \
                                if task.gim_info.mysql_task_type is not None else '',
                            'description': task.gim_info.description \
                                if task.gim_info.description is not None else '',
                            'options': drop_down_options,
                            'option01': task.gim_info.option01 if task.gim_info.option01 is not None else '',
                            'option02': task.gim_info.option02 if task.gim_info.option02 is not None else '',
                            'option03': task.gim_info.option03 if task.gim_info.option03 is not None else '',
                            'option04': task.gim_info.option04 if task.gim_info.option04 is not None else '',
                            'unit': task.gim_info.unit if task.gim_info.unit is not None else '',
                            'format': task.gim_info.format if task.gim_info.format is not None else '',
                            'icinga_check_constant_errors': task.gim_info.icinga_check_constant_errors \
                                if task.gim_info.icinga_check_constant_errors is not None else '',
                        }
                }

    def setup_sensors(self):
        """
        This function sets up various sensors.

        This function takes care of setting up various sensors according to the
        katcp format and adding them to the list of katcp sensors.

        """
        pass

    def register_sensors(self):
        """
        This function takes care of adding the sensors to the list of katcp
        sensors on the server.
        """

        for task in self.tasks.values():
            if task.katcp_sensor is not None:
                self.add_sensor(task.katcp_sensor)
                self._log_writer.write_info("Registering sensor {}".format(task.katcp_sensor.name))

        self._log_writer.write_info("Done: Registered sensors.")

    def register_requests(self):
        """
        Creates request based on task list.
        """

        # Init the request handlers based on the default ones
        # Necessary to make multiple instances of KatcpServer work properly.
        # Without all requests are added to same dict!
        self._request_handlers = dict(self._request_handlers)

        for task in self.tasks.values():
            if task.katcp_request is not None:
                new_request = task.katcp_request
                self._request_handlers[new_request.name] = \
                    self.req_callback_function(new_request.name,
                                               new_request.description,
                                               new_request.params)

        self._log_writer.write_info("Done: Registered requests.")

    def req_callback_function(self, request_name, description, params):
        """
        Generic function to create the reply functions to the katcp requests.

        :param request_name: Name of the request
        :param description: Description of the request
        :param params: Parameters of the request

        :return: Dynamically created specific request function.
        """

        @docstring_parameter(description, request_name, params)
        def generic_set_operations(self, *args):
            """{0} (?{1} {2})"""
            for i in range(len(args)):
                if type(args[i]) == katcp.server.ClientRequestConnection:
                    req = args[i]
                elif type(args[i]) == katcp.core.Message:
                    msg = args[i]
                else:
                    pass

            # Callback function should return a dict "status, reply [,inform]"
            # Old implementation has [status, info] as two outputs
            ret = self.callback_function(msg)

            # Some requests also provide inform messages (only works with dict output)
            if isinstance(ret, dict) is True:

                # Check whether inform is available at all
                inform_message = ret.get('inform', '')
                if inform_message != '':
                    # If inform has list type, send multiple messages
                    if isinstance(inform_message, list):
                        for single_inform in inform_message:
                            if isinstance(single_inform, list) or isinstance(single_inform, tuple):
                                req.inform(*single_inform)
                            else:
                                req.inform(single_inform)
                    else:
                        req.inform(inform_message)

                # Reply in the end
                return req.make_reply(ret['status'], ret['reply'])
            else:
                # Extract status and info
                status, info = ret

                # Make reply
                return req.make_reply(status, info)

        return generic_set_operations

    def request_gim_config(self, req, msg):
        """
        gim-config

        param req: Name of the request
        param msg: Message

        :return success: {'ok, 'error'}
            Whether sending the list of values succeeded.
        :return informs: gimhelp.
        """

        return req.make_reply('ok', json.dumps(self.gim_config_dict))

    def request_sensor_value(self, req, msg):
        """
        Generic function to reply to sensor-value requests.

        param req: Name of the request
        param msg: Message

        :return success: {'ok, 'error'}
            Whether sending the list of values succeeded.
        :return informs: Number of #sensor-value inform messages sent.
        """
        return super().request_sensor_value(req, msg)

    def request_help(self, req, msg):
        """
        Return help on the available requests.
        Return a description of the available requests using a sequence of
        #help informs.
        :param req: Request for which the help is needed.
        :param msg: Reqeust message
        :return success: {'ok', 'fail'} - Whether sending the help succeeded.
        :return informs: Number of #help inform messages sent
        """
        if not msg.arguments:
            for name, method in sorted(self._request_handlers.items()):
                doc = method.__doc__.splitlines()[0]

                if method.__name__ != 'generic_set_operations':
                    doc = doc + '(?' + method.__name__.replace('_', '-') + ')'
                    doc = doc.replace('request-', '')
                req.inform(name, doc)
            num_methods = len(self._request_handlers)
            return req.make_reply("ok", str(num_methods))
        else:
            name = katcp.core.ensure_native_str(msg.arguments[0])
            if name in self._request_handlers:
                method = self._request_handlers[name]
                doc = method.__doc__.splitlines()[0]
                req.inform(name, doc)
                return req.make_reply("ok", "1")
            return req.make_reply("fail", "Unknown request method.")
