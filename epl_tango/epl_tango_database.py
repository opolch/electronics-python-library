import asyncio
import os
from tango import Database, DbDevInfo
from tango._tango import GreenMode
from tango.server import Device, DeviceMeta, run, command, attribute, device_property
from enum import Enum
from epl.epl_logging.epl_logging_log import Log


class TangoDatabase:

    def __init__(self,
                 device_host,
                 device_port,
                 loop=None,
                 logger=None
                 ):

        self.tango_host = str(device_host)+":"+str(device_port)
        os.environ["TANGO_HOST"] = self.tango_host
        self.db = Database()

    # TODO ADD attributes for tango interaction with he/vac
    def add_tango_device(self,device_class,device_server, device_name):
        device_info = DbDevInfo()
        device_info._class = device_class
        device_info.server = device_server
        device_info.name   = device_name
        self.db.add_device(device_info)

