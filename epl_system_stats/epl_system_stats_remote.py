import datetime

from fabric import ThreadingGroup, exceptions
from enum import Enum, auto
from epl.epl_networking.epl_networking_ifconfig import IfconfigParser


class SystemStatsRemoteLinux:
    class ExceptionRunCommand(Exception):
        """
        Exception indicating some general error with a command executed.
        """
        pass

    class ProcessListColumns(Enum):
        """
        Enum to define process list columns.
        """

        COMMAND = "command"
        CPU = "%cpu"
        MEM = "%mem"

    class RetValState(Enum):
        """
        Enum to define run command return values.
        """

        OK = auto()
        FAIL = auto()

    class RetVal:
        """
        Class defining a common struct for return values.
        """

        def __init__(self, host, status, output):
            """
            Init.
            :param host: Host string.
            :param status: RetValState
            :param output: Output string.
            """
            self.host = host
            self.status = status
            self.output = output

    class TopVersion(Enum):
        """
        Enum to define the top version used on the host.
        """
        DEFAULT = auto()
        BUSY_BOX = auto()

    class IfConfigVersion(Enum):
        """
        Enum to define the ifconfig version used on the host.
        """
        DEFAULT = auto()
        OLD = auto()

    class TopFullOutput:

        def __init__(self, pid, user, pr, ni, virt, res, shr, s, cpu, mem, time, command):
            """
            Init the top output object. The variables represent 1:1 the top columns.
            :param pid:
            :param user:
            :param pr:
            :param ni:
            :param virt:
            :param res:
            :param shr:
            :param s:
            :param cpu:
            :param mem:
            :param time:
            :param command:
            """
            self.pid = pid
            self.user = user
            self.pr = pr
            self.ni = ni
            self.virt = virt
            self.res = res
            self.shr = shr
            self.stat = s
            self.cpu = cpu
            self.mem = mem
            self.time = time
            self.command = command

    class TopBusyBoxOutput:

        def __init__(self, pid, ppid, user, stat, mem, mem_percent, cpu, command):
            """
            Init the top output object. The variables represent 1:1 the top columns.
            :param pid:
            :param ppid:
            :param user:
            :param stat:
            :param mem:
            :param mem_percent:
            :param cpu:
            :param command:
            """
            self.pid = pid
            self.ppid = ppid
            self.user = user
            self.stat = stat
            self.cpu = cpu.replace("%", "")
            self.mem_bytes = mem
            self.mem = mem_percent.replace("%", "")
            self.command = command

    class UptimeVersion(Enum):
        """
        Enum to define the top version used on the host.
        """
        DEFAULT = auto()
        BUSY_BOX = auto()

    class UptimeOutput:

        def __init__(self, uptime_output):
            """
            Class to handle fields from uptime output:
            :param uptime_output: str containing the content from /proc/uptime + /proc/loadavg space separated.
            """
            output_list = uptime_output.split(" ")
            self.uptime = float(output_list[0]) / 86400
            self.load_1min = float(output_list[-5])
            self.load_5min = float(output_list[-4])
            self.load_15min = float(output_list[-3])

    class UptimeBusyBoxOutput:

        def __init__(self, uptime_output):
            """
            Class to handle fields from uptime output:
            :param uptime_output: str containing the content from /proc/uptime + /proc/loadavg space separated.
            """
            output_list = uptime_output.split(",")
            self.uptime = float(output_list[0].split(" ")[-2])
            self.load_1min = float(output_list[-3].split(" ")[-1])
            self.load_5min = float(output_list[-2])
            self.load_15min = float(output_list[-1])

    class IfConfigOutput:

        def __init__(self, iface, hw_address, ip4_address, ip6_address, mtu, rx_pck, rx_bytes, rx_err, rx_drp, rx_ovr,
                     tx_pck, tx_bytes, tx_err, tx_drp, tx_ovr, rx_bytes_rate=0, rx_pck_rate=0, rx_err_rate=0,
                     rx_drp_rate=0, rx_ovr_rate=0, tx_bytes_rate=0, tx_pck_rate=0, tx_err_rate=0, tx_drp_rate=0,
                     tx_ovr_rate=0, rate_in_mbytes=True):
            """
            Init the ifconfig output object.
            """

            # Assign values in a loop to easily catch non-floats
            arg_dict = {key: value for key, value in locals().items() if key not in ["self"]}
            non_floats = ["iface", "rate_in_mbytes", "hw_address", "ip4_address", "ip6_address"]
            for key, value in arg_dict.items():
                try:
                    if key in non_floats:
                        setattr(self, key, value)
                    else:
                        setattr(self, key, float(value))
                except (ValueError, TypeError):
                    setattr(self, key, -9999)

            # Non-float values
            self.iface = iface
            self._rate_in_mbytes = rate_in_mbytes

            # Rates need to be calculated separately
            self.rx_bytes_rate_set(rx_bytes_rate)
            self.rx_err_rate_set(rx_err_rate)
            self.rx_drp_rate_set(rx_drp_rate)
            self.rx_ovr_rate_set(rx_ovr_rate)
            self.rx_pck_rate_set(rx_pck_rate)
            self.tx_bytes_rate_set(tx_bytes_rate)
            self.tx_err_rate_set(tx_err_rate)
            self.tx_drp_rate_set(tx_drp_rate)
            self.tx_ovr_rate_set(tx_ovr_rate)
            self.tx_pck_rate_set(tx_pck_rate)

        def rx_bytes_rate_set(self, rate):
            """
            Sets the rx_rate. Based on self._rate_in_mbytes this value represents bytes or mbytes.
            :param rate: The rate in bytes.
            :return:
            """
            self.rx_mbytes_rate = rate / 1024 / 1024 if self._rate_in_mbytes else rate

        def rx_drp_rate_set(self, rate):
            """
            Sets the rx_drp_rate.
            :param rate: The rate value.
            :return:
            """
            self.rx_drp_rate = rate

        def rx_ovr_rate_set(self, rate):
            """
            Sets the rx_ovr_rate.
            :param rate: The rate value.
            :return:
            """
            self.rx_ovr_rate = rate

        def rx_err_rate_set(self, rate):
            """
            Sets the rx_err_rate.
            :param rate: The rate value.
            :return:
            """
            self.rx_err_rate = rate

        def rx_pck_rate_set(self, rate):
            """
            Sets the rx_pck_rate.
            :param rate: The rate value.
            :return:
            """
            self.rx_pck_rate = rate

        def tx_bytes_rate_set(self, tx_rate):
            """
            Sets the tx_rate. Based on self._rate_in_mbytes this value represents bytes or mbytes.
            :param tx_rate: The rate in bytes.
            :return:
            """
            self.tx_mbytes_rate = tx_rate / 1024 / 1024 if self._rate_in_mbytes else tx_rate

        def tx_drp_rate_set(self, rate):
            """
            Sets the tx_drp_rate.
            :param rate: The rate value.
            :return:
            """
            self.tx_drp_rate = rate

        def tx_ovr_rate_set(self, rate):
            """
            Sets the tx_ovr_rate.
            :param rate: The rate value.
            :return:
            """
            self.tx_ovr_rate = rate

        def tx_err_rate_set(self, rate):
            """
            Sets the tx_err_rate.
            :param rate: The rate value.
            :return:
            """
            self.tx_err_rate = rate

        def tx_pck_rate_set(self, rate):
            """
            Sets the tx_pck_rate.
            :param rate: The rate value.
            :return:
            """
            self.tx_pck_rate = rate

    def __init__(self, logger, hosts, user, password="", persistent_connections=True, port=22,
                 connect_kwargs: dict = None):
        """
        Initialize the module.
        :param logger: epl_logging.log.Log object.
        :param hosts: List with hosts to be queried (str).
        :param user: Username to be used for hosts connection.
        :param password: password for login (leave empty for pubkey).
        :param persistent_connections: True->Persistent connections to hosts.
        :param port: SSH-Port.
        :param connect_kwargs: dict with additional connect_kwargs to be passed to the ssh client.
        """

        self._logger = logger
        self._hosts = hosts
        self._user = user
        self._password = password
        self._connect_kwargs_timeout = 20
        self._connect_kwargs_banner_timeout = 20
        self._connect_kwargs_auth_timeout = 20
        self._connection_keep_open = persistent_connections
        self._port = port

        # ssh connect kwargs
        self._connect_kwargs = {
            "timeout": self._connect_kwargs_timeout,
            "banner_timeout": self._connect_kwargs_banner_timeout,
            "auth_timeout": self._connect_kwargs_auth_timeout,
            "password": self._password,
        }
        # If additional kwargs are passed, update the dict
        if connect_kwargs:
            self._connect_kwargs.update(connect_kwargs)

        if self._connection_keep_open:
            self._t_group = ThreadingGroup(*self._hosts,
                                           user=self._user,
                                           port=self._port,
                                           connect_kwargs=self._connect_kwargs
                                           )

        # Special task related variables
        # ifconfig
        self._ifconfig_last = {}

    def _host_string_create(self, connection):
        """
        Creates a host string based on connection info.
        :param connection: The connection object.
        """
        # return "{}:{}".format(connection.host, connection.port) \
        #    if connection.port != 22 else connection.host
        return connection.host

    def _result_handle_failed(self, result_failed, ret_list):
        """
        Implements a generic way to handle failed results.
        :param result_failed: Failed items.
        :param ret_list: List to store RetVals in.
        :return:
        """

        # Loop over failed items and handle errors
        for con, result in result_failed.items():
            self._logger.write_error('Command failed: `{}; {}`'.format(con, result))
            # Append ret val
            ret_list.append(
                self.RetVal(
                    host=self._host_string_create(con),
                    status=self.RetValState.FAIL,
                    output=result)
            )

    def close_connections(self):
        """
        When using persistent connections, closes all open connections.
        """
        if self._connection_keep_open:
            self._t_group.close()

    def run_command(self, cmd):
        """
        Executes a command on the host.
        :param cmd: Command to be executed.
        :return: GroupResult.
        """

        try:
            self._logger.write_debug('Executing command: `{}`'.format(cmd))

            if self._connection_keep_open is False:
                with ThreadingGroup(*self._hosts,
                                    user=self._user,
                                    connect_kwargs={
                                        "timeout": self._connect_kwargs_timeout,
                                        "banner_timeout": self._connect_kwargs_banner_timeout,
                                        "auth_timeout": self._connect_kwargs_auth_timeout
                                    }) as t_group:

                    # Execute command and return result
                    g_result = t_group.run(cmd, hide=True)
            else:
                # Execute command and return result
                g_result = self._t_group.run(cmd, hide=True)

            self._logger.write_debug('Command result: `{}`'.format(g_result))
            return g_result

        except exceptions.GroupException as e:
            # GroupExceptions also contain results which must be returned
            self._logger.write_debug(e.args)
            return e.result
        except Exception as e:
            self._logger.write_exception(e.args)
            raise self.ExceptionRunCommand(e.args)

    def test_connection(self):
        """
        Tests a host connection by simply running a ls command and catching connections errors.
        :return:
        """

        # Execute command
        ret = self.run_command('ls')

        # List for return values
        ret_list = []

        # First handle `failed` items (need to be treated specially)
        self._result_handle_failed(ret.failed, ret_list)

        # Now loop over succeeded items
        for con in ret.succeeded.items():
            ret_list.append(
                self.RetVal(
                    host=self._host_string_create(con[0]),
                    status=self.RetValState.OK,
                    output="connected")
            )

        return ret_list

    def uptime(self, uptime_version=UptimeVersion.DEFAULT.name):
        """
        Returns uptime and load average.
        :param uptime_version: Version of uptime command as per UptimeVersion enum.
        :return:
        """
        if uptime_version.upper() == self.UptimeVersion.BUSY_BOX.name:
            ret = self.run_command('uptime')
        else:
            ret = self.run_command('echo "$(</proc/uptime)" "$(</proc/loadavg)"')

        # List for return values
        ret_list = []

        # First handle `failed` items (need to be treated specially)
        self._result_handle_failed(ret.failed, ret_list)

        # Now loop over `succeeded` items
        for con in ret.succeeded.items():
            try:
                self._logger.write_debug('Uptime output: `{}`'.format(con[1].stdout.strip()))
                if uptime_version.upper() == self.UptimeVersion.BUSY_BOX.name:
                    upt_output = self.UptimeBusyBoxOutput(con[1].stdout.strip())
                else:
                    upt_output = self.UptimeOutput(con[1].stdout.strip())

                # Append UptimeOutput object to ret list
                ret_list.append(
                    self.RetVal(
                        host=self._host_string_create(con[0]),
                        status=self.RetValState.OK,
                        output=upt_output
                    )
                )
            except ValueError as e:
                self._logger.write_exception(
                    "Error converting uptime output; connection:{}, ex:{}".format(con, e)
                )

        return ret_list

    def top_processes(self, command_names=[], top_version=TopVersion.DEFAULT.name):
        """
        Query top command.
        :param command_names: List with command names to be queried. Empty list for ALL.
        :param top_version: Version of top on host as per TopVersion.
        :return: List with an entry per host.
        """

        # Create the command. Run top in batch mode, 1 batch.
        if top_version.upper() == self.TopVersion.BUSY_BOX.name:
            cmd = 'top -b -n1'
        else:
            cmd = 'top -c -b -n1 -w512'

        # Execute the command
        ret = self.run_command(cmd)

        # List for return values
        ret_list = []

        # First handle `failed` items (need to be treated specially)
        self._result_handle_failed(ret.failed, ret_list)

        # Now loop over `succeeded` items
        for con in ret.succeeded.items():
            try:
                self._logger.write_debug('Top output: `{};{}`'.format(con, con[1].stdout.strip()))

                # Create TopOutput objects list
                # Note: top output is `\n` separated and contains (double) spaces
                # Note: Replace "," by "." as fractional separator
                entry_outputs = [
                    " ".join(ent.split()).split(" ")
                    for ent in con[1].stdout.replace(",", ".").strip().split('\n')
                ]

                # Create objects
                i = 0
                # First search header (line number can differ)
                for i, entry_output in enumerate(entry_outputs):
                    if "PID" in entry_output[0].upper():
                        break

                # We join list elements [11:] to support spaces in command names
                if top_version.upper() == self.TopVersion.BUSY_BOX.name:
                    top_output = [self.TopBusyBoxOutput(*x[0:7], " ".join(x[7:])) for x in entry_outputs[i + 1:]]
                else:
                    top_output = [self.TopFullOutput(*x[0:11], " ".join(x[11:])) for x in entry_outputs[i + 1:]]

                # Filter outputs
                if len(command_names) == 0:
                    ret_output = [x for x in top_output]
                else:
                    ret_output = []
                    for t_output in top_output:
                        for cmd in command_names:
                            if cmd in t_output.command:
                                ret_output.append(t_output)
                                break

                # Create ret list (filter command names)
                ret_list.append(
                    self.RetVal(
                        host=self._host_string_create(con[0]),
                        status=self.RetValState.OK,
                        output=ret_output
                    )
                )
            except ValueError as e:
                self._logger.write_exception(
                    "Error converting top output; connection:{}, ex:{}".format(con, e)
                )

        return ret_list

    def process_list(self, process_names=[], stat_fields=[]):
        """
        Query list of processes.
        :param process_names: List with process names to be queried. Empty list for ALL.
        :param stat_fields: List containing fields to query.
        :return: List with an entry per host.
        """

        # Create the command
        cmd = 'ps ax -o {},{} | grep -e "{}" | grep -v "grep"'.format(
            ",".join(stat_fields),
            self.ProcessListColumns.COMMAND.value,
            '" -e "'.join(process_names)
        )

        # Execute the command
        ret = self.run_command(cmd)

        # List for return values
        ret_list = []

        # First handle `failed` items (need to be treated specially)
        self._result_handle_failed(ret.failed, ret_list)

        # Now loop over `succeeded` items
        for con in ret.succeeded.items():
            self._logger.write_debug('PS output: `{};{}`'.format(con, con[1].stdout.strip()))

            # Create return list:
            # ps output is `\n` separated and contains (doubled) spaces
            entry_output = [
                " ".join(ent.split()).split(" ")
                for ent in con[1].stdout.strip().split('\n')
            ]

            # Format for output is {COMMAND:[OUTPUT_ARGS]}
            ret_list.append(
                self.RetVal(
                    host=self._host_string_create(con[0]),
                    status=self.RetValState.OK,
                    output=[{out[-1]: out[:-1]} for out in entry_output]
                )
            )

        return ret_list

    def ifconfig(self, interfaces=[], ifconfig_version=IfConfigVersion.DEFAULT.name):
        """

        :return:
        """

        # Create the command
        cmd = '/sbin/ifconfig'

        # Execute the command
        ret = self.run_command(cmd)

        # List for return values
        ret_list = []

        # First handle `failed` items (need to be treated specially)
        self._result_handle_failed(ret.failed, ret_list)

        # Now loop over `succeeded` items
        for con in ret.succeeded.items():

            ifconfig_results = {}

            # Get the output of the ifconfig command
            ifconfig_output = con[1].stdout.strip()
            self._logger.write_debug('ifconfig output: `{};{}`'.format(con, ifconfig_output))

            # Parse the ifconfig outputs (not all the fields might be present)
            ifconfig_parser = IfconfigParser(
                ifconfig_output=ifconfig_output,
                ifconfig_type=IfconfigParser.IfcfgType.LINUX
                if ifconfig_version == self.IfConfigVersion.DEFAULT.name else IfconfigParser.IfcfgType.LINUX_OLD)

            for device_name, device_params in ifconfig_parser.devices.items():
                ifconfig_results[device_name] = self.IfConfigOutput(
                    iface=device_name,
                    ip4_address=device_params.get(IfconfigParser.IfcfgFieldsLinux.INET.value, None),
                    ip6_address=device_params.get(IfconfigParser.IfcfgFieldsLinux.INET6.value, None),
                    hw_address=device_params.get(IfconfigParser.IfcfgFieldsLinux.HW_ADDR.value, None),
                    mtu=device_params.get(IfconfigParser.IfcfgFieldsLinux.MTU.value, None),
                    rx_pck=device_params.get(IfconfigParser.IfcfgFieldsLinux.RX_PACKETS.value, None),
                    rx_bytes=device_params.get(IfconfigParser.IfcfgFieldsLinux.RX_BYTES.value, None),
                    rx_err=device_params.get(IfconfigParser.IfcfgFieldsLinux.RX_ERRORS.value, None),
                    rx_drp=device_params.get(IfconfigParser.IfcfgFieldsLinux.RX_DROPPED.value, None),
                    rx_ovr=device_params.get(IfconfigParser.IfcfgFieldsLinux.RX_OVERRUNS.value, None),
                    tx_pck=device_params.get(IfconfigParser.IfcfgFieldsLinux.TX_PACKETS.value, None),
                    tx_bytes=device_params.get(IfconfigParser.IfcfgFieldsLinux.TX_BYTES.value, None),
                    tx_err=device_params.get(IfconfigParser.IfcfgFieldsLinux.TX_ERRORS.value, None),
                    tx_drp=device_params.get(IfconfigParser.IfcfgFieldsLinux.TX_DROPPED.value, None),
                    tx_ovr=device_params.get(IfconfigParser.IfcfgFieldsLinux.TX_OVERRUNS.value, None)
                )

            # Calculate rates
            if con[0].host in self._ifconfig_last:
                history = self._ifconfig_last[con[0].host]
                for device, ifconfig_history in history["data"].items():
                    time_delta_s = datetime.datetime.now().timestamp() - history["timestamp"]

                    # rx rates
                    ifconfig_results[device].rx_bytes_rate_set((float(ifconfig_results[device].rx_bytes) - float(
                        ifconfig_history.rx_bytes)) / time_delta_s)
                    ifconfig_results[device].rx_pck_rate_set((float(ifconfig_results[device].rx_pck) - float(
                        ifconfig_history.rx_pck)) / time_delta_s)
                    ifconfig_results[device].rx_drp_rate_set((float(ifconfig_results[device].rx_drp) - float(
                        ifconfig_history.rx_drp)) / time_delta_s)
                    ifconfig_results[device].rx_ovr_rate_set((float(ifconfig_results[device].rx_ovr) - float(
                        ifconfig_history.rx_ovr)) / time_delta_s)
                    ifconfig_results[device].rx_err_rate_set((float(ifconfig_results[device].rx_err) - float(
                        ifconfig_history.rx_err)) / time_delta_s)

                    # tx rates
                    ifconfig_results[device].tx_bytes_rate_set((float(ifconfig_results[device].tx_bytes) - float(
                        ifconfig_history.tx_bytes)) / time_delta_s)
                    ifconfig_results[device].tx_pck_rate_set((float(ifconfig_results[device].tx_pck) - float(
                        ifconfig_history.tx_pck)) / time_delta_s)
                    ifconfig_results[device].tx_drp_rate_set((float(ifconfig_results[device].tx_drp) - float(
                        ifconfig_history.tx_drp)) / time_delta_s)
                    ifconfig_results[device].tx_ovr_rate_set((float(ifconfig_results[device].tx_ovr) - float(
                        ifconfig_history.tx_ovr)) / time_delta_s)
                    ifconfig_results[device].tx_err_rate_set((float(ifconfig_results[device].tx_err) - float(
                        ifconfig_history.tx_err)) / time_delta_s)

            # Save current results
            self._ifconfig_last[con[0].host] = {
                "timestamp": datetime.datetime.now().timestamp(),
                "data": ifconfig_results
            }

            # Filter interfaces
            if len(interfaces) == 0:
                ret_output = ifconfig_results
            else:
                ret_output = {}
                for iface in interfaces:
                    if iface in ifconfig_results:
                        ret_output[iface] = ifconfig_results[iface]
                        break

            # Format for output is {COMMAND:[OUTPUT_ARGS]}
            ret_list.append(
                self.RetVal(
                    host=self._host_string_create(con[0]),
                    status=self.RetValState.OK,
                    output=ret_output
                )
            )

        return ret_list
