from enum import Enum, auto
from epl.epl_logging.epl_logging_log import Log

class ValueTypesGim(Enum):
    """
    Enum defining gim value types.
    """
    none = 'None'
    int = 'int'
    float = 'float'
    str = 'str'

"""
LUT for conversion of value types.
"""
_VALUE_TYPE_LUT = {
    None: ValueTypesGim.none.value,
    int: ValueTypesGim.int.value,
    float: ValueTypesGim.float.value,
    str: ValueTypesGim.str.value,
    list: ValueTypesGim.str.value,
    bool: ValueTypesGim.int.value,
    'discrete': ValueTypesGim.str.value,
    'None': ValueTypesGim.none.value,
    'int': ValueTypesGim.int.value,
    'integer': ValueTypesGim.int.value,
    'float': ValueTypesGim.float.value,
    'str': ValueTypesGim.str.value,
    'real': ValueTypesGim.float.value,
    'bool': ValueTypesGim.int.value,
    'DevVoid': ValueTypesGim.str.value,
    'DevBoolean': ValueTypesGim.str.value,
    'DevShort': ValueTypesGim.int.value,
    'DevLong': ValueTypesGim.int.value,
    'DevFloat': ValueTypesGim.float.value,
    'DevDouble': ValueTypesGim.float.value,
    'DevUShort': ValueTypesGim.int.value,
    'DevULong': ValueTypesGim.int.value,
    'DevString': ValueTypesGim.str.value,
    'DevVarCharArray': ValueTypesGim.str.value,
    'DevVarShortArray': ValueTypesGim.str.value,
    'DevVarLongArray': ValueTypesGim.str.value,
    'DevVarFloatArray': ValueTypesGim.str.value,
    'DevVarDoubleArray': ValueTypesGim.str.value,
    'DevVarUShortArray': ValueTypesGim.str.value,
    'DevVarULongArray': ValueTypesGim.str.value,
    'DevVarStringArray': ValueTypesGim.str.value,
    'DevVarLongStringArray': ValueTypesGim.str.value,
    'DevVarDoubleStringArray': ValueTypesGim.str.value,
    'DevState': ValueTypesGim.str.value,
    'ConstDevString': ValueTypesGim.str.value,
    'DevVarBooleanArray': ValueTypesGim.str.value,
    'DevUChar': ValueTypesGim.int.value,
    'DevLong64': ValueTypesGim.int.value,
    'DevULong64': ValueTypesGim.int.value,
    'DevVarLong64Array': ValueTypesGim.str.value,
    'DevVarULong64Array': ValueTypesGim.str.value,
    'DevInt': ValueTypesGim.int.value,
    'DevEncoded': ValueTypesGim.str.value,
    'DevEnum': ValueTypesGim.str.value,
    'DevPipeBlob': ValueTypesGim.str.value
}


def value_type_lookup(value_type):
    """
    Wrapper for the value_type dict lookup.
    :param value_type: value_type as either string or type class.
    :return:
    """
    return _VALUE_TYPE_LUT[value_type]


class KatcpGimInfo:
    """
    Class to handle gim specific info.
    """

    class WidgetType(Enum):
        """
        Enum to set the widget value_type.
        """
        NONE = auto()
        GET = auto()
        SET = auto()
        GET_SET = auto()
        BOOL = auto()
        DROPDOWN = auto()
        IMAGE = auto()
        # Only for InteRCoM compatibility from here
        DIGITAL_ATTENUATION = auto()
        FILTERSELECT = auto()
        GETARRAY = auto()
        GETSETARRAY = auto()
        GETSETDROPDOWN = auto()
        IOMATRIX = auto()
        LIFTMOTOR = auto()
        MULTIPLEXER_CONNECT = auto()

    class OptionsAvailable(Enum):
        """
        Enum to set available task options.
        """
        NONE = 0
        GET = 1
        SET = 2
        GET_SET = 3

    def __init__(self,
                 logger=None,
                 value_type=None,
                 mysql_task_type=None,
                 sampling_strategy=None,
                 sampling_strategy_params=None,
                 description=None,
                 options_available=OptionsAvailable.NONE,
                 display_name=None,
                 limits=None,
                 option01=None,
                 option02=None,
                 option03=None,
                 option04=None,
                 unit=None,
                 format_display=None,
                 icinga_check_constant_errors=None):
        """
        Inits the object.
        :param logger: Logger for the log functionality
        :param value_type: Type of value (for sensor as well as request)
        :param mysql_task_type:
        :param sampling_strategy:
        :param sampling_strategy_params:
        :param description:
        :param options_available:
        :param display_name:
        :param limits: Representing limits/options depending on value_type.
        :param option01: Option01 field.
        :param option02: Option01 field.
        :param option03: Option01 field.
        :param option04: Option01 field.
        :param unit: Value's unit.
        :param format_display: Display format for value on gim-gui.
        :param icinga_check_constant_errors: Activate constant error checks from icinga.
        """

        # Init the logger
        if logger is None:
            self._log_writer = Log()
        else:
            self._log_writer = logger

        self.sampling_strategy = sampling_strategy
        self.sampling_strategy_params = sampling_strategy_params
        self.description = description
        self.options_available = options_available
        self.display_name = display_name
        self.limits = limits
        self.option01 = option01
        self.option02 = option02
        self.option03 = option03
        self.option04 = option04
        self.unit = unit
        self.format = format_display
        self.icinga_check_constant_errors = icinga_check_constant_errors

        # Get value type from LUT
        self.value_type = _VALUE_TYPE_LUT.get(value_type, ValueTypesGim.str.value)

        if mysql_task_type is None:
            self.mysql_task_type = self.WidgetType.NONE
        else:
            try:
                if isinstance(mysql_task_type, self.WidgetType):
                    self.mysql_task_type = mysql_task_type
                else:
                    # Inconsistency between intercom-igui and gim-gui
                    if mysql_task_type == 'getset':
                        mysql_task_type = 'GET_SET'

                    self.mysql_task_type = self.WidgetType[mysql_task_type.upper()]
            except:
                self.mysql_task_type = self.WidgetType.GET_SET
                self._log_writer.write_debug(
                    '{}: mysql_task_type "{}" unknown for task "{}"'.format(
                        self.__class__.__name__, mysql_task_type, self.display_name))
