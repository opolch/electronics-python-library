from influxdb_client import InfluxDBClient, Point
from influxdb_client.client.write_api import SYNCHRONOUS
from epl.epl_logging.epl_logging_log import Log
from datetime import datetime
from enum import Enum


class Constants(Enum):
    """
    Constants to be used with the module.
    """

    date_time_format_zulu= "%Y-%m-%dT%H:%M:%SZ"


class DeleteInfo:

    def __init__(self, start: datetime, stop: datetime, predicate: str = ""):
        """
        Inits the object.
        :param start: Start datetime.
        :param stop: Stop datetime.
        :param predicate: Predicate syntax to determine what data points to delete: `key1="value1" AND key2="value"`
        """

        self.start = start
        self.stop = stop
        self.predicate = predicate


class InfluxDBInteraction:
    """
    Wrapper for the influxdb_client.
    """

    def __init__(self, logger: Log, influxdb_host: str, influxdb_port: int, bucket: str, token: str, organisation: str):
        """
        Init the object.
        :param logger: epl_logging.Log
        :param influxdb_host: The host running the Redis server.
        :param influxdb_port: The port of the Redis server.
        :param bucket: The bucket to interact with.
        :param token: Token for authentication.
        :param organisation: The Organisation.
        """

        self._logger = logger

        # InfluxDB host/port
        self._host = influxdb_host
        self._port = influxdb_port

        # InfluxDB variables
        self._bucket = bucket
        self._token = token
        self._organisation = organisation

        # Create the client
        client = InfluxDBClient(url=f"{self._host}:{self._port}", token=self._token,
                                org=self._organisation)
        self._write_api = client.write_api(write_options=SYNCHRONOUS)
        self._delete_api = client.delete_api()

    @property
    def influxdb_host(self) -> str:
        """
        Returns the InfluxDB host.
        :return:
        """
        return self._host

    @property
    def influxdb_port(self) -> int:
        """
        Returns the InfluxDB port.
        :return:
        """
        return self._port

    @property
    def influxdb_organization(self) -> str:
        """
        Returns the InfluxDB organization.
        :return:
        """
        return self._organisation

    @property
    def influxdb_token(self) -> str:
        """
        Returns the InfluxDB token.
        :return:
        """
        return self._token

    def write_point(self, point: Point):
        """
        Writes a single Point object to influxDB.
        :param point: Point object.
        :return:
        """
        self._write_api.write(bucket=self._bucket, record=point)

    def write_points(self, points: [Point]):
        """
        Writes a list of points to InfluxDB
        :param points: List of Point objects.
        :return:
        """
        for point in points:
            self.write_point(point)

    def delete_list(self, delete_info: [DeleteInfo]):
        """
        Deletes entries via multiple queries.
        :param delete_info: List of DeleteInfo objects.
        :return:
        """

        for d_info in delete_info:
            self.delete(delete_info=d_info)

    def delete(self, delete_info: DeleteInfo):
        """
        Deletes entries from the corresponding bucket.
        :param delete_info: DeleteInfo with delete information.
        :return:
        """
        self._delete_api.delete(bucket=self._bucket,
                                start=f"{delete_info.start.strftime(Constants.date_time_format_zulu.value)}",
                                stop=f"{delete_info.stop.strftime(Constants.date_time_format_zulu.value)}",
                                predicate=delete_info.predicate)
